package com.weddingplus.cloud_messaging;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.weddingplus.R;
import com.weddingplus.user_interface.activities.MainActivity;
import com.weddingplus.user_interface.interfaces.NotificationListener;
import com.weddingplus.utilities.AppConstants;
import com.weddingplus.utilities.Helper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class FirebaseMessaging extends FirebaseMessagingService implements AppConstants {

    private final String TAG = "FirebaseMessaging";
    private String type, message, title;

    private static NotificationListener notificationListener;

    int notification_id=0;


    public static void setNotificationListener(NotificationListener listener) {
        notificationListener = listener;
    }

    public static void removeNotificationListener() {
        notificationListener = null;


    }


    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.e(TAG, "remoteData :: " + remoteMessage.getData());


        if (Helper.isLoggedIn()) {
                createNotification(remoteMessage);
        } else {
            Log.e(TAG, "==not Login==:: ");
        }

    }

    private void createNotification(RemoteMessage remoteMessage) {
        Map<String, String> messageData = remoteMessage.getData();
        type = messageData.get(FCM_KEY_TYPE);

        title = messageData.get(FCM_KEY_TITLE);
        message = messageData.get(FCM_KEY_MESSAGE);

        if (null != notificationListener) {
            notificationListener.onNotificationClick(
                    type,
                    message

            );
        }

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(getIconResource())
                        .setContentText(message)
                        .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                        .setLargeIcon(getLargeIcon())
                        .setContentTitle(title)
                        .setTicker(message);

        mBuilder.setContentIntent(getResultIntent(type));
        mBuilder.setDefaults(NotificationCompat.DEFAULT_ALL);
        mBuilder.setAutoCancel(true);
        notification_id = (int) System.currentTimeMillis();
        NotificationManager mNotifyMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        mNotifyMgr.notify(notification_id, mBuilder.build());

    }

    private CharSequence getContentTitle(String type) {
        return getString(R.string.app_name);
    }


    private int getIconResource() {
        return R.mipmap.ic_launcher;

    }

    private Bitmap getLargeIcon() {
        return BitmapFactory.decodeResource(getResources(), R.mipmap.ic_launcher);
    }

    private PendingIntent getResultIntent(String type) {
        Intent resultIntent = null;
        if (Helper.isLoggedIn()) {
            Log.e("call", "firebase notification");
            resultIntent = new Intent(this, MainActivity.class);
        }
        switch (type) {
            case NOTIFICATION_TYPE_HISTRY_STATUS:
                resultIntent.putExtra(FCM_KEY_TYPE, type);
                break;
            case NOTIFICATION_TYPE_CUSTOM_ADMIN:
                resultIntent.putExtra(FCM_KEY_TYPE, type);
                break;
            case NOTIFICATION_TYPE_STUDENT_REPORT_HISTORY:
                resultIntent.putExtra(FCM_KEY_TYPE, type);
                break;
        }
        return PendingIntent.getActivity(this, notification_id, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

    }


    private boolean isValidMessage(Map<String, String> messageData) {
        if (!messageData.containsKey(FCM_KEY_MESSAGE)) {
            return false;
        }
        return true;
    }
}
