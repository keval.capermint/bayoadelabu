package com.weddingplus.cloud_messaging;

import com.weddingplus.utilities.Helper;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

/**
 * Created by Manndeep Vachhani on 11/2/17.
 */
public class FirebaseIdService extends FirebaseInstanceIdService {

    private final String TAG = this.getClass().getSimpleName();

    @Override
    public void onTokenRefresh() {
        // Get updated InstanceID token.
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Helper.debugLog(TAG, "Refreshed token: " + refreshedToken);
    }
}