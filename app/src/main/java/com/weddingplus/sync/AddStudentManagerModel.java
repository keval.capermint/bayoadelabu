package com.weddingplus.sync;

public class AddStudentManagerModel {
    String attachment_type;
    String auth_key;
    String description;
    String student_category_id;
    String student_subcategory_id;
    String latitude;
    String longitude;
    String path;
    String type;

    public String getAttachment_type() {
        return attachment_type;
    }

    public void setAttachment_type(String attachment_type) {
        this.attachment_type = attachment_type;
    }

    public String getAuth_key() {
        return auth_key;
    }

    public void setAuth_key(String auth_key) {
        this.auth_key = auth_key;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStudent_category_id() {
        return student_category_id;
    }

    public void setStudent_category_id(String student_category_id) {
        this.student_category_id = student_category_id;
    }

    public String getStudent_subcategory_id() {
        return student_subcategory_id;
    }

    public void setStudent_subcategory_id(String student_subcategory_id) {
        this.student_subcategory_id = student_subcategory_id;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
