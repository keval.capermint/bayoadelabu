package com.weddingplus.sync;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

@RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
public class NetworkSchedulerService extends JobService implements
        ConnectivityReceiver.ConnectivityReceiverListener {

    private static final String TAG = NetworkSchedulerService.class.getSimpleName();
    List<CheckInModel> list;
    List<AddStudentManagerModel> addStudentManagerModelList;
    PreferenceApp pref;
    private ConnectivityReceiver mConnectivityReceiver;

    public static MultipartBody.Part createPartFileFromPath(String filepath) {
        File destination = new File(filepath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("video/*"), destination);
        return MultipartBody.Part.createFormData("attachment", destination.getName(), requestFile);
    }

    @Override
    public void onCreate() {
        super.onCreate();
        Log.i(TAG, "Service created");
        mConnectivityReceiver = new ConnectivityReceiver(this);
        registerReceiver(mConnectivityReceiver,
                new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
    }

    /**
     * When the app's NetworkConnectionActivity is created, it starts this service. This is so that the
     * activity and this service can communicate back and forth. See "setUiCallback()"
     */
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(TAG, "onStartCommand");
        return START_NOT_STICKY;
    }

    @Override
    public boolean onStartJob(JobParameters params) {
//        Log.i(TAG, "onStartJob" + mConnectivityReceiver);
        Toast.makeText(this, "Job started", Toast.LENGTH_SHORT).show();
        registerReceiver(mConnectivityReceiver, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        Log.i(TAG, "onStopJob");
//        unregisterReceiver(mConnectivityReceiver);
        return true;
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
//        String message = isConnected ? "Good! Connected to Internet" : "Sorry! Not connected to internet";
//        Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();

        if (isConnected) {
            pref = new PreferenceApp(this);
            list = new ArrayList<>();
            addStudentManagerModelList = new ArrayList<>();
            list = pref.getCheckInRequestList();
            addStudentManagerModelList = pref.getStudentManagerList();
            if (list == null)
                list = new ArrayList<>();

            if (addStudentManagerModelList == null)
                addStudentManagerModelList = new ArrayList<>();

            if (addStudentManagerModelList == null)
                addStudentManagerModelList = new ArrayList<>();

            for (int i = 0; i < list.size(); i++) {
                CheckInApi(list.get(i), i);
            }

            for (int j = 0; j < addStudentManagerModelList.size(); j++) {
                addStudentManager(addStudentManagerModelList.get(j), j);
            }
        }
    }

    void CheckInApi(final CheckInModel obj, final int pos) {
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                try {
                    list.remove(pos);
                    pref.setCheckInRequestList(list);
                } catch (Exception e) {
                    e.printStackTrace();
                }
//                stopProgress();
//                UiHelper.toast(response.body().getMessage());
            }

            @Override
            public void Failure(String message) {
//                stopProgress();
//                UiHelper.toast("Please try again later.");
                //toast(message);
            }

            @Override
            public void SessionExpired(String message) {
//                toast(message);
//                Helper.clearPreferences();
//                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        MultipartBody.Part part = null;
        if (obj.getType().equals("photo")) {
            part = createPartImageFromBitmap(obj.getPath());
        } else if (obj.getType().equals("video")) {
            part = createPartFileFromPath(obj.getPath());
        } else if (obj.getType().equals("voice")) {
            part = getPartFileFromVoice(obj.getPath());
        }


        WebServiceHelper.CheckIn(obj.getAttachment_type(), part, "0", obj.getDescription(), obj.getIs_upload()).enqueue(callback);
    }

    private void addStudentManager(AddStudentManagerModel obj, final int pos) {
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                try {
                    addStudentManagerModelList.remove(pos);
                    pref.setStudentManagerList(addStudentManagerModelList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void Failure(String message) {
//                robberyDialog.dismiss();
            }

            @Override
            public void SessionExpired(String message) {
            }
        };

        MultipartBody.Part part = null;
        if (obj.getType().equals("photo")) {
            part = createPartImageFromBitmap(obj.getPath());
        } else if (obj.getType().equals("video")) {
            part = createPartFileFromPath(obj.getPath());
        } else if (obj.getType().equals("voice")) {
            part = getPartFileFromVoice(obj.getPath());
        }

        WebServiceHelper.AddStudentManagerLog(obj.getAttachment_type(), part, obj.getDescription(), obj.student_category_id, obj.getStudent_subcategory_id()).enqueue(callback);
    }

    public MultipartBody.Part createPartImageFromBitmap(String path) {

        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), new File(path));
        return MultipartBody.Part.createFormData("attachment", new File(path).getName(), reqFile1);
    }

    private MultipartBody.Part getPartFileFromVoice(String path) {
        File destination = new File(path);
        RequestBody reqFile = RequestBody.create(MediaType.parse("audio/*"), destination);

        return MultipartBody.Part.createFormData("attachment", destination.getName(), reqFile);
    }

}

