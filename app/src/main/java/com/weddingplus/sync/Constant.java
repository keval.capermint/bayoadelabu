package com.weddingplus.sync;

class Constant {
    public static final String CheckInRequest = "checkinrequest";
    public static final String AddStudentManager = "addStudentManager";
    public static final String getStudentCategories = "getStudentCategories";
}
