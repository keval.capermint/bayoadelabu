package com.weddingplus.sync;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;


/**
 * Created by VR-46 on 1/27/2017.
 */

public class PreferenceApp {

    public static final String FIREBASE_CLOUD_MESSAGING = "fcm";
    public static final String SET_NOTIFY = "set_notify";
    private Context mContext;
    private SharedPreferences prefs;

    public PreferenceApp(Context mContext) {
        this.mContext = mContext;
        prefs = PreferenceManager.getDefaultSharedPreferences(mContext);
    }

    public void clear() {
        SharedPreferences.Editor editor = prefs.edit();
        editor.clear();
        editor.apply();
    }

    public void setCheckInRequestList(List<CheckInModel> list){
        SharedPreferences mPrefs = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        prefsEditor.putString(Constant.CheckInRequest, json);
        prefsEditor.apply();
    }

    public List<CheckInModel> getCheckInRequestList(){
        List<CheckInModel> arraylist = new ArrayList<>();
        SharedPreferences mPrefs = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(Constant.CheckInRequest, "");
        if (json.isEmpty()) {
            arraylist = new ArrayList<CheckInModel>();
        } else {
            Type type = new TypeToken<List<CheckInModel>>() {
            }.getType();
            arraylist = gson.fromJson(json, type);
        }
        return arraylist;
    }

    public void setStudentManagerList(List<AddStudentManagerModel> list){
        SharedPreferences mPrefs = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(list);
        prefsEditor.putString(Constant.AddStudentManager, json);
        prefsEditor.apply();
    }

    public List<AddStudentManagerModel> getStudentManagerList(){
        List<AddStudentManagerModel> arraylist = new ArrayList<>();
        SharedPreferences mPrefs = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(Constant.AddStudentManager, "");
        if (json.isEmpty()) {
            arraylist = new ArrayList<AddStudentManagerModel>();
        } else {
            Type type = new TypeToken<List<AddStudentManagerModel>>() {
            }.getType();
            arraylist = gson.fromJson(json, type);
        }
        return arraylist;
    }

    public void setStudentCategories(StudentCategoriesModel obj){
        SharedPreferences mPrefs = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String json = gson.toJson(obj);
        prefsEditor.putString(Constant.getStudentCategories, json);
        prefsEditor.apply();
    }

    public StudentCategoriesModel getStudentCategories(){
        SharedPreferences mPrefs = mContext.getSharedPreferences("pref", mContext.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = mPrefs.getString(Constant.getStudentCategories, "");
        StudentCategoriesModel obj = gson.fromJson(json, StudentCategoriesModel.class);
        return obj;
    }

    /*public ArrayList<String> getSearchList(){
        SharedPreferences mPrefs = mContext.getSharedPreferences(Constant.CartPreference, mContext.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        Gson gson = new Gson();
        String jsonText = mPrefs.getString(Constant.SearchList, null);
        return gson.fromJson(jsonText,ArrayList.class);
    }

    public void setSearchList(List<String> list){
        SharedPreferences mPrefs = mContext.getSharedPreferences(Constant.CartPreference, mContext.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        List<String> textList = new ArrayList<String>();
        textList.addAll(list);
        Gson gson = new Gson();
        String jsonText = gson.toJson(textList);
        prefsEditor.putString(Constant.SearchList, jsonText);
        prefsEditor.apply();
    }*/

    public void removeCartList(){
        SharedPreferences mPrefs = mContext.getSharedPreferences(Constant.CheckInRequest, mContext.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = mPrefs.edit();
        prefsEditor.remove(Constant.CheckInRequest)
                .apply();
    }
}
