package com.weddingplus.sync;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.widget.Toast;

import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

public class NetworkMonitor extends BroadcastReceiver {
    List<CheckInModel> list;
    List<AddStudentManagerModel> addStudentManagerModelList;
    PreferenceApp pref;
    @Override
    public void onReceive(Context context, Intent intent) {
        if (isNetworkAvailable(context)) {
            Toast.makeText(context, "sync started", Toast.LENGTH_LONG).show();

            pref = new PreferenceApp(context);
            list = new ArrayList<>();
            addStudentManagerModelList = new ArrayList<>();
            list = pref.getCheckInRequestList();
            addStudentManagerModelList = pref.getStudentManagerList();
            if (list == null)
                list = new ArrayList<>();

            if (addStudentManagerModelList == null)
                addStudentManagerModelList = new ArrayList<>();

            if (addStudentManagerModelList == null)
                addStudentManagerModelList = new ArrayList<>();

            for (int i = 0 ; i < list.size() ; i++){
                CheckInApi(list.get(i),i);
            }

            for (int j = 0 ; j < addStudentManagerModelList.size() ; j++){
                addStudentManager(addStudentManagerModelList.get(j),j);
            }
        }
    }

    public boolean isNetworkAvailable(Context context) {
        final ConnectivityManager connectivityManager = ((ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE));
        return connectivityManager.getActiveNetworkInfo() != null && connectivityManager.getActiveNetworkInfo().isConnected();
    }

    void CheckInApi(final CheckInModel obj, final int pos) {
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                list.remove(pos);
                pref.setCheckInRequestList(list);
//                stopProgress();
//                UiHelper.toast(response.body().getMessage());
            }

            @Override
            public void Failure(String message) {
//                stopProgress();
//                UiHelper.toast("Please try again later.");
                //toast(message);
            }

            @Override
            public void SessionExpired(String message) {
//                toast(message);
//                Helper.clearPreferences();
//                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        MultipartBody.Part part = null;
        if (obj.getType().equals("photo")){
            part = createPartImageFromBitmap(obj.getPath());
        }

        else if (obj.getType().equals("video")){
            part = createPartFileFromPath(obj.getPath());
        }

        else if (obj.getType().equals("voice")){
            part = getPartFileFromVoice(obj.getPath());
        }


        WebServiceHelper.CheckIn(obj.getAttachment_type(),part, "0", obj.getDescription(), obj.getIs_upload()).enqueue(callback);
    }

    private void addStudentManager(AddStudentManagerModel obj, final int pos){
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                addStudentManagerModelList.remove(pos);
                pref.setStudentManagerList(addStudentManagerModelList);
            }

            @Override
            public void Failure(String message) {
//                robberyDialog.dismiss();
            }

            @Override
            public void SessionExpired(String message) {
            }
        };

        MultipartBody.Part part = null;
        if (obj.getType().equals("photo")){
            part = createPartImageFromBitmap(obj.getPath());
        }

        else if (obj.getType().equals("video")){
            part = createPartFileFromPath(obj.getPath());
        }

        else if (obj.getType().equals("voice")){
            part = getPartFileFromVoice(obj.getPath());
        }

        WebServiceHelper.AddStudentManagerLog(obj.getAttachment_type(), part, obj.getDescription(), obj.student_category_id, obj.getStudent_subcategory_id()).enqueue(callback);
    }

    public MultipartBody.Part createPartImageFromBitmap(String path) {

        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), new File(path));
        return MultipartBody.Part.createFormData("attachment", new File(path).getName(), reqFile1);
    }

    public static MultipartBody.Part createPartFileFromPath(String filepath) {
        File destination = new File(filepath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("video/*"), destination);
        return MultipartBody.Part.createFormData("attachment", destination.getName(), requestFile);
    }

    private MultipartBody.Part getPartFileFromVoice(String path){
        File destination = new File(path);
        RequestBody reqFile = RequestBody.create(MediaType.parse("audio/*"), destination);

        return MultipartBody.Part.createFormData("attachment", destination.getName(), reqFile);
    }
}
