package com.weddingplus.web_services.responses;

import android.os.Process;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.weddingplus.models.GalleryData;
import com.weddingplus.models.ProductData;
import com.weddingplus.web_services.BaseResponse;

import java.util.ArrayList;

/**
 * Created by keval on 24/7/17.
 */

public class ProductListResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private ArrayList<ProductData> data;

    public ArrayList<ProductData> getProductData() {
        return data;
    }

}
