package com.weddingplus.web_services.responses;

import android.os.Parcel;
import android.os.Parcelable;

import com.weddingplus.models.Category;
import com.weddingplus.web_services.BaseResponse;

import java.util.ArrayList;

/**
 * Created by Jay Kikani on 20/7/17.
 */

public class CategoryResponse extends BaseResponse implements Parcelable {

    private ArrayList<Category> data = new ArrayList<>();

    public ArrayList<Category> getData() {
        return data;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(this.data);
    }

    public CategoryResponse() {
    }

    protected CategoryResponse(Parcel in) {
        this.data = in.createTypedArrayList(Category.CREATOR);
    }

    public static final Creator<CategoryResponse> CREATOR = new Creator<CategoryResponse>() {
        @Override
        public CategoryResponse createFromParcel(Parcel source) {
            return new CategoryResponse(source);
        }

        @Override
        public CategoryResponse[] newArray(int size) {
            return new CategoryResponse[size];
        }
    };
}
