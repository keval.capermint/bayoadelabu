package com.weddingplus.web_services.responses;

import com.weddingplus.models.HistoryData;
import com.weddingplus.web_services.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by keval on 24/7/17.
 */

public class HistoryResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private HistoryData data;

    public HistoryData getData() {
        return data;
    }

    public void setData(HistoryData data) {
        this.data = data;
    }
}
