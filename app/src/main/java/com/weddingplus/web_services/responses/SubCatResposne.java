package com.weddingplus.web_services.responses;

import com.weddingplus.models.SubCategoryData;
import com.weddingplus.web_services.BaseResponse;

import java.util.List;

/**
 * Created by keval on 23/2/18.
 */

public class SubCatResposne extends BaseResponse {

    private List<SubCategoryData> data = null;

    public List<SubCategoryData> getData() {
        return data;
    }

    public void setData(List<SubCategoryData> data) {
        this.data = data;
    }
}
