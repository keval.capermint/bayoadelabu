package com.weddingplus.web_services.responses;

import com.weddingplus.models.User;
import com.weddingplus.web_services.BaseResponse;

/**
 * Created by Manndeep Vachhani on 3/4/17.
 */

public class UserResponse extends BaseResponse {

    private User data;

    public void setData(User data) {
        this.data = data;
    }

    public User getUser() {
        return data;
    }
}
