package com.weddingplus.web_services.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.weddingplus.models.GalleryData;
import com.weddingplus.models.HistoryData;
import com.weddingplus.web_services.BaseResponse;

/**
 * Created by keval on 24/7/17.
 */

public class GalleryResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private GalleryData data;

    public GalleryData getData() {
        return data;
    }

    public void setData(GalleryData data) {
        this.data = data;
    }
}
