package com.weddingplus.web_services.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.weddingplus.models.CartData;
import com.weddingplus.models.CartListData;
import com.weddingplus.models.ProductData;
import com.weddingplus.web_services.BaseResponse;

import java.util.ArrayList;

/**
 * Created by keval on 24/7/17.
 */

public class CartListResponse extends BaseResponse {


    @SerializedName("data")
    @Expose
    private CartListData data;

    public CartListData getData() {
        return data;
    }



}
