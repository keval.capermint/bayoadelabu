package com.weddingplus.web_services.responses;

import com.weddingplus.models.USSDData;
import com.weddingplus.web_services.BaseResponse;

/**
 * Created by cap-13-pv-16 on 10/4/18.
 */

public class ApplyUSSDCodeResponse extends BaseResponse{

    private USSDData data;

    public USSDData getData() {
        return data;
    }
}
