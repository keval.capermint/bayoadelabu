package com.weddingplus.web_services.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.weddingplus.models.CartListData;
import com.weddingplus.web_services.BaseResponse;

/**
 * Created by keval on 24/7/17.
 */

public class CartItemCountResponse extends BaseResponse {


    @SerializedName("data")
    @Expose
    private String data;

    public String getData() {
        return data;
    }



}
