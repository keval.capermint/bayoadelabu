package com.weddingplus.web_services.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.weddingplus.models.StudentHistoryData;
import com.weddingplus.web_services.BaseResponse;

/**
 * Created by keval on 24/7/17.
 */

public class GetStudentStatusResponse extends BaseResponse {

    @SerializedName("data")
    @Expose
    private StudentHistoryData data;

    public StudentHistoryData getData() {
        return data;
    }

    public void setData(StudentHistoryData data) {
        this.data = data;
    }
}
