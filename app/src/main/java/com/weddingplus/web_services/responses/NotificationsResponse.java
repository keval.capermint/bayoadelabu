package com.weddingplus.web_services.responses;

import com.weddingplus.models.Notification;
import com.weddingplus.web_services.BaseResponse;

import java.util.ArrayList;

/**
 * Created by Jay Kikani on 20/7/17.
 */

public class NotificationsResponse extends BaseResponse {

    private ArrayList<Notification> data = new ArrayList<>();

    public ArrayList<Notification> getData() {
        return data;
    }

}
