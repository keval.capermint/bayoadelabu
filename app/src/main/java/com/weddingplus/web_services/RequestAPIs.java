package com.weddingplus.web_services;

import com.weddingplus.sync.StudentCategoriesModel;
import com.weddingplus.web_services.responses.ApplyUSSDCodeResponse;
import com.weddingplus.web_services.responses.CartItemCountResponse;
import com.weddingplus.web_services.responses.CartListResponse;
import com.weddingplus.web_services.responses.CategoryResponse;
import com.weddingplus.web_services.responses.GalleryResponse;
import com.weddingplus.web_services.responses.GetStudentStatusResponse;
import com.weddingplus.web_services.responses.HistoryResponse;
import com.weddingplus.web_services.responses.NotificationsResponse;
import com.weddingplus.web_services.responses.PaymenyResponse;
import com.weddingplus.web_services.responses.ProductListResponse;
import com.weddingplus.web_services.responses.SubCatResposne;
import com.weddingplus.web_services.responses.UserResponse;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

/**
 * Interface that contains WebService requests
 * Created by Manndeep Vachhani on 21/11/16.
 */
interface RequestAPIs {

    /**
     * Web Services EndPoints
     */
    /*1*/ String ENDPOINT_AUTO_LOGIN = "auto_login";
    /*1*/ String ENDPOINT_SIGN_UP = "signup";
    /*2*/ String ENDPOINT_LOGIN = "login";
    /*2*/ String ENDPOINT_SOCIAL_LOGIN = "beforeauth/sociallogin";
    /*2*/ String ENDPOINT_CHANGE_PASSWORD = "change_password";
    /*2*/ String ENDPOINT_FORGOT_PASSWORD = "forgot_password";
    /*2*/ String ENDPOINT_MY_PROFILE = "my_profile";

    /*2*/ String ENDPOINT_UPDATE_PROFILE = "user/updateprofile";
    /*2*/ String ENDPOINT_LOGOUT = "logout";
    /*2*/ String ENDPOINT_CATEGORIES = "get_emergency_categories";
    /*2*/ String ENDPOINT_ON_OFF_NOTIFICATION = "on_off_pushnotification";
    /*2*/ String ENDPOINT_GET_EMERGENCY_HISTORY = "get_emergency_history";
    /*2*/ String ENDPOINT_GET_NOTIFICATIONS = "get_user_notification";
    /*2*/ String ENDPOINT_DELETE_NOTIFICATIONS = "delete_user_notification";
    /*2*/ String ENDPOINT_DELETE_MULTI_NOTIFICATIONS = "delete_multi_user_notification";
    /*2*/ String ENDPOINT_GET_SOCIAL_CAT = "get_social_categories";
    /*2*/ String ENDPOINT_CHECKIN = "checkin";
    /*2*/ String ENDPOINT_ADD_EMERGENCY = "add_emergency";
    /*2*/ String ENDPOINT_EDIT_PROFILE = "edit_profile";
    /*2*/ String ENDPOINT_SEND_MEDIA = "send_media";
    /*2*/ String ENDPOINT_GET_MEDIA_LIST = "get_media_list";
    /*2*/ String ENDPOINT_GET_PRODUCT_LIST = "get_product_list";
    /*2*/ String ENDPOINT_ADD_TO_CART = "add_to_cart";
    /*2*/ String ENDPOINT_GET_CART_LIST = "get_cart_list";
    /*2*/ String ENDPOINT_DELETE_CART_ITEM = "delete_cart_item";
    /*2*/ String ENDPOINT_GET_CART_ITEM_COUNT = "get_cart_count";
    /*2*/ String ENDPOINT_PAYSTACK_PAYENT = "paystackpayment";
    String URL_SUBMIT_PIN= "verifypin";

    String URL_SUBMIT_OTP= "verifyotp";

    /*2*/ String ENDPOINT_PLACE_ORDER = "place_order";
    /*2*/ String ENDPOINT_KIDNAP = "kidnap";
    String ENDPOINT_STUDENT_CATEGORY = "getstudentcategorylist";
    String ENDPOINT_STUDENT_SUB_CATEGORY = "getstudentsubcategory";

    String ENDPOINT_ADD_STUDENT_MANAGER_LOG = "addstudentmanagerlog";
    String ENDPOINT_GET_STUDENT_MANAGER_LOG = "getstudentmanagerlog";
    String ENDPOINT_APPLY_USSD_CODE = "applyussdcode";

    /**
     * Web Services Params
     */
    String PARAM_AUTH_KEY = "Authkey";
    String PARAM_EMERGANCY_ID = "emergency_category_id";

    String PARAM_USER_F_NAME = "first_name";
    String PARAM_USER_L_NAME = "last_name";
    String PARAM_USER_EMAIL = "email";
    String PARAM_USER_PASSWORD = "password";
    String PARAM_USER_PHONE_NO = "phone_number";

    String PARAM_USER_AGE = "age";
    String PARAM_USER_SOCIAL_CAT_ID = "social_category_id";
    String PARAM_USER_LATITUDE = "latitude";
    String PARAM_USER_LONGITUDE = "longitude";
    String PARAM_USER_LOCATION = "location";
    String PARAM_USER_DESCRIPTION = "description";

    String PARAM_USER_APP_VERSION = "app_version";
    String PARAM_USER_DEVICE_PLATFORM = "device_type";
    String PARAM_USER_DEVICE_TOKEN = "device_token";
    String PARAM_USER_TYPE = "user_type";

    String PARAM_USER_PROFILE = "profile";


    String PARAM_APP_VERSION = "app_version";

    String PARAM_OLD_PASSWORD = "old_password";
    String PARAM_NEW_PASSWORD = "new_password";
    String PARAM_CONFIRM_CONFIRM = "confirm_password";

    String PARAM_USER_NAME = "user_name";
    String PARAM_COMPANY_CODE = "compony_code";
    String PARAM_ADDRESS = "address";
    String PARAM_BIRTH_DATE = "birth_date";
    String PARAM_CONTACT_PERSON_ONE_NAME = "contact_person_name1";
    String PARAM_CONTACT_PERSON_TWO_NAME = "contact_person_name2";
    String PARAM_CONTACT_PERSON_ONE_NUMBER = "contact_person_number1";
    String PARAM_CONTACT_PERSON_TWO_NUMBER = "contact_person_number2";

    String PARAM_EMERGENCY_CONTACT = "emergency_contacts";
        String PARAM_CONTACT_NO = "contact_number";
    String PARAM_HOME_ADDRESS = "home_address";

    String PARAM_PERSON_TO_CALL_name = "person_to_call_name";
    String PARAM_PERSON_TO_CALL_number = "person_to_call_number";

    String PARAM_BLOOD_GROUP = "blood_group";
    String PARAM_IS_NOTIFY = "is_notify";
    String PARAM_PAGE_NO = "page_no";

    String PARAM_LATITUDE = "latitude";
    String PARAM_LONGITUDE = "longitude";
    String PARAM_TITLE = "title";
    String PARAM_MOBILE = "mobile";
    String PARAM_ISUPLOAD = "is_upload";

    String PARAM_ATTECHMENT_TYPE = "attachment_type";

    String PARAM_REG_NO = "reg_no";
    String PARAM_FACULTY = "faculty";
    String PARAM_PROGRAM = "program";
    String PARAM_LEVEL = "level";
    String PARAM_CAT_ID = "category_id";


    String PARAM_STUDENT_CAT_ID = "student_category_id";
    String PARAM_STUDENT_SUB_CAT_ID = "student_subcategory_id";
    String PARAM_NOTIFICATION_ID = "unotification_ids";
    String PARAM_CODE = "code";

    String KEY_NUMBER = "number";
    String KEY_CVV = "cvv";
    String KEY_EXPIRY_MONTH = "expiry_month";
    String KEY_EXPIRY_YEAR = "expiry_year";

    @FormUrlEncoded
    @POST(ENDPOINT_AUTO_LOGIN)
    Call<UserResponse> autoLoginCall(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_USER_DEVICE_PLATFORM) String deviceType,
            @Field(PARAM_USER_APP_VERSION) String appVersion,
            @Field(PARAM_USER_DEVICE_TOKEN) String deviceToken);

    @FormUrlEncoded
    @POST("getstudentcategorylist_new")
    Call<StudentCategoriesModel> get_all_categories(
            @Field(PARAM_AUTH_KEY) String authKey);



    @Multipart
    @POST(ENDPOINT_SIGN_UP)
    Call<UserResponse> signUpIndividualCall(
            @Part MultipartBody.Part part,
            @Part(PARAM_USER_TYPE) RequestBody user_type,

            @Part(PARAM_USER_F_NAME) RequestBody first_name,
            @Part(PARAM_USER_L_NAME) RequestBody last_name,
           // @Part(PARAM_USER_NAME) RequestBody user_name,
            @Part(PARAM_COMPANY_CODE) RequestBody company_code,
            @Part(PARAM_USER_EMAIL) RequestBody email,
            @Part(PARAM_USER_PASSWORD) RequestBody password,
            @Part(PARAM_USER_PHONE_NO) RequestBody contact_number,
            @Part(PARAM_BIRTH_DATE) RequestBody dob,
            @Part(PARAM_ADDRESS) RequestBody home_address,
            @Part(PARAM_CONTACT_PERSON_ONE_NAME) RequestBody person_to_call_name,
            @Part(PARAM_CONTACT_PERSON_ONE_NUMBER) RequestBody person_to_call_number,
            @Part(PARAM_BLOOD_GROUP) RequestBody blood_group,

            @Part(PARAM_USER_DEVICE_PLATFORM) RequestBody deviceType,
            @Part(PARAM_USER_APP_VERSION) RequestBody appVersion,
            @Part(PARAM_USER_DEVICE_TOKEN) RequestBody deviceToken,

           // @Part(PARAM_USER_NAME) RequestBody person_name,


          //  @Part(PARAM_ADDRESS) RequestBody addres,
          //  @Part(PARAM_CONTACT_NO) RequestBody phone_number,

           // @Part(PARAM_CONTACT_PERSON_ONE_NAME) RequestBody contact_person_name1,
           // @Part(PARAM_CONTACT_PERSON_ONE_NUMBER) RequestBody contact_person_number1,
           // @Part(PARAM_CONTACT_PERSON_TWO_NAME) RequestBody contact_person_name2,
           // @Part(PARAM_CONTACT_PERSON_TWO_NUMBER) RequestBody contact_person_number2,

            @Part(PARAM_EMERGENCY_CONTACT) RequestBody emergency_con,
            @Part(PARAM_REG_NO) RequestBody reg_no,
            @Part(PARAM_FACULTY) RequestBody faculty,
            @Part(PARAM_PROGRAM) RequestBody program,
            @Part(PARAM_LEVEL) RequestBody level


    );

    @Multipart
    @POST(ENDPOINT_SIGN_UP)
    Call<UserResponse> signUpCall(
            @Part MultipartBody.Part part,
            @Part(PARAM_USER_TYPE) RequestBody user_type,

            @Part(PARAM_USER_F_NAME) RequestBody first_name,
            @Part(PARAM_USER_L_NAME) RequestBody last_name,
            @Part(PARAM_FACULTY) RequestBody faculty,
            @Part(PARAM_HOME_ADDRESS) RequestBody addres,
            @Part(PARAM_USER_EMAIL) RequestBody email,
            @Part(PARAM_USER_PASSWORD) RequestBody password,
            @Part(PARAM_CONTACT_NO) RequestBody phone_number,
            @Part(PARAM_BIRTH_DATE) RequestBody dob,

            @Part(PARAM_PERSON_TO_CALL_name) RequestBody contact_person_name1,
            @Part(PARAM_PERSON_TO_CALL_number) RequestBody contact_person_number1,
            @Part(PARAM_CONTACT_PERSON_TWO_NAME) RequestBody contact_person_name2,
            @Part(PARAM_CONTACT_PERSON_TWO_NUMBER) RequestBody contact_person_number2,


            @Part(PARAM_EMERGENCY_CONTACT) RequestBody emergency_con,

            @Part(PARAM_USER_DEVICE_PLATFORM) RequestBody deviceType,
            @Part(PARAM_USER_APP_VERSION) RequestBody appVersion,
            @Part(PARAM_USER_DEVICE_TOKEN) RequestBody deviceToken


           // @Part(PARAM_CONTACT_NO) RequestBody contact_number,
           // @Part(PARAM_HOME_ADDRESS) RequestBody home_address,
           // @Part(PARAM_PERSON_TO_CALL_name) RequestBody person_to_call_name,
           // @Part(PARAM_PERSON_TO_CALL_number) RequestBody person_to_call_number,
          //  @Part(PARAM_BLOOD_GROUP) RequestBody blood_group

    );


    @FormUrlEncoded
    @POST(ENDPOINT_LOGIN)
    Call<UserResponse> loginCall(
            @Field(PARAM_USER_EMAIL) String email,
            @Field(PARAM_USER_PASSWORD) String password,
            @Field(PARAM_USER_TYPE) String user_type,
            @Field(PARAM_USER_DEVICE_PLATFORM) String deviceType,
            @Field(PARAM_USER_APP_VERSION) String appVersion,
            @Field(PARAM_USER_DEVICE_TOKEN) String deviceToken);


    @FormUrlEncoded
    @POST(ENDPOINT_CHANGE_PASSWORD)
    Call<BaseResponse> changePasswordCall(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_OLD_PASSWORD) String old_password,
            @Field(PARAM_NEW_PASSWORD) String new_password,
            @Field(PARAM_CONFIRM_CONFIRM) String confirm_password
    );

    @FormUrlEncoded
    @POST(ENDPOINT_FORGOT_PASSWORD)
    Call<BaseResponse> forgotPasswordCall(
            @Field(PARAM_USER_EMAIL) String authKey
    );

    @FormUrlEncoded
    @POST(ENDPOINT_MY_PROFILE)
    Call<UserResponse> getMyProfile(
            @Field(PARAM_AUTH_KEY) String authKey);

    @FormUrlEncoded
    @POST(ENDPOINT_LOGOUT)
    Call<BaseResponse> logout(
            @Field(PARAM_AUTH_KEY) String authKey);

    @FormUrlEncoded
    @POST(ENDPOINT_CATEGORIES)
    Call<CategoryResponse> categories(
            @Field(PARAM_AUTH_KEY) String authKey);

    @FormUrlEncoded
    @POST(ENDPOINT_ON_OFF_NOTIFICATION)
    Call<BaseResponse> switchNotification(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_IS_NOTIFY) String isNotify);


    @FormUrlEncoded
    @POST(ENDPOINT_GET_EMERGENCY_HISTORY)
    Call<HistoryResponse> History(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_PAGE_NO) String page_no);

    @FormUrlEncoded
    @POST(ENDPOINT_GET_STUDENT_MANAGER_LOG)
    Call<GetStudentStatusResponse> GetStudentStatusReport(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_PAGE_NO) String page_no);

    @FormUrlEncoded
    @POST(ENDPOINT_GET_NOTIFICATIONS)
    Call<NotificationsResponse> Notifications(
            @Field(PARAM_AUTH_KEY) String authKey);


    @FormUrlEncoded
    @POST(ENDPOINT_DELETE_NOTIFICATIONS)
    Call<BaseResponse> ClearNotifications(
            @Field(PARAM_AUTH_KEY) String authKey);


    @FormUrlEncoded
    @POST(ENDPOINT_DELETE_MULTI_NOTIFICATIONS)
    Call<BaseResponse> DeleteNotifications(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_NOTIFICATION_ID) String notifid);


    @FormUrlEncoded
    @POST(ENDPOINT_GET_SOCIAL_CAT)
    Call<CategoryResponse> socialcategories(
            @Field(PARAM_AUTH_KEY) String authKey);

    @Multipart
    @POST(ENDPOINT_CHECKIN)
    Call<BaseResponse> checkIn(
            @Part(PARAM_ATTECHMENT_TYPE) RequestBody type,
            @Part(PARAM_AUTH_KEY) RequestBody authKey,
            @Part(PARAM_USER_SOCIAL_CAT_ID) RequestBody social_id,
            @Part(PARAM_USER_DESCRIPTION) RequestBody desciption,
            @Part(PARAM_USER_LATITUDE) RequestBody latitude,
            @Part(PARAM_USER_LONGITUDE) RequestBody longitude,
            @Part(PARAM_ISUPLOAD) RequestBody isupload,
            @Part MultipartBody.Part file
    );

    @Multipart
    @POST(ENDPOINT_ADD_EMERGENCY)
    Call<BaseResponse> addEmergency(
            @Part(PARAM_ATTECHMENT_TYPE) RequestBody type,
            @Part(PARAM_AUTH_KEY) RequestBody authKey,
            @Part(PARAM_USER_DESCRIPTION) RequestBody desc,
            @Part(PARAM_EMERGANCY_ID) RequestBody emergancy_id,
            @Part(PARAM_TITLE) RequestBody selected_option,
            @Part MultipartBody.Part file,
            @Part(PARAM_LATITUDE) RequestBody latitude,
            @Part(PARAM_LONGITUDE) RequestBody longtitude
    );

    @Multipart
    @POST(ENDPOINT_ADD_STUDENT_MANAGER_LOG)
    Call<BaseResponse> addStudentManagerLog(
            @Part(PARAM_ATTECHMENT_TYPE) RequestBody type,
            @Part(PARAM_AUTH_KEY) RequestBody authKey,
            @Part(PARAM_USER_DESCRIPTION) RequestBody desc,
            @Part(PARAM_STUDENT_CAT_ID) RequestBody stud_cat_id,
            @Part(PARAM_STUDENT_SUB_CAT_ID) RequestBody stud_sub_cat_id,
            @Part MultipartBody.Part file,
            @Part(PARAM_LATITUDE) RequestBody latitude,
            @Part(PARAM_LONGITUDE) RequestBody longtitude
    );


    @Multipart
    @POST(ENDPOINT_EDIT_PROFILE)
    Call<UserResponse> EditProfileCall(
            @Part(PARAM_AUTH_KEY) RequestBody authKey,

            @Part MultipartBody.Part part,
            @Part(PARAM_USER_TYPE) RequestBody user_type,
            @Part(PARAM_USER_F_NAME) RequestBody first_name,
            @Part(PARAM_USER_L_NAME) RequestBody last_name,
            @Part(PARAM_FACULTY) RequestBody faculty,
            /*@Part(PARAM_COMPANY_CODE) RequestBody company_code,*/
            @Part(PARAM_ADDRESS) RequestBody addres,
            @Part(PARAM_USER_EMAIL) RequestBody email,
            @Part(PARAM_USER_PHONE_NO) RequestBody phone_number,
            @Part(PARAM_BIRTH_DATE) RequestBody dob,

            @Part(PARAM_CONTACT_PERSON_ONE_NAME) RequestBody contact_person_name1,
            @Part(PARAM_CONTACT_PERSON_ONE_NUMBER) RequestBody contact_person_number1,
            @Part(PARAM_CONTACT_PERSON_TWO_NAME) RequestBody contact_person_name2,
            @Part(PARAM_CONTACT_PERSON_TWO_NUMBER) RequestBody contact_person_number2,


            @Part(PARAM_EMERGENCY_CONTACT) RequestBody emergency_con,

            @Part(PARAM_USER_DEVICE_PLATFORM) RequestBody deviceType,
            @Part(PARAM_USER_APP_VERSION) RequestBody appVersion,
            @Part(PARAM_USER_DEVICE_TOKEN) RequestBody deviceToken

    );

    @Multipart
    @POST(ENDPOINT_EDIT_PROFILE)
    Call<UserResponse> EditProfileIndividualCall(
            @Part(PARAM_AUTH_KEY) RequestBody authKey,

            @Part MultipartBody.Part part,
            @Part(PARAM_USER_TYPE) RequestBody user_type,
            @Part(PARAM_USER_F_NAME) RequestBody first_name,
            @Part(PARAM_USER_L_NAME) RequestBody last_name,
          //  @Part(PARAM_USER_NAME) RequestBody user_name,
            @Part(PARAM_COMPANY_CODE) RequestBody company_code,
            @Part(PARAM_USER_EMAIL) RequestBody email,
            @Part(PARAM_CONTACT_NO) RequestBody contact_number,
            @Part(PARAM_BIRTH_DATE) RequestBody dob,
            @Part(PARAM_HOME_ADDRESS) RequestBody home_address,
            @Part(PARAM_PERSON_TO_CALL_name) RequestBody person_to_call_name,
            @Part(PARAM_PERSON_TO_CALL_number) RequestBody person_to_call_number,
            @Part(PARAM_BLOOD_GROUP) RequestBody blood_group,

            @Part(PARAM_USER_DEVICE_PLATFORM) RequestBody deviceType,
            @Part(PARAM_USER_APP_VERSION) RequestBody appVersion,
            @Part(PARAM_USER_DEVICE_TOKEN) RequestBody deviceToken,

            @Part(PARAM_REG_NO) RequestBody reg_no,
            @Part(PARAM_FACULTY) RequestBody faculty,
            @Part(PARAM_PROGRAM) RequestBody program,
            @Part(PARAM_LEVEL) RequestBody level
    );


    @Multipart
    @POST(ENDPOINT_SEND_MEDIA)
    Call<BaseResponse> sendMassage(
            @Part(PARAM_AUTH_KEY) RequestBody authKey,
            @Part(PARAM_ATTECHMENT_TYPE) RequestBody type,
            @Part("caption") RequestBody caption,
            @Part(PARAM_USER_DESCRIPTION) RequestBody desc,
            @Part(PARAM_LATITUDE) RequestBody latitude,
            @Part(PARAM_LONGITUDE) RequestBody longtitude,
            @Part MultipartBody.Part part
    );

    @Multipart
    @POST(ENDPOINT_KIDNAP)
    Call<BaseResponse> kidnap(
            @Part(PARAM_AUTH_KEY) RequestBody authKey,
            @Part(PARAM_USER_DESCRIPTION) RequestBody desc,
            @Part(PARAM_LATITUDE) RequestBody latitude,
            @Part(PARAM_LONGITUDE) RequestBody longtitude

    );

    @FormUrlEncoded
    @POST(ENDPOINT_STUDENT_CATEGORY)
    Call<CategoryResponse> studentcategories(
            @Field(PARAM_AUTH_KEY) String authKey);

    @FormUrlEncoded
    @POST(ENDPOINT_STUDENT_SUB_CATEGORY)
    Call<SubCatResposne> studentSubCategories(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_CAT_ID) String cat_id
    );

    @FormUrlEncoded
    @POST(ENDPOINT_APPLY_USSD_CODE)
    Call<ApplyUSSDCodeResponse> applyUSSDCode(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_CODE) String code);


    @FormUrlEncoded
    @POST(ENDPOINT_GET_MEDIA_LIST)
    Call<GalleryResponse> getMediaList(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(PARAM_PAGE_NO) String page_no);


    @FormUrlEncoded
    @POST(ENDPOINT_GET_PRODUCT_LIST)
    Call<ProductListResponse> getProductList(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field("type") String type);

    @FormUrlEncoded
    @POST(ENDPOINT_ADD_TO_CART)
    Call<BaseResponse> addtoCart(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field("product_id") String type);

    @FormUrlEncoded
    @POST(ENDPOINT_GET_CART_LIST)
    Call<CartListResponse> getCartList(
            @Field(PARAM_AUTH_KEY) String authKey);

    @FormUrlEncoded
    @POST(ENDPOINT_DELETE_CART_ITEM)
    Call<BaseResponse> getdeleteCartItem(
            @Field(PARAM_AUTH_KEY) String authKey,
                @Field("id") String type);

    @FormUrlEncoded
    @POST(ENDPOINT_GET_CART_ITEM_COUNT)
    Call<CartItemCountResponse> getCartItemCount(
            @Field(PARAM_AUTH_KEY) String authKey);


    /*payment API Call*/
    @FormUrlEncoded
    @POST(ENDPOINT_PAYSTACK_PAYENT)
    Call<PaymenyResponse> paymentAPI(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field(KEY_NUMBER) String number,
            @Field(KEY_CVV) String cvv,
            @Field(KEY_EXPIRY_MONTH) String month,
            @Field(KEY_EXPIRY_YEAR) String year,
            @Field("amount") String amount);


    @FormUrlEncoded
    @POST(URL_SUBMIT_PIN)
    Call<PaymenyResponse> submitPinAPI(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field("reference") String referance,
            @Field("pin") String pin);

    @FormUrlEncoded
    @POST(URL_SUBMIT_OTP)
    Call<PaymenyResponse> submitOTPAPI(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field("reference") String referance,
            @Field("otp") String otp);


    @FormUrlEncoded
    @POST(ENDPOINT_PLACE_ORDER)
    Call<BaseResponse> placeOrderCall(
            @Field(PARAM_AUTH_KEY) String authKey,
            @Field("payment_token") String payment_token,
            @Field("payment_status") String payment_status,
            @Field("tax_percentage") String tax_percentage);


}
