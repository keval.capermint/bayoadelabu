package com.weddingplus.web_services;


import com.weddingplus.App;
import com.weddingplus.R;
import com.weddingplus.utilities.StringUtils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jay Kikani on 7/6/17.
 */

public abstract class BaseCallback<T> implements Callback<T> {

    @Override
    public void onResponse(Call<T> call, Response<T> response) {
        if (response.body() == null) {
            Failure(App.getAppInstance().getString(R.string.ws_failure));
            return;
        }
        BaseResponse json = (BaseResponse) response.body();
        int status = json.getStatus();
        boolean statusexpired = json.isExpired();
        String message = json.getMessage();
        if (status == 1) {
            Success(response);
        }
     /*   else if (status == 0) {
            Success(response);
        }
        else if (status == 5) {
            Success(response);
        }
        else if (status == 6) {
            Success(response);
        }*/
        else if (statusexpired) {
            SessionExpired(message);
        } else {
            if (StringUtils.isValid(message)) {
                Failure(message);
            } else {
                Failure(App.getAppInstance().getString(R.string.ws_failure));
            }
        }
    }

    @Override
    public void onFailure(Call<T> call, Throwable t) {
        if (StringUtils.equals(t.getMessage(), "Canceled")) {
            return;
        }
        Failure(t.getMessage());
    }

    public abstract void Success(Response<T> response);

    public abstract void Failure(String message);

    public abstract void SessionExpired(String message);
}
