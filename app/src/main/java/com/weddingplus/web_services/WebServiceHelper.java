package com.weddingplus.web_services;

import com.weddingplus.App;
import com.weddingplus.R;
import com.weddingplus.sync.StudentCategoriesModel;
import com.weddingplus.utilities.AppConstants;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.PreferenceConstants;
import com.weddingplus.web_services.responses.ApplyUSSDCodeResponse;
import com.weddingplus.web_services.responses.CartItemCountResponse;
import com.weddingplus.web_services.responses.CartListResponse;
import com.weddingplus.web_services.responses.CategoryResponse;
import com.weddingplus.web_services.responses.GalleryResponse;
import com.weddingplus.web_services.responses.GetStudentStatusResponse;
import com.weddingplus.web_services.responses.HistoryResponse;
import com.weddingplus.web_services.responses.NotificationsResponse;
import com.weddingplus.web_services.responses.PaymenyResponse;
import com.weddingplus.web_services.responses.ProductListResponse;
import com.weddingplus.web_services.responses.SubCatResposne;
import com.weddingplus.web_services.responses.UserResponse;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Manndeep Vachhani on 3/4/17.
 */
public class WebServiceHelper implements AppConstants, PreferenceConstants {

    private static String baseUrl = App.getAppInstance().getString(R.string.base_url);

    private static OkHttpClient getClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return
                new OkHttpClient.Builder()
                        .addInterceptor(interceptor)
                        .readTimeout(READ_TIMEOUT, TimeUnit.SECONDS)
                        .connectTimeout(CONNECT_TIMEOUT, TimeUnit.SECONDS)
                        .build();
    }

    private static Retrofit getRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(getClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static Call<UserResponse> getAutoLoginCall(String appVersion, String platform) {
        return getRetrofit().create(RequestAPIs.class)
                .autoLoginCall(
                        App.getAuthKey(),
                        platform,
                        appVersion,
                        FirebaseInstanceId.getInstance().getToken()

                );
    }
    public static Call<StudentCategoriesModel> getAllCategories() {
        return getRetrofit().create(RequestAPIs.class)
                .get_all_categories(App.getAuthKey());
    }
    public static Call<UserResponse> getIndividualSignUpCall(MultipartBody.Part part, String first_name,String last_name, String company_code,String email_address,
                                                             String password, String phone_no, String birth_date,
                                                             String home_address, String person_to_call_name, String person_to_call_number, String blood_group,
                                                             String reg_no, String faculty, String program, String level) {

        RequestBody rfirst_name = RequestBody.create(MediaType.parse("multipart/form-data"), first_name);
        RequestBody rlast_name = RequestBody.create(MediaType.parse("multipart/form-data"), last_name);
        RequestBody rcompany_code = RequestBody.create(MediaType.parse("multipart/form-data"), company_code);
        RequestBody remail_address = RequestBody.create(MediaType.parse("multipart/form-data"), email_address);
        RequestBody rpassword = RequestBody.create(MediaType.parse("multipart/form-data"), password);
        RequestBody rphone_no = RequestBody.create(MediaType.parse("multipart/form-data"), phone_no);
        RequestBody rbirth_date = RequestBody.create(MediaType.parse("multipart/form-data"), birth_date);
        RequestBody rhome_address = RequestBody.create(MediaType.parse("multipart/form-data"), home_address);
        RequestBody rperson_to_call_name = RequestBody.create(MediaType.parse("multipart/form-data"), person_to_call_name);
        RequestBody rperson_to_call_number = RequestBody.create(MediaType.parse("multipart/form-data"), person_to_call_number);
        RequestBody rblood_group = RequestBody.create(MediaType.parse("multipart/form-data"), blood_group);


        RequestBody rdevicetype = RequestBody.create(MediaType.parse("multipart/form-data"), DEVICE_TYPE);
        RequestBody rappversion = RequestBody.create(MediaType.parse("multipart/form-data"), APP_VERSION);
        RequestBody rdevice_token = RequestBody.create(MediaType.parse("multipart/form-data"),
                FirebaseInstanceId.getInstance().getToken());

        RequestBody ruser_type = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_USER_TYPE));

        RequestBody rempty = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody r_reg_no = RequestBody.create(MediaType.parse("multipart/form-data"), reg_no);
        RequestBody r_faculty = RequestBody.create(MediaType.parse("multipart/form-data"), faculty);
        RequestBody r_program = RequestBody.create(MediaType.parse("multipart/form-data"), program);
        RequestBody r_level = RequestBody.create(MediaType.parse("multipart/form-data"), level);


        return getRetrofit().create(RequestAPIs.class).signUpIndividualCall(part, ruser_type, rfirst_name,rlast_name, rcompany_code,remail_address, rpassword, rphone_no,
                rbirth_date, rhome_address, rperson_to_call_name, rperson_to_call_number, rblood_group, rdevicetype, rappversion, rdevice_token, rempty, r_reg_no, r_faculty, r_program, r_level);


    }


    public static Call<UserResponse> getSignUpCall(MultipartBody.Part part, String first_name,String last_name, String faculty, String address, String email_address,
                                                   String password, String phone_no, String birth_date,
                                                   String contact_per_one_name, String contact_per_one_number, String contact_per_two_name,
                                                   String contact_per_two_number, String emergency_contact) {
        RequestBody rfirst_name = RequestBody.create(MediaType.parse("multipart/form-data"), first_name);
        RequestBody rlast_name = RequestBody.create(MediaType.parse("multipart/form-data"), last_name);
        RequestBody r_faculty = RequestBody.create(MediaType.parse("multipart/form-data"), faculty);
        RequestBody raddress = RequestBody.create(MediaType.parse("multipart/form-data"), address);
        RequestBody remail_address = RequestBody.create(MediaType.parse("multipart/form-data"), email_address);
        RequestBody rpassword = RequestBody.create(MediaType.parse("multipart/form-data"), password);
        RequestBody rphone_no = RequestBody.create(MediaType.parse("multipart/form-data"), phone_no);
        RequestBody rbirth_date = RequestBody.create(MediaType.parse("multipart/form-data"), birth_date);
        RequestBody rcontact_per_one_name = RequestBody.create(MediaType.parse("multipart/form-data"), contact_per_one_name);
        RequestBody rcontact_per_one_number = RequestBody.create(MediaType.parse("multipart/form-data"), contact_per_one_number);
        RequestBody rcontact_per_two_name = RequestBody.create(MediaType.parse("multipart/form-data"), contact_per_two_name);
        RequestBody rcontact_per_two_number = RequestBody.create(MediaType.parse("multipart/form-data"), contact_per_two_number);
        RequestBody remergency_contact = RequestBody.create(MediaType.parse("multipart/form-data"), emergency_contact);

        RequestBody rdevicetype = RequestBody.create(MediaType.parse("multipart/form-data"), DEVICE_TYPE);
        RequestBody rappversion = RequestBody.create(MediaType.parse("multipart/form-data"), APP_VERSION);
        RequestBody rdevice_token = RequestBody.create(MediaType.parse("multipart/form-data"),
                FirebaseInstanceId.getInstance().getToken());

        RequestBody ruser_type = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_USER_TYPE));

        RequestBody rempty = RequestBody.create(MediaType.parse("multipart/form-data"), "");

        return getRetrofit().create(RequestAPIs.class).signUpCall(part, ruser_type, rfirst_name,rlast_name, r_faculty, raddress,
                remail_address, rpassword, rphone_no, rbirth_date, rcontact_per_one_name, rcontact_per_one_number, rcontact_per_two_name, rcontact_per_two_number, remergency_contact,
                rdevicetype, rappversion, rdevice_token);


    }


    public static Call<UserResponse> getLoginCall(String email, String password) {
        return getRetrofit().create(RequestAPIs.class)
                .loginCall(
                        email,
                        password,
                        Helper.getStringValue(KEY_USER_TYPE),
                        DEVICE_TYPE,
                        APP_VERSION,
                        FirebaseInstanceId.getInstance().getToken()
                );
    }

    public static Call<BaseResponse> getForgotPasswordCall(String email) {
        return getRetrofit().create(RequestAPIs.class)
                .forgotPasswordCall(email);
    }

    public static Call<BaseResponse> getChangePasswordCall(String oldPassword, String newPassword, String confirmPassword) {
        return getRetrofit().create(RequestAPIs.class)
                .changePasswordCall(App.getAuthKey(), oldPassword, newPassword, confirmPassword);
    }

    public static Call<UserResponse> getMyProfile() {
        return getRetrofit().create(RequestAPIs.class)
                .getMyProfile(App.getAuthKey());
    }

    public static Call<BaseResponse> logout() {
        return getRetrofit().create(RequestAPIs.class)
                .logout(App.getAuthKey());
    }

    public static Call<CategoryResponse> getEmergencyCategory() {
        return getRetrofit().create(RequestAPIs.class)
                .categories(App.getAuthKey());
    }


    public static Call<BaseResponse> switchNotify(String notify) {
        return getRetrofit().create(RequestAPIs.class)
                .switchNotification(App.getAuthKey(), notify);

    }

    public static Call<HistoryResponse> getHistory(String page_no) {
        return getRetrofit().create(RequestAPIs.class)
                .History(App.getAuthKey(), page_no);
    }
    public static Call<GetStudentStatusResponse> getStdentSatusReport(String page_no) {
        return getRetrofit().create(RequestAPIs.class)
                .GetStudentStatusReport(App.getAuthKey(), page_no);
    }

    public static Call<NotificationsResponse> getNotifications() {
        return getRetrofit().create(RequestAPIs.class)
                .Notifications(App.getAuthKey());
    }

    public static Call<BaseResponse> clearNotifications() {
        return getRetrofit().create(RequestAPIs.class)
                .ClearNotifications(App.getAuthKey());
    }

    public static Call<BaseResponse> deleteNotifications(String notifid) {
        return getRetrofit().create(RequestAPIs.class)
                .DeleteNotifications(App.getAuthKey(),notifid);
    }

    public static Call<CategoryResponse> getSocialCategory() {
        return getRetrofit().create(RequestAPIs.class)
                .socialcategories(App.getAuthKey());
    }

    public static Call<BaseResponse> CheckIn(String attachtype, MultipartBody.Part part, String social_cat_id, String desc, String is_upload) {

        RequestBody rsocial_cat_id = RequestBody.create(MediaType.parse("multipart/form-data"), social_cat_id);
        RequestBody rdesc = RequestBody.create(MediaType.parse("multipart/form-data"), desc);
        RequestBody rlattitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LATTITUDE));
        RequestBody rlongitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LONGITUDE));
        RequestBody rauth_key = RequestBody.create(MediaType.parse("multipart/form-data"), App.getAuthKey());
        RequestBody ris_uploade = RequestBody.create(MediaType.parse("multipart/form-data"), is_upload);

        RequestBody attachtypebody = RequestBody.create(MediaType.parse("multipart/form-data"), attachtype);


        return getRetrofit().create(RequestAPIs.class)

                .checkIn(attachtypebody, rauth_key, rsocial_cat_id, rdesc, rlattitude, rlongitude, ris_uploade, part);
    }

    public static Call<UserResponse> EditProfile(MultipartBody.Part part, String first_name,String last_name, String faculty, String address, String email_address,
                                                 String phone_no, String birth_date,
                                                 String contact_per_one_name, String contact_per_one_number, String contact_per_two_name,
                                                 String contact_per_two_number, String emergency_contact) {
        RequestBody rfirst_name = RequestBody.create(MediaType.parse("multipart/form-data"), first_name);
        RequestBody rlast_name = RequestBody.create(MediaType.parse("multipart/form-data"), last_name);
        RequestBody r_faculty = RequestBody.create(MediaType.parse("multipart/form-data"), faculty);
        RequestBody raddress = RequestBody.create(MediaType.parse("multipart/form-data"), address);
        RequestBody remail_address = RequestBody.create(MediaType.parse("multipart/form-data"), email_address);
        RequestBody rphone_no = RequestBody.create(MediaType.parse("multipart/form-data"), phone_no);
        RequestBody rbirth_date = RequestBody.create(MediaType.parse("multipart/form-data"), birth_date);
        RequestBody rcontact_per_one_name = RequestBody.create(MediaType.parse("multipart/form-data"), contact_per_one_name);
        RequestBody rcontact_per_one_number = RequestBody.create(MediaType.parse("multipart/form-data"), contact_per_one_number);
        RequestBody rcontact_per_two_name = RequestBody.create(MediaType.parse("multipart/form-data"), contact_per_two_name);
        RequestBody rcontact_per_two_number = RequestBody.create(MediaType.parse("multipart/form-data"), contact_per_two_number);
        RequestBody remergency_contact = RequestBody.create(MediaType.parse("multipart/form-data"), emergency_contact);

        RequestBody rdevicetype = RequestBody.create(MediaType.parse("multipart/form-data"), DEVICE_TYPE);
        RequestBody rappversion = RequestBody.create(MediaType.parse("multipart/form-data"), APP_VERSION);
        RequestBody rdevice_token = RequestBody.create(MediaType.parse("multipart/form-data"),
                FirebaseInstanceId.getInstance().getToken());

        RequestBody ruser_type = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_USER_TYPE));

        RequestBody rempty = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody rauth_key = RequestBody.create(MediaType.parse("multipart/form-data"), App.getAuthKey());

        return getRetrofit().create(RequestAPIs.class).EditProfileCall(rauth_key, part, ruser_type, rfirst_name,rlast_name, r_faculty, raddress,
                remail_address, rphone_no, rbirth_date, rcontact_per_one_name, rcontact_per_one_number, rcontact_per_two_name, rcontact_per_two_number, remergency_contact,
                rdevicetype, rappversion, rdevice_token);


    }

    public static Call<UserResponse> getIndividualEditProfileCall(MultipartBody.Part part, String first_name,String last_name, String company_code, String email_address,
                                                                  String phone_no, String birth_date,
                                                                  String home_address, String person_to_call_name, String person_to_call_number, String blood_group,
                                                                  String reg_no, String faculty, String program, String level) {
        RequestBody rcompany_code = RequestBody.create(MediaType.parse("multipart/form-data"), company_code);
        RequestBody rfirst_name = RequestBody.create(MediaType.parse("multipart/form-data"), first_name);
        RequestBody rlast_name = RequestBody.create(MediaType.parse("multipart/form-data"), last_name);

        //RequestBody ruser_name = RequestBody.create(MediaType.parse("multipart/form-data"), username_name);
        RequestBody remail_address = RequestBody.create(MediaType.parse("multipart/form-data"), email_address);
        RequestBody rphone_no = RequestBody.create(MediaType.parse("multipart/form-data"), phone_no);
        RequestBody rbirth_date = RequestBody.create(MediaType.parse("multipart/form-data"), birth_date);
        RequestBody rhome_address = RequestBody.create(MediaType.parse("multipart/form-data"), home_address);
        RequestBody rperson_to_call_name = RequestBody.create(MediaType.parse("multipart/form-data"), person_to_call_name);
        RequestBody rperson_to_call_number = RequestBody.create(MediaType.parse("multipart/form-data"), person_to_call_number);
        RequestBody rblood_group = RequestBody.create(MediaType.parse("multipart/form-data"), blood_group);


        RequestBody rdevicetype = RequestBody.create(MediaType.parse("multipart/form-data"), DEVICE_TYPE);
        RequestBody rappversion = RequestBody.create(MediaType.parse("multipart/form-data"), APP_VERSION);
        RequestBody rdevice_token = RequestBody.create(MediaType.parse("multipart/form-data"),
                FirebaseInstanceId.getInstance().getToken());

        RequestBody ruser_type = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_USER_TYPE));

        RequestBody rempty = RequestBody.create(MediaType.parse("multipart/form-data"), "");
        RequestBody rauth_key = RequestBody.create(MediaType.parse("multipart/form-data"), App.getAuthKey());

        RequestBody r_reg_no = RequestBody.create(MediaType.parse("multipart/form-data"), reg_no);
        RequestBody r_faculty = RequestBody.create(MediaType.parse("multipart/form-data"), faculty);
        RequestBody r_program = RequestBody.create(MediaType.parse("multipart/form-data"), program);
        RequestBody r_level = RequestBody.create(MediaType.parse("multipart/form-data"), level);


        return getRetrofit().create(RequestAPIs.class).EditProfileIndividualCall(rauth_key, part, ruser_type, rfirst_name,rlast_name,rcompany_code, remail_address, rphone_no,
                rbirth_date, rhome_address, rperson_to_call_name, rperson_to_call_number, rblood_group, rdevicetype, rappversion, rdevice_token,
                r_reg_no, r_faculty, r_program, r_level);


    }

    public static Call<BaseResponse> AddEmergency(String attachtype, MultipartBody.Part file, String desc, String emergancy_id, String selected_option) {

        RequestBody rauth_key = RequestBody.create(MediaType.parse("multipart/form-data"), App.getAuthKey());
        RequestBody rdesc = RequestBody.create(MediaType.parse("multipart/form-data"), desc);
        RequestBody remergancy_id = RequestBody.create(MediaType.parse("multipart/form-data"), emergancy_id);
        RequestBody rselected_option = RequestBody.create(MediaType.parse("multipart/form-data"), selected_option);

        RequestBody rlatitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LATTITUDE));
        RequestBody rlongitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LONGITUDE));

        RequestBody attachtypebody = RequestBody.create(MediaType.parse("multipart/form-data"), attachtype);


        return getRetrofit().create(RequestAPIs.class).addEmergency(attachtypebody, rauth_key, rdesc, remergancy_id, rselected_option, file, rlatitude, rlongitude);
    }

    public static Call<BaseResponse> SendMessage(MultipartBody.Part part, String attechtype, String caption, String message) {

        RequestBody rauth_key = RequestBody.create(MediaType.parse("multipart/form-data"), App.getAuthKey());
        RequestBody rtype = RequestBody.create(MediaType.parse("multipart/form-data"), attechtype);
        RequestBody rcaption = RequestBody.create(MediaType.parse("multipart/form-data"), caption);
        RequestBody rdesc = RequestBody.create(MediaType.parse("multipart/form-data"), message);
        RequestBody rlatitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LATTITUDE));
        RequestBody rlongitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LONGITUDE));

        return getRetrofit().create(RequestAPIs.class).sendMassage(rauth_key,rtype,rcaption,rdesc, rlatitude, rlongitude,part);
    }

    public static Call<BaseResponse> kidnap(String desc) {

        RequestBody rauth_key = RequestBody.create(MediaType.parse("multipart/form-data"), App.getAuthKey());
        RequestBody rdesc = RequestBody.create(MediaType.parse("multipart/form-data"), desc);
        RequestBody rlatitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LATTITUDE));
        RequestBody rlongitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LONGITUDE));

        return getRetrofit().create(RequestAPIs.class).kidnap(rauth_key, rdesc, rlatitude, rlongitude);
    }

    public static Call<CategoryResponse> getStudentCategory() {
        return getRetrofit().create(RequestAPIs.class)
                .studentcategories(App.getAuthKey());
    }

    public static Call<SubCatResposne> getStudentSubCategory(String cat_id) {
        return getRetrofit().create(RequestAPIs.class)
                .studentSubCategories(App.getAuthKey(), cat_id);
    }

    public static Call<BaseResponse> AddStudentManagerLog(String attachtype, MultipartBody.Part file, String desc, String stud_cat_id, String stud_sub_cat_id) {

        RequestBody rauth_key = RequestBody.create(MediaType.parse("multipart/form-data"), App.getAuthKey());
        RequestBody rdesc = RequestBody.create(MediaType.parse("multipart/form-data"), desc);
        RequestBody rstu_cat_id = RequestBody.create(MediaType.parse("multipart/form-data"), stud_cat_id);
        RequestBody rstu_sub_cat_id = RequestBody.create(MediaType.parse("multipart/form-data"), stud_sub_cat_id);

        RequestBody rlatitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LATTITUDE));
        RequestBody rlongitude = RequestBody.create(MediaType.parse("multipart/form-data"), Helper.getStringValue(KEY_LONGITUDE));

        RequestBody attachtypebody = RequestBody.create(MediaType.parse("multipart/form-data"), attachtype);


        return getRetrofit().create(RequestAPIs.class).addStudentManagerLog(attachtypebody, rauth_key, rdesc, rstu_cat_id,rstu_sub_cat_id,  file, rlatitude, rlongitude);
    }

    public static Call<ApplyUSSDCodeResponse> applyUSSDCode(String code) {
        return getRetrofit().create(RequestAPIs.class)
                .applyUSSDCode(App.getAuthKey(),code);
    }
    public static Call<GalleryResponse> getMediaList(String page_no) {
        return getRetrofit().create(RequestAPIs.class)
                .getMediaList(App.getAuthKey(), page_no);
    }

    public static Call<ProductListResponse> getProductList(String type) {
        return getRetrofit().create(RequestAPIs.class)
                .getProductList(App.getAuthKey(), type);
    }

    public static Call<BaseResponse> addToCart(String product_id) {
        return getRetrofit().create(RequestAPIs.class)
                .addtoCart(App.getAuthKey(), product_id);
    }
    public static Call<CartListResponse> getCartList() {
        return getRetrofit().create(RequestAPIs.class)
                .getCartList(App.getAuthKey());
    }
    public static Call<BaseResponse> getDeleteItem(String id) {
        return getRetrofit().create(RequestAPIs.class)
                .getdeleteCartItem(App.getAuthKey(),id);
    }
    public static Call<CartItemCountResponse> getCartItemCount() {
        return getRetrofit().create(RequestAPIs.class)
                .getCartItemCount(App.getAuthKey());
    }


    /*payment api Call*/
    public static Call<PaymenyResponse> paymentCall(String number, String cvv, String month, String year, String amount) {
        return getRetrofit().create(RequestAPIs.class)
                .paymentAPI(App.getAuthKey(),number,cvv,month,year,amount);
    }

    public static Call<BaseResponse> placeOrderCall(String token,String status,String taxamount) {
        return getRetrofit().create(RequestAPIs.class)
                .placeOrderCall(App.getAuthKey(),token,status,taxamount);
    }


    public static Call<PaymenyResponse> verifyPinCall(String reference,String pin) {
        return getRetrofit().create(RequestAPIs.class)
                .submitPinAPI(App.getAuthKey(),reference,pin);
    }
    public static Call<PaymenyResponse> verifyOtpCall(String reference,String otp) {
        return getRetrofit().create(RequestAPIs.class)
                .submitOTPAPI(App.getAuthKey(),reference,otp);
    }

}
