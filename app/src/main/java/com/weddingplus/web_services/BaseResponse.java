package com.weddingplus.web_services;

import com.weddingplus.utilities.AppConstants;

/**
 * Created by Manndeep Vachhani on 9/2/17.
 */
public class BaseResponse implements AppConstants {

    private int result;
    private int status;
    private String message;


    public int getStatus() {
        return result;
    }

    public String getMessage() {
        return message;
    }

    public boolean isSuccess() {
        /**
         * Redirect user to registration if authorization failed
         */
        /*if (isUnauthorised()) {
            Intent intent = new Intent(App.getAppInstance(), RegisterActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            App.getAppInstance().startActivity(intent);
            UiHelper.toast(App.getAppInstance(), "Authorization Failed");
        }*/
/*
        if (isUnauthorised()) {
            UiHelper.toast("Unauthorized User");
            Helper.clearPreferences();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    System.exit(0);
                }
            }, 1000);
        }
*/
        return result == 1;
    }

    @Override
    public String toString() {
        return super.toString();
    }

    /**
     * Checks if user is registed or not by response
     *
     * @return true if registered, false otherwise
     */
    public boolean isRegistered() {
        return result == 4;
    }

    /**
     * Checks if user is authorised or not by response
     *
     * @return true if authorised, false otherwise
     */
    private boolean isUnauthorised() {
        return result == 3;
    }

    public boolean isExpired() {
        return status == 2;
    }

}
