package com.weddingplus;

import android.app.Application;

import com.weddingplus.models.User;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.PreferenceConstants;

/**
 * Created by Manndeep Vachhani on 4/2/17.
 */
public class App extends Application {

    private static App mAppInstance;
    public static User currentUser;

    public static String ISUPLOAD = "N";
    @Override
    public void onCreate() {
        super.onCreate();
        mAppInstance = this;
    }

    public static User getCurrentUser() {
        return currentUser;
    }

    public static App getAppInstance() {
        return mAppInstance;
    }

    public static String getAuthKey(){
        return Helper.getStringValue(PreferenceConstants.KEY_AUTH_KEY);
    }

}
