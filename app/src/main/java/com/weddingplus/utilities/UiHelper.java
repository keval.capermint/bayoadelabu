package com.weddingplus.utilities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ScrollView;
import android.widget.Toast;

import com.weddingplus.App;
import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.models.ProductData;
import com.weddingplus.models.VideoData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.activities.CartActivity;
import com.weddingplus.user_interface.activities.ChangePasswordActivity;
import com.weddingplus.user_interface.activities.CheckInActivity;
import com.weddingplus.user_interface.activities.ContactUsActivity;
import com.weddingplus.user_interface.activities.DonateActivity;
import com.weddingplus.user_interface.activities.EmergencyInstructionActivity;
import com.weddingplus.user_interface.activities.EmergencyResponseActivity;
import com.weddingplus.user_interface.activities.ForgotPasswordActivity;
import com.weddingplus.user_interface.activities.FullScreenVideoActivity;
import com.weddingplus.user_interface.activities.GallaryActivity;
import com.weddingplus.user_interface.activities.ImageFullViewActivity;
import com.weddingplus.user_interface.activities.InstructionActivity;
import com.weddingplus.user_interface.activities.LoginActivity;
import com.weddingplus.user_interface.activities.LoginCorporateActivity;
import com.weddingplus.user_interface.activities.MainActivity;
import com.weddingplus.user_interface.activities.MessageCenterActivity;
import com.weddingplus.user_interface.activities.NewsActivity;
import com.weddingplus.user_interface.activities.PaymentActivity;
import com.weddingplus.user_interface.activities.PrivacyPolicyActivity;
import com.weddingplus.user_interface.activities.ProductDetailsActivity;
import com.weddingplus.user_interface.activities.ProductListActivity;
import com.weddingplus.user_interface.activities.ShoppingActivity;
import com.weddingplus.user_interface.activities.SignUpActivity;
import com.weddingplus.user_interface.activities.SignUpCorporateActivity;
import com.weddingplus.user_interface.activities.SocialActivity;
import com.weddingplus.user_interface.activities.SocialCheckInActivity;
import com.weddingplus.user_interface.activities.StudentManagerActivity;
import com.weddingplus.user_interface.activities.TermConditionActivity;
import com.weddingplus.user_interface.activities.USSDActivity;
import com.weddingplus.user_interface.activities.UpdateProfileActivity;
import com.weddingplus.user_interface.activities.UpdateProfileCorporateActivity;
import com.weddingplus.user_interface.activities.Welcome2Activity;
import com.weddingplus.user_interface.activities.WelcomeActivity;
import com.github.ybq.android.spinkit.style.FadingCircle;

import java.io.IOException;
import java.util.List;
import java.util.Locale;


public class UiHelper implements AppConstants {

    private static final String TAG = "UiHelper";
    private static ProgressDialog progress;

    public static int getDeviceWidthInPercentage(int percentage) {
        DisplayMetrics displayMetrics = App.getAppInstance().getResources().getDisplayMetrics();
        float toMultiply = percentage / 100f;
        float pixels = displayMetrics.widthPixels * toMultiply;
        return (int) pixels;
    }

    public static void animateTransition(BaseAppCompatActivity mActivity) {
        mActivity.overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
    }

    /*
        public static void shareIntent(Context context, String shareText) {
            Helper.putDebugLog(TAG, "shareText :: " + shareText);
            if (null != shareText) {
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(android.content.Intent.EXTRA_TEXT,
                        shareText
                );
                context.startActivity(Intent.createChooser(sharingIntent, "Share using"));
            }
        }
    */


    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        View view = activity.getCurrentFocus();
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    static void showNetworkDialog(final BaseAppCompatActivity context) {
        new AlertDialog.Builder(context, R.style.AlertDialogStyle)
                .setTitle(R.string.app_name)
                .setMessage(R.string.no_internet)
                .setIcon(R.mipmap.ic_launcher)
                .setCancelable(false)
                .setPositiveButton(R.string.connect, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        Intent settingIntent = new Intent(Settings.ACTION_SETTINGS);
                        settingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(settingIntent);
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        context.finish();
                    }
                }).show();
    }

    public static void showNotificationDialog(final Context context, String msg, DialogInterface.OnClickListener positiveButtonListener) {
        new AlertDialog.Builder(context, R.style.AlertDialogStyle)
                .setTitle(R.string.app_name)
                .setMessage(msg)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(R.string.ok, positiveButtonListener)
                .show();
    }

    public static void showConfirmationDialog(final Context context, String msg, DialogInterface.OnClickListener positiveButtonListener) {
        new AlertDialog.Builder(context, R.style.AlertDialogStyle)
                .setTitle(R.string.app_name)
                .setMessage(msg)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(R.string.ok, positiveButtonListener)
                .setNegativeButton(R.string.cancel, null)
                .show();
    }

    public static void showAlertDialog(final Context context, String msg) {
        new AlertDialog.Builder(context, R.style.AlertDialogStyle)
                .setTitle(R.string.app_name)
                .setMessage(msg)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    public static void showLogoutDialog(BaseAppCompatActivity mActivity, String msg, DialogInterface.OnClickListener positiveButtonListener) {
        new AlertDialog.Builder(mActivity, R.style.AlertDialogStyle)
                .setTitle(R.string.app_name)
                .setMessage(msg)
                .setIcon(R.mipmap.ic_launcher)
                .setPositiveButton(R.string.log_out, positiveButtonListener)
//                .setNegativeButton(R.string.cancel, null)
                .show();
    }


    public static AlertDialog.Builder getCustomDialogBuilder(Context context, View dialogView) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setView(dialogView);
        builder.setCancelable(false);
        return builder;
    }

    public static void startProgress(BaseAppCompatActivity activity, boolean isCancelable) {
        if (null != activity && !activity.isFinishing() && !activity.isDestroyed()) {

            stopProgress(activity);
            progress = new ProgressDialog(activity, R.style.ProgressBarTheme);
            progress.setProgressStyle(android.R.style.Widget_ProgressBar_Small);
            FadingCircle doubleBounce = new FadingCircle();
            doubleBounce.setColor(ContextCompat.getColor(activity, R.color.colorPrimary));
            progress.setIndeterminateDrawable(doubleBounce);
            progress.setCancelable(isCancelable);
            progress.setCanceledOnTouchOutside(isCancelable);
            progress.show();
        }
    }

    public static void toasterror(String message) {
//        Toasty.info(App.getAppInstance(), message, Toast.LENGTH_SHORT, true).show();
        Toast.makeText(App.getAppInstance(), message, Toast.LENGTH_SHORT).show();

    }


    public static void toastinfo(String message) {
//        Toasty.info(App.getAppInstance(), message, Toast.LENGTH_SHORT, true).show();
        Toast.makeText(App.getAppInstance(), message, Toast.LENGTH_SHORT).show();

    }

    public static Toast toast(String toastMessage) {
        Toast toast = Toast.makeText(App.getAppInstance(), toastMessage, Toast.LENGTH_LONG);
        toast.show();
        return toast;
    }

    public static void stopProgress(Activity activity) {
        if (null != activity && null != progress && !activity.isFinishing() && !activity.isDestroyed()) {
            try {
                if (progress.isShowing())
                    progress.dismiss();

            } catch (Exception e) {

            }

        }
    }

    /*****************************************************************/

    public static void startAfterSplash(BaseAppCompatActivity activity) {
        if (Helper.isLoggedIn()) {
            startWelcome2Activity(activity);
        } else {
            Intent intent = new Intent(activity, WelcomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            activity.startActivity(intent);
            animateTransition(activity);
        }
    }

    public static void startWelcomeActivity(BaseAppCompatActivity mActivity) {
        Intent intent = new Intent(mActivity, WelcomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        mActivity.startActivity(intent);
        animateTransition(mActivity);
    }

    public static void startLoginActivity(BaseAppCompatActivity mActivity) {
        Intent intent = new Intent(mActivity, LoginActivity.class);
        mActivity.startActivity(intent);
        animateTransition(mActivity);
    }

    public static void startLoginCorporateActivity(BaseAppCompatActivity mActivity) {
        Intent intent = new Intent(mActivity, LoginCorporateActivity.class);
        mActivity.startActivity(intent);
        animateTransition(mActivity);
    }


    public static void startSignUpActivity(BaseAppCompatActivity mActivity) {
        Intent intent = new Intent(mActivity, SignUpActivity.class);
        mActivity.startActivity(intent);
        animateTransition(mActivity);
    }

    public static void startSignUpCorporateActivity(BaseAppCompatActivity mActivity) {
        Intent intent = new Intent(mActivity, SignUpCorporateActivity.class);
        mActivity.startActivity(intent);
        animateTransition(mActivity);
    }

//    public static void startSignUpActivity(BaseAppCompatActivity mActivity, boolean isInEditMode) {
//        Intent intent = new Intent(mActivity, SignUpActivity.class);
//        intent.putExtra(EXTRA_KEY_IS_IN_EDIT_MODE, isInEditMode);
//        mActivity.startActivity(intent);
//        animateTransition(mActivity);
//    }

//    public static void startSignUpCorporateActivity(BaseAppCompatActivity mActivity, boolean isInEditMode) {
//        Intent intent = new Intent(mActivity, SignUpCorporateActivity.class);
//        intent.putExtra(EXTRA_KEY_IS_IN_EDIT_MODE, isInEditMode);
//        mActivity.startActivity(intent);
//        animateTransition(mActivity);
//    }

    public static void startForgotPasswordActivity(BaseAppCompatActivity mActivity) {
        Intent intent = new Intent(mActivity, ForgotPasswordActivity.class);
        mActivity.startActivity(intent);
        animateTransition(mActivity);
    }

    public static void startTermCondtionActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, TermConditionActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startPrivacyPolicyActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, PrivacyPolicyActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startSocialActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, SocialActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startGalleryActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, GallaryActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startCheckInActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, CheckInActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startEmergencyResponseActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, EmergencyResponseActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startStudentManagerActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, StudentManagerActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startChangePasswordActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, ChangePasswordActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startInstructionActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, InstructionActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startUpdateProfileActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, UpdateProfileActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startUpdateProfileCorporateActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, UpdateProfileCorporateActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startEmergencyInstructionActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, EmergencyInstructionActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startContactUsActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, ContactUsActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startMessageCenterActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, MessageCenterActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startDonateActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, DonateActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startSocialCheckInActivity(BaseAppCompatActivity activity, Category category) {
        Intent intent = new Intent(activity, SocialCheckInActivity.class);
        intent.putExtra(AppConstants.EXTRA_KEY_CATEGORY, category);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startMainActivity(BaseAppCompatActivity mActivity, boolean toClearTop) {
        Intent intent = new Intent(mActivity, MainActivity.class);
        if (toClearTop) {
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        }
        mActivity.startActivity(intent);
        animateTransition(mActivity);
    }

    public static void startImageFullViewActivity(BaseAppCompatActivity activity, String url) {
        Intent intent = new Intent(activity, ImageFullViewActivity.class);
        intent.putExtra(AppConstants.EXTRA_KEY_URL, url);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startFullScreenVideoActivity(BaseAppCompatActivity activity, VideoData data) {
        Intent intent = new Intent(activity, FullScreenVideoActivity.class);
        intent.putExtra(AppConstants.EXTRA_KEY_DATA, data);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startUSSDActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, USSDActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startProductListActivity(BaseAppCompatActivity activity, String type) {
        Intent intent = new Intent(activity, ProductListActivity.class);
        intent.putExtra(AppConstants.EXTRA_KEY_TYPE, type);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startProductDetailsActivity(BaseAppCompatActivity activity, ProductData data) {
        Intent intent = new Intent(activity, ProductDetailsActivity.class);
        intent.putExtra(AppConstants.EXTRA_KEY_DATA, data);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void starCartActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, CartActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void starShoppingActivity(BaseAppCompatActivity activity) {
        Intent intent = new Intent(activity, ShoppingActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void starPaymentActivity(BaseAppCompatActivity activity, String taxparsantage, String totalamount) {
        Intent intent = new Intent(activity, PaymentActivity.class);
        intent.putExtra("tax", taxparsantage);
        intent.putExtra("amount", totalamount);
        activity.startActivity(intent);
        animateTransition(activity);
    }
    public static void startWelcome2Activity(BaseAppCompatActivity activity)
    {
        Intent intent = new Intent(activity, Welcome2Activity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static void startNewsActivity(BaseAppCompatActivity activity)
    {
        Intent intent = new Intent(activity, NewsActivity.class);
        activity.startActivity(intent);
        animateTransition(activity);
    }

    public static String getAddressFromLatLong(final Context context, double latitude, double longitude) {
        String full_address = "", city = "", country, state = "", postalCode = "", knownName;
        String address = "";
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder((Activity) context, Locale.getDefault());
        try {
            full_address = "";
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            if (addresses != null && addresses.size() > 0) {
                address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                city = addresses.get(0).getLocality();
                state = addresses.get(0).getAdminArea();
                country = addresses.get(0).getCountryName();
                if (null != addresses.get(0).getPostalCode())
                    postalCode = addresses.get(0).getPostalCode();
                knownName = addresses.get(0).getFeatureName();

                full_address = address + "," + city + "," + state;
//                Log.e("city::", city);
//                Log.e("knownName::", state);
                Log.e("address::", full_address);
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException illegalArgumentException) {
            illegalArgumentException.printStackTrace();
            full_address = "";
        }
        return full_address;
    }

    public static void makeViewScrollableInside(View editText, final ScrollView scrollView) {
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                scrollView.requestDisallowInterceptTouchEvent(false);
                return false;
            }
        });
    }


}