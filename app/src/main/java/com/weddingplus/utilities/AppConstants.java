package com.weddingplus.utilities;

/**
 * Created by Manndeep Vachhani on 7/2/17.
 */
public interface AppConstants {

    int videolimit=120100;

    String about_us = "\n" +
            "Adebayo Adelabu Adelabu was born to Aderibigbe Adelabu of Oke-Oluokun compound, Kudeti Area in Ibadan in September 28, 1970. His grandfather is Adegoke Adelabu of the ow widely acclaimed Penkelemesi political school. He attended Ibadan Municipal Government Primary School, Agodi Ibadan from 1976 to 1982 and Lagelu Grammar School, Ibadan from 1982 to 1987.\n" +
            "\n" +
            "Adelabu received a First Class Degree in Accounting from Obafemi Awolowo University, Ile-Ife. He is a Fellow of the Institute of Chartered Accountants of Nigeria (ICAN), a Fellow of Chartered Institute of Bankers of Nigeria and an Associate Member of the Institute of Directors of Nigeria and the United Kingdom. Adelabu has also taken up professional courses in various business schools, including Harvard, Stanford, Wharton, Columbia, Kelloggs, Euromoney, and the University of London.";

    String wedding_info = "\n" +
            "The Families of ABUBAKAR and NWANKWO\n" +
            "\n" +
            "cordially invite you to the \n" +
            "Holy Solemnization \n" +
            "between their Daughter \n" +
            "\n" +
            "AMAKA TEMILOLUWA \n" +
            "\n" +
            "and their Son\n" +
            "\n" +
            "BABAJIDE UMAR UCHE\n" +
            "\n" +
            "which holds:\n" +
            "\n" +
            "Date: June 12, 2019\n" +
            "Venue: RELIGIOUS CENTER FOR ALL\n" +
            "Time: 10am prompt \n" +
            "\n" +
            "Reception holds immediately after at the Transcorp Hilton Hotel, Maiduguri, Enugu State. \n" +
            "\n" +
            "\"The melodies of our heart will be so loud that the Angels guarding Earth will dance to our rhythm\". ";

    String privacy_policy = "<p>EPPME DIGITAL TECHNOLOGIES LIMITED (Owners of the Bayo Adelabu App)</p>\n" +
            "\n" +
            "<p>Privacy Policy</p>\n" +
            "\n" +
            "<p>This privacy policy has been compiled to better serve those who are concerned with how their 'Personally Identifiable Information' (PII) is being used online. PII, as described in elaw and information security, is information that can be used on its own or with other information to identify, contact, or locate a single person, or to identify an guest in context. Please read our privacy policy carefully to get a clear understanding of how we collect, use, protect or otherwise handle your Personally Identifiable Information in accordance with our website.</p>\n" +
            "\n" +
            "<p>What personal information do we collect from the people that visit our blog, website or app?</p>\n" +
            "\n" +
            "<p>When ordering or registering on our site, as appropriate, you may be asked to enter your name, email address, mailing address, phone number, or other details to help you with your experience.</p>\n" +
            "\n" +
            "<p>When do we collect information?</p>\n" +
            "\n" +
            "<p>We collect information from you when you register on our site, place an order, subscribe to a newsletter, respond to a survey, fill out a form, Use Live Chat, Open a Support Ticket or enter information on our site.</p>\n" +
            "\n" +
            "<p>Provide us with feedback on our products or services </p>\n" +
            "\n" +
            "<p>How do we use your information?</p>\n" +
            "\n" +
            "<p>We may use the information we collect from you when you register, make a purchase, sign up for our newsletter, respond to a survey or marketing communication, surf the website, or use certain other site features in the following ways:</p>\n" +
            "\n" +
            "<p> &bull; To personalize your experience and to allow us to deliver the type of content and product offerings in which you are most interested.</p>\n" +
            "\n" +
            "<p> &bull; To improve our website in order to better serve you.</p>\n" +
            "\n" +
            "<p> &bull; To allow us to better service you in responding to your customer service requests.</p>\n" +
            "\n" +
            "<p> &bull; To administer a contest, promotion, survey or other site feature.</p>\n" +
            "\n" +
            "<p> &bull; To quickly process your transactions.</p>\n" +
            "\n" +
            "<p> &bull; To ask for ratings and reviews of services or products</p>\n" +
            "\n" +
            "<p> &bull; To follow up with them after correspondence (live chat, email or phone inquiries)</p>\n" +
            "\n" +
            "<p>How do we protect your information?</p>\n" +
            "\n" +
            "<p>We do not use vulnerability scanning and/or scanning to PCI standards.</p>\n" +
            "\n" +
            "<p>We use regular Malware Scanning.</p>\n" +
            "\n" +
            "<p>Your personal information is contained behind secured networks and is only accessible by a limited number of persons who have special access rights to such systems, and are required to keep the information confidential. In addition, all sensitive/credit information you supply is encrypted via Secure Socket Layer (SSL) technology.</p>\n" +
            "\n" +
            "<p>We implement a variety of security measures when a user places an order enters, submits, or accesses their information to maintain the safety of your personal information.</p>\n" +
            "\n" +
            "<p>All transactions are processed through a gateway provider and are not stored or processed on our servers.</p>\n" +
            "\n" +
            "<p>Do we use 'cookies'?</p>\n" +
            "\n" +
            "<p>Yes. Cookies are small files that a site or its service provider transfers to your computer's hard drive through your Web browser (if you allow) that enables the site's or service provider's systems to recognize your browser and capture and remember certain information. For instance, we use cookies to help us remember and process the items in your shopping cart. They are also used to help us understand your preferences based on previous or current site activity, which enables us to provide you with improved services. We also use cookies to help us compile aggregate data about site traffic and site interaction so that we can offer better site experiences and tools in the future.</p>\n" +
            "\n" +
            "<p>We use cookies to:</p>\n" +
            "\n" +
            "<p> &bull; Help remember and process the items in the shopping cart.</p>\n" +
            "\n" +
            "<p> &bull; Understand and save user's preferences for future visits.</p>\n" +
            "\n" +
            "<p> &bull; Keep track of advertisements.</p>\n" +
            "\n" +
            "<p> &bull; Compile aggregate data about site traffic and site interactions in order to offer better site experiences and tools in the future. We may also use trusted third-party services that track this information on our behalf.</p>\n" +
            "\n" +
            "<p>You can choose to have your computer warn you each time a cookie is being sent, or you can choose to turn off all cookies. You do this through your browser settings. Since browser is a little different, look at your browser's Help Menu to learn the correct way to modify your cookies.</p>\n" +
            "\n" +
            "<p>If users disable cookies in their browser:</p>\n" +
            "\n" +
            "<p>If you turn cookies off, Some of the features that make your site experience more efficient may not function properly.Some of the features that make your site experience more efficient and may not function properly.</p>\n" +
            "\n" +
            "<p>Third-party disclosure</p>\n" +
            "\n" +
            "<p>We do not sell, trade, or otherwise transfer to outside parties your Personally Identifiable Information unless we provide users with advance notice. This does not include website hosting partners and other parties who assist us in operating our website, conducting our business, or serving our users, so long as those parties agree to keep this information confidential. We may also release information when it's release is appropriate to comply with the law, enforce our site policies, or protect ours or others' rights, property or safety. </p>\n" +
            "\n" +
            "<p>However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, or other uses.</p>\n" +
            "\n" +
            "<p>Third-party links</p>\n" +
            "\n" +
            "<p>Occasionally, at our discretion, we may include or offer third-party products or services on our website. These third-party sites have separate and independent privacypolicies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the integrity of our site and welcome any feedback about these sites.</p>\n" +
            "\n" +
            "<p>Google</p>\n" +
            "\n" +
            "<p>Google's advertising requirements can be summed up by Google's Advertising Principles. They are put in place to provide a positive experience for users. https://support.google.com/adwordspolicy/answer/1316548?hl=en </p>\n" +
            "\n" +
            "<p>We use Google AdSense Advertising on our website.</p>\n" +
            "\n" +
            "<p>\n" +
            "Google, as a third-party vendor, uses cookies to serve ads on our site. Google's use of the DART cookie enables it to serve ads to our users based on previous visits to our site and other sites on the Internet. Users may opt-out of the use of the DART cookie by visiting the Google Ad and Content Network privacy policy.</p>\n" +
            "\n" +
            "<p>\n" +
            "We have implemented the following:</p>\n" +
            "\n" +
            "<p> &bull; Remarketing with Google AdSense</p>\n" +
            "\n" +
            "<p> &bull; Google Display Network Impression Reporting</p>\n" +
            "\n" +
            "<p> &bull; Demographics and Interests Reporting</p>\n" +
            "\n" +
            "<p> &bull; DoubleClick Platform Integration</p>\n" +
            "\n" +
            "<p>We, along with third-party vendors such as Google use first-party cookies (such as the Google Analytics cookies) and third-party cookies (such as the DoubleClick cookie) or other third-party identifiers together to compile data regarding user interactions with ad impressions and other ad service functions as they relate to our website.</p>\n" +
            "\n" +
            "<p>Opting out:<br />\n" +
            "Users can set preferences for how Google advertises to you using the Google Ad Settings page. Alternatively, you can opt out by visiting the Network Advertising Initiative Opt Out page or by using the Google Analytics Opt Out Browser add on.</p>\n" +
            "\n" +
            "<p>\n" +
            "CAN SPAM Act</p>\n" +
            "\n" +
            "<p>The CAN-SPAM Act is a law that sets the rules for commercial email, establishes requirements for commercial messages, gives recipients the right to have emails stopped from being sent to them, and spells out tough penalties for violations.</p>\n" +
            "\n" +
            "<p>We collect your email address in order to:</p>\n" +
            "\n" +
            "<p> &bull; Send information, respond to inquiries, and/or other requests or questions</p>\n" +
            "\n" +
            "<p> &bull; Process orders and to send information and updates pertaining to orders.</p>\n" +
            "\n" +
            "<p> &bull; Send you additional information related to your product and/or service</p>\n" +
            "\n" +
            "<p>\n" +
            "To be in accordance with CANSPAM, we agree to the following:</p>\n" +
            "\n" +
            "<p>If at any time you would like to unsubscribe from receiving future emails, you can email us at info@eppmeapp.com and we will promptly remove you from ALL correspondence.</p>\n" +
            "\n" +
            "<p>Contacting Us</p>\n" +
            "\n" +
            "<p>If there are any questions regarding this privacy policy, you may contact us using the information below.</p>\n" +
            "\n" +
            "<p>http://www.eppmeapp.com</p>\n" +
            "\n" +
            "<p>Lagos</p>\n" +
            "\n" +
            "<p>Nigeria</p>\n" +
            "\n" +
            "<p>info@eppmeapp.com</p>";


    String Instruction_one = "To use the Bayo Adelabu App, you need the following:\n" +
            "\n" +
            "1) A data enabled device\n" +
            "\n" +
            "2) Active GPS service \n" +
            "\n" +
            "3) Updated Bayo Adelabu app\n";


    String Instruction_second =
            "Kindly note that you are responsible for every report sent and that every established case of abuse will lead to a cancellation of your access to the services for a period of up to one year \n" +
                    "\n" +
                    "We will also press legal charges against deliberate misinformation. Kindly ensure that you are the only one with access to your account and where you suspect a breach, please mail us immediately: info@eppmeapp.com \n";

    String Instuction_thired = "How to use the Dynamic Reporting System (Bayo Adelabu Hub)\n" +
            "\n" +
            "1) Launch mobile app.\n" +
            "\n" +
            "2) Select Bayo Adelabu Hub\n" +
            "\n" +
            "3) Select Report Category of intended report (Academics, Security, etc).\n" +
            "\n" +
            "4) Select a sub category. Where the sub category is not specified, select \"Other\".\n" +
            "\n" +
            "5) Include additional information by entering text into the open box on display with Additional Info shadow text\n" +
            "\n" +
            "6) Attach an audio recording, photo or video\n" +
            "\n" +
            "7) Select SEND.\n" +
            "\n" +
            "This action triggers automatic notices to relevant authorities."+"\n";


    String Instruction_fourth =
            "For use during Emergencies \n" +
            "\n" +
            "1) Ensure use of the app only under safe conditions. Your personal safety is important. \n" +
            "\n" +
            "2) After you have ensured personal safety, you may then select \"Emergency Response\" and choose from the options presented. \n" +
            "\n" +
            "3) If your emergency is not listed, select \"Other Emergency\". You may then provide a description of the emergency to enable back end processing. \n" +
            "\n" +
            "4) Fill all sections where convenient as accurately as possible knowing that the information that you provide will be used for preliminary reports to authorities. \n" +
            "\n" +
            "5) You may attach a picture, a voice recording, or short video clip to enhance your report by selecting Attach Photo/Video/Voice option and be sure to click SEND after that. \n" +
            "\n" +
            "6) You may select the \"Other\" location option to report incidents happening in an area outside 1 KM of where you are. \n" +
            "\n" +
            "7) Ensure to SUBMIT your report. There is no back end process for unsubmitted reports.\n" +
            "\n" +
            "Your account information will be updated under \"Hub Tracker\" to show all successfully sent reports.";


    String new_terms = "<p>EPPME DIGITAL DIGITAL TECHNOLOGIES LIMITED (Owners of the Bayo Adelabu Mobile App)&nbsp;<br />\n" +
            "July 2017&nbsp;<br />\n" +
            "Terms of&nbsp;Service<br />\n" +
            "Welcome to&nbsp;Bayo Adelabu, Inc (&ldquo;Bayo Adelabu&rdquo;). We&rsquo;ve put together here some detailed terms and&nbsp;conditions. You should read and&nbsp;understand these Terms of&nbsp;Service, which include our Privacy Policy, and&nbsp;may also include our Channel Partner Agreement and&nbsp;Master Service Agreement, as&nbsp;applicable, because they govern your use of&nbsp;any&nbsp;of&nbsp;the&nbsp;Bayo Adelabu products and &nbsp;services (in whole or&nbsp;in&nbsp;part, the&nbsp;&ldquo;Products and&nbsp;Services&rdquo;), including all features and&nbsp;functionalities, our user interfaces such as&nbsp;our mobile app, website, and&nbsp;all content and&nbsp;software associated therewith.<br />\n" +
            "These Terms of&nbsp;Service provide that all disputes between you and&nbsp;Bayo Adelabu will be&nbsp;resolved by&nbsp;BINDING ARBITRATION. YOU AGREE TO&nbsp;GIVE UP&nbsp;YOUR RIGHT TO&nbsp;GO&nbsp;TO&nbsp;COURT to&nbsp;assert or&nbsp;defend your rights under this contract (except for&nbsp;matters that may be&nbsp;taken to&nbsp;small claims court). Your rights will be&nbsp;determined by&nbsp;a&nbsp;NEUTRAL ARBITRATOR and&nbsp;NOT a&nbsp;judge or&nbsp;jury and&nbsp;your claims cannot be&nbsp;brought as&nbsp;a&nbsp;class action. Please review the&nbsp;Arbitration Agreement&nbsp;below for&nbsp;the&nbsp;details regarding your agreement to&nbsp;arbitrate any&nbsp;disputes with Bayo Adelabu.<br />\n" +
            "These Terms of&nbsp;Service are also a&nbsp;resource for&nbsp;you to&nbsp;have a&nbsp;deeper understanding of&nbsp;how our Products and&nbsp;Services work, including the&nbsp;kinds of&nbsp;data we&nbsp;collect, the&nbsp;way we&nbsp;bill, how we&nbsp;interact with you and&nbsp;other useful details. We&nbsp;encourage you to&nbsp;revisit these Terms of&nbsp;Service when you have a&nbsp;question about the&nbsp;Products and&nbsp;Services or&nbsp;want to&nbsp;know how something works. We&nbsp;hope you enjoy your Bayo Adelabu experience.<br />\n" +
            "Acceptance of&nbsp;Terms of&nbsp;Service<br />\n" +
            "By&nbsp;using, visiting, or&nbsp;browsing the&nbsp;Products and&nbsp;Services as&nbsp;a&nbsp;user or&nbsp;paid or&nbsp;unpaid member, you accept and&nbsp;agree to&nbsp;be&nbsp;bound by&nbsp;these Terms of&nbsp;Service, available at&nbsp;www.Bayo Adelabu.net/terms-of-service. If&nbsp;you do&nbsp;not agree to&nbsp;these Terms of&nbsp;Service, you should not use the&nbsp;Products and&nbsp;Services, including our mobile app, website and&nbsp;software. Violation of&nbsp;any&nbsp;terms will result in&nbsp;termination of&nbsp;your account. Questions about the&nbsp;Terms of&nbsp;Service may be&nbsp;sent to&nbsp;info@weddingplus.net.<br />\n" +
            "These Terms of&nbsp;Service, together with the&nbsp;Privacy Policy, Channel Partner Agreement, and&nbsp;Master Service Agreement, constitute the&nbsp;entire agreement between you and&nbsp;Bayo Adelabu and&nbsp;supersede any&nbsp;and&nbsp;all previous agreements, written or&nbsp;oral, between you and&nbsp;Bayo Adelabu, including previous versions of&nbsp;the&nbsp;Terms of&nbsp;Service.<br />\n" +
            "Any failure of&nbsp;Bayo Adelabu to&nbsp;enforce or&nbsp;exercise a&nbsp;right provided in&nbsp;these terms is&nbsp;not a&nbsp;waiver of&nbsp;that right.<br />\n" +
            "Changes to&nbsp;Terms of&nbsp;Service<br />\n" +
            "Bayo Adelabu reserves the&nbsp;right, from time to&nbsp;time, with or&nbsp;without notice to&nbsp;you, to&nbsp;change these Terms of&nbsp;Service, including the&nbsp;Privacy Policy, Channel Partner Agreement, and&nbsp;Master Service Agreement, in&nbsp;our sole and&nbsp;absolute discretion. Bayo Adelabu may also freely assign or&nbsp;transfer this Agreement. The most current version of&nbsp;these Terms of&nbsp;Service can be&nbsp;reviewed by&nbsp;visiting our website and&nbsp;clicking on&nbsp;&ldquo;Terms of&nbsp;Service&rdquo; located at&nbsp;the&nbsp;bottom of&nbsp;the&nbsp;pages of&nbsp;the&nbsp;Bayo Adelabu website. The most current version of&nbsp;the&nbsp;Terms of&nbsp;Service will supersede all previous versions. We&nbsp;will endeavor to&nbsp;post prior version(s) on&nbsp;our website when the&nbsp;Terms of&nbsp;Service are updated.<br />\n" +
            "If&nbsp;any&nbsp;provision of&nbsp;this Agreement is&nbsp;declared void or&nbsp;unenforceable by&nbsp;any&nbsp;judicial authority, this shall not nullify the&nbsp;remaining provisions of&nbsp;the&nbsp;Agreement, which shall remain in&nbsp;full force and&nbsp;effect.<br />\n" +
            "Account Terms Overview<br />\n" +
            "You are responsible for&nbsp;using the&nbsp;Products and&nbsp;Services in&nbsp;a&nbsp;private and&nbsp;secure manner. Bayo Adelabu is&nbsp;not liable for&nbsp;any&nbsp;damage or&nbsp;loss due to&nbsp;unauthorized account access resulting from your actions, such as&nbsp;not logging out of&nbsp;the&nbsp;Products and&nbsp;Services or&nbsp;sharing your account password.<br />\n" +
            "The Bayo Adelabu Emergency Response platform is a best effort service and rests squarely on the efficiency of support services all of which are outside our control.<br />\n" +
            "Use of the Bayo Adelabu app is no replacement for diligent report of incidents to statutory government agencies. Bayo Adelabu does not bear any liabilities whatsoever for failure of users to make reports to authorities.<br />\n" +
            "Bayo Adelabu can refuse registration or&nbsp;cancel an&nbsp;account at&nbsp;its sole discretion at&nbsp;any&nbsp;time.<br />\n" +
            "You may not use the&nbsp;Products and&nbsp;Services for&nbsp;any&nbsp;illegal activity or&nbsp;to&nbsp;violate laws in&nbsp;your jurisdiction.<br />\n" +
            "You may not use the&nbsp;Products and&nbsp;Services to&nbsp;distribute unsolicited email (&ldquo;spam&rdquo;) or&nbsp;malicious content such as&nbsp;viruses or&nbsp;worms.<br />\n" +
            "You may not exploit the&nbsp;Products and&nbsp;Services to&nbsp;access unauthorized information.<br />\n" +
            "Bayo Adelabu reserves the&nbsp;right to&nbsp;modify, suspend, or&nbsp;discontinue the&nbsp;Products and&nbsp;Services for&nbsp;any&nbsp;reason, with or&nbsp;without notice.<br />\n" +
            "Abuse or&nbsp;excessively frequent requests to&nbsp;the&nbsp;Bayo Adelabu website or&nbsp;other Products and&nbsp;Services may result in&nbsp;the&nbsp;temporary or&nbsp;permanent suspension of&nbsp;your account&rsquo;s access to&nbsp;the&nbsp;Products and&nbsp;Services. Bayo Adelabu, at&nbsp;its sole discretion, will determine abuse or&nbsp;excessive usage. Bayo Adelabu will make a&nbsp;reasonable attempt via email to&nbsp;warn the&nbsp;account owner prior to&nbsp;suspension.<br />\n" +
            "Bayo Adelabu owns intellectual property rights to&nbsp;any&nbsp;protectable part of&nbsp;the&nbsp;Products and&nbsp;Services, including but not limited to&nbsp;the&nbsp;design, artwork, functionality, and&nbsp;documentation. You may not copy, modify, or&nbsp;reverse engineer any&nbsp;part of&nbsp;the&nbsp;Products and&nbsp;Services owned by&nbsp;Bayo Adelabu. For more details, see below the&nbsp;Intellectual Property&nbsp;section of&nbsp;these Terms of&nbsp;Service.<br />\n" +
            "You are responsible for&nbsp;the&nbsp;security of&nbsp;cardholder data you possess or&nbsp;otherwise store, processes, or&nbsp;transmits on&nbsp;behalf of&nbsp;your customer, or&nbsp;to&nbsp;the&nbsp;extent that you could impact the&nbsp;security of&nbsp;your customer&rsquo;s cardholder data.<br />\n" +
            "User Conduct and&nbsp;Submissions<br />\n" +
            "Any text, graphics, photographs, or&nbsp;other information communicated to&nbsp;Bayo Adelabu (collectively, &ldquo;Content&rdquo;) belongs to&nbsp;the&nbsp;person who posted such content. You may use any&nbsp;Content posted by&nbsp;you in&nbsp;any&nbsp;other way without restriction. You may only use Content posted by&nbsp;others in&nbsp;the&nbsp;ways described in&nbsp;these Terms of&nbsp;Service.<br />\n" +
            "You give Bayo Adelabu a&nbsp;non-exclusive,&nbsp;free, worldwide license for&nbsp;the&nbsp;duration of&nbsp;the&nbsp;applicable author&rsquo;s rights, to&nbsp;publish your Content. In&nbsp;addition to&nbsp;the&nbsp;right to&nbsp;publish, you also grant the&nbsp;following rights, without limitation: (i) the&nbsp;right to&nbsp;reproduce; (ii) the&nbsp;right to&nbsp;transfer, which includes the&nbsp;distribution via computer and&nbsp;networks; (iii) the&nbsp;right to&nbsp;edit, modify, adapt, arrange, improve, correct, translate, in&nbsp;all or&nbsp;in&nbsp;part; (iv) the&nbsp;right to&nbsp;update/upgrade by&nbsp;adding or&nbsp;removing; and&nbsp;(v) the&nbsp;right to&nbsp;film, perform or&nbsp;post the&nbsp;Content in&nbsp;any&nbsp;media. Except as&nbsp;described in&nbsp;our Privacy Policy, Bayo Adelabu will not be&nbsp;required to&nbsp;treat any&nbsp;Content as&nbsp;confidential.<br />\n" +
            "Bayo Adelabu cannot control all Content posted by&nbsp;third parties to&nbsp;the&nbsp;Products and&nbsp;Services, including to&nbsp;our user interfaces, and&nbsp;does not guarantee the&nbsp;accuracy, integrity or&nbsp;quality of&nbsp;such Content. You understand that by&nbsp;using the&nbsp;Products and&nbsp;Services you may be&nbsp;exposed to&nbsp;Content that you may find offensive, indecent, incorrect or&nbsp;objectionable, and&nbsp;you agree that under no&nbsp;circumstances will Bayo Adelabu be&nbsp;liable in&nbsp;any&nbsp;way for&nbsp;any&nbsp;Content, including any&nbsp;errors or&nbsp;omissions in&nbsp;any&nbsp;Content, or&nbsp;any&nbsp;loss or&nbsp;damage of&nbsp;any&nbsp;kind incurred as&nbsp;a&nbsp;result of&nbsp;your use of&nbsp;any&nbsp;Content. You understand that you must evaluate and&nbsp;bear all risks associated with the&nbsp;use of&nbsp;any&nbsp;Content, including any&nbsp;reliance on&nbsp;the&nbsp;content, integrity, and&nbsp;accuracy of&nbsp;such Content. If&nbsp;you would like to&nbsp;report objectionable materials, please contact us at&nbsp;info@eppmeapp.com<br />\n" +
            "By&nbsp;accessing the&nbsp;Products and&nbsp;Services, including our website and&nbsp;other user interfaces, you agree to&nbsp;use the&nbsp;Products and&nbsp;Services, including all features and&nbsp;functionalities associated therewith, our website, other user interfaces and&nbsp;all content and&nbsp;software associated therewith in&nbsp;accordance with all applicable laws, rules and&nbsp;regulations, or&nbsp;other restrictions on&nbsp;use of&nbsp;the&nbsp;Products and&nbsp;Services or&nbsp;Content therein. In&nbsp;addition, you agree not to&nbsp;upload, post,&nbsp;e-mail&nbsp;or&nbsp;otherwise send or&nbsp;transmit any&nbsp;material that contains software viruses or&nbsp;any&nbsp;other computer code, files or&nbsp;programs designed to&nbsp;interrupt, destroy or&nbsp;limit the&nbsp;functionality of&nbsp;any&nbsp;computer software or&nbsp;hardware or&nbsp;telecommunications equipment associated with the&nbsp;Products and&nbsp;Services. You also agree not to&nbsp;interfere with the&nbsp;servers or&nbsp;networks connected to&nbsp;any&nbsp;portions of&nbsp;the&nbsp;Products and&nbsp;Services or&nbsp;to&nbsp;violate any&nbsp;of&nbsp;the&nbsp;procedures, policies or&nbsp;regulations of&nbsp;networks connected to&nbsp;the&nbsp;Products and&nbsp;Services. You also agree not to&nbsp;impersonate any&nbsp;other person while using the&nbsp;Products and&nbsp;Services, conduct yourself in&nbsp;a&nbsp;vulgar or&nbsp;offensive manner while using the&nbsp;Products and&nbsp;Services, or&nbsp;use the&nbsp;Products and&nbsp;Services for&nbsp;any&nbsp;unlawful purpose.<br />\n" +
            "Use of&nbsp;the&nbsp;Products and&nbsp;Services and&nbsp;any&nbsp;personally identifying information submitted through the&nbsp;Products and&nbsp;Services, such as&nbsp;through our user interfaces containing community forums, is&nbsp;subject to&nbsp;our Privacy Policy, the&nbsp;terms of&nbsp;which are incorporated herein, and&nbsp;a&nbsp;copy of&nbsp;which is&nbsp;currently located at&nbsp;www.Bayo Adelabu.net/privacy-policy. Please review our Privacy Policy to&nbsp;understand our practices. The date of&nbsp;any&nbsp;changes to&nbsp;our Privacy Policy will be&nbsp;noted at&nbsp;the&nbsp;bottom of&nbsp;the&nbsp;policy.<br />\n" +
            "By&nbsp;using the&nbsp;Products and&nbsp;Services, you are consenting to&nbsp;receive certain communications from us. For example, Bayo Adelabu may send you newsletters about new Bayo Adelabu features, special offers, promotional announcements and&nbsp;customer surveys via email or&nbsp;other methods.<br />\n" +
            "Please review our Privacy Policy for&nbsp;further details on&nbsp;our marketing communications. You can also find the&nbsp;unsubscribe instructions there. By&nbsp;using the&nbsp;Products and&nbsp;Services, you consent to&nbsp;receiving electronic communications from Bayo Adelabu. These communications may include notices about your account (for example, change in&nbsp;password or&nbsp;Payment Method, confirmation&nbsp;e-mails&nbsp;and&nbsp;other transactional information) and&nbsp;information concerning or&nbsp;related to&nbsp;our service. These communications are part of&nbsp;your relationship with Bayo Adelabu and&nbsp;you receive them as&nbsp;part of&nbsp;the&nbsp;Bayo Adelabu membership. You agree that any&nbsp;notice, agreements, disclosure or&nbsp;other communications that we&nbsp;send to&nbsp;you electronically will satisfy any&nbsp;legal communication requirements, including that such communications be&nbsp;in&nbsp;writing.<br />\n" +
            "The member who created the&nbsp;Bayo Adelabu account and&nbsp;whose Payment Method is&nbsp;charged is&nbsp;referred to&nbsp;here as&nbsp;the&nbsp;&ldquo;Account Owner&rdquo;. The Account Owner has access and&nbsp;control over the&nbsp;Bayo Adelabu account. The Account Owner&rsquo;s control is&nbsp;exercised through use of&nbsp;the&nbsp;Account Owner&rsquo;s password and&nbsp;therefore to&nbsp;maintain exclusive control, the&nbsp;Account Owner should not reveal the&nbsp;password to&nbsp;anyone. In&nbsp;addition, if&nbsp;the&nbsp;Account Owner wishes to&nbsp;prohibit others from contacting Bayo Adelabu Customer Service and&nbsp;potentially altering the&nbsp;Account Owner&rsquo;s control, the&nbsp;Account Owner should not reveal the&nbsp;Payment Method details (for example, the&nbsp;last four digits of&nbsp;their credit or&nbsp;debit card, or&nbsp;their email address if&nbsp;they use PayPal) associated with their account. BY&nbsp;SHARING THE Bayo Adelabu DIGITAL TECHNOLOGIES ACCOUNT PASSWORD, THE ACCOUNT OWNER AGREES TO&nbsp;BE&nbsp;RESPONSIBLE FOR ASSURING THAT THE PERSONS WITH WHOM IT&nbsp;SHARES THE PASSWORD COMPLY WITH THE TERMS OF&nbsp;SERVICE AND SUCH ACCOUNT OWNER SHALL BE&nbsp;RESPONSIBLE FOR THE ACTIONS OF&nbsp;SUCH PERSONS.<br />\n" +
            "Any abuse or&nbsp;threatened abuse of&nbsp;other users of&nbsp;the&nbsp;Products and&nbsp;Services or&nbsp;of&nbsp;Bayo Adelabu personnel will result in&nbsp;immediate account termination.<br />\n" +
            "Payment and&nbsp;Fees<br />\n" +
            "To&nbsp;access any&nbsp;of&nbsp;our Products and&nbsp;Services, you must have access to&nbsp;the&nbsp;Internet and&nbsp;must create an&nbsp;account, either on&nbsp;a&nbsp;free or&nbsp;paid-subscription basis.<br />\n" +
            "A&nbsp;valid credit/payment card or&nbsp;Paypal account is&nbsp;required for&nbsp;paid subscription Products and&nbsp;Services.<br />\n" +
            "When you subscribe to&nbsp;paid Products and&nbsp;Services, your credit card or&nbsp;Paypal account will be&nbsp;billed according to&nbsp;current Bayo Adelabu rates for&nbsp;the&nbsp;selected subscription plan to&nbsp;prepay usage of&nbsp;Product and&nbsp;Services for&nbsp;the&nbsp;current period. For monthly plans you will be&nbsp;charged subsequently on&nbsp;the&nbsp;same day of&nbsp;each month. If&nbsp;the&nbsp;billing date is&nbsp;the&nbsp;29th&nbsp;&mdash; 31st, and&nbsp;the&nbsp;billing month does not contain this date, then the&nbsp;billing date will be&nbsp;adjusted to&nbsp;the&nbsp;last day of&nbsp;the&nbsp;current calendar month. Annual plan renewals will be&nbsp;charged on&nbsp;same day of&nbsp;the&nbsp;following year to&nbsp;prepay usage of&nbsp;Product and&nbsp;Services for&nbsp;the&nbsp;next annual service period.<br />\n" +
            "If&nbsp;you choose to&nbsp;downgrade your subscription by&nbsp;switching to&nbsp;a&nbsp;free subscription plan, Bayo Adelabu will change your account immediately. No&nbsp;credit will be&nbsp;applied for&nbsp;the&nbsp;unused portion of&nbsp;service. The payment for&nbsp;the&nbsp;unused portion of&nbsp;service from your Old Plan will be&nbsp;credited toward the&nbsp;price of&nbsp;the&nbsp;New Plan. Your future monthly billing date will always remain the&nbsp;same. If&nbsp;the&nbsp;Plan change is&nbsp;a&nbsp;downgrade, credit will be&nbsp;applied to&nbsp;future months and&nbsp;your next monthly fee will be&nbsp;charged when the&nbsp;credit from the&nbsp;Old Plan has been used. If&nbsp;the&nbsp;Plan change is&nbsp;an&nbsp;upgrade, you will be&nbsp;charged the&nbsp;remaining portion for&nbsp;the&nbsp;current billing period immediately, and&nbsp;subsequent months will be&nbsp;charged on&nbsp;your previously existing billing date. Remaining credit from the&nbsp;current billing period will be&nbsp;calculated based on&nbsp;the&nbsp;number of&nbsp;remaining days in&nbsp;the&nbsp;current billing month. Questions about Bayo Adelabu billing are directed to Billing@weddingplus.net&nbsp;and&nbsp;answered within 24&nbsp;hours, Monday&nbsp;&mdash; Friday.<br />\n" +
            "Bayo Adelabu does not provide refunds for&nbsp;Products and&nbsp;Services.</p>\n" +
            "\n" +
            "<p>Cancellation and&nbsp;Termination<br />\n" +
            "You can cancel your paid subscription to&nbsp;Bayo Adelabu Products and&nbsp;Services at&nbsp;any&nbsp;time by&nbsp;downgrading to&nbsp;a&nbsp;free plan. There will be&nbsp;no&nbsp;subsequent charges to&nbsp;your credit card or&nbsp;Paypal account after cancellation.<br />\n" +
            "You may terminate your account by&nbsp;selecting &ldquo;Close my&nbsp;account&rdquo; option in&nbsp;your account.&nbsp;Your need to&nbsp;cancel your paid subscription before you can close your account.<br />\n" +
            "Features and&nbsp;Functionality<br />\n" +
            "The Products and&nbsp;Services include an&nbsp;online shopping cart that can be&nbsp;integrated with your existing website, blog, social network as&nbsp;applicable. We&nbsp;reserve the&nbsp;right in&nbsp;our sole and&nbsp;absolute discretion to&nbsp;make changes from time to&nbsp;time and&nbsp;without notice in&nbsp;how we&nbsp;operate the&nbsp;Products and&nbsp;Services. Any description of&nbsp;how the&nbsp;Products and&nbsp;Services work should not be&nbsp;considered a&nbsp;representation or&nbsp;obligation with respect to&nbsp;how the&nbsp;Products and&nbsp;Services always will work. We&nbsp;are making constant adjustments to&nbsp;the&nbsp;Products and&nbsp;Services and&nbsp;often these Terms of&nbsp;Service do&nbsp;not capture these adjustments completely.<br />\n" +
            "Social Media<br />\n" +
            "For members in&nbsp;certain countries, some of&nbsp;the&nbsp;Products and&nbsp;Services, such as&nbsp;our online shopping cart, can be&nbsp;added to&nbsp;your page on&nbsp;social media networks, such as&nbsp;Facebook.<br />\n" +
            "BY&nbsp;CONNECTING YOUR Bayo Adelabu &nbsp;ACCOUNT TO&nbsp;YOUR FACEBOOK Account where applicable, YOU ACKNOWLEDGE AND AGREE THAT YOU ARE CONSENTING TO&nbsp;THE CONTINUOUS RELEASE OF&nbsp;INFORMATION ABOUT YOU TO&nbsp;OTHERS, INCLUDING TO&nbsp;FACEBOOK (IN ACCORDANCE WITH YOUR PRIVACY SETTINGS ON&nbsp;FACEBOOK AND YOUR ACCOUNT SETTINGS ON&nbsp;Bayo Adelabu.NET). IF&nbsp;YOU DO&nbsp;NOT WANT INFORMATION ABOUT YOU TO&nbsp;BE&nbsp;SHARED IN&nbsp;THIS MANNER, DO&nbsp;NOT USE THE FACEBOOK CONNECT FEATURE. We&nbsp;and&nbsp;Facebook are continually making changes and&nbsp;improvements to&nbsp;this feature, and&nbsp;therefore the&nbsp;available features and&nbsp;information that is&nbsp;shared may change from time to&nbsp;time. These changes may take place without notice to&nbsp;you and&nbsp;may not be&nbsp;described in&nbsp;these Terms of&nbsp;Service.<br />\n" +
            "Identity Protection<br />\n" +
            "You are responsible for&nbsp;updating and&nbsp;maintaining the&nbsp;truth and&nbsp;accuracy of&nbsp;the&nbsp;information you provide to&nbsp;us&nbsp;relating to&nbsp;your account. You are also responsible for&nbsp;maintaining the&nbsp;confidentiality of&nbsp;your account and&nbsp;password and&nbsp;for&nbsp;restricting access to&nbsp;your Bayo Adelabu account. If&nbsp;you disclose your password to&nbsp;anyone or&nbsp;share your account with other people, you take full responsibility for&nbsp;their actions. Where possible, users of&nbsp;public or&nbsp;shared networks should log out at&nbsp;the&nbsp;completion of&nbsp;each visit. If&nbsp;you find that you are a&nbsp;victim of&nbsp;identity theft and&nbsp;it&nbsp;involves an&nbsp;Bayo Adelabu account, you should notify us&nbsp;at&nbsp;info@eppmeapp.net. Then, you should report this instance to&nbsp;all your card issuers, as&nbsp;well as&nbsp;your local law enforcement agency. Also, you should be&nbsp;mindful of&nbsp;any&nbsp;communication requesting that you submit credit card or&nbsp;other account information. Providing your information in&nbsp;response to&nbsp;these types of&nbsp;communications can result in&nbsp;identity theft. Always access your sensitive account information by&nbsp;going directly to&nbsp;the&nbsp;Bayo Adelabu website and&nbsp;not through a&nbsp;hyperlink in&nbsp;an&nbsp;email or&nbsp;any&nbsp;other electronic communication, even if&nbsp;it&nbsp;looks official. Bayo Adelabu reserves the&nbsp;right to&nbsp;place any&nbsp;account on&nbsp;hold anytime with or&nbsp;without notification to&nbsp;the&nbsp;member in&nbsp;order to&nbsp;protect itself and&nbsp;its partners from what it&nbsp;believes to&nbsp;be&nbsp;fraudulent activity. Bayo Adelabu is&nbsp;not obligated to&nbsp;credit or&nbsp;discount a&nbsp;membership for&nbsp;holds placed on&nbsp;the&nbsp;account by&nbsp;either a&nbsp;representative of&nbsp;Bayo Adelabu or&nbsp;by&nbsp;the&nbsp;automated processes of&nbsp;Bayo Adelabu .<br />\n" +
            "Warranties<br />\n" +
            "UNLESS EXPRESSLY SET FORTH IN&nbsp;THIS AGREEMENT, Bayo Adelabu MAKES NO&nbsp;WARRANTY, EXPRESS OR&nbsp;IMPLIED, WITH RESPECT TO&nbsp;ANY MATTER, INCLUDING WITHOUT LIMITATION ADVERTISING AND OTHER SERVICES, AND EXPRESSLY DISCLAIMS THE IMPLIED WARRANTIES OR&nbsp;CONDITIONS OF&nbsp;NON INFRINGEMENT, MERCHANTABILITY AND FITNESS FOR ANY PARTICULAR PURPOSE. Bayo Adelabu DOES NOT WARRANT THE RESULTS OF&nbsp;USE OF&nbsp;ANY OF&nbsp;THE PRODUCTS AND SERVICES, AND ACCOUNT HOLDER ASSUMES ALL RISK AND RESPONSIBILITY WITH RESPECT THERETO.<br />\n" +
            "Limitation of&nbsp;Liability and&nbsp;Damages. UNDER NO&nbsp;CIRCUMSTANCES, INCLUDING, BUT NOT LIMITED TO, NEGLIGENCE, SYSTEM FAILURE OR&nbsp;NETWORK OUTAGE, WILL EITHER PARTY OR&nbsp;ITS AFFILIATES BE&nbsp;LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, CONSEQUENTIAL, PUNITIVE, RELIANCE, OR&nbsp;EXEMPLARY DAMAGES THAT RESULT FROM THIS AGREEMENT, EVEN IF&nbsp;SUCH PARTY OR&nbsp;ITS AUTHORIZED REPRESENTATIVE HAS BEEN ADVISED OF&nbsp;THE POSSIBILITY OF&nbsp;SUCH DAMAGES. EXCEPT FOR THE INDEMNITY OBLIGATIONS SET FORTH HEREIN, IN&nbsp;NO&nbsp;EVENT WILL EITHER PARTY&rsquo;S OR&nbsp;ITS AFFILIATES&rsquo; TOTAL LIABILITY TO&nbsp;THE OTHER PARTY FOR ALL DAMAGES, LOSSES, AND CAUSES OF&nbsp;ACTION ARISING OUT OF&nbsp;OR&nbsp;RELATING TO&nbsp;THIS AGREEMENT (WHETHER IN&nbsp;CONTRACT OR&nbsp;TORT, INCLUDING NEGLIGENCE, WARRANTY, OR&nbsp;OTHERWISE) EXCEED THE AMOUNTS PAID BY&nbsp;ACCOUNT HOLDER TO&nbsp;Bayo Adelabu.Net HEREUNDER.<br />\n" +
            "Indemnification<br />\n" +
            "IN&nbsp;NO&nbsp;EVENT SHALL Bayo Adelabu, OR&nbsp;ITS SUBSIDIARIES OR&nbsp;ANY OF&nbsp;THEIR SHAREHOLDERS, DIRECTORS, OFFICERS, EMPLOYEES OR&nbsp;LICENSORS BE&nbsp;LIABLE (JOINTLY OR&nbsp;SEVERALLY) TO&nbsp;YOU FOR ANY SPECIAL, INCIDENTAL, INDIRECT OR&nbsp;CONSEQUENTIAL DAMAGES OF&nbsp;ANY KIND, OR&nbsp;ANY DAMAGES WHATSOEVER RESULTING FROM LOSS OF&nbsp;USE, DATA OR&nbsp;PROFITS, GOODWILL, BUSINESS INTERRUPTION OR&nbsp;ANY OTHER COMMERCIAL OR&nbsp;INTANGIBLE DAMAGES OR&nbsp;LOSS, WHETHER OR&nbsp;NOT ADVISED OF&nbsp;THE POSSIBILITY OF&nbsp;DAMAGE, AND ON&nbsp;ANY THEORY OF&nbsp;LIABILITY, ARISING OUT OF&nbsp;OR&nbsp;IN&nbsp;CONNECTION WITH THE USE OR&nbsp;PERFORMANCE OF&nbsp;THE PRODUCTS AND SERVICES, OUR WEBSITE, AND ALL CONTENTS AND SOFTWARE ASSOCIATED THEREWITH, OR&nbsp;OTHERWISE RELATED TO&nbsp;THE PRODUCTS AND SERVICES, INCLUDING ANY FEATURES OR&nbsp;FUNCTIONALITIES ASSOCIATED THEREWITH. IN&nbsp;NO&nbsp;EVENT SHALL OUR TOTAL LIABILITY TO&nbsp;YOU FOR ALL DAMAGES FOR LOSSES ARISING FROM THE USE OR&nbsp;INABILITY TO&nbsp;USE THE PRODUCTS AND SERVICES, INCLUDING OUR WEBSITE AND USER INTERFACES, AND ALL CONTENT AND SOFTWARE ASSOCIATED THEREWITH (OTHER THAN AS&nbsp;MAY BE&nbsp;REQUIRED BY&nbsp;APPLICABLE LAW IN&nbsp;CASES INVOLVING PERSONAL INJURY) EXCEED THE AMOUNT OF&nbsp;YOUR PREVIOUS THREE (3) MONTHS OF&nbsp;FEES PAID TO&nbsp;Bayo Adelabu ON&nbsp;YOUR MEMBERSHIP PLAN. THE FOREGOING LIMITATIONS WILL APPLY EVEN IF&nbsp;THE ABOVE STATED REMEDY FAILS OF&nbsp;ITS ESSENTIAL PURPOSE. IF&nbsp;ANY APPLICABLE AUTHORITY HOLDS ANY PORTION OF&nbsp;THIS SECTION OR&nbsp;OTHER SECTIONS OF&nbsp;THESE TERMS OF&nbsp;SERVICE, INCLUDING ANY PORTION OF&nbsp;THE PRIVACY POLICY, TO&nbsp;BE&nbsp;UNENFORCEABLE, THEN THOSE PORTIONS DEEMED UNENFORCEABLE SHALL BE&nbsp;SEVERED AND THE TERMS OF&nbsp;SERVICE SHALL BE&nbsp;ENFORCED ABSENT THOSE PROVISIONS AND ANY LIABILITY WILL BE&nbsp;LIMITED TO&nbsp;THE FULLEST POSSIBLE EXTENT PERMITTED BY&nbsp;APPLICABLE LAW.<br />\n" +
            "You shall indemnify Bayo Adelabu for&nbsp;damages, costs and&nbsp;attorneys fees that Bayo Adelabu incurs from any&nbsp;third party claim that (i) arise from your modification of&nbsp;the&nbsp;Products and&nbsp;Services or&nbsp;(ii) the&nbsp;use by&nbsp;Bayo Adelabu of&nbsp;any&nbsp;materials provided to&nbsp;Bayo Adelabu by&nbsp;you for&nbsp;use in&nbsp;the&nbsp;Products and&nbsp;Services infringe any&nbsp;U.S. patent, copyright, trademark, trade secret or&nbsp;other intellectual property right under U.S. law. You shall assume the&nbsp;defense of&nbsp;any&nbsp;third party claim with counsel reasonably satisfactory to&nbsp;Bayo Adelabu. Bayo Adelabu may employ its own counsel in&nbsp;any&nbsp;such case, and&nbsp;shall pay such counsel&rsquo;s fees and&nbsp;expenses. You shall have the&nbsp;right to&nbsp;settle any&nbsp;claim for&nbsp;which indemnification is&nbsp;available; provided, however, that to&nbsp;the&nbsp;extent that such settlement requires Bayo Adelabu being indemnified to&nbsp;take or&nbsp;refrain from taking any&nbsp;action or&nbsp;purports to&nbsp;obligate Bayo Adelabu being indemnified, then you shall not settle such claim without the&nbsp;prior written consent of&nbsp;Bayo Adelabu, which consent shall not be&nbsp;unreasonably withheld, conditioned or&nbsp;delayed. THE PARTIES DISCLAIM THE IMPLIED WARRANTY OF&nbsp;NON-INFRINGEMENT,&nbsp;RELYING INSTEAD ON&nbsp;THE TERMS OF&nbsp;THIS SECTION.<br />\n" +
            "Additional disclaimers or&nbsp;limitations of&nbsp;liability may be&nbsp;contained in&nbsp;the&nbsp;various third party software, licensing and&nbsp;service agreements you may have agreed to&nbsp;in&nbsp;order to&nbsp;access the&nbsp;Products and&nbsp;Services.<br />\n" +
            "Intellectual Property<br />\n" +
            "Copyright.&nbsp;The Products and&nbsp;Services, including all content included on&nbsp;our user interfaces, or&nbsp;delivered to&nbsp;members as&nbsp;part of&nbsp;the&nbsp;Products and&nbsp;Services, including, but not limited to, source code, data compilations, and&nbsp;software, are the&nbsp;property of&nbsp;Bayo Adelabu or&nbsp;its licensors and&nbsp;are protected by&nbsp;United States and&nbsp;international copyright, trade secret or&nbsp;other intellectual property laws and&nbsp;treaties. While the&nbsp;Products and&nbsp;Services include&nbsp;open-source&nbsp;software available for&nbsp;free, the&nbsp;compilation of&nbsp;all content and&nbsp;any&nbsp;software or&nbsp;other materials provided by&nbsp;Bayo Adelabu, or&nbsp;in&nbsp;connection with the&nbsp;Products and&nbsp;Services are the&nbsp;exclusive property of&nbsp;Bayo Adelabu and&nbsp;its licensors and&nbsp;are protected by&nbsp;the&nbsp;copyright and&nbsp;trade secret laws in&nbsp;the&nbsp;territories in&nbsp;which the&nbsp;Products and&nbsp;Service operate and&nbsp;by&nbsp;international treaty provisions. Content shall not be&nbsp;reproduced or&nbsp;used without express written permission from Bayo Adelabu or&nbsp;its licensors. You agree to&nbsp;adhere to&nbsp;the&nbsp;restrictions set forth in&nbsp;these Terms of&nbsp;Service. You agree not to&nbsp;decompile, reverse engineer or&nbsp;disassemble any&nbsp;software or&nbsp;other products or&nbsp;processes accessible from Bayo Adelabu, not to&nbsp;insert any&nbsp;code or&nbsp;product into or&nbsp;manipulate the&nbsp;content of&nbsp;the&nbsp;Products and&nbsp;Services in&nbsp;any&nbsp;way, and&nbsp;not to&nbsp;use any&nbsp;data mining, data gathering or&nbsp;extraction method. Bayo Adelabu reserves the&nbsp;right to&nbsp;terminate your membership hereunder if&nbsp;Bayo Adelabu , in&nbsp;its sole and&nbsp;absolute discretion, believes that you are in&nbsp;violation of&nbsp;Bayo Adelabu software restrictions, restrictions against copying the&nbsp;Products and&nbsp;Services provided to&nbsp;you by&nbsp;us, or&nbsp;other unauthorized copying or&nbsp;use of&nbsp;our proprietary content in&nbsp;violation of&nbsp;the&nbsp;copyrights of&nbsp;Bayo Adelabu and&nbsp;its licensors. Bayo Adelabu does not promote, foster or&nbsp;condone the&nbsp;copying of&nbsp;third party products or&nbsp;categories or&nbsp;any&nbsp;other infringing activity. While the&nbsp;use of&nbsp;the&nbsp;Products and&nbsp;Services, including demos of&nbsp;online stores, store products and&nbsp;services, are for&nbsp;your commercial use, such items proprietary to&nbsp;third parties are not. Please see the&nbsp;instructions at&nbsp;the&nbsp;end of&nbsp;these Terms of&nbsp;Service for&nbsp;notifying us&nbsp;of&nbsp;the&nbsp;presence of&nbsp;any&nbsp;allegedly infringing content of&nbsp;the&nbsp;Products and&nbsp;Services, including any&nbsp;on&nbsp;the&nbsp;mobike app or website, Eppnet<br />\n" +
            "Use of&nbsp;Information Submitted<br />\n" +
            "Bayo Adelabu Digital Technologies Limited is&nbsp;free to&nbsp;use any&nbsp;comments, information, ideas, concepts, reviews, or&nbsp;techniques or&nbsp;any&nbsp;other material contained in&nbsp;any&nbsp;communication you may send to&nbsp;us&nbsp;(&ldquo;Feedback&rdquo;), including responses to&nbsp;questionnaires or&nbsp;through postings to&nbsp;the&nbsp;Products and&nbsp;Services, including the&nbsp;Bayo Adelabu app, website and&nbsp;user interfaces, without further compensation, acknowledgement or&nbsp;payment to&nbsp;you for&nbsp;any&nbsp;purpose whatsoever including, but not limited to, developing, manufacturing and&nbsp;marketing products and&nbsp;creating, modifying or&nbsp;improving the&nbsp;Products and&nbsp;Services. Furthermore, by&nbsp;posting any&nbsp;Feedback on&nbsp;our site, submitting Feedback to&nbsp;us, or&nbsp;in&nbsp;responding to&nbsp;questionnaires, you grant us&nbsp;a&nbsp;perpetual, worldwide,&nbsp;non-exclusive,&nbsp;royalty-free&nbsp;irrevocable license, including the&nbsp;right to&nbsp;sublicense such right, and&nbsp;right to&nbsp;display, use, reproduce or&nbsp;modify the&nbsp;Feedback submitted in&nbsp;any&nbsp;media, software or&nbsp;technology of&nbsp;any&nbsp;kind now existing or&nbsp;developed in&nbsp;the&nbsp;future.<br />\n" +
            "Please note Bayo Adelabu does not accept unsolicited materials or&nbsp;ideas for&nbsp;use or&nbsp;publication, and&nbsp;is&nbsp;not responsible for&nbsp;the&nbsp;similarity of&nbsp;any&nbsp;of&nbsp;its content or&nbsp;programming in&nbsp;any&nbsp;media to&nbsp;materials or&nbsp;ideas transmitted to&nbsp;Bayo Adelabu. Should you send any&nbsp;unsolicited materials or&nbsp;ideas, you do&nbsp;so&nbsp;with the&nbsp;understanding that no&nbsp;additional consideration of&nbsp;any&nbsp;sort will be&nbsp;provided to&nbsp;you, and&nbsp;you are waiving any&nbsp;claim against Bayo Adelabu and&nbsp;its affiliates regarding the&nbsp;use of&nbsp;such materials and&nbsp;ideas, even if&nbsp;material or&nbsp;an&nbsp;idea is&nbsp;used that is&nbsp;substantially similar to&nbsp;the&nbsp;idea you sent.<br />\n" +
            "Service Testing<br />\n" +
            "From time to&nbsp;time, we&nbsp;test various aspects of&nbsp;our Products and&nbsp;Services, including our website, other user interfaces, service levels, plans, promotions, features, delivery, and&nbsp;pricing, and&nbsp;we&nbsp;reserve the&nbsp;right to&nbsp;include you in&nbsp;or&nbsp;exclude you from these tests without notice.<br />\n" +
            "Customer Service<br />\n" +
            "You can &nbsp;reach us&nbsp;with customer service questions at support@weddingplus.net.<br />\n" +
            "Limitations on&nbsp;Use<br />\n" +
            "You must be&nbsp;18&nbsp;years of&nbsp;age or&nbsp;older to&nbsp;become a&nbsp;member and&nbsp;end user of&nbsp;the&nbsp;Products and&nbsp;Services. In&nbsp;certain jurisdictions, the&nbsp;age of&nbsp;majority may be&nbsp;older than 18, in&nbsp;which case you must satisfy that age in&nbsp;order to&nbsp;become a&nbsp;member. While individuals under the&nbsp;age of&nbsp;18&nbsp;may utilize the&nbsp;Products and&nbsp;Services, they may do&nbsp;so&nbsp;only with the&nbsp;involvement of&nbsp;a&nbsp;parent or&nbsp;legal guardian, under such person&rsquo;s account and&nbsp;otherwise subject to&nbsp;these Terms of&nbsp;Service. While Bayo Adelabu does distribute products that may be&nbsp;used by&nbsp;children, the&nbsp;Products and&nbsp;Services are not intended to&nbsp;be&nbsp;used by&nbsp;children, without involvement, supervision, and&nbsp;approval of&nbsp;a&nbsp;parent or&nbsp;legal guardian (see also &ldquo;Parental Controls&rdquo;). Unless otherwise specified, we&nbsp;grant you a&nbsp;limited, non exclusive, non transferable, license to&nbsp;access the&nbsp;Products and&nbsp;Services. Except for&nbsp;the&nbsp;foregoing limited license, no&nbsp;right, title or&nbsp;interest shall be&nbsp;transferred to&nbsp;you. You may not download (other than through page caching necessary for&nbsp;personal use, or&nbsp;as&nbsp;otherwise expressly permitted by&nbsp;these Terms of&nbsp;Service), modify, copy, distribute, transmit, display, perform, reproduce, duplicate, publish, license, create derivative works from, or&nbsp;offer for&nbsp;sale any&nbsp;information contained on, or&nbsp;obtained from or&nbsp;through, the&nbsp;Products and&nbsp;Services, without our express written consent. Bayo Adelabu does not promote, foster or&nbsp;condone the&nbsp;copying of&nbsp;third-party&nbsp;content, or&nbsp;any&nbsp;other infringing activity. You may not circumvent, remove, alter, deactivate, degrade or&nbsp;thwart any&nbsp;of&nbsp;the&nbsp;content protections in&nbsp;the&nbsp;Products and&nbsp;Services. You may not frame or&nbsp;utilize any&nbsp;framing techniques to&nbsp;enclose any&nbsp;trademark, logo, or&nbsp;other proprietary information (including images, text, page layout, or&nbsp;form) of&nbsp;Bayo Adelabu without our express written consent. You may not purchase search terms or&nbsp;use any&nbsp;meta tags or&nbsp;any&nbsp;other &ldquo;hidden text&rdquo; utilizing the&nbsp;Bayo Adelabu name or&nbsp;trademarks without our express written consent. Any unauthorized use of&nbsp;the&nbsp;Products and&nbsp;Services or&nbsp;its contents will terminate the&nbsp;limited license granted by&nbsp;us&nbsp;and&nbsp;will result in&nbsp;the&nbsp;cancellation of&nbsp;your membership.<br />\n" +
            "Links and&nbsp;Pages<br />\n" +
            "Some of&nbsp;the&nbsp;hyperlinks on&nbsp;the&nbsp;Bayo Adelabu website may lead to&nbsp;other websites or&nbsp;other content that are not controlled by, or&nbsp;affiliated with, Bayo Adelabu. In&nbsp;addition, other websites may link to&nbsp;the&nbsp;Bayo Adelabu website or&nbsp;Bayo Adelabu may include links to&nbsp;the&nbsp;websites of&nbsp;businesses, including those that have associations with us&nbsp;through certain programs. For example, Bayo Adelabu may include pages that display and&nbsp;provide information on&nbsp;Applications or&nbsp;other products. These pages may provide links to&nbsp;third party sites where Bayo Adelabu members may obtain or&nbsp;purchase such Applications. Bayo Adelabu has not reviewed these websites and&nbsp;is&nbsp;not responsible for&nbsp;the&nbsp;offerings of&nbsp;any&nbsp;of&nbsp;these sites or&nbsp;the&nbsp;content, privacy policies or&nbsp;Terms of&nbsp;Service of&nbsp;these websites or&nbsp;Applications. You acknowledge and&nbsp;agree that Bayo Adelabu is&nbsp;not responsible or&nbsp;liable for, and&nbsp;does not otherwise warrant, the&nbsp;actions of&nbsp;these third parties, the&nbsp;products or&nbsp;contents on&nbsp;their websites, or&nbsp;the&nbsp;performance of&nbsp;any&nbsp;Applications or&nbsp;other products. These linked websites have separate and&nbsp;independent privacy statements, notices and&nbsp;Terms of&nbsp;Service, which we&nbsp;recommend you read carefully. You should also refer to&nbsp;the&nbsp;section of&nbsp;these terms titled &ldquo;How Our Products and&nbsp;Services Work.&rdquo;<br />\n" +
            "Claims of&nbsp;Copyright Infringement<br />\n" +
            "It&nbsp;is&nbsp;the&nbsp;policy of&nbsp;Bayo Adelabu to&nbsp;respect the&nbsp;intellectual property rights of&nbsp;others. Bayo Adelabu does not condone the&nbsp;unauthorized reproduction or&nbsp;distribution of&nbsp;copyrighted content. If&nbsp;you believe your work has been reproduced or&nbsp;distributed in&nbsp;a&nbsp;way that constitutes copyright infringement or&nbsp;are aware of&nbsp;any&nbsp;infringing material available through the&nbsp;Products and&nbsp;Services, please view our&nbsp;Copyright Policy&nbsp;for&nbsp;explicit instruction on&nbsp;how to&nbsp;address the&nbsp;issue.<br />\n" +
            "Governing Law<br />\n" +
            "These Terms of&nbsp;Service shall be&nbsp;governed by&nbsp;and&nbsp;construed in&nbsp;accordance with the&nbsp;laws of&nbsp;the&nbsp;state of&nbsp;California, U.S.A. without regard to&nbsp;conflict of&nbsp;laws provisions. If&nbsp;any&nbsp;provision or&nbsp;provisions of&nbsp;these terms shall be&nbsp;held to&nbsp;be&nbsp;invalid, illegal, unenforceable or&nbsp;in&nbsp;conflict with the&nbsp;law of&nbsp;any&nbsp;jurisdiction, the&nbsp;validity, legality and&nbsp;enforceability of&nbsp;the&nbsp;remaining provisions shall not in&nbsp;any&nbsp;way be&nbsp;affected or&nbsp;impaired thereby, and&nbsp;shall remain in&nbsp;full force and&nbsp;effect.<br />\n" +
            "Arbitration Agreement<br />\n" +
            "You and&nbsp;Bayo Adelabu agree that any&nbsp;dispute, claim or&nbsp;controversy arising out of&nbsp;or&nbsp;relating in&nbsp;any&nbsp;way to&nbsp;the&nbsp;Products and&nbsp;Services, including our website, user interfaces, these Terms of&nbsp;Service and&nbsp;this Arbitration Agreement, shall be&nbsp;determined by&nbsp;binding arbitration in&nbsp;Santa Clara County, California, instead of&nbsp;in&nbsp;courts of&nbsp;general jurisdiction. Arbitration is&nbsp;more informal than a&nbsp;lawsuit in&nbsp;court. Arbitration uses a&nbsp;neutral arbitrator instead of&nbsp;a&nbsp;judge or&nbsp;jury, allows for&nbsp;more limited discovery than in&nbsp;court, and&nbsp;is&nbsp;subject to&nbsp;very limited review by&nbsp;courts. Arbitrators can award the&nbsp;same damages and&nbsp;relief that a&nbsp;court can award. You agree that, by&nbsp;agreeing to&nbsp;these Terms of&nbsp;Service, the&nbsp;U.S. Federal Arbitration Act governs the&nbsp;interpretation and&nbsp;enforcement of&nbsp;this provision, and&nbsp;that you and&nbsp;Bayo Adelabu are each waiving the&nbsp;right to&nbsp;a&nbsp;trial by&nbsp;jury or&nbsp;to&nbsp;participate in&nbsp;a&nbsp;class action. This arbitration provision shall survive termination of&nbsp;this Agreement and&nbsp;the&nbsp;termination of&nbsp;your Bayo Adelabu membership.<br />\n" +
            "YOU AND Bayo Adelabu AGREE THAT EACH MAY BRING CLAIMS AGAINST THE OTHER ONLY IN&nbsp;YOUR OR&nbsp;ITS INDIVIDUAL CAPACITY, AND NOT AS&nbsp;A&nbsp;PLAINTIFF OR&nbsp;CLASS MEMBER IN&nbsp;ANY PURPORTED CLASS OR&nbsp;REPRESENTATIVE PROCEEDING. Further, unless both you and&nbsp;Bayo Adelabu agree otherwise, the&nbsp;arbitrator may not consolidate more than one person&rsquo;s claims with your claims, and&nbsp;may not otherwise preside over any&nbsp;form of&nbsp;a&nbsp;representative or&nbsp;class proceeding. If&nbsp;this specific provision is&nbsp;found to&nbsp;be&nbsp;unenforceable, then the&nbsp;entirety of&nbsp;this arbitration provision shall be&nbsp;null and&nbsp;void. The arbitrator may award declaratory or&nbsp;injunctive relief only in&nbsp;favor of&nbsp;the&nbsp;individual party seeking relief and&nbsp;only to&nbsp;the&nbsp;extent necessary to&nbsp;provide relief warranted by&nbsp;that party&rsquo;s individual claims.<br />\n" +
            "&nbsp;</p>\n";

    String faqs = "1) The police hardly respond to calls and whenever they do, they basically do nothing because the criminals would have left.\n" +
            "\n" +
            "Bayo Adelabu is a REPORTING platform only and does not have control over the police. The technology however has dual wins for users and the police because users do not have to call when they are in danger - Bayo Adelabu does the calling for them while the Police are also equally helped when reports to them are sent in early enough while a crime is ongoing or about to commence  - the minutes saved could be vital.  \n" +
            "\n" +
            "2) What happens if I have no emergency within a year? \n" +
            "You become entitled to a 10% discount off your renewal sum. The 10% continues till the 5th year. \n" +
            "\n" +
            "3) Are group downloads possible?\n" +
            " Yes, they are for Android devices. Special arrangements have to be made for iPhones however. \n" +
            "\n" +
            "4) Do I have to pay for a new download if I lose my phone to theft or damage? \n" +
            "No, you will not have to. You only need to register with your approved username and detail on new device. \n" +
            "\n" +
            "5) Where there is no data, what can I do? Change network provider or send information while at the last known place with data. ";

    String tips = "1) Do not flaunt this app to untrusted persons: While the temptation to test and show the features of this app is expected, users are advised to use app in a discreet manner. If an untrusted person knows that you have the app, it could endanger and compromise the effectiveness of the services you should normally enjoy.\n" +
            "\n" +
            " 2) Criminals are fast thinkers and can devise a number of means to outsmart users of this app. You are best served with early reports of suspicious behaviour or movements. This increases the window of response that responsible agencies have to react to prompts from our 24/7 support center.\n" +
            "\n" +
            "3) If you are aware of areas without strong data connections and which you must be, obtain service from an alternative telecoms provider for that area. The chances of strong data connection increases with access to different networks.\n" +
            "\n" +
            "4) Take advantage of the unusual request offer to a class of users. Remember however that requests are solely at our discretion and may be turned down wilfully without explanation where found outside immediate coverage.\n" +
            "\n" +
            "5) Use the social function when you can to enable collaborations with people who are in same categories of social activity with you.\n" +
            "\n" +
            "6) Use this app for its whistle-blower functionality. You can report frauds, plans against the people and country, amongst many others. You can be anonymous if you so choose but our operations largely remain subjected to tests of authenticity to guarantee integrity of transmission.\n" +
            "\n" +
            "7) This app may be sent to users outside Google Play or the App store. Reach us if you need the app independently from our arrangement .\n" +
            "\n" +
            "8) Device GPS signals can be turned on and off before a report is made to enhance accuracy. ";

    String check_in_instruction = "The Check In /Business Check In Feature\n" +
            "\n" +
            "The Check In function under 'Guests' functions slightly differently from users registered under 'Vendors' accounts. This feature transmits online, real-time GPS pings to our global GPS/SMS switchboard and has capabilities for generating reports that aid business decisions or help lost guests.\n" +
            "\n" +
            "Guest users of the App can select the Check In function to view nearby environments and their current location. This helps users to identify or concur with geographical tags of trips, movements or visits. If lost, they can be helped about where they are relative to intended locations. Premium users of the App can call/request directions to points of interests around where they are or if lost. A best effort mechanism is instituted to assist users with points of interest around 1KM, 3KM and 5 KMs radius from point of incident. \n" +
            "Premium users can make these requests 12 times in a year of active connections.\n" +
            "\n" +
            "HOW TO USE THE CHECK IN FUNCTION \n" +
            "\n" +
            "1) Launch the Check In on App.\n" +
            "\n" +
            "2) After display of static location map, select \"Check In\".\n" +
            "\n" +
            "3) You may type a report in the provided space of up to 3 A4 Pages in the Location Report/Description box displayed.\n" +
            "\n" +
            "4) You may Attach Photo/Video/Voice where required.\n" +
            "\n" +
            "5) Click Send to submit Remote Report.\n" +
            "\n" +
            "Note: Some devices lose accuracy of GPS positioning after extended GPS activity. It is often helpful to turn the function On and Off where more accuracy is required.\n" +
            "\n" +
            "DO NOT USE PHONE WHILE DRIVING. Safety COMES FIRST WITH Bayo Adelabu. \n" +
            "\n" +
            "You can call our lines +2347044911911 or +2347045911911 or mail us at support@weddingplusapp.com for more.";

   /* String welcome_to_eppme = "\n" +
            "Welcome to Bayo Adelabu, Nigeria's first and most comprehensive emergency and business reporting app. Now that we are here, we can effectively collaborate in unthinkable terms to make ourselves, loved ones and local communities safer. In your hand is one of the most powerful tools you'd ever require to report emergencies or to manage salesmen, marketers or outbound staff generally. We welcome you to a journey of innovative services, protection and professional care.\n" +
            "\n" +
            "As you familiarize yourself with this app, we are confident that you will enjoy many of the innovative features it offers. \n" +
            "\n" +
            "We welcome you to Bayo Adelabu.\n" +
            "\n" +
            "The Bayo Adelabu Team";*/

    String welcome_to_eppme = "\n" +
            "Now that you have downloaded your premium Bayo Adelabu app, how about sending well wishes to the couple or just checking in to let them know you arrived at your home safely? Do you require directions to the Engagement venue and are lost? Or do you perhaps need to quickly check up information on the wedding invitation? All of that and more can be achieved right here on the Bayo Adelabu App, your one-stop hub for wedding specific detail. The user interface has been designed with ease while you can bank on highly secure, and highly efficient back end systems to provide all round superlative performance.  \n" +
            "\n" +
            "Feel free, share in the happy times, click along! ! \n" +
            "\n" +
            "The Bayo Adelabu Team";



    String contact_us = "You may reach us via the following channels:\n" +
            "\n" +
            "Technical issues on App: technical@weddingplus.com\n" +
            "\n" +
            "Sales and Upgrades:\n" +
            "sales@weddingplus.com\n" +
            "\n" +
            "General enquiries: info@weddingplus.com\n" +
            "\n" +
            "Phone: 07045 911911\n" +
            "Premium Users/Corporate: 07042 911911\n" +
            "\n" +
            "Web: www.weddingplus.com\n" +
            "\n" +
            "Social Media: @weddingplus on Facebook, Instagram, Twitter, YouTube and Google +";

    int RC_SIGN_IN = 999;
    int SUCCESS = 1;
    int INVALID = -1;
    int FAILURE = 0;
    int UNAUTHORISED = 3;

    int REQUEST_CAMERA = 1;
    int REQUEST_VIDEO = 5;
    int REQUEST_GALLERY = 2;
    int SELECT_FILE = 3;
    int SELECT_VIDEO = 4;

    int READ_TIMEOUT = 60*5;
    int CONNECT_TIMEOUT = READ_TIMEOUT;

    String NOTIFICATION_TYPE_HISTRY_STATUS = "emergency_history";
    String NOTIFICATION_TYPE_STUDENT_REPORT_HISTORY = "student_history";
    String NOTIFICATION_TYPE_CUSTOM_ADMIN = "custom_admin";
    String FCM_KEY_TYPE = "type";
    String FCM_KEY_MESSAGE = "message";
    String FCM_KEY_TITLE = "title";

    String STORE_SEARCH_RANGE_KMS = "20";

    String LOGIN_TYPE_FACEBOOK = "F";

    String APP_VERSION = "105" +
            "";
    String DEVICE_TYPE = "A";

    int PLACE_PICKER_REQUEST = 2004;

    String[] REQUIRED_PERMISSIONS_FACEBOOK = {"email", "public_profile"};

    String FACEBOOK_REQUEST_PARAMS = "name,email";

    String ROBERRY = "Robbery";
    String RAPE = "Rape";
    String FIRE = "Fire";
    String HOME = "Home";

    String INDIVIDUAL = "3";
    String CORPORATE = "2";


    int INITIAL_QUANTITY = 0;
    int CATOGORIES_TO_DISPLAY_AT_HOME_SCREEN = 3;

    String YES = "Y";
    String NO = "N";

    String CART_TYPE_SELF = "M";
    String CART_TYPE_FAMILY = "F";

    String EXTRA_KEY_BRAND = "key_brand";
    String EXTRA_KEY_SUB_CATEGORY = "extra_sub_category";
    String EXTRA_KEY_CATEGORY = "key_category";
    String EXTRA_KEY_SUB_CATEGORY_DETAIL = "key_sub_category_detail";
    String EXTRA_KEY_PRODUCT = "key_product";
    String EXTRA_KEY_PRODUCTS = "key_products";
    String EXTRA_KEY_TITLE = "key_title";
    String EXTRA_KEY_IS_IN_EDIT_MODE = "key_is_in_mode";
    String EXTRA_KEY_URL = "key_url";
    String PREF_KEY_AUTH_KEY = "pref_auth_key";
    String EXTRA_KEY_DATA = "key_extra_data";
    String EXTRA_KEY_TYPE = "key_extra_type";
//    String PREF_KEY_IS_STORE_SELECTED = "pref_is_store_selected";
}
