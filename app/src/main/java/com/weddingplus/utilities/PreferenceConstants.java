package com.weddingplus.utilities;


/**
 * Created by Manndeep Vachhani on 11/2/17.
 */
public interface PreferenceConstants {

  String KEY_AUTH_KEY="auth_key";
  String KEY_ID="user_id";
  String KEY_USER_NAME="first_name";
  String KEY_EMAIL="email";
  String KEY_IMAGE="profile_url";
  String KEY_COMPANY_NAME="company_name";
  String KEY_LAST_NAME="last_name";
  String KEY_IS_NOTIFY="is_notify";
  String KEY_DEVICE_TOKEN="device_token";
  String KEY_USER_TYPE="user_type";
  String KEY_LATTITUDE = "key_lattitude";
  String KEY_LONGITUDE = "key_longitude";
  String KEY_CONTACT_NUMBER = "key_contact_number";
  String KEY_CONTACT_PERSON_NUMBER_1 = "key_contact_person_number_1";
  String KEY_CONTACT_PERSON_NUMBER_2 = "key_contact_person_number_2";
  String KEY_PERSON_TO_CALL_NUMBER = "key_person_to_call_number2";
}

