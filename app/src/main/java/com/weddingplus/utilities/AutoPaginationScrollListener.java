package com.weddingplus.utilities;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;

/**
 * Created by Manndeep Vachhani on 16/9/17.
 */

public abstract class AutoPaginationScrollListener extends RecyclerView.OnScrollListener {

    private final int LAYOUT_MANAGER_TYPE_LINEAR = 0;
    private final int LAYOUT_MANAGER_TYPE_STAGGERED_GRID = 1;

    private RecyclerView.LayoutManager mLayoutManager;
    private LinearLayoutManager mLinearLayoutManager;
    private StaggeredGridLayoutManager mStaggeredGridLayoutManager;
    private int mLayoutType;
    private int PAGINATION_PAGE_SIZE=8;

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);

        if (null == mLayoutManager) {
            this.mLayoutManager = recyclerView.getLayoutManager();
            if (mLayoutManager instanceof LinearLayoutManager) {
                mLayoutType = LAYOUT_MANAGER_TYPE_LINEAR;
                mLinearLayoutManager = (LinearLayoutManager) mLayoutManager;
                mLayoutManager.setItemPrefetchEnabled(false);
            } else if (mLayoutManager instanceof StaggeredGridLayoutManager) {
                mLayoutType = LAYOUT_MANAGER_TYPE_STAGGERED_GRID;
                mStaggeredGridLayoutManager = (StaggeredGridLayoutManager) mLayoutManager;
                mStaggeredGridLayoutManager.setItemPrefetchEnabled(false);
            }
        }

        onScrolledUpOrDown(dy > 0);

        int visibleItemCount = mLayoutManager.getChildCount();
        int totalItemCount = mLayoutManager.getItemCount();

        int firstVisibleItemPosition;

        switch (mLayoutType) {

            case LAYOUT_MANAGER_TYPE_LINEAR:

                firstVisibleItemPosition = mLinearLayoutManager.findFirstVisibleItemPosition();

                if ((firstVisibleItemPosition + visibleItemCount) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGINATION_PAGE_SIZE
                        ) {
                    requestNewPage();
                }
                break;

            case LAYOUT_MANAGER_TYPE_STAGGERED_GRID:

                int[] firstVisibleItemPositions = mStaggeredGridLayoutManager.findFirstVisibleItemPositions(null);

                firstVisibleItemPosition = firstVisibleItemPositions[0];

                if ((firstVisibleItemPosition + visibleItemCount) >= totalItemCount
                        && firstVisibleItemPosition >= 0
                        && totalItemCount >= PAGINATION_PAGE_SIZE
                        ) {
                    requestNewPage();
                }
                break;

        }
    }

    protected void requestNewPage() {

    }

    protected void onScrolledUpOrDown(boolean toUp) {

    }
}
