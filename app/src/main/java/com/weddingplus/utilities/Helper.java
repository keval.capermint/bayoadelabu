package com.weddingplus.utilities;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.text.Html;
import android.text.Spanned;
import android.util.Log;

import com.weddingplus.App;
import com.weddingplus.models.User;
import com.weddingplus.user_interface.BaseAppCompatActivity;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.TimeZone;

/**
 * Created by Manndeep Vachhani on 8/2/17.
 */
public class Helper implements AppConstants, PreferenceConstants {

    private static final String TAG = "Helper";
    private static SharedPreferences sharedpreferences;
    private static String PREFERENCE = "pref_arch_out";

    public static void debugLog(String tag, String log) {
        Log.e(tag, log);
    }

    /**
     * Set string value in shared preferences
     *
     * @param key   key to put value for
     * @param value value of the key
     */
    public static void setStringValue(String key, String value) {
        sharedpreferences = App.getAppInstance().getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putString(key, value);
        editor.apply();
    }

    /**
     * Set Integer value in shared preferences
     *
     * @param key   key to put value for
     * @param value value of the key
     */
    public static void setIntValue(String key, int value) {
        sharedpreferences = App.getAppInstance().getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    /**
     * Get string value from shared preferences
     *
     * @param key key of value
     * @return value of the key
     */
    public static String getStringValue(String key) {
        sharedpreferences = App.getAppInstance().getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        return sharedpreferences.getString(key, StringUtils.EMPTY_STRING);
    }

    /**
     * Get Integer value from shared preferences
     *
     * @param key key of value
     * @return value of the key
     */
    public static int getIntValue(String key) {
        sharedpreferences = App.getAppInstance().getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        return sharedpreferences.getInt(key, INVALID);
    }

    /**
     * Clear all the data stored in shared preferences
     */
    public static void clearPreferences() {
        sharedpreferences = App.getAppInstance().getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
        sharedpreferences.edit().clear().apply();
    }

    /**
     * Checks if user is logged in or not
     *
     * @return true if logged in, false otherwise
     */


    /**
     * Save user to shared preferences
     *
     * @param user
     */

    public static void setUserValues(User user) {
        if (null != user) {
            sharedpreferences = App.getAppInstance().getSharedPreferences(PREFERENCE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedpreferences.edit();

            if (INVALID != user.getUserId())
                editor.putInt(KEY_ID, user.getUserId());
            if (StringUtils.isValid(user.getProfileUrl()))
                editor.putString(KEY_IMAGE, user.getProfileUrl());
            if (StringUtils.isValid(user.getFirstName()))
                editor.putString(KEY_USER_NAME, user.getFirstName());
            if (StringUtils.isValid(user.getUserName()))
                editor.putString(KEY_COMPANY_NAME, user.getUserName());
            if (StringUtils.isValid(user.getLastName()))
                editor.putString(KEY_LAST_NAME, user.getLastName());
            if (StringUtils.isValid(user.getEmail()))
                editor.putString(KEY_EMAIL, user.getEmail());
            if (StringUtils.isValid(String.valueOf(user.getIsNotify())))
                editor.putString(KEY_IS_NOTIFY, String.valueOf(user.getIsNotify()));
            if (StringUtils.isValid(user.getAuthkey())) {
                editor.putString(KEY_AUTH_KEY, user.getAuthkey());
            }
            if (StringUtils.isValid(String.valueOf(user.getUserType()))) {
                editor.putString(KEY_USER_TYPE, String.valueOf(user.getUserType()));
            }
            if (StringUtils.isValid(String.valueOf(user.getContactPersonNumber1()))) {
                editor.putString(KEY_CONTACT_PERSON_NUMBER_1, String.valueOf(user.getContactPersonNumber1()));
            }
            if (StringUtils.isValid(String.valueOf(user.getContactPersonNumber2()))) {
                editor.putString(KEY_CONTACT_PERSON_NUMBER_2, String.valueOf(user.getContactPersonNumber2()));
            }
            if (StringUtils.isValid(String.valueOf(user.getPersonToCallNumber()))) {
                editor.putString(KEY_PERSON_TO_CALL_NUMBER, String.valueOf(user.getPersonToCallNumber()));
            }

            editor.apply();
        }
    }

    public static String getAuthKey() {
        Log.e("authkey::", "" + getStringValue(KEY_AUTH_KEY));
        return getStringValue(KEY_AUTH_KEY);

    }

    public static String getUserType() {
        Log.e("uset_type::", "" + getStringValue(KEY_USER_TYPE));
        return getStringValue(KEY_USER_TYPE);

    }

    public static boolean isLoggedIn() {
        return StringUtils.isValid(getStringValue(KEY_AUTH_KEY));
    }



/*
    public static User getUser() {
        String authKey, name, email, imageUrl, userType;
        int userId, profileStatus;

        userId = getIntValue(KEY_ID);
        authKey = getStringValue(KEY_AUTH_KEY);
        name = getStringValue(KEY_USER_NAME);
        email = getStringValue(KEY_EMAIL);
        imageUrl = getStringValue(KEY_IMAGE);
        userType = getStringValue(KEY_USER_TYPE);
        profileStatus = getIntValue(KEY_PROFILE_STATUS);

        if (INVALID != userId
                && StringUtils.isValid(authKey)
                && StringUtils.isValid(name)
                && StringUtils.isValid(imageUrl)
                && StringUtils.isValid(userType)
                && INVALID != profileStatus) {

            User user = new User();

            user.setUserId(userId);
            user.setAuthKey(authKey);
            user.setUserName(name);
            user.setEmail(email);
            user.setImageUrl(imageUrl);
            user.setUserType(userType);
            user.setProfileStatus(profileStatus);

            return user;
        }

        return null;
    }
*/

    /**
     * Check for network availability
     *
     * @return true if available. false otherwise
     */
    public static boolean isNetworkAvailable(BaseAppCompatActivity context) {
        boolean toReturn;
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        toReturn = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        if (!toReturn) {
            UiHelper.showNetworkDialog(context);
        }
        return toReturn;
    }

    /*public static String getDateAndTimeFromMillis(long timeStamp) {
        Calendar calendar = new GregorianCalendar(TimeZone.getDefault());
        calendar.setTimeInMillis(timeStamp);

        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH) + 1;
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        return StringUtils.formatDate(mDay, mMonth, mYear)
                + "\n"
                + StringUtils.formatTime(hour, minute, second);
    }

    public static String getTimeFromMillis(long timeStamp) {
        Calendar calendar = new GregorianCalendar(TimeZone.getDefault());
        calendar.setTimeInMillis(timeStamp);

        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int minute = calendar.get(Calendar.MINUTE);
        int second = calendar.get(Calendar.SECOND);

        return StringUtils.formatTime(hour, minute, second);
    }
    */
    public static String getDateFromMillis(long timeStamp) {
        Calendar calendar = new GregorianCalendar(TimeZone.getDefault());
        calendar.setTimeInMillis(timeStamp);

        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH) + 1;
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);


        return StringUtils.formatDate(mDay, mMonth, mYear);
    }


    public static Spanned getSpannedHtmlForTextView(String text) {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
            return Html.fromHtml(text, Html.FROM_HTML_MODE_LEGACY);
        } else {
            return Html.fromHtml(text);
        }
    }
    public static void shareIntent(Context context, String shareText) {
        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
     /*   sharingIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        sharingIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK );*/
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareText);
        context.startActivity(Intent.createChooser(sharingIntent, "Share using"));
    }
    public static void WhatsAppIntent(Context context) {
        try {
            String toNumber = "+23407045911911"; // contains spaces.
            toNumber = toNumber.replace("+", "").replace(" ", "");
            Intent sendIntent = new Intent("android.intent.action.MAIN");
            sendIntent.setComponent(new ComponentName("com.whatsapp", "com.whatsapp.Conversation"));
            sendIntent.putExtra("jid", toNumber + "@s.whatsapp.net");
            context.startActivity(sendIntent);
        }
        catch (Exception e)
        {e.printStackTrace();}
    }
    public static void AppRedirectViewIntent(Context context,String shareText) {
        try {
            Intent i = new Intent(Intent.ACTION_VIEW);
            i.setData(Uri.parse(shareText));
            context.startActivity(i);
        }
        catch (Exception e)
        {e.printStackTrace();}
    }
}
