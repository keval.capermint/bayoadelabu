package com.weddingplus.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.v7.widget.AppCompatRadioButton;
import android.util.AttributeSet;

import com.weddingplus.R;
import com.weddingplus.utilities.FontCache;


/**
 * Created by Manndeep Vachhani on 11/24/2016.
 */

public class XRadioButton extends AppCompatRadioButton {

    public XRadioButton(Context context) {
        super(context);
    }

    public XRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public XRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if (isInEditMode()) {
            return;
        }
        if (attrs != null) {
            TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.CustomFont);
            String fontName = a.getString(R.styleable.CustomFont_fontName);
            if (fontName != null) {
                setTypeface(FontCache.getTypeface(fontName, getContext()));
            }
            a.recycle();
        }
    }
}
