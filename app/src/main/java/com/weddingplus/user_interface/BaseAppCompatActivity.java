package com.weddingplus.user_interface;

import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.weddingplus.R;
import com.weddingplus.utilities.AppConstants;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.LocationProviderHelper;
import com.weddingplus.utilities.PreferenceConstants;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.google.android.gms.location.LocationListener;

/**
 * Created by Manndeep Vachhani on 4/2/17.
 */
public class BaseAppCompatActivity extends AppCompatActivity implements AppConstants {

    public String TAG = "";
    public BaseAppCompatActivity mActivity;
    private XTextView txtHeader;
    private XTextView txtDashboardHeader;
    private Toolbar toolbar;

    public Toolbar getToolbar() {
        return toolbar;
    }
    LocationProviderHelper providerHelper;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = this;

        providerHelper = new LocationProviderHelper(BaseAppCompatActivity.this, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                Helper.setStringValue(PreferenceConstants.KEY_LATTITUDE, String.valueOf(location.getLatitude()));
                Helper.setStringValue(PreferenceConstants.KEY_LONGITUDE, String.valueOf(location.getLongitude()));
            }
        });

        if (!StringUtils.isValid(Helper.getStringValue(PreferenceConstants.KEY_LATTITUDE))) {
            Helper.setStringValue(PreferenceConstants.KEY_LATTITUDE, String.valueOf(0.0));
            Helper.setStringValue(PreferenceConstants.KEY_LONGITUDE, String.valueOf(0.0));
//            Log.e(TAG, "==lat==long==");
        }
    }

    public void setUpToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);


        txtDashboardHeader = (XTextView) toolbar.findViewById(R.id.txt_dashboard_header);
        txtHeader = (XTextView) toolbar.findViewById(R.id.txt_header);

    }

    public void updateToolbar(String toolbarTitle) {
        if (null != toolbarTitle) {
            txtHeader.setVisibility(View.VISIBLE);
            txtDashboardHeader.setVisibility(View.GONE);
            txtHeader.setText(toolbarTitle);
        } else {
            txtHeader.setVisibility(View.GONE);
            txtDashboardHeader.setVisibility(View.VISIBLE);
        }
    }

/*
    public void startMainActivity() {
        UiHelper.startMainActivity(this);
    }
*/

    public boolean isNetworkAvailable() {
        return Helper.isNetworkAvailable(this);
    }

    public void startProgress(boolean isCancelable) {
        UiHelper.startProgress(this, isCancelable);
    }

    public void stopProgress() {
        UiHelper.stopProgress(this);
    }

    public Toast toast(String toastMsg) {
        return UiHelper.toast(toastMsg);
    }

    @Override
    protected void onStop() {
        super.onStop();
        UiHelper.hideKeyboard(this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        UiHelper.hideKeyboard(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

}
