package com.weddingplus.user_interface.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.weddingplus.R;
import com.weddingplus.cloud_messaging.FirebaseMessaging;
import com.weddingplus.user_interface.BaseFragment;
import com.weddingplus.user_interface.adapters.recycler_adapters.NotificationsAdapter;
import com.weddingplus.user_interface.interfaces.NotificationListener;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.NotificationsResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by Manndeep Vachhani on 9/2/17.
 */
public class NotificationsFragment extends BaseFragment implements NotificationListener {

    Unbinder unbinder;
    NotificationsAdapter notificationsAdapter;
    @BindView(R.id.txt_no_data)
    XTextView txtNoData;
    @BindView(R.id.swiperefreshlayout)
    SwipeRefreshLayout swiperefreshlayout;
    NotificationManager notificationManager;
    @BindView(R.id.rcv_notifications)
    RecyclerView rcvNotifications;
    private boolean isnotificationfound = false;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notifications, container, false);
        unbinder = ButterKnife.bind(this, view);
        txtNoData.setText(R.string.no_notifications);

        notificationManager = (NotificationManager) mActivity.getSystemService(Context.NOTIFICATION_SERVICE);

        FirebaseMessaging.setNotificationListener(this);

        notificationsAdapter = new NotificationsAdapter(mActivity);
        rcvNotifications.setAdapter(notificationsAdapter);

        getNotifications();

        swiperefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNotifications();
            }
        });

        manageVisibility();

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void getNotifications() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<NotificationsResponse> callback = new BaseCallback<NotificationsResponse>() {
            @Override
            public void Success(Response<NotificationsResponse> response) {
                if (null != swiperefreshlayout) {

                    swiperefreshlayout.setRefreshing(false);
                }
                notificationsAdapter.clearData();
                notificationsAdapter.addAll(response.body().getData());
                manageVisibility();
                if (null != txtNoData) {
                    txtNoData.setVisibility(View.GONE);
                }

                if (response.body().getData().size() > 0) {
                    if (null != txtNoData) {

                        txtNoData.setVisibility(View.GONE);
                    }
                } else {
                    if (null != txtNoData) {

                        txtNoData.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void Failure(String message) {
                Log.e("Message,", "" + message);
                if (null != swiperefreshlayout) {

                    swiperefreshlayout.setRefreshing(false);
                }
//                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        if (null != swiperefreshlayout) {
            swiperefreshlayout.setRefreshing(true);
        }
        WebServiceHelper.getNotifications().enqueue(callback);
    }


    private void manageVisibility() {
        int itemCount = notificationsAdapter.getItemCount();
        if (itemCount > 0) {
            isnotificationfound = true;
        }
        else
        {
            isnotificationfound = false;
        }
        if(null!=getActivity()) {
            getActivity().invalidateOptionsMenu();
        }
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(isnotificationfound ? R.menu.clear_all : R.menu.invisible, menu);
        // inflater.inflate(R.menu.clear_all, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_clear_all) {
            UiHelper.showConfirmationDialog(mActivity, "Are you sure you want to delete all Notifications?", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if(isNetworkAvailable()) {
                        clearNotifications();
                    }
                }
            });
        }
        return super.onOptionsItemSelected(item);

    }

    private void clearNotifications() {

        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                notificationsAdapter.clearAll();
                txtNoData.setVisibility(notificationsAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
                manageVisibility();
            }

            @Override
            public void Failure(String message) {
                Log.e("Message,", "" + message);
                stopProgress();
                txtNoData.setVisibility(notificationsAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
//                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                stopProgress();
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(true);
        WebServiceHelper.clearNotifications().enqueue(callback);



    }

  /*  @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.invisible, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }*/

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void NotificationListener(String type) {
    }

    @Override
    public void onNotificationClick(String type, String message) {
        SetOnUiCall();
    }

    private void SetOnUiCall() {
        if (mActivity != null)
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getNotifications();
                }
            });
    }


    @Override
    public void onStop() {
        super.onStop();
        FirebaseMessaging.removeNotificationListener();
    }
}
