package com.weddingplus.user_interface.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.weddingplus.App;
import com.weddingplus.R;
import com.weddingplus.models.User;
import com.weddingplus.user_interface.BaseFragment;
import com.weddingplus.user_interface.interfaces.EventProfileApiRefresh;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.UserResponse;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by Manndeep Vachhani on 9/2/17.
 */
public class ProfileFragment extends BaseFragment {

    @BindView(R.id.tv_name)
    XTextView tvName;
    @BindView(R.id.tv_address)
    XTextView tvAddress;
    @BindView(R.id.tv_contact_no)
    XTextView tvContactNo;
    @BindView(R.id.tv_email)
    XTextView tvEmail;
    @BindView(R.id.btn_edit_profile)
    XButton btnEditProfile;
    Unbinder unbinder;

    Picasso picasso;
    @BindView(R.id.iv_user_imag)
    CircularImageView ivUserImag;
    @BindView(R.id.tv_company_code)
    XTextView tvCompanyCode;
    @BindView(R.id.tv_birth_date)
    XTextView tvBirthDate;
    @BindView(R.id.tv_reg_no)
    XTextView tvRegNo;
    @BindView(R.id.tv_faculty)
    XTextView tvFaculty;
    @BindView(R.id.tv_programme)
    XTextView tvProgramme;
    @BindView(R.id.tv_leval)
    XTextView tvLeval;
    @BindView(R.id.lay_profile_student)
    LinearLayout layProfileStudent;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        unbinder = ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        picasso = Picasso.with(mActivity);

        setUpUiData();

        return view;
    }

    private void setUpUiData() {
        User user = App.getCurrentUser();
        tvName.setText("" + user.getName());
        tvAddress.setText(user.getAddress());
        tvContactNo.setText(user.getContactNumber());
        tvEmail.setText(user.getEmail());
        tvCompanyCode.setText(user.getComponyCode());
        tvBirthDate.setText(user.getBirthDate());
        tvRegNo.setText(user.getReg_no());
        tvFaculty.setText(user.getFaculty());
        tvProgramme.setText(user.getProgram());
        tvLeval.setText(user.getLevel());

        if (StringUtils.equals(String.valueOf(App.getCurrentUser().getUserType()), "2")) {
            layProfileStudent.setVisibility(View.GONE);
        } else if (StringUtils.equals(String.valueOf(App.getCurrentUser().getUserType()), "3")) {
            layProfileStudent.setVisibility(View.VISIBLE);
        }
        if (StringUtils.isValid(user.getProfileUrl())) {
            picasso.load(user.getProfileUrl()).resize(UiHelper.getDeviceWidthInPercentage(50), 0).into(ivUserImag);
        }
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.invisible, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @OnClick(R.id.btn_edit_profile)
    public void onViewClicked() {
        if (StringUtils.equals(String.valueOf(App.getCurrentUser().getUserType()), "2")) {
            UiHelper.startUpdateProfileCorporateActivity(mActivity);
        } else if (StringUtils.equals(String.valueOf(App.getCurrentUser().getUserType()), "3")) {
            UiHelper.startUpdateProfileActivity(mActivity);
        }

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshApi(EventProfileApiRefresh refresh) {
        getMyProfile();
    }

    private void getMyProfile() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<UserResponse> callback = new BaseCallback<UserResponse>() {
            @Override
            public void Success(Response<UserResponse> response) {
                stopProgress();
                App.currentUser = response.body().getUser();
                setUpUiData();

            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.getMyProfile().enqueue(callback);
    }
}
