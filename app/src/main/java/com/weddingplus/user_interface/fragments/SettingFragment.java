package com.weddingplus.user_interface.fragments;

import android.content.ActivityNotFoundException;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weddingplus.App;
import com.weddingplus.R;
import com.weddingplus.user_interface.BaseFragment;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by Manndeep Vachhani on 9/2/17.
 */
public class SettingFragment extends BaseFragment {

    @BindView(R.id.cardview_change_pass)
    CardView cardviewChangePass;
    @BindView(R.id.cv_switch_notify)
    CardView cvSwitchNotify;
    @BindView(R.id.cardview_terms_of_use)
    CardView cardviewTermsOfUse;
    @BindView(R.id.cardview_privacy_policy)
    CardView cardviewPrivacyPolicy;
    @BindView(R.id.cardview_rate_app)
    CardView cardviewRateApp;
    @BindView(R.id.cardview_logout)
    CardView cardviewLogout;
    Unbinder unbinder;
    @BindView(R.id.iv_switch)
    SwitchCompat ivSwitch;
    @BindView(R.id.cardview_contact_us)
    CardView cardviewContactUs;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_setting, container, false);

        unbinder = ButterKnife.bind(this, view);

        setUpSwitch();


        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.invisible, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }


    @OnClick({R.id.cv_switch_notify,R.id.iv_switch, R.id.cardview_change_pass, R.id.cardview_terms_of_use, R.id.cardview_privacy_policy, R.id.cardview_rate_app, R.id.cardview_logout, R.id.cardview_contact_us})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.cv_switch_notify:
                switchApi();
                break;
            case R.id.iv_switch:
                switchApi();
                break;
            case R.id.cardview_change_pass:
                UiHelper.startChangePasswordActivity(mActivity);
                break;
            case R.id.cardview_terms_of_use:
                UiHelper.startTermCondtionActivity(mActivity);
                break;
            case R.id.cardview_privacy_policy:
                UiHelper.startPrivacyPolicyActivity(mActivity);
                break;
            case R.id.cardview_rate_app:
                Uri uri = Uri.parse("market://details?id=" + mActivity.getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (ActivityNotFoundException e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + mActivity.getPackageName())));
                }
                break;
            case R.id.cardview_logout:
                UiHelper.showConfirmationDialog(mActivity, getString(R.string.logout_confirmation), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        logout();

                    }
                });

                break;
            case R.id.cardview_contact_us:
                UiHelper.startContactUsActivity(mActivity);
                break;
        }
    }

    private void switchApi() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        final String isNotify;
        if (App.getCurrentUser().getIsNotify() == 1) {
            isNotify = "0";
        } else {
            isNotify = "1";
        }

        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                App.getCurrentUser().setIsNotify(Integer.parseInt(isNotify));
                setUpSwitch();
            }

            @Override
            public void Failure(String message) {
                stopProgress();
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.switchNotify(isNotify).enqueue(callback);
    }

    private void logout() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.logout().enqueue(callback);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    private void setUpSwitch() {
        if (App.getCurrentUser() != null) {
            if (App.getCurrentUser().getIsNotify() == 1) {
                ivSwitch.setChecked(true);
            } else {
                ivSwitch.setChecked(false);
            }
        }
    }
}
