package com.weddingplus.user_interface.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.content.FileProvider;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.weddingplus.BuildConfig;
import com.weddingplus.R;
import com.weddingplus.sync.PreferenceApp;
import com.weddingplus.sync.StudentCategoriesModel;
import com.weddingplus.user_interface.BaseFragment;
import com.weddingplus.user_interface.activities.EmergencyResponseActivity;
import com.weddingplus.user_interface.dialog.AddMediaDialog;
import com.weddingplus.user_interface.interfaces.EventSend;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manndeep Vachhani on 9/2/17.
 */
public class NewHomeFragment extends BaseFragment {


    Unbinder unbinder;

    @BindView(R.id.ll_chekin_reporter)
    LinearLayout llChekinReporter;
    @BindView(R.id.ll_emergency_service)
    LinearLayout llEmergencyService1;

    @BindView(R.id.ll_student_manager)
    LinearLayout llStudentManager;
    @BindView(R.id.ll_student_hub)
    LinearLayout llStudentHub;
/*    @BindView(R.id.ll_message_center)
    LinearLayout llMessageCenter1;*/

    @BindView(R.id.rl_sos)
    RelativeLayout rlSos;
    @BindView(R.id.action_divider)
    FrameLayout actionDivider;
    @BindView(R.id.ll_instruction)
    LinearLayout llInstruction;

    @BindView(R.id.txt_ussd)
    XTextView txtUssd;
    /* @BindView(R.id.fab)
     FloatingActionButton fab;*/
    PreferenceApp pref;


    Uri outputFileUri;
    File photoFile;
    MultipartBody.Part part;
    int attechtype;

    public static MultipartBody.Part createPartFileFromPath(String filepath) {
        File destination = new File(filepath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("video/*"), destination);
        return MultipartBody.Part.createFormData("attachment", destination.getName(), requestFile);
    }

    public static String getVideoFilePath(Uri uri, Context appCompatActivity) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = appCompatActivity.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            Log.e("video path", cursor.getString(column_index) + "");
            return cursor.getString(column_index);
        } else {
            return null;
        }

    }

    public static File createImageFile() throws IOException {
      /*  File myDir = new File(Environment.getExternalStorageDirectory() + "/" + "beLocum");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
*/
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        // File file = new File(myDir, "locum_" + System.currentTimeMillis() + ".jpg");
        return destination;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home_new, container, false);

        unbinder = ButterKnife.bind(this, view);
        EventBus.getDefault().register(this);
        if (Helper.getUserType().equalsIgnoreCase(INDIVIDUAL)) {
            //txtStudentManager.setText(getString(R.string.student_nmanager));
            // txtStudentHub.setText(getString(R.string.student_nhub));
        } else {
            // txtStudentManager.setText(getString(R.string.staff_nmanager));
            // txtStudentHub.setText(getString(R.string.staff_nhub));

        }
        txtUssd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                UiHelper.startUSSDActivity(mActivity);
            }
        });
        pref = new PreferenceApp(getActivity());

        if (is_Network_Available())
            getAllCategories();


        return view;
    }

    public boolean is_Network_Available() {
        boolean toReturn;
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        toReturn = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        return toReturn;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.invisible, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        EventBus.getDefault().unregister(this);
        unbinder.unbind();
    }



    private void getAllCategories() {
        WebServiceHelper.getAllCategories().enqueue(new Callback<StudentCategoriesModel>() {
            @Override
            public void onResponse(Call<StudentCategoriesModel> call, Response<StudentCategoriesModel> response) {
                StudentCategoriesModel allCategories = response.body();
                pref.setStudentCategories(allCategories);

            }

            @Override
            public void onFailure(Call<StudentCategoriesModel> call, Throwable t) {

            }
        });
    }

/*    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outputFileUri != null) {
            outState.putString("cameraImageUri", outputFileUri.toString());
        }

    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        if (savedInstanceState.containsKey("cameraImageUri")) {
            outputFileUri = Uri.parse(savedInstanceState.getString("cameraImageUri"));
        }
    }*/

    private void onSendMessagePost(String message, String caption) {

        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                if (response.body().isSuccess()) {
                    toast(response.body().getMessage());

                } else {
                    toast(response.body().getMessage());

                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.SendMessage(part,""+attechtype,caption,message).enqueue(callback);

    }

    @OnClick({R.id.ll_chekin_reporter, R.id.ll_emergency_service, R.id.ll_student_manager, R.id.ll_student_hub, R.id.ll_online_shop, R.id.rl_sos, R.id.ll_instruction})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_chekin_reporter:
                UiHelper.startCheckInActivity(mActivity);
                break;
            case R.id.ll_emergency_service:
                UiHelper.startEmergencyResponseActivity(mActivity);
                break;
            case R.id.ll_student_manager:
                UiHelper.startStudentManagerActivity(mActivity);
                break;
            case R.id.ll_student_hub:
                UiHelper.startGalleryActivity(mActivity);
                // UiHelper.startSocialActivity(mActivity);
//                UiHelper.startSocialCheckInActivity(mActivity);
                break;
            case R.id.ll_online_shop:
                UiHelper.startDonateActivity(mActivity);
//                UiHelper.starShoppingActivity(mActivity);
                break;
            case R.id.rl_sos:
                selectImage();
                //onSendMessagePost();
                break;
            case R.id.ll_instruction:
                UiHelper.startInstructionActivity(mActivity);
                break;
        }
    }

    private void selectImage() {

        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.take_video),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    cameraIntent("photo");
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                } else if (items[item].equals(getString(R.string.take_video))) {
                    cameraIntent("video");
                }
            }
        });
        builder.show();
    }

    private void cameraIntent(String flag) {
        if (flag.equals("photo")) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(mActivity.getPackageManager()) != null) {
                photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (photoFile != null) {
                    outputFileUri = FileProvider.getUriForFile(mActivity, BuildConfig.APPLICATION_ID + ".com.weddingplus.provider", photoFile);
                    // outputFileUri = Uri.fromFile(photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            }
     /*       Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);*/
        } else if (flag.equals("video")) {
            Intent captureVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            captureVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
            captureVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
            startActivityForResult(captureVideoIntent, REQUEST_VIDEO);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (requestCode == REQUEST_VIDEO)
                onSelectFromVideoResult(data);

        }
    }

    private void onCaptureImageResult(Intent data) {
        if (outputFileUri != null) {
            /*ContentResolver cr = mActivity.getContentResolver();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(cr, outputFileUri);
                Bitmap newbitmap = mark(bitmap,"Bayo Adelabu Exclusive",new Point(50,50));
                String file_path = Environment.getExternalStorageDirectory().getAbsolutePath() +
                        "/PhysicsSketchpad";
                File dir = new File(file_path);
                if(!dir.exists())
                    dir.mkdirs();
                File file = new File(dir, "sketchpad" + ".png");
                FileOutputStream fOut = new FileOutputStream(file);

                newbitmap.compress(Bitmap.CompressFormat.PNG, 85, fOut);
                fOut.flush();
                fOut.close();
            } catch (IOException e) {
                e.printStackTrace();
            }*/


            try {
                String selectedImagePath = photoFile.getPath();
                // path = selectedImagePath;
                // String selectedImagePath = outputFileUri.getPath();
                Log.e("selectedImagePath", "" + selectedImagePath);
                part = createPartImageFromBitmap(selectedImagePath);
                attechtype = 1;

                AddMediaDialog addMediaDialog = new AddMediaDialog(mActivity, selectedImagePath);
                if (!addMediaDialog.isShowing()) {
                    addMediaDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                }
                addMediaDialog.show();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    }

    private void onSelectFromVideoResult(Intent data) {
        int msec = MediaPlayer.create(mActivity, data.getData()).getDuration();
        Log.e("video length", "" + msec);                // minutes 911111
        if (msec > videolimit) {
            toast(getString(R.string.video_limit));
            part = null;
        } else {
            Log.d(TAG, "onSelectFromVideoResult: video come" + data.getData().getPath());
            String selectedVideoPath = getVideoFilePath(data.getData(), mActivity);
            //   path = selectedVideoPath;
            part = createPartFileFromPath(selectedVideoPath);
            attechtype = 2;
            AddMediaDialog addMediaDialog = new AddMediaDialog(mActivity, selectedVideoPath);
            if (!addMediaDialog.isShowing()) {
                addMediaDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
            }
            addMediaDialog.show();
        }

//        Uri selectedImageUri = data.getData();
//        String selectedPath = selectedImageUri.getPath();
//        System.out.println("SELECT_VIDEO Path : " + selectedPath);

    }

    public MultipartBody.Part createPartImageFromBitmap(String path) {

        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), new File(path));
        return MultipartBody.Part.createFormData("attachment", new File(path).getName(), reqFile1);

    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSendClick(EventSend send) {
        onSendMessagePost(send.getMessage(), send.getEmergency_category_id()); //getEmergency_category_id known as caption
    }

    public static Bitmap mark(Bitmap src, String watermark, Point location) {
        int w = src.getWidth();
        int h = src.getHeight();
        Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());

        Canvas canvas = new Canvas(result);
        canvas.drawBitmap(src, 0, 0, null);

        Paint paint = new Paint();
        paint.setColor(Color.RED);
      //  paint.setAlpha(100);
        paint.setTextSize(200);
        paint.setAntiAlias(true);
       // paint.setUnderlineText(underline);
        canvas.drawText(watermark, w/2-1000, h-100, paint);

        return result;
    }
}
