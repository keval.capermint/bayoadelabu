package com.weddingplus.user_interface.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.weddingplus.R;
import com.weddingplus.user_interface.BaseFragment;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.CartItemCountResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

import static com.weddingplus.utilities.PreferenceConstants.KEY_USER_NAME;

/**
 * Created by Manndeep Vachhani on 9/2/17.
 */
public class ShoppingFragment extends BaseFragment {


    Unbinder unbinder;

    Picasso picasso;
    @BindView(R.id.iv_header_logo_whatsapp)
    ImageView ivHeaderLogoWhatsapp;
    @BindView(R.id.iv_header_logo_fb)
    ImageView ivHeaderLogoFb;
    @BindView(R.id.iv_header_logo_twiter)
    ImageView ivHeaderLogoTwiter;
    @BindView(R.id.iv_header_logo_insta)
    ImageView ivHeaderLogoInsta;
    @BindView(R.id.iv_header_logo)
    LinearLayout ivHeaderLogo;
/*    @BindView(R.id.edt_search)
    XEditText edtSearch;
    @BindView(R.id.ll_search)
    LinearLayout llSearch;*/
    @BindView(R.id.lay_shop_couple)
    LinearLayout layShopCouple;
    @BindView(R.id.lay_shop_general)
    LinearLayout layShopGeneral;
    @BindView(R.id.txt_cart_count)
    XTextView txtCartCount;
    @BindView(R.id.txt_welcome_msg)
    XTextView txtWelcomeMsg;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shopping, container, false);
        unbinder = ButterKnife.bind(this, view);
        picasso = Picasso.with(mActivity);

        setUpUiData();


        return view;
    }

    private void setUpUiData() {
    txtWelcomeMsg.setText(""+"Hello " + Helper.getStringValue(KEY_USER_NAME) + ", Enjoy your Shopping");
    }

    @Override
    public void onResume() {
        super.onResume();
        getItemCount();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.invisible, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();

    }

    private void getItemCount() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<CartItemCountResponse> callback = new BaseCallback<CartItemCountResponse>() {
            @Override
            public void Success(Response<CartItemCountResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    txtCartCount.setVisibility(View.VISIBLE);
                    txtCartCount.setText(response.body().getData());
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);

        WebServiceHelper.getCartItemCount().enqueue(callback);
    }

    @OnClick({R.id.iv_header_logo_whatsapp, R.id.iv_header_logo_fb, R.id.iv_header_logo_twiter, R.id.iv_header_logo_insta, R.id.lay_shop_couple, R.id.lay_shop_general, R.id.img_cart, R.id.img_user})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header_logo_whatsapp:
                Helper.WhatsAppIntent(mActivity);
                break;
            case R.id.iv_header_logo_fb:
                String url = "https://www.facebook.com/EppmeApp-192065647999244/";
                Helper.AppRedirectViewIntent(mActivity, url);
                break;
            case R.id.iv_header_logo_twiter:
                String url1 = "https://twitter.com/EppmeApp";
                Helper.AppRedirectViewIntent(mActivity, url1);
                break;
            case R.id.iv_header_logo_insta:
                String url2 = "https://www.instagram.com/eppmeapp/";
                Helper.AppRedirectViewIntent(mActivity, url2);
                break;
            case R.id.lay_shop_couple:
                UiHelper.startProductListActivity(mActivity, "SC");
                break;
            case R.id.lay_shop_general:
                UiHelper.startProductListActivity(mActivity, "GS");
                break;
            case R.id.img_user:
                break;
            case R.id.img_cart:
                UiHelper.starCartActivity(mActivity);
                break;
        }
    }
}
