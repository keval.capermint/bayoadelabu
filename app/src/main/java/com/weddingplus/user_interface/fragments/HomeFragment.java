package com.weddingplus.user_interface.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.andexert.library.RippleView;
import com.weddingplus.R;
import com.weddingplus.user_interface.BaseFragment;
import com.weddingplus.user_interface.dialog.KidnapDialog;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by Manndeep Vachhani on 9/2/17.
 */
public class HomeFragment extends BaseFragment {

    @BindView(R.id.rl_check_in)
    RelativeLayout rlCheckIn;
    @BindView(R.id.rl_emergency_res)
    RelativeLayout rlEmergencyRes;
    @BindView(R.id.rl_socail)
    RelativeLayout rlSocail;
    @BindView(R.id.rl_other)
    RelativeLayout rlOther;
    @BindView(R.id.rl_sos)
    RelativeLayout rlSos;
    Unbinder unbinder;
    @BindView(R.id.ll_instruction)
    LinearLayout llInstruction;

    KidnapDialog kidnapDialog;
    @BindView(R.id.txt_checkin)
    XTextView txtCheckin;
    @BindView(R.id.action_divider)
    RippleView actionDivider;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        unbinder = ButterKnife.bind(this, view);

        if (Helper.getUserType().equalsIgnoreCase(INDIVIDUAL)) {
            txtCheckin.setText(getString(R.string.remote_reporter));
        } else {
            txtCheckin.setText(getString(R.string.remote_reporter));

        }
        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.invisible, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @OnClick({R.id.ll_instruction, R.id.rl_check_in, R.id.rl_emergency_res, R.id.rl_socail, R.id.rl_other, R.id.rl_sos})
    public void onViewClicked(View view) {
        switch (view.getId()) {

            case R.id.ll_instruction:
                UiHelper.startInstructionActivity(mActivity);
                break;
            case R.id.rl_check_in:
                UiHelper.startCheckInActivity(mActivity);
                break;
            case R.id.rl_emergency_res:
                UiHelper.startEmergencyResponseActivity(mActivity);
                break;
            case R.id.rl_socail:
                UiHelper.startSocialActivity(mActivity);
                break;
            case R.id.rl_other:
//                MainActivity.callDialog(mActivity);
                kidnapDialog = new KidnapDialog(mActivity);
                kidnapDialog.show();
                break;
            case R.id.rl_sos:
               // onSendMessagePost();
                break;
        }
    }
/*
    private void onSendMessagePost() {

        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                if (response.body().isSuccess()) {
                    toast(response.body().getMessage());

                } else {
//                    toast(response.body().getMessage());
                    toast("Invalid Number");
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast("Invalid Number");
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.SendMessage(part, attechtype, caption, message).enqueue(callback);

    }*/

}
