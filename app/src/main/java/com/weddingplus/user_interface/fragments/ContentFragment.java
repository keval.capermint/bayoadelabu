package com.weddingplus.user_interface.fragments;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toolbar;

import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.EmergencyResponseAdapter;
import com.weddingplus.utilities.CustomGridLayoutManager;

import java.util.ArrayList;

/**
 * Created by cap-13-pv-16 on 8/9/18.
 */

public class ContentFragment extends Fragment {

    ArrayList<Category> mCategorieslist=new ArrayList<>();
    EmergencyResponseAdapter emergencyResponseAdapter;
    public static BaseAppCompatActivity mContext;
    public ContentFragment()
    {

    }

    public static Fragment newInstance(BaseAppCompatActivity mActivity, ArrayList<Category> list, int position) {
        Bundle args = new Bundle();
        mContext=mActivity;
        //args.putString("title", title);
        args.putParcelableArrayList("list",list);
        args.putInt("position", position);
        ContentFragment fragment = new ContentFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_content, container, false);
        initRecyclerView(view);
        return view;
    }



    private void initRecyclerView(View view) {
        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.rcv_emergency_response);
        emergencyResponseAdapter=new EmergencyResponseAdapter(mContext){
            @Override
            protected void onItemClick(int adapterPosition, Category category) {
                super.onItemClick(adapterPosition, category);

            }
        };
 /*       CustomGridLayoutManager customGridLayoutManager=new CustomGridLayoutManager(getActivity());
        customGridLayoutManager.setScrollEnabled(false);
        recyclerView.setLayoutManager(customGridLayoutManager);*/
        recyclerView.setAdapter(emergencyResponseAdapter);
        emergencyResponseAdapter.addAll(getCategories());
        /*recyclerView.addOnItemTouchListener(new RecyclerView.OnItemTouchListener() {
            @Override
            public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }

            @Override
            public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            }

            @Override
            public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

            }
        });*/


    }

/*    public String getTitle() {
        return getArguments().getString("title");
    }*/
    public ArrayList<Category> getCategories() {
        return getArguments().getParcelableArrayList("list");
    }
    public int getPosition() {
        return getArguments().getInt("position");
    }

}
