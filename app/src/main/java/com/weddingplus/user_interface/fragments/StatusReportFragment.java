package com.weddingplus.user_interface.fragments;

import android.app.NotificationManager;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weddingplus.R;
import com.weddingplus.cloud_messaging.FirebaseMessaging;
import com.weddingplus.user_interface.BaseFragment;
import com.weddingplus.user_interface.adapters.recycler_adapters.StatusReportAdapter;
import com.weddingplus.user_interface.interfaces.NotificationListener;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.GetStudentStatusResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import retrofit2.Response;

/**
 * Created by Manndeep Vachhani on 9/2/17.
 */
public class StatusReportFragment extends BaseFragment implements NotificationListener {

    @BindView(R.id.rcv_history)
    RecyclerView rcvHistory;
    Unbinder unbinder;
    StatusReportAdapter historyAdapter;
    @BindView(R.id.txt_no_data)
    XTextView txtNoData;
    @BindView(R.id.swiperefreshlayout)
    SwipeRefreshLayout swiperefreshlayout;
    NotificationManager notificationManager;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        unbinder = ButterKnife.bind(this, view);
        txtNoData.setText(R.string.no_status_report);

        notificationManager = (NotificationManager) mActivity.getSystemService(Context.NOTIFICATION_SERVICE);

        FirebaseMessaging.setNotificationListener(this);

        historyAdapter = new StatusReportAdapter(mActivity);
        rcvHistory.setAdapter(historyAdapter);

        getStatusLog();

        swiperefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getStatusLog();
            }
        });

        return view;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    private void getStatusLog() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<GetStudentStatusResponse> callback = new BaseCallback<GetStudentStatusResponse>() {
            @Override
            public void Success(Response<GetStudentStatusResponse> response) {
                if (null != swiperefreshlayout) {

                    swiperefreshlayout.setRefreshing(false);
                }
                historyAdapter.clearData();
                historyAdapter.addAll(response.body().getData().getList());
                if (null != txtNoData) {
                    txtNoData.setVisibility(View.GONE);
                }

                if (response.body().getData().getList().size() > 0) {
                    if (null != txtNoData) {

                        txtNoData.setVisibility(View.GONE);
                    }
                } else {
                    if (null != txtNoData) {

                        txtNoData.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void Failure(String message) {
                Log.e("Message,",""+message);
                if (null != swiperefreshlayout) {

                    swiperefreshlayout.setRefreshing(false);
                }
//                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        if (null != swiperefreshlayout) {
            swiperefreshlayout.setRefreshing(true);
        }
        WebServiceHelper.getStdentSatusReport("").enqueue(callback);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.invisible, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        unbinder.unbind();
    }

    @Override
    public void NotificationListener(String type) {
    }

    @Override
    public void onNotificationClick(String type, String message) {
        SetOnUiCall();
    }

    private void SetOnUiCall() {
        if (mActivity != null)
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getStatusLog();
                }
            });
    }


    @Override
    public void onStop() {
        super.onStop();
        FirebaseMessaging.removeNotificationListener();
    }
}
