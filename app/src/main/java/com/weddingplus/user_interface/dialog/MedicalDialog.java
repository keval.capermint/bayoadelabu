package com.weddingplus.user_interface.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.interfaces.EventSend;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by keval on 30/6/17.
 */

public class MedicalDialog extends Dialog {
    Context context;
    BaseAppCompatActivity activity;
    @BindView(R.id.btn_send)
    XButton btnSend;
    @BindView(R.id.txt_dialog_title)
    XTextView txtDialogTitle;
    @BindView(R.id.radio_accident)
    RadioButton radioAccident;
    @BindView(R.id.radio_heart_attack)
    RadioButton radioHeartAttack;
    @BindView(R.id.radio_suffocation)
    RadioButton radioSuffocation;
    @BindView(R.id.radio_other)
    RadioButton radioOther;
    @BindView(R.id.myRadioGroup)
    RadioGroup myRadioGroup;
    @BindView(R.id.edt_additional_info)
    XEditText edtAdditionalInfo;

    String selectedOptions;
    String dialog_title;

    public MedicalDialog(Context context, String title) {
        super(context);
        this.activity = (BaseAppCompatActivity) context;
        this.dialog_title = title;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_medical);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id

        ButterKnife.bind(this);

        txtDialogTitle.setText(dialog_title);

        myRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                View radioButton = myRadioGroup.findViewById(checkedId);
                int index = myRadioGroup.indexOfChild(radioButton);
                // Add logic here

                switch (index) {
                    case 0: // first button
                        selectedOptions = radioAccident.getText().toString();
                        break;
                    case 1: // secondbutton
                        selectedOptions = radioHeartAttack.getText().toString();
                        break;
                    case 2: // 3button
                        selectedOptions = radioSuffocation.getText().toString();
                    case 3: // 4button
                        selectedOptions = radioOther.getText().toString();

                        break;
                }
            }
        });

    }

    @Override
    public void dismiss() {
        super.dismiss();
    }

    @OnClick(R.id.btn_send)
    public void onViewClicked() {
        if (StringUtils.isValid(selectedOptions)) {
            EventBus.getDefault().post(new EventSend(edtAdditionalInfo.getText().toString(), selectedOptions));
            dismiss();
        } else {
            UiHelper.toast(activity.getString(R.string.please_select_option));
        }

    }
}
