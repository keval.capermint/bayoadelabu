package com.weddingplus.user_interface.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.interfaces.EventSend;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by keval on 30/6/17.
 */

public class AddMediaDialog extends Dialog {
    Context context;
    BaseAppCompatActivity activity;
    String path;


    boolean isAttached = false;
    @BindView(R.id.txt_dialog_title)
    XTextView txtDialogTitle;
    @BindView(R.id.edt_caption)
    XEditText edtCaption;
    @BindView(R.id.edt_additional_info)
    XEditText edtAdditionalInfo;
    @BindView(R.id.txt_attach_url)
    XTextView txtAttachUrl;
    @BindView(R.id.ll_attch)
    LinearLayout llAttch;
    @BindView(R.id.btn_submit)
    XButton btnSubmit;

    public AddMediaDialog(Context context, String path) {
        super(context);
        this.activity = (BaseAppCompatActivity) context;
        this.path = path;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_add_media);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        getWindow().getAttributes().windowAnimations = R.style.DialogTheme;

        ButterKnife.bind(this);
        if (StringUtils.isValid(path)) {
            txtAttachUrl.setText(path);
        }

    }

    @Override
    public void dismiss() {
        super.dismiss();
    }


    @OnClick({R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_submit:
                if (!StringUtils.isValid(edtCaption.getText().toString())) {
                    UiHelper.toast("Please enter caption");
                    return;
                } else if (!StringUtils.isValid(edtAdditionalInfo.getText().toString())) {
                    UiHelper.toast("Please enter description");
                    return;
                } else if (!StringUtils.isValid(path)) {
                    UiHelper.toast("Media not found!");
                    return;
                }
                EventBus.getDefault().post(new EventSend(edtAdditionalInfo.getText().toString(), edtCaption.getText().toString()));
                dismiss();

                break;
        }
    }

    public void update(String file_url) {

        txtAttachUrl.setText(file_url);
        isAttached = true;

    }

}
