package com.weddingplus.user_interface.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by keval on 30/6/17.
 */

public class KidnapDialog extends Dialog {
    Context context;
    BaseAppCompatActivity activity;
    @BindView(R.id.btn_send)
    XButton btnSend;
    @BindView(R.id.txt_dialog_title)
    XTextView txtDialogTitle;
    @BindView(R.id.edt_desciption)
    XEditText edtDesciption;
    boolean isAttached = false;

    public KidnapDialog(Context context) {
        super(context);
        this.activity = (BaseAppCompatActivity) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_kidnap);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        getWindow().getAttributes().windowAnimations = R.style.DialogTheme; //style id

        ButterKnife.bind(this);


    }

    @Override
    public void dismiss() {
        super.dismiss();
    }


    @OnClick({R.id.btn_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_send:

                kidnapPost(edtDesciption.getText().toString());
                dismiss();
                break;
        }
    }

    private void kidnapPost(String Desc) {

        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                UiHelper.stopProgress(activity);
                if (response.body().isSuccess()) {
                    UiHelper.toast(response.body().getMessage());

                } else {
                    UiHelper.toast(response.body().getMessage());
                }
            }

            @Override
            public void Failure(String message) {
                UiHelper.stopProgress(activity);
                UiHelper.toast("Please try again later.");
               // UiHelper.toast(activity.getResources().getString(R.string.server_error));
            }

            @Override
            public void SessionExpired(String message) {
                UiHelper.toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(activity);
            }
        };
        UiHelper.startProgress(activity,false);
        WebServiceHelper.kidnap(Desc).enqueue(callback);

    }

}
