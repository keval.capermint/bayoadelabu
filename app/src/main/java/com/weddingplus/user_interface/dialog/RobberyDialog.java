package com.weddingplus.user_interface.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.v7.widget.AppCompatSpinner;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.interfaces.EventSend;
import com.weddingplus.user_interface.interfaces.EvnetDialogRefresh;
import com.weddingplus.user_interface.interfaces.RemoveImage;
import com.weddingplus.user_interface.interfaces.StopRecording;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;

import org.greenrobot.eventbus.EventBus;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by keval on 30/6/17.
 */

public class RobberyDialog extends Dialog {
    Context context;
    BaseAppCompatActivity activity;
    @BindView(R.id.btn_send)
    XButton btnSend;
    @BindView(R.id.myRadioGroup)
    RadioGroup myRadioGroup;
    @BindView(R.id.txt_dialog_title)
    XTextView txtDialogTitle;

    String dialog_title;
    @BindView(R.id.ll_attch)
    LinearLayout llAttch;
    @BindView(R.id.txt_attach_url)
    XTextView txtAttachUrl;
    @BindView(R.id.edt_additional_info)
    XEditText edtAdditionalInfo;
    String selectedOptions;
    @BindView(R.id.iv_attachment)
    ImageView ivAttachment;

    boolean isAttached = false;

    @BindView(R.id.rbt_here)
    RadioButton rbtHere;
    @BindView(R.id.rbt_1km)
    RadioButton rbt1km;
    @BindView(R.id.rbt_other)
    RadioButton rbtOther;
    @BindView(R.id.spinner_cat)
    AppCompatSpinner spinnerCat;

    ArrayList<String> main_cat = new ArrayList<>();

    public RobberyDialog(Context context, String title) {
        super(context);
        this.activity = (BaseAppCompatActivity) context;
        this.dialog_title = title;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_robary);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        getWindow().getAttributes().windowAnimations = R.style.DialogTheme;

        main_cat.add("Report Missings");
        main_cat.add("Robary Missings");
        main_cat.add("Rap Missings");

        ButterKnife.bind(this);

        txtDialogTitle.setText(dialog_title);

        myRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {
                View radioButton = myRadioGroup.findViewById(checkedId);
                int index = myRadioGroup.indexOfChild(radioButton);
                // Add logic here

                switch (index) {
                    case 0: // first button
//                        edtAdditionalInfo.setEnabled(false);
//                        edtAdditionalInfo.setText("");
                        selectedOptions = rbtHere.getText().toString();
//                        edtAdditionalInfo.setClickable(false);
//                        edtAdditionalInfo.setFocusable(false);

                        break;
                    case 1: // secondbutton
                        selectedOptions = rbt1km.getText().toString();

//                        edtAdditionalInfo.setEnabled(false);
//                        edtAdditionalInfo.setText("");

//                        edtAdditionalInfo.setClickable(false);
//                        edtAdditionalInfo.setFocusable(false);

                        break;
                    case 2: // secondbutton
//                        edtAdditionalInfo.setEnabled(true);
                        selectedOptions = rbtOther.getText().toString();
//                        edtAdditionalInfo.setFocusable(true);

                        break;
                }
            }
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(activity, R.layout.item_spinner, main_cat);
        adapter.setDropDownViewResource(R.layout.item_spinner_dropdown);
        spinnerCat.setAdapter(adapter);


        spinnerCat.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                /*Category selectedCat = (Category) parent.getSelectedItem();
                catId="";
                catId = selectedCat.getICategoryId();*/

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    @Override
    public void dismiss() {
        super.dismiss();

    }


    @OnClick({R.id.ll_attch, R.id.btn_send, R.id.iv_attachment})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ll_attch:
                EventBus.getDefault().post(new EvnetDialogRefresh(""));
                break;
            case R.id.btn_send:
                if (btnSend.getText().toString().equals("Send")) {
                    if (StringUtils.isValid(selectedOptions)) {
                        UiHelper.hideKeyboard(activity);

                        EventBus.getDefault().post(new EventSend(edtAdditionalInfo.getText().toString(), selectedOptions));
                        dismiss();
                    } else {

                        UiHelper.toast(activity.getString(R.string.please_select_option));
                    }
                } else {
                    EventBus.getDefault().post(new StopRecording());
                }

                break;
            case R.id.iv_attachment: {
                if (isAttached) {
                    removefile();
                }

            }
        }
    }

    public void removefile() {

        ivAttachment.setImageResource(R.drawable.iv_plus_icon);
        EventBus.getDefault().post(new RemoveImage());
        txtAttachUrl.setText("Photo / Video / Voice");
        isAttached = false;
    }

    public void update(String file_url) {
        txtAttachUrl.setText(file_url);
        isAttached = true;
        ivAttachment.setImageResource(R.drawable.iv_cross);

    }

    public void updateudiofinish() {
        txtAttachUrl.setText("Audio recorded");
        isAttached = true;
        ivAttachment.setImageResource(R.drawable.iv_cross);
        btnSend.setText("Send");
        llAttch.setEnabled(true);

    }

    public void updateBtntext(String status) {
        if (status.equals("start")) {
            btnSend.setText("Stop voice record");
            llAttch.setEnabled(false);
        } else {
            txtAttachUrl.setText("Audio recorded");
            isAttached = true;
            ivAttachment.setImageResource(R.drawable.iv_cross);
            btnSend.setText("Send");
            llAttch.setEnabled(true);
        }

    }
}
