package com.weddingplus.user_interface.dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.interfaces.EventSend;
import com.weddingplus.user_interface.interfaces.EvnetDialogRefresh;
import com.weddingplus.user_interface.interfaces.RemoveImage;
import com.weddingplus.user_interface.interfaces.StopRecording;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;

import org.greenrobot.eventbus.EventBus;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by keval on 30/6/17.
 */

public class OtherEmergencyDialog extends Dialog {
    Context context;
    BaseAppCompatActivity activity;
    @BindView(R.id.btn_send)
    XButton btnSend;
    String type;
    @BindView(R.id.ll_attch)
    LinearLayout llAttch;
    @BindView(R.id.txt_dialog_title)
    XTextView txtDialogTitle;
    @BindView(R.id.iv_attachment)
    ImageView ivAttachment;
    @BindView(R.id.edt_additional_info)
    XEditText edtAdditionalInfo;
    @BindView(R.id.txt_attach_url)
    XTextView txtAttachUrl;

    boolean isAttached = false;

    public OtherEmergencyDialog(Context context, String type) {
        super(context);
        this.activity = (BaseAppCompatActivity) context;
        this.type = type;

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_other_emergency);
        getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
//        getWindow().getAttributes().windowAnimations = R.style.DialogTheme;

        ButterKnife.bind(this);
        if (StringUtils.isValid(type)) {
            llAttch.setVisibility(View.GONE);
        } else {
            llAttch.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void dismiss() {
        super.dismiss();
    }


    @OnClick({R.id.iv_attachment, R.id.ll_attch, R.id.btn_send})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_attachment:

                if (isAttached) {
                    removefile();
                }
                break;
            case R.id.ll_attch:
                EventBus.getDefault().post(new EvnetDialogRefresh(""));

                break;
            case R.id.btn_send:
                if (btnSend.getText().toString().equals("Send")) {
                    EventBus.getDefault().post(new EventSend(edtAdditionalInfo.getText().toString(), "0"));
                    dismiss();
                } else {
                    EventBus.getDefault().post(new StopRecording());
                }
                break;
        }
    }

    public void removefile() {

        ivAttachment.setImageResource(R.drawable.iv_plus_icon);
        EventBus.getDefault().post(new RemoveImage());
        txtAttachUrl.setText("Photo / Video / Voice");
        isAttached = false;
    }

    public void update(String file_url) {

        txtAttachUrl.setText(file_url);
        isAttached = true;
        ivAttachment.setImageResource(R.drawable.iv_cross);

    }

    public void updateudiofinish() {
        txtAttachUrl.setText("Audio recorded");
        isAttached = true;
        ivAttachment.setImageResource(R.drawable.iv_cross);
        btnSend.setText("Send");
        llAttch.setEnabled(true);

    }

    public void updateBtntext(String status) {
        if (status.equals("start")) {
            btnSend.setText("Stop voice record");
            llAttch.setEnabled(false);
        } else {
            txtAttachUrl.setText("Audio recorded");
            isAttached = true;
            ivAttachment.setImageResource(R.drawable.iv_cross);
            btnSend.setText("Send");
            llAttch.setEnabled(true);
        }
    }
}
