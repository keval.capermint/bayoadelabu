package com.weddingplus.user_interface;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.widget.Toast;

import com.weddingplus.utilities.AppConstants;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;

/**
 * Created by Manndeep Vachhani on 6/2/17.
 */
public class BaseFragment extends Fragment implements AppConstants {

    public BaseAppCompatActivity mActivity;
    public static String TAG = BaseFragment.class.getSimpleName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mActivity = (BaseAppCompatActivity) getActivity();
        setHasOptionsMenu(true);
    }

/*
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_cart) {
            UiHelper.startCartActivity(mActivity);
        }
        return super.onOptionsItemSelected(item);
    }
*/

    public Toast toast(String toastMsg) {
        return UiHelper.toast(toastMsg);
    }

    @Override
    public void onStop() {
        super.onStop();
        UiHelper.hideKeyboard(mActivity);
    }

    @Override
    public void onPause() {
        super.onPause();
        UiHelper.hideKeyboard(mActivity);
    }

    public boolean isNetworkAvailable() {
        return Helper.isNetworkAvailable(mActivity);
    }

    public void startProgress(boolean isCancelable) {
        UiHelper.startProgress(mActivity, isCancelable);
    }



    public void stopProgress() {
        UiHelper.stopProgress(mActivity);
    }

}
