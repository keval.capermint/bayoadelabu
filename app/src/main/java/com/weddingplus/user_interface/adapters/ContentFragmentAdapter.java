package com.weddingplus.user_interface.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.weddingplus.models.Category;
import com.weddingplus.user_interface.fragments.ContentFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by cap-13-pv-16 on 8/9/18.
 */

public class ContentFragmentAdapter extends FragmentPagerAdapter {
        private List<Fragment> fragments = new ArrayList<>();

    public ContentFragmentAdapter(FragmentManager fm) {
            super(fm);
            this.fragments = new ArrayList<>();
        }

        @Override
        public Fragment getItem(int position) {
            return fragments.get(position);
        }

        @Override
        public int getCount() {
            return fragments.size();
        }

/*
        @Override
        public CharSequence getPageTitle(int position) {
            return ((ContentFragment)fragments.get(position)).getTitle();
        }
*/

        public static class Holder {
            private final List<Fragment> fragments = new ArrayList<>();
            private FragmentManager manager;
            public Holder(FragmentManager manager) {
                this.manager = manager;
            }

            public Holder add(Fragment f) {
                fragments.add(f);
                return this;
            }

          /*  public ContentFragmentAdapter set() {
                return new ContentFragmentAdapter(manager, fragments);
            }*/
        }

    public void addAll(List<Fragment> fragments) {
        this.fragments.addAll(fragments);
        notifyDataSetChanged();
    }

}
