package com.weddingplus.user_interface.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.weddingplus.R;
import com.weddingplus.views.XTextView;


/**
 * Adapter to manage listing of Navigation Items
 * Created by Manndeep Vachhani on 25/10/16.
 */
public class NavigationItemsAdapter extends BaseAdapter {

    private LayoutInflater mLayoutInflater;

    private int[] navigationTitles = {
            R.string.welcome_to_eppme,
            R.string.about,

            R.string.home,
            R.string.profile,
            R.string.status_reporter,
    /*        R.string.faqs,*/
            R.string.history,
    /*        R.string.shopping,*/
         /*   R.string.tips,*/
            R.string.settings,
    };


    private int[] navigationIcons = {
            R.drawable.iv_welcome,
            R.drawable.iv_menu_about,
            R.drawable.iv_menu_home,
            R.drawable.iv_menu_profile,
            R.drawable.iv_menu_status_report,
 /*           R.drawable.iv_faqs,*/
            R.drawable.iv_menu_history,
         /*   R.drawable.ic_cart,*/
           /* R.drawable.iv_tips,*/
            R.drawable.iv_menu_settings,
    };


    public static final int NAV_POSITION_WEL_COME = 0;
    public static final int NAV_POSITION_ABOUT = 1;
    /*public static final int NAV_POSITION_WEDDING_INFO = 2;*/
    public static final int NAV_POSITION_HOME = 2;
    public static final int NAV_POSITION_PROFILE = 3;
    public static final int NAV_POSITION_NOTIFICATIONS = 4;
   // public static final int NAV_POSITION_FAQS = 5;
    public static final int NAV_POSITION_HISTORY = 5;//6
   // public static final int NAV_POSITION_SHOPPING = 7;//6
   // public static final int NAV_POSITION_TIPS = 7;
    public static final int NAV_POSITION_SETTINGS = 6;//8

    public NavigationItemsAdapter(Context context) {
        this.mLayoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return navigationTitles.length;
    }

    @Override
    public Integer getItem(int position) {
        return navigationTitles[position];
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (null == convertView) {
            convertView = mLayoutInflater.inflate(R.layout.item_drawer_menu, parent, false);
        }

        XTextView tvText = (XTextView) convertView.findViewById(R.id.txt_title);
        tvText.setText(navigationTitles[position]);


        ImageView iv = (ImageView) convertView.findViewById(R.id.iv);
        iv.setImageResource(navigationIcons[position]);


        return convertView;
    }
}
