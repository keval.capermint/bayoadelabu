package com.weddingplus.user_interface.adapters.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.weddingplus.R;
import com.weddingplus.models.ProductData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.RoundedCornersTransformation;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 29/6/17.
 */

public class ProductListAdapter extends RecyclerView.Adapter<ProductListAdapter.CustomViewHolder> {



    private LayoutInflater mInflater;
    private BaseAppCompatActivity mContext;

    ArrayList<ProductData> mArrayList;
    Picasso picasso;

    public ProductListAdapter(BaseAppCompatActivity context) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mArrayList = new ArrayList<>();
        picasso = Picasso.with(mContext);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_product, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final ProductData productData = mArrayList.get(holder.getAdapterPosition());

        if (StringUtils.isValid(productData.getAttachment())) {
            picasso.load(productData.getAttachment()).placeholder(R.mipmap.ic_launcher).transform(new RoundedCornersTransformation(5, 0, RoundedCornersTransformation.CornerType.TOP)).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(holder.imgProduct);
        } else {
//            picasso.load(R.drawable.capsule_yellow).into(holder.ivCat);
        }
        holder.txtProductName.setText(productData.getName());
        holder.txtProductPrice.setText(mContext.getResources().getString(R.string.currency_symbol, productData.getPrice()));

        holder.layItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onItemClick(productData,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();

    }
    public void clear() {
        this.mArrayList.clear();
        notifyDataSetChanged();
    }
    public void addAll(ArrayList<ProductData> result) {
        mArrayList.addAll(result);
        notifyDataSetChanged();
    }

    public void updateAll(ArrayList<ProductData> mArrayList) {
        this.mArrayList = new ArrayList<>();
        this.mArrayList.addAll(mArrayList);
        notifyDataSetChanged();
//        notifyItemRangeInserted(getItemCount(), mArrayList.size());
    }


    public void clearData() {
        mArrayList.clear();
        notifyDataSetChanged();
    }
    protected void onItemClick(ProductData data,int position){}

    class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_product)
        ImageView imgProduct;
        @BindView(R.id.txt_product_name)
        XTextView txtProductName;
        @BindView(R.id.txt_product_price)
        XTextView txtProductPrice;
        @BindView(R.id.lay_item)
        LinearLayout layItem;

        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
