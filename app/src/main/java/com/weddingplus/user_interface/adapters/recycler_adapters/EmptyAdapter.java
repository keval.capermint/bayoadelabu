package com.weddingplus.user_interface.adapters.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weddingplus.R;
import com.weddingplus.utilities.AppConstants;

import butterknife.ButterKnife;


/**
 * Created by Manndeep Vachhani on 17/11/16.
 */
public class EmptyAdapter extends RecyclerView.Adapter<EmptyAdapter.CustomViewHolder> implements AppConstants {

    //    private ArrayList<Partner> mArrayList;

    /**
     * Constructor to get instance of the class
     *
     * @param context context
     */
    public EmptyAdapter(Context context) {
//        this.mArrayList = new ArrayList<>();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_drawer_menu, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, int position) {
/*
        final Partner mPartner = mArrayList.get(position);

        holder.txtName.setText(mPartner.getName());
        holder.txtProfileStatus.setText(mPartner.mGetProfileStatus());
        holder.txtTime.setText(Helper.getDateFromMillis(mPartner.getUpdatedTime()));
        holder.progress.setProgress(mPartner.getProfileStatus());
        holder.imgProfile.setImageResource(
                StringUtils.equals(mPartner.getGender(), GENDER_MALE) ?
                        R.drawable.img_profile_male : R.drawable.img_profile_female);

        holder.lytRoot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(holder.getAdapterPosition(), mPartner);
            }
        });
*/
    }

    @Override
    public int getItemCount() {
        return 10;
    }

/*
    protected void onItemClick(int position, Partner item) {
    }

    public void addAll(ArrayList<Partner> list) {
        int start = getItemCount();
        mArrayList.addAll(list);
        notifyItemRangeInserted(start, list.size());
    }

    */
/**
 * Clear the list of existing {@link Partner} and update UI
 *//*

    public void clearAll() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    */
/**
 * Remove the partner from the list
 *
 * @param partner - Partner which should e removed.
 *//*

    public void removeItem(Partner partner) {
        if (mArrayList.contains(partner)) {
            int position = mArrayList.indexOf(partner);
            mArrayList.remove(partner);
            notifyItemRemoved(position);

        }
    }
*/

    /**
     * ViewHolder class to hold announcement view
     * Created by Manndeep Vachhani on 17/11/16.
     */
    class CustomViewHolder extends RecyclerView.ViewHolder {

        /**
         * Constructor to get instance of Announcement list view item
         *
         * @param view empty view
         */
        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
