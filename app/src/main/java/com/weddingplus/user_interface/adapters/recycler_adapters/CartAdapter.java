package com.weddingplus.user_interface.adapters.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.squareup.picasso.Picasso;
import com.weddingplus.R;
import com.weddingplus.models.CartData;
import com.weddingplus.models.ProductData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.RoundedCornersTransformation;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 29/6/17.
 */

public class CartAdapter extends RecyclerView.Adapter<CartAdapter.CustomViewHolder> {



    private LayoutInflater mInflater;
    private BaseAppCompatActivity mContext;

    ArrayList<CartData> mArrayList;
    Picasso picasso;

    public CartAdapter(BaseAppCompatActivity context) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mArrayList = new ArrayList<>();
        picasso = Picasso.with(mContext);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_cart, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final CartData mCartData = mArrayList.get(holder.getAdapterPosition());

        if (StringUtils.isValid(mCartData.getAttachment())) {
            picasso.load(mCartData.getAttachment()).placeholder(R.mipmap.ic_launcher).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(holder.imgProduct);
        } else {
//            picasso.load(R.drawable.capsule_yellow).into(holder.ivCat);
        }
        holder.txtProductName.setText(mCartData.getName());
        holder.txtProductPrice.setText(mContext.getResources().getString(R.string.currency_symbol, mCartData.getPrice()));
        holder.txtProductQty.setText("Quantity " + mCartData.getNo_of_item());

      holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteClick(mCartData,position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();

    }

    public void addAll(ArrayList<CartData> result) {
        mArrayList.addAll(result);
        notifyDataSetChanged();
    }
    public void updateAll(ArrayList<CartData> result) {
        mArrayList=new ArrayList<>();
        mArrayList.addAll(result);
        notifyDataSetChanged();
    }
    public void clearData() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    protected void onDeleteClick(CartData data, int position) {
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_delete)
        ImageView imgDelete;
        @BindView(R.id.img_product)
        ImageView imgProduct;
        @BindView(R.id.txt_product_name)
        XTextView txtProductName;
        @BindView(R.id.txt_product_qty)
        XTextView txtProductQty;
        @BindView(R.id.txt_product_price)
        XTextView txtProductPrice;
        @BindView(R.id.lay_item)
        LinearLayout layItem;

        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
