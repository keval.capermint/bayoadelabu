package com.weddingplus.user_interface.adapters.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.views.XTextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 29/6/17.
 */

public class SocialAdapter extends RecyclerView.Adapter<SocialAdapter.CustomViewHolder> {


    private LayoutInflater mInflater;
    private BaseAppCompatActivity mContext;

    ArrayList<Category> mArrayList;

    public SocialAdapter(BaseAppCompatActivity context) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mArrayList = new ArrayList<>();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_social, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final Category category = mArrayList.get(holder.getAdapterPosition());
        holder.txtCatName.setText(category.getCategory_name());
        holder.txtTotalCheckin.setText(String.valueOf(category.getCheckin_count()) + StringUtils.SPACE_STRING + "Check ins");

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(holder.getAdapterPosition(), category);
            }
        });
    }

    protected void onItemClick(int adapterPosition, Category category) {

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();

    }

    public void addAll(ArrayList<Category> result) {
        mArrayList.addAll(result);
        notifyDataSetChanged();
    }

    public void clearData() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_cat)
        ImageView ivCat;
        @BindView(R.id.txt_cat_name)
        XTextView txtCatName;
        @BindView(R.id.txt_total_checkin)
        XTextView txtTotalCheckin;
        @BindView(R.id.card_view)
        CardView cardView;

        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
