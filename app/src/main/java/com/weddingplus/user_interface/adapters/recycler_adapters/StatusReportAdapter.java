package com.weddingplus.user_interface.adapters.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.weddingplus.R;
import com.weddingplus.models.StudentHistoryDataDetail;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.views.XTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 29/6/17.
 */

public class StatusReportAdapter extends RecyclerView.Adapter<StatusReportAdapter.CustomViewHolder> {


    private LayoutInflater mInflater;
    private BaseAppCompatActivity mContext;

    ArrayList<StudentHistoryDataDetail> mArrayList;
    Picasso picasso;

    public StatusReportAdapter(BaseAppCompatActivity context) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mArrayList = new ArrayList<>();
        picasso = Picasso.with(mContext);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_history, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        StudentHistoryDataDetail historyDataDetail = mArrayList.get(holder.getAdapterPosition());

        if (StringUtils.isValid(historyDataDetail.getStudentCategoryImage())) {
            picasso.load(historyDataDetail.getStudentCategoryImage()).placeholder(R.mipmap.ic_launcher).into(holder.ivCat);
        }
        else {
//            picasso.load(R.drawable.capsule_yellow).into(holder.ivCat);
        }
        holder.txtTitle.setText(historyDataDetail.getStudentCategoryName());
        holder.txtDate.setText(historyDataDetail.getReportedDate());
//        holder.txtAddress.setText(UiHelper.getAddressFromLatLong(mContext, historyDataDetail.getLatitude(), historyDataDetail.getLongitude()));
        holder.txtAddress.setText(historyDataDetail.getAddress());

        if (historyDataDetail.getStatus().equalsIgnoreCase("created")) {
            holder.ivStatus.setImageResource(R.drawable.capsule_blue);
        } else if (historyDataDetail.getStatus().equalsIgnoreCase("process")) {
            holder.ivStatus.setImageResource(R.drawable.cirecle_yellow);
        } else if (historyDataDetail.getStatus().equalsIgnoreCase("completed")) {
            holder.ivStatus.setImageResource(R.drawable.cirecle_green);
        } else if (historyDataDetail.getStatus().equalsIgnoreCase("rejected")) {
            holder.ivStatus.setImageResource(R.drawable.cirecle_black);
        }
    }

    @Override
    public int getItemCount() {
        return mArrayList.size();

    }

    public void addAll(ArrayList<StudentHistoryDataDetail> result) {
        mArrayList.addAll(result);
        notifyDataSetChanged();
    }

    public void clearData() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_cat)
        ImageView ivCat;
        @BindView(R.id.txt_title)
        XTextView txtTitle;
        @BindView(R.id.txt_date)
        XTextView txtDate;
        @BindView(R.id.txt_address)
        XTextView txtAddress;
        @BindView(R.id.iv_status)
        ImageView ivStatus;

        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
