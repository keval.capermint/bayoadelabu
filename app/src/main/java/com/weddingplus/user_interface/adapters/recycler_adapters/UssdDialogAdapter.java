package com.weddingplus.user_interface.adapters.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.views.XTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 29/6/17.
 */

public class UssdDialogAdapter extends RecyclerView.Adapter<UssdDialogAdapter.CustomViewHolder> {



    private LayoutInflater mInflater;
    private BaseAppCompatActivity mContext;

    ArrayList<Category> mArrayList;
    Picasso picasso;

    public UssdDialogAdapter(BaseAppCompatActivity context) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mArrayList = new ArrayList<>();
        picasso = Picasso.with(mContext);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_ussd, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        Category category = mArrayList.get(holder.getAdapterPosition());

        holder.txtTitle.setText(category.getCategory_name());
        holder.txtId.setText(""+category.getCategory_id()+".");

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();

    }

    public void addAll(ArrayList<Category> result) {
        mArrayList.addAll(result);
        notifyDataSetChanged();
    }

    public void clearData() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_id)
        XTextView txtId;
        @BindView(R.id.txt_title)
        XTextView txtTitle;
        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
