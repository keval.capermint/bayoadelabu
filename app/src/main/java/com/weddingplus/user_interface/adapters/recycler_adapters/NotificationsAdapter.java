package com.weddingplus.user_interface.adapters.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.models.Notification;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.DoubleClickListener;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.views.XTextView;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 29/6/17.
 */

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.CustomViewHolder> {


    //public ArrayList<String> selectedNotificationId = new ArrayList<>();

    private LayoutInflater mInflater;
    private BaseAppCompatActivity mContext;
    public boolean isdoubletap=false;

    ArrayList<Notification> mArrayList;
    Picasso picasso;

    Notification mData;
    int dataPosition;

    public NotificationsAdapter(BaseAppCompatActivity context) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mArrayList = new ArrayList<>();
        picasso = Picasso.with(mContext);
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_notifications, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        Notification notification = mArrayList.get(position);
        //holder.checkBox.setChecked(notification.isChecked());

        if (notification.isAllClicked()) {
          //  holder.swipeLayout.open(false);
            holder.checkBox.setVisibility(View.VISIBLE);

        } else {

            holder.checkBox.setVisibility(View.GONE);
           // holder.swipeLayout.close(false);
        }



        holder.txtTitle.setText(notification.getTitle());
        holder.txtDate.setText(notification.getCreated_at());
        holder.txtDesc.setText(notification.getMessage());

        mData=notification;
        dataPosition=position;
        holder.checkBox.setTag(mData);


        holder.checkBox.setChecked(mArrayList.get(position).isSelected());

        holder.checkBox.setTag(mArrayList.get(position));

        holder.checkBox.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                AppCompatCheckBox cb = (AppCompatCheckBox) v;
                Notification model = (Notification) cb.getTag();

                model.setSelected(cb.isChecked());
                mArrayList.get(position).setSelected(cb.isChecked());

            }
        });
       /* holder.checkBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppCompatCheckBox cb = (AppCompatCheckBox) v;
                Notification activities = (Notification) cb.getTag();
                //  Notification activities = (Notification) mArrayList.get(dataPosition);

                activities.setChecked(true);
                mArrayList.set(dataPosition, activities);

                if (selectedNotificationId.contains(activities.getUnotification_id())) {
                    selectedNotificationId.remove(activities.getUnotification_id());
                } else {
                    selectedNotificationId.add(activities.getUnotification_id());
                }

              //  notifyItemChanged(dataPosition);



          *//*      AppCompatCheckBox cb = (AppCompatCheckBox) v;
                Notification activities = (Notification) cb.getTag();
                if (cb.isChecked()) {


                  //  Notification activities = (Notification) mArrayList.get(dataPosition);

                    if (selectedNotificationId.contains(activities.getUnotification_id())) {
                        activities.setChecked(false);
                        mArrayList.set(dataPosition, activities);
                        selectedNotificationId.remove(activities.getUnotification_id());
                    } else {
                        activities.setChecked(true);
                        mArrayList.set(dataPosition, activities);
                        selectedNotificationId.add(activities.getUnotification_id());
                    }

                    notifyItemChanged(dataPosition);
                } else {
                  //  AppCompatCheckBox cb = (AppCompatCheckBox) v;
                  //  Notification activities = (Notification) cb.getTag();
                   // Notification activities = (Notification) mArrayList.get(dataPosition);


                    if (selectedNotificationId.contains(activities.getUnotification_id())) {
                        activities.setChecked(false);
                        mArrayList.set(dataPosition, activities);
                        selectedNotificationId.remove(activities.getUnotification_id());
                    }
                    notifyItemChanged(dataPosition);
                }
            }*//*
            }

        });*/

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();

    }

    public void addAll(ArrayList<Notification> result) {
        mArrayList.addAll(result);
        notifyDataSetChanged();
    }

    public void clearData() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    public void clearAll() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    public void removeItem(int position) {
        this.mArrayList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mArrayList.size());

    }

    class CustomViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.check_box)
        AppCompatCheckBox checkBox;
        @BindView(R.id.txt_title)
        XTextView txtTitle;
        @BindView(R.id.txt_desc)
        XTextView txtDesc;
        @BindView(R.id.txt_date)
        XTextView txtDate;
        @BindView(R.id.card_view)
        LinearLayout cardView;

        @BindView(R.id.ll_listitem)
        LinearLayout llListitem;




        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);

        /*    cardView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return false;
                }
            });*/
            view.setOnClickListener(new DoubleClickListener() {
                @Override
                public void onDoubleClick() {
                    Log.e("double tap:","true");
                    if(!isdoubletap) {
                        for (int i = 0; i < mArrayList.size(); i++) {
                            Notification activities = (Notification) mArrayList.get(i);
                            activities.setAllClicked(true);
                            mArrayList.set(i, activities);

                        }
                        isdoubletap = true;
                    }
                    else
                    {
                        for (int i = 0; i < mArrayList.size(); i++) {
                            Notification activities = (Notification) mArrayList.get(i);
                            activities.setAllClicked(false);
                            mArrayList.set(i, activities);

                        }
                        isdoubletap = false;
                    }

                    notifyDataSetChanged();
                    onDoubleClickItem(mData, dataPosition,isdoubletap);
                }


            });



        }
    }
    protected void onDoubleClickItem(Notification activities, int position, boolean isdoubletap) {
    }

    public ArrayList<String> getSelectedNotificationId() {
        ArrayList<String> list=new ArrayList<>();
        for(int i=0;i<mArrayList.size();i++)
        {
            if(mArrayList.get(i).isSelected())
            {
                list.add(mArrayList.get(i).getUnotification_id());
            }

        }
        return  list;
       // return selectedNotificationId;
    }


    public void onCancelButtonClicked() {
        for (int i = 0; i < mArrayList.size(); i++) {
            Notification activities = (Notification) mArrayList.get(i);
            activities.setAllClicked(false);
            mArrayList.set(i, activities);
        }

        notifyDataSetChanged();
    }

    public void removeItem(ArrayList<String> strings) {
        for (int i = 0; i < mArrayList.size(); i++) {
            Notification activities = (Notification) mArrayList.get(i);
            if (i == strings.size()) {
                break;
            }
            if (StringUtils.equals(activities.getUnotification_id(), strings.get(i))) {
                mArrayList.remove(i);
            }


        }
        notifyDataSetChanged();
    }

    public void reArrangeItem() {
        for (int i = 0; i < mArrayList.size(); i++) {
            Notification activities = (Notification) mArrayList.get(i);
            activities.setSelected(false);
            activities.setAllClicked(false);
            mArrayList.set(i, activities);
        }

        notifyDataSetChanged();
    }

}
