package com.weddingplus.user_interface.adapters;

import android.annotation.SuppressLint;
import android.support.v4.view.PagerAdapter;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;

import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.EmergencyResponseAdapter;
import com.weddingplus.utilities.AutoPaginationScrollListener;

import java.util.ArrayList;

/**
 * Created by cap-13-pv-16 on 8/9/18.
 */

public class CustomPagerAdapter extends PagerAdapter {
    ArrayList<ArrayList<Category>> mCategorieslist = new ArrayList<>();
    EmergencyResponseAdapter emergencyResponseAdapter;
    private BaseAppCompatActivity mContext;
    private boolean isLoading = false;
    private int page = 1;
    private boolean hasMore = true;
    boolean isScrollup=false;
    public CustomPagerAdapter(BaseAppCompatActivity context) {
        mContext = context;
        android_gesture_detector=new Android_Gesture_Detector(){
            @Override
            protected void onTouchVInnter(boolean isScrollup1) {
                super.onTouchVInnter(isScrollup1);
                isScrollup=isScrollup1;
            }
        };
        mGestureDetector = new GestureDetector(mContext,android_gesture_detector);
    }
    GestureDetector mGestureDetector;
    Android_Gesture_Detector  android_gesture_detector;

    @SuppressLint("ClickableViewAccessibility")
    @Override
    public Object instantiateItem(ViewGroup collection, int position) {
        final ArrayList<Category> categories = mCategorieslist.get(position);
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.fragment_content, collection, false);
        final RecyclerView recyclerView = (RecyclerView) layout.findViewById(R.id.rcv_emergency_response);
        recyclerView.setNestedScrollingEnabled(false);


      /*  recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                GridLayoutManager layoutManager=GridLayoutManager.class.cast(recyclerView.getLayoutManager());
                int totalItemCount = layoutManager.getItemCount();
                int lastVisible = layoutManager.findLastVisibleItemPosition();

                boolean endHasBeenReached = lastVisible + 8 >= totalItemCount;
                if (totalItemCount > 0 && endHasBeenReached) {
                    //you have reached to the bottom of your recycler view
                    Log.e("last pos:","reach");

                }
            }
        });*/
        /*recyclerView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View rv, MotionEvent e) {
                int action = e.getAction();
                switch (action) {
                    case MotionEvent.ACTION_MOVE:
                        rv.getParent().requestDisallowInterceptTouchEvent(true);
                        break;
                }
                return false;
            }
        });*/
        emergencyResponseAdapter = new EmergencyResponseAdapter(mContext) {
            @Override
            protected void onItemClick(int adapterPosition, Category category) {
                super.onItemClick(adapterPosition, category);
                    onClick(category,adapterPosition);
            }
        };
        recyclerView.setAdapter(emergencyResponseAdapter);
    //    recyclerView.requestDisallowInterceptTouchEvent(true);
        recyclerView.addOnItemTouchListener(mOnItemTouchListener);
        recyclerView.addOnScrollListener(new AutoPaginationScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if (dx > 0) {
                    System.out.println("Scrolled Right");
                } else if (dx < 0) {
                    System.out.println("Scrolled Left");
                } else {
                    System.out.println("No Horizontal Scrolled");
                }

                if (dy > 0) {
                    System.out.println("Scrolled Downwards");
                    //isScrollup=false;
                } else if (dy < 0) {
                    System.out.println("Scrolled Upwards");
                   // isScrollup=true;
                } else {
                    System.out.println("No Vertical Scrolled");
                }
            }

            @Override
            protected void onScrolledUpOrDown(boolean toUp) {
                System.out.println("The UP/Down"+toUp);
            }

            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                switch (newState) {
                    case RecyclerView.SCROLL_STATE_IDLE:
                        System.out.println("The RecyclerView is not scrolling");
                        break;
                    case RecyclerView.SCROLL_STATE_DRAGGING:
                        System.out.println("Scrolling now");
                        break;
                    case RecyclerView.SCROLL_STATE_SETTLING:
                        System.out.println("Scroll Settling");
                        onTouchV(isScrollup);
                       // isScrollup=true;
                        break;

                }
            }
        });


        /*recyclerView.addOnScrollListener(new AutoPaginationScrollListener() {
            @Override
            protected void requestNewPage() {
                super.requestNewPage();
                if(hasMore) {
                    onTouchV(recyclerView);
                    hasMore = false;
                }
            }
        });*/

        emergencyResponseAdapter.addAll(categories);



        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(ViewGroup collection, int position, Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return mCategorieslist.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    public void addAll(ArrayList<ArrayList<Category>> categories) {
        mCategorieslist.addAll(categories);
        notifyDataSetChanged();
    }
      /*  @Override
        public CharSequence getPageTitle(int position) {
            ModelObject customPagerEnum = ModelObject.values()[position];
            return mContext.getString(customPagerEnum.getTitleResId());
        }*/

    protected void onClick(Category categories, int adapterPosition){}
    protected void onTouchV(boolean isScrollup){}

    RecyclerView.OnItemTouchListener mOnItemTouchListener = new RecyclerView.OnItemTouchListener() {
        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
            mGestureDetector.onTouchEvent(e);
          /*      if (e.getAction() == MotionEvent.ACTION_DOWN && rv.getScrollState() == RecyclerView.SCROLL_STATE_SETTLING) {
                    Log.d("okkk", "onInterceptTouchEvent: click performed");
                    rv.findChildViewUnder(e.getX(), e.getY()).performClick();
                    return true;
                }*/
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {

            mGestureDetector.onTouchEvent(e);

        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {}
    };
}
class Android_Gesture_Detector implements GestureDetector.OnGestureListener,
        GestureDetector.OnDoubleTapListener {

    @Override
    public boolean onDown(MotionEvent e) {
        Log.d("Gesture ", " onDown");
        return true;
    }

    @Override
    public boolean onSingleTapConfirmed(MotionEvent e) {
        Log.d("Gesture ", " onSingleTapConfirmed");
        return true;
    }

    @Override
    public boolean onSingleTapUp(MotionEvent e) {
        Log.d("Gesture ", " onSingleTapUp");
        return true;
    }

    @Override
    public void onShowPress(MotionEvent e) {
        Log.d("Gesture ", " onShowPress");
    }

    @Override
    public boolean onDoubleTap(MotionEvent e) {
        Log.d("Gesture ", " onDoubleTap");
        return true;
    }

    @Override
    public boolean onDoubleTapEvent(MotionEvent e) {
        Log.d("Gesture ", " onDoubleTapEvent");
        return true;
    }

    @Override
    public void onLongPress(MotionEvent e) {
        Log.d("Gesture ", " onLongPress");
    }

    @Override
    public boolean onScroll(MotionEvent e1, MotionEvent e2, float distanceX, float distanceY) {

        Log.d("Gesture ", " onScroll");
        if (e1.getY() < e2.getY()){
            Log.d("Gesture ", " Scroll Down");
            onTouchVInnter(true);
            return true;

        }
        if(e1.getY() > e2.getY()){
            Log.d("Gesture ", " Scroll Up");
            onTouchVInnter(false);
            return true;
        }
        return true;
    }

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        if (e1.getX() < e2.getX()) {
            Log.d("Gesture ", "Left to Right swipe: "+ e1.getX() + " - " + e2.getX());
            Log.d("Speed ", String.valueOf(velocityX) + " pixels/second");
        }
        if (e1.getX() > e2.getX()) {
            Log.d("Gesture ", "Right to Left swipe: "+ e1.getX() + " - " + e2.getX());
            Log.d("Speed ", String.valueOf(velocityX) + " pixels/second");
        }
        if (e1.getY() < e2.getY()) {
            Log.d("Gesture ", "Up to Down swipe: " + e1.getX() + " - " + e2.getX());
            Log.d("Speed ", String.valueOf(velocityY) + " pixels/second");
        }
        if (e1.getY() > e2.getY()) {
            Log.d("Gesture ", "Down to Up swipe: " + e1.getX() + " - " + e2.getX());
            Log.d("Speed ", String.valueOf(velocityY) + " pixels/second");
        }
        return true;

    }
    protected void onTouchVInnter(boolean isScrollup){}
}