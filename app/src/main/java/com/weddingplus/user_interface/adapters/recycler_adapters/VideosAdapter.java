package com.weddingplus.user_interface.adapters.recycler_adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.models.VideoData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 29/6/17.
 */

public class VideosAdapter extends RecyclerView.Adapter<VideosAdapter.CustomViewHolder> {


    private LayoutInflater mInflater;
    private BaseAppCompatActivity mContext;

    ArrayList<VideoData> mArrayList;

    public VideosAdapter(BaseAppCompatActivity context) {
        this.mContext = context;
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mArrayList = new ArrayList<>();
    }

    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.item_videos, parent, false);
        return new CustomViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final CustomViewHolder holder, final int position) {
        final VideoData videoData = mArrayList.get(holder.getAdapterPosition());
        if(StringUtils.isValid(videoData.getAttachment()))
        {
            Picasso.with(mContext).load(videoData.getAttachment()).resize(UiHelper.getDeviceWidthInPercentage(30),0).error(R.drawable.img_bg_shop_couple).into(holder.ivVideoThumb);
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClick(holder.getAdapterPosition(), videoData);
            }
        });
    }

    protected void onItemClick(int adapterPosition, VideoData videoData) {

    }

    @Override
    public int getItemCount() {
        return mArrayList.size();

    }

    public void addAll(ArrayList<VideoData> result) {
        mArrayList.addAll(result);
        notifyDataSetChanged();
    }

    public void clearData() {
        mArrayList.clear();
        notifyDataSetChanged();
    }

    class CustomViewHolder extends RecyclerView.ViewHolder {


        @BindView(R.id.iv_video_thumb)
        ImageView ivVideoThumb;
        @BindView(R.id.iv_play)
        ImageView ivPlay;
        CustomViewHolder(View view) {
            super(view);
            ButterKnife.bind(this, view);
        }
    }
}
