package com.weddingplus.user_interface.interfaces;

/**
 * Created by cap-27-mv-17 on 29/7/17.
 */

public class EventSend {
    private final String message;
    private final String select_option_title;



    public EventSend(String message, String select_option_title) {
        this.message = message;
        this.select_option_title = select_option_title;
    }

    public String getMessage() {
        return message;
    }
    public String getEmergency_category_id() {
        return select_option_title;
    }
}
