package com.weddingplus.user_interface.interfaces;

/**
 * Created by cap-27-mv-17 on 29/7/17.
 */

public class EventProfileApiRefresh {

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    String refresh;
    public EventProfileApiRefresh(String refresh) {
        this.refresh=refresh;
    }

}
