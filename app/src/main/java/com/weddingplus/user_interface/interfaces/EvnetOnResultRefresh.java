package com.weddingplus.user_interface.interfaces;

/**
 * Created by keval on 3/6/17.
 */

public class EvnetOnResultRefresh {
    private final String message;

    public EvnetOnResultRefresh(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}
