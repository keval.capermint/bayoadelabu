package com.weddingplus.user_interface.interfaces;

/**
 * Created by Manndeep Vachhani on 29/4/17.
 */

public interface NotificationListener {

    void NotificationListener(String type);

    void  onNotificationClick(
            String type,
            String total_credit
    );
}
