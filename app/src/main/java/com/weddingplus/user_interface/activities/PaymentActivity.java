package com.weddingplus.user_interface.activities;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.LinearLayout;

import com.goodiebag.pinview.Pinview;
import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.PaymenyResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by keval on 5/5/17.
 */

public class PaymentActivity extends BaseAppCompatActivity {


    @BindView(R.id.edtCardNo)
    XEditText edtCardNo;
    @BindView(R.id.edtCardname)
    XEditText edtCardname;
    @BindView(R.id.edtExpireDate)
    XEditText edtExpireDate;
    @BindView(R.id.edtCvv)
    XEditText edtCvv;
    @BindView(R.id.linExpireDate)
    LinearLayout linExpireDate;
    @BindView(R.id.btn_check_out)
    XButton btnCheckOut;

    String taxpersent;
    String totalamount;
    @BindView(R.id.tv_toolbar_title)
    XTextView tvToolbarTitle;

    String expMonth, expYear;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        setUpToolbar();
        tvToolbarTitle.setText("Payment");

        taxpersent = getIntent().getStringExtra("tax");
        totalamount = getIntent().getStringExtra("amount");

        edtExpireDate.addTextChangedListener(new TextWatcher() {
            int len = 0;

            @Override
            public void afterTextChanged(Editable s) {
                String str = edtExpireDate.getText().toString();
                if (str.length() == 2 && len < str.length()) {//len check for backspace
                    edtExpireDate.append("/");
                }
            }

            @Override
            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {

                String str = edtExpireDate.getText().toString();
                len = str.length();
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }


        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.btn_check_out)
    public void onViewClicked() {
        if (isValid()) {
            paymentCall();
        } else {
            toast(getResources().getString(R.string.card_valid));
        }
    }
    private void paymentCall() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<PaymenyResponse> callback = new BaseCallback<PaymenyResponse>() {
            @Override
            public void Success(Response<PaymenyResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    if (response.body().isSuccess()) {
                        toast("" + response.body().getMessage());
                        placeOrderCall(""+response.body().getReference());
                    }
                    else if(response.body().getStatus()==5)
                    {
                        showPinDialog(response.body().getReference());
                    }
                    else if(response.body().getStatus()==6)
                    {
                        showOTPDialog(response.body().getReference());
                    }
                    else {
                        toast("" + response.body().getMessage());
                    }
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast("" + message);
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);

        WebServiceHelper.paymentCall(edtCardNo.getText().toString(), edtCvv.getText().toString(), expMonth, expYear,totalamount).enqueue(callback);
    }

    private void placeOrderCall(String token) {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    toast(response.body().getMessage());
                    UiHelper.startMainActivity(mActivity, true);
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);

        WebServiceHelper.placeOrderCall(token, "S", "" + taxpersent).enqueue(callback);
    }

    private boolean isValid() {
        String expDate = edtExpireDate.getText().toString();
        String arrdate[];
        if (expDate.contains("/")) {

            arrdate = expDate.split("/");


        } else {
            arrdate = new String[2];
            arrdate[0] = "";
            arrdate[1] = "";
        }
        if(arrdate[0]!=null && arrdate[0].length()==2) {
            expMonth = arrdate[0];
        }
        else
        {
            expMonth="";
        }
        if(arrdate.length==2) {
            expYear = arrdate[1];
        }
        else
        {
            expYear="";
        }


        if (!StringUtils.isValid(edtCardNo.getText().toString(), 16)) {
             UiHelper.toast(getString(R.string.error_card_number));
            return false;
        }
        else if (!StringUtils.isValid(edtCardname.getText().toString())) {
            UiHelper.toast(getString(R.string.error_card_name));
            return false;
        } else if (!StringUtils.isValidCardDate(edtExpireDate.getText().toString())) {
             UiHelper.toast(getString(R.string.error_card_expiry));
            return false;
        } else if (!StringUtils.isValid(edtCvv.getText().toString(), 3)) {
             UiHelper.toast(getString(R.string.error_card_cvv));
            return false;
        }

        return true;
    }

    public void showPinDialog(final String reference)
    {
        final Dialog dialog = new Dialog(mActivity);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_pinview);
        dialog.setCanceledOnTouchOutside(false);

        // set values for custom dialog components - text, image and button
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final Pinview pinview = (Pinview) dialog.findViewById(R.id.pinview);
        XButton btn_submit = (XButton) dialog.findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isValidpin(pinview))
                {
                    if (!Helper.isNetworkAvailable(mActivity)) {
                        return;
                    }
                    BaseCallback<PaymenyResponse> callback = new BaseCallback<PaymenyResponse>() {
                        @Override
                        public void Success(Response<PaymenyResponse> response) {
                            stopProgress();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    dialog.dismiss();
                                    toast("" + response.body().getMessage());
                                    placeOrderCall(""+response.body().getReference());
                                }
                                else if(response.body().getStatus()==6)
                                {
                                    dialog.dismiss();
                                    showOTPDialog(reference);
                                }
                                else {
                                    dialog.dismiss();
                                    toast("" + response.body().getMessage());
                                }
                            }
                        }

                        @Override
                        public void Failure(String message) {
                            stopProgress();
                            Log.e("Message,", "" + message);

                        }

                        @Override
                        public void SessionExpired(String message) {
                            toast(message);
                            Helper.clearPreferences();
                            UiHelper.startWelcomeActivity(mActivity);
                        }
                    };
                    startProgress(false);

                    WebServiceHelper.verifyPinCall(""+reference,""+pinview.getValue()).enqueue(callback);

                }
            }
        });


        dialog.show();
    }
    public void showOTPDialog(final String referance)
    {
        final Dialog dialog = new Dialog(mActivity);
        // Include dialog.xml file
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_enter_otp);
        dialog.setCanceledOnTouchOutside(false);

        // set values for custom dialog components - text, image and button
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        final XEditText edt_otp = (XEditText) dialog.findViewById(R.id.edt_otp);
        XButton btn_submit = (XButton) dialog.findViewById(R.id.btn_submit);

        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(StringUtils.isValid(edt_otp.getText().toString())) {
                    if (!Helper.isNetworkAvailable(mActivity)) {
                        return;
                    }
                    BaseCallback<PaymenyResponse> callback = new BaseCallback<PaymenyResponse>() {
                        @Override
                        public void Success(Response<PaymenyResponse> response) {
                            stopProgress();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    dialog.dismiss();
                                    toast("" + response.body().getMessage());
                                    placeOrderCall(""+response.body().getReference());
                                } else {
                                    dialog.dismiss();
                                    toast("" + response.body().getMessage());
                                }
                            }
                        }

                        @Override
                        public void Failure(String message) {
                            stopProgress();
                            Log.e("Message,", "" + message);

                        }

                        @Override
                        public void SessionExpired(String message) {
                            toast(message);
                            Helper.clearPreferences();
                            UiHelper.startWelcomeActivity(mActivity);
                        }
                    };
                    startProgress(false);

                    WebServiceHelper.verifyOtpCall("" + referance, "" + edt_otp.getText().toString()).enqueue(callback);


                }
                else
                {
                    toast(getString(R.string.validation_otp));
                }
            }
        });


        dialog.show();
    }
    private boolean isValidpin(Pinview pinview) {
        if (!isNetworkAvailable()) {
            return false;
        }

        String otp = pinview.getValue();
        if (otp.length()!=4) {
            toast(getString(R.string.validation_pin));
            return false;
        }
        return true;
    }
}
