package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;

import butterknife.ButterKnife;

public class DonateActivity extends BaseAppCompatActivity{
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_donate);
        ButterKnife.bind(this);

        setUpToolbar();
    }
}
