package com.weddingplus.user_interface.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.weddingplus.R;
import com.weddingplus.models.CartData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.CartAdapter;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.CartListResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.weddingplus.utilities.PreferenceConstants.KEY_USER_NAME;

/**
 * Created by keval on 5/5/17.
 */

public class CartActivity extends BaseAppCompatActivity {


    @BindView(R.id.rcv_list)
    RecyclerView rcvList;
    @BindView(R.id.txt_product_name)
    XTextView txtProductName;
    @BindView(R.id.txt_sub_total)
    XTextView txtSubTotal;
    @BindView(R.id.txt_tax_amount)
    XTextView txtTaxAmount;
    @BindView(R.id.txt_total_amount)
    XTextView txtTotalAmount;
    CartAdapter mCartAdapter;
    @BindView(R.id.txt_cart_count)
    XTextView txtCartCount;
    @BindView(R.id.btn_check_out)
    XButton btnCheckOut;
    @BindView(R.id.lay_empty)
    CardView layEmpty;
    @BindView(R.id.lay_card)
    CardView layCard;

    String taxpersentage;
    String totalamount;
    @BindView(R.id.txt_welcome_msg)
    XTextView txtWelcomeMsg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);
        setUpToolbar();

        txtWelcomeMsg.setText(""+"Hello " + Helper.getStringValue(KEY_USER_NAME) + ", Enjoy your Shopping");
        mCartAdapter = new CartAdapter(mActivity) {
            @Override
            protected void onDeleteClick(final CartData data, int position) {
                super.onDeleteClick(data, position);
                UiHelper.showConfirmationDialog(mActivity, "Are you sure you want to delete?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isNetworkAvailable()) {
                            deleteCall(data.getId());
                        }
                    }
                });
            }
        };
        rcvList.setAdapter(mCartAdapter);

        getCartList();

    }

    private void getCartList() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<CartListResponse> callback = new BaseCallback<CartListResponse>() {
            @Override
            public void Success(Response<CartListResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    if (response.body().getData().getCartData().size() > 0) {
                        layCard.setVisibility(View.VISIBLE);
                        layEmpty.setVisibility(View.GONE);
                    } else {
                        layCard.setVisibility(View.GONE);
                        layEmpty.setVisibility(View.VISIBLE);
                    }

                    taxpersentage = response.body().getData().getTax_percentage();
                    totalamount=response.body().getData().getTotal_amount();
                    mCartAdapter.updateAll(response.body().getData().getCartData());
                    txtCartCount.setVisibility(View.VISIBLE);
                    txtCartCount.setText("" + response.body().getData().getCartData().size());

                    txtSubTotal.setText(getResources().getString(R.string.currency_symbol, response.body().getData().getSub_tot()));
                    txtTaxAmount.setText(getResources().getString(R.string.currency_symbol, response.body().getData().getTax_amount()));
                    txtTotalAmount.setText(getResources().getString(R.string.currency_symbol, response.body().getData().getTotal_amount()));

                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);

        WebServiceHelper.getCartList().enqueue(callback);
    }

    private void deleteCall(String id) {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    getCartList();
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.getDeleteItem(id).enqueue(callback);
    }

    @OnClick({R.id.iv_header_logo_whatsapp, R.id.iv_header_logo_fb, R.id.iv_header_logo_twiter, R.id.iv_header_logo_insta, R.id.ll_instruction, R.id.btn_check_out, R.id.img_cart, R.id.img_user})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header_logo_whatsapp:
                Helper.WhatsAppIntent(mActivity);
                break;
            case R.id.iv_header_logo_fb:
                String url = "https://www.facebook.com/EppmeApp-192065647999244/";
                Helper.AppRedirectViewIntent(mActivity, url);
                break;
            case R.id.iv_header_logo_twiter:
                String url1 = "https://twitter.com/EppmeApp";
                Helper.AppRedirectViewIntent(mActivity, url1);
                break;
            case R.id.iv_header_logo_insta:
                String url2 = "https://www.instagram.com/eppmeapp/";
                Helper.AppRedirectViewIntent(mActivity, url2);
                break;
            case R.id.ll_instruction:

                break;
            case R.id.btn_check_out:
                UiHelper.starPaymentActivity(mActivity, taxpersentage,totalamount);
                break;
            case R.id.img_user:
                break;
            case R.id.img_cart:
                break;

        }
    }


}
