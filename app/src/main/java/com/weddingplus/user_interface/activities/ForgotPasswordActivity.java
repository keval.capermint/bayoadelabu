package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manndeep Vachhani on 9/5/17.
 */

public class ForgotPasswordActivity extends BaseAppCompatActivity {


    @BindView(R.id.tv_toolbar_title)
    XTextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.ll_main)
    LinearLayout llMain;
    @BindView(R.id.edt_email)
    XEditText edtEmail;
    @BindView(R.id.btn_send)
    XButton btnSend;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        ButterKnife.bind(this);
        setUpToolbar();
        tvToolbarTitle.setText(getResources().getString(R.string.forgot_password_text));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.btn_send)
    public void onViewClicked() {
        if (validateData()) {
            ForgotPassWordApi();
        }
    }

    private void ForgotPassWordApi() {
        if (Helper.isNetworkAvailable(this)) {
            startProgress(false);
            WebServiceHelper.getForgotPasswordCall(edtEmail.getText().toString())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            stopProgress();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    UiHelper.toasterror(response.body().getMessage());
                                    finish();
                                } else {
                                    UiHelper.toasterror(response.body().getMessage());
                                }
                            } else {
                                UiHelper.toasterror(getString(R.string.login_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            UiHelper.toasterror(getString(R.string.login_error));
                            stopProgress();

                        }
                    });
        }

    }

    private boolean validateData() {
        if (!StringUtils.isValidEmail(edtEmail.getText().toString())) {
            UiHelper.toasterror(getString(R.string.email_validation_msg));
            return false;
        }
        return true;
    }
}
