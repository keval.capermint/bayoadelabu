package com.weddingplus.user_interface.activities;

import android.content.ComponentName;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.models.ImageData;
import com.weddingplus.models.VideoData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.PicturessAdapter;
import com.weddingplus.user_interface.adapters.recycler_adapters.VideosAdapter;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.ItemDecorationAlbumColumns;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.GalleryResponse;
import com.weddingplus.web_services.responses.HistoryResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by keval on 5/5/17.
 */

public class GallaryActivity extends BaseAppCompatActivity {


    @BindView(R.id.toolbar)
    Toolbar toolbar;


    VideosAdapter mVideosAdapter;
    PicturessAdapter mPicturessAdapter;

    @BindView(R.id.ll_instruction)
    LinearLayout llInstruction;
    @BindView(R.id.rcv_videos)
    RecyclerView rcvVideos;
    @BindView(R.id.lay_swipe_up)
    LinearLayout laySwipeUp;
    @BindView(R.id.rcv_images)
    RecyclerView rcvImages;
    @BindView(R.id.lay_swipe_up_iimage)
    LinearLayout laySwipeUpIimage;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallary);
        ButterKnife.bind(this);
        setUpToolbar();

        mVideosAdapter=new VideosAdapter(mActivity){
            @Override
            protected void onItemClick(int adapterPosition, VideoData videoData) {
                super.onItemClick(adapterPosition, videoData);
                UiHelper.startFullScreenVideoActivity(mActivity,videoData);
            }
        };
        rcvVideos.setAdapter(mVideosAdapter);

        mPicturessAdapter=new PicturessAdapter(mActivity){
            @Override
            protected void onItemClick(int adapterPosition, ImageData imageData) {
                super.onItemClick(adapterPosition, imageData);
                UiHelper.startImageFullViewActivity(mActivity,""+imageData.getAttachment());
            }
        };
        rcvImages.setAdapter(mPicturessAdapter);

        rcvImages.addItemDecoration(new ItemDecorationAlbumColumns(
                getResources().getDimensionPixelSize(R.dimen._5sdp),
                getResources().getDimensionPixelSize(R.dimen._75sdp)));

        getHistory();

    }

    private void getHistory() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<GalleryResponse> callback = new BaseCallback<GalleryResponse>() {
            @Override
            public void Success(Response<GalleryResponse> response) {

                mPicturessAdapter.addAll(response.body().getData().getResultImage());
                mVideosAdapter.addAll(response.body().getData().getResultVideo());

            }

            @Override
            public void Failure(String message) {
                Log.e("Message,",""+message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        WebServiceHelper.getMediaList("").enqueue(callback);
    }

    @OnClick({R.id.iv_header_logo_whatsapp, R.id.iv_header_logo_fb, R.id.iv_header_logo_twiter, R.id.iv_header_logo_insta, R.id.ll_instruction, R.id.lay_swipe_up, R.id.lay_swipe_up_iimage})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header_logo_whatsapp:
                Helper.WhatsAppIntent(mActivity);
                break;
            case R.id.iv_header_logo_fb:
                String url = "https://www.facebook.com/EppmeApp-192065647999244/";
                Helper.AppRedirectViewIntent(mActivity,url);
                break;
            case R.id.iv_header_logo_twiter:
                String url1 = "https://twitter.com/EppmeApp";
                Helper.AppRedirectViewIntent(mActivity,url1);
                break;
            case R.id.iv_header_logo_insta:
                String url2 = "https://www.instagram.com/eppmeapp/";
                Helper.AppRedirectViewIntent(mActivity,url2);
                break;
            case R.id.ll_instruction:

                break;
            case R.id.lay_swipe_up:
                break;
            case R.id.lay_swipe_up_iimage:
                break;
        }
    }
}
