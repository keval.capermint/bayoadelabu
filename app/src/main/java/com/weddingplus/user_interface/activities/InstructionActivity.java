package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.InstructionAdapter;
import com.weddingplus.views.XTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 30/6/17.
 */

public class InstructionActivity extends BaseAppCompatActivity {

    @BindView(R.id.txt_dashboard_header)
    XTextView txtDashboardHeader;
    @BindView(R.id.txt_header)
    XTextView txtHeader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
//    @BindView(R.id.rcv_instruction)
//    RecyclerView rcvInstruction;

    InstructionAdapter instructionAdapter;
    @BindView(R.id.txt_privacy_policy)
    XTextView txtPrivacyPolicy;
    @BindView(R.id.txt_instruction_second)
    XTextView txtInstructionSecond;
    @BindView(R.id.txt_instruction_thired)
    XTextView txtInstructionThired;
    @BindView(R.id.txt_instruction_fourth)
    XTextView txtInstructionFourth;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instruction);
        ButterKnife.bind(this);
        setUpToolbar();
        txtDashboardHeader.setText("Instructions");
        txtPrivacyPolicy.setText(Instruction_one);
        txtInstructionSecond.setText(Instruction_second);
        txtInstructionThired.setText(Instuction_thired);
        txtInstructionFourth.setText(Instruction_fourth);

//        instructionAdapter = new InstructionAdapter(this);
//        rcvInstruction.setAdapter(instructionAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }
}
