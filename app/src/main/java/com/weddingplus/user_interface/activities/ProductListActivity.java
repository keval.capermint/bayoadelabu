package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.weddingplus.R;
import com.weddingplus.models.ProductData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.ProductListAdapter;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.CartItemCountResponse;
import com.weddingplus.web_services.responses.ProductListResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.weddingplus.utilities.PreferenceConstants.KEY_USER_NAME;

/**
 * Created by keval on 5/5/17.
 */

public class ProductListActivity extends BaseAppCompatActivity {


    @BindView(R.id.iv_header_logo_whatsapp)
    ImageView ivHeaderLogoWhatsapp;
    @BindView(R.id.iv_header_logo_fb)
    ImageView ivHeaderLogoFb;
    @BindView(R.id.iv_header_logo_twiter)
    ImageView ivHeaderLogoTwiter;
    @BindView(R.id.iv_header_logo_insta)
    ImageView ivHeaderLogoInsta;
    @BindView(R.id.iv_header_logo)
    LinearLayout ivHeaderLogo;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.ll_instruction)
    LinearLayout llInstruction;
    @BindView(R.id.edt_search)
    XEditText edtSearch;
    @BindView(R.id.ll_search)
    LinearLayout llSearch;
    @BindView(R.id.rcv_product_list)
    RecyclerView rcvProductList;

    ProductListAdapter productListAdapter;

    String type;
    @BindView(R.id.txt_cart_count)
    XTextView txtCartCount;


    ArrayList<ProductData> mdata;
    ArrayList<ProductData> mdatafilter;
    @BindView(R.id.txt_welcome_msg)
    XTextView txtWelcomeMsg;
    @BindView(R.id.img_user)
    ImageView imgUser;
    @BindView(R.id.img_cart)
    FrameLayout imgCart;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_list);
        ButterKnife.bind(this);
        setUpToolbar();

        type = getIntent().getStringExtra(EXTRA_KEY_TYPE);
        txtWelcomeMsg.setText(""+"Hello " + Helper.getStringValue(KEY_USER_NAME) + ", Enjoy your Shopping");
        productListAdapter = new ProductListAdapter(mActivity) {
            @Override
            protected void onItemClick(ProductData data, int position) {
                super.onItemClick(data, position);

                UiHelper.startProductDetailsActivity(mActivity, data);

            }
        };
        rcvProductList.setAdapter(productListAdapter);

        getProductList();


        edtSearch.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (!charSequence.toString().isEmpty()) {
                    mdatafilter = new ArrayList<>();
                    if (null != mdata) {
                        for (ProductData cap : mdata) {

                            if (null != cap.getName()) {
                                if (cap.getName().toLowerCase().contains(charSequence.toString().toLowerCase())) {
                                    mdatafilter.add(cap);
                                }
                            }

                        }
                        productListAdapter.updateAll(mdatafilter);

                    }

                } else {
                    if (null != mdata) {
                        productListAdapter.updateAll(mdata);
                    }
                }
                //  textNotFound.setVisibility(articlesListAdapter.getCount() > 0 ? View.GONE : View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });

        edtSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    if (v.length() != 0) {
                        // TODO: 22/08/2016 Implement Search API mechanism
                        UiHelper.hideKeyboard(mActivity);
                    }
                    return true;
                }
                return false;
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        getItemCount();
    }

    private void getItemCount() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<CartItemCountResponse> callback = new BaseCallback<CartItemCountResponse>() {
            @Override
            public void Success(Response<CartItemCountResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    txtCartCount.setVisibility(View.VISIBLE);
                    txtCartCount.setText(response.body().getData());
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);

        WebServiceHelper.getCartItemCount().enqueue(callback);
    }

    private void getProductList() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<ProductListResponse> callback = new BaseCallback<ProductListResponse>() {
            @Override
            public void Success(Response<ProductListResponse> response) {

                mdata = new ArrayList<>();
                mdata.addAll(response.body().getProductData());
                mdatafilter = new ArrayList<>();
                mdatafilter = mdata;
                productListAdapter.clear();
                productListAdapter.addAll(mdatafilter);

            }

            @Override
            public void Failure(String message) {
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        WebServiceHelper.getProductList(type).enqueue(callback);
    }

    @OnClick({R.id.iv_header_logo_whatsapp, R.id.iv_header_logo_fb, R.id.iv_header_logo_twiter, R.id.iv_header_logo_insta, R.id.ll_instruction, R.id.img_user, R.id.img_cart})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header_logo_whatsapp:
                Helper.WhatsAppIntent(mActivity);
                break;
            case R.id.iv_header_logo_fb:
                String url = "https://www.facebook.com/EppmeApp-192065647999244/";
                Helper.AppRedirectViewIntent(mActivity, url);
                break;
            case R.id.iv_header_logo_twiter:
                String url1 = "https://twitter.com/EppmeApp";
                Helper.AppRedirectViewIntent(mActivity, url1);
                break;
            case R.id.iv_header_logo_insta:
                String url2 = "https://www.instagram.com/eppmeapp/";
                Helper.AppRedirectViewIntent(mActivity, url2);
                break;
            case R.id.ll_instruction:

                break;
            case R.id.img_user:
                break;
            case R.id.img_cart:
                UiHelper.starCartActivity(mActivity);
                break;

        }
    }

}
