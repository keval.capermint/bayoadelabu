package com.weddingplus.user_interface.activities;

import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;
import com.weddingplus.App;
import com.weddingplus.R;
import com.weddingplus.sync.NetworkSchedulerService;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.NavigationItemsAdapter;
import com.weddingplus.user_interface.dialog.KidnapDialog;
import com.weddingplus.user_interface.fragments.AboutFragment;
import com.weddingplus.user_interface.fragments.HistoryFragment;
import com.weddingplus.user_interface.fragments.NewHomeFragment;
import com.weddingplus.user_interface.fragments.ProfileFragment;
import com.weddingplus.user_interface.fragments.SettingFragment;
import com.weddingplus.user_interface.fragments.StatusReportFragment;
import com.weddingplus.user_interface.fragments.WelcomeEppmeFragment;
import com.weddingplus.user_interface.interfaces.EventProfileApiRefresh;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.UserResponse;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.weddingplus.utilities.PreferenceConstants.KEY_USER_NAME;

/**
 * Created by Manndeep Vachhani on 8/5/17.
 */

public class MainActivity extends BaseAppCompatActivity {

    String TAG = "MainActivity";

    @BindView(R.id.lv_drawer)
    ListView lvDrawer;
    @BindView(R.id.lyt_drawer)
    DrawerLayout lytDrawer;
    @BindView(R.id.iv_header_logo)
    LinearLayout ivHeaderLogo;
    @BindView(R.id.ll_extra_margin)
    LinearLayout llExtraMargin;
    boolean doubleBackToExitPressedOnce = false;
    @BindView(R.id.tv_name)
    XTextView tvName;
    @BindView(R.id.iv_image)
    CircularImageView ivImage;
    @BindView(R.id.ll_welcome)
    LinearLayout llWelcome;
    @BindView(R.id.txt_username)
    XTextView txtUsername;
    @BindView(R.id.iv_profile_pic)
    CircularImageView ivProfilePic;
    @BindView(R.id.lay_profile)
    RelativeLayout layProfile;
    @BindView(R.id.iv)
    ImageView iv;
    @BindView(R.id.txt_title)
    XTextView txtTitle;
    @BindView(R.id.iv_header_logo_fb)
    ImageView ivHeaderLogoFb;
    @BindView(R.id.iv_header_logo_twiter)
    ImageView ivHeaderLogoTwiter;
    @BindView(R.id.iv_header_logo_insta)
    ImageView ivHeaderLogoInsta;
    @BindView(R.id.iv_header_logo_whatsapp)
    ImageView ivHeaderLogoWhatsapp;
    @BindView(R.id.img_messages)
    ImageView imgMessages;

    @OnClick({R.id.iv_header_logo_fb, R.id.iv_header_logo_twiter, R.id.iv_header_logo_insta, R.id.iv_header_logo_whatsapp, R.id.img_messages})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header_logo_fb:

                String url = "https://www.facebook.com/EppmeApp-192065647999244/";
                Helper.AppRedirectViewIntent(mActivity, url);

                break;
            case R.id.iv_header_logo_twiter:
                String url1 = "https://twitter.com/EppmeApp";
                Helper.AppRedirectViewIntent(mActivity, url1);
                break;
            case R.id.iv_header_logo_insta:
                String url2 = "https://www.instagram.com/eppmeapp/";
                Helper.AppRedirectViewIntent(mActivity, url2);
                break;
            case R.id.iv_header_logo_whatsapp:
                Helper.WhatsAppIntent(mActivity);
                break;
            case R.id.img_messages:
                UiHelper.startMessageCenterActivity(mActivity);
                break;
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            clearBackStack();
            selectItem(position);
        }
    }

    /**
     * Add a Fragment to back-stack over the HomeFragment
     *
     * @param fragment       Fragment to add
     * @param transactionTag Fragment title
     */
    private void addFragmentToBackStack(Fragment fragment, String transactionTag) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        transaction.addToBackStack(transactionTag)
                .replace(R.id.content_frame, fragment)
                .commit();
    }

    /**
     * Clear fragment back-stack
     *
     * @return true if called, false otherwise
     */
    private boolean clearBackStack() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            /*FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
            Fragment fragment = getSupportFragmentManager().getFragments()
                    .get(getSupportFragmentManager().getBackStackEntryCount() - 1);
            transaction.remove(fragment).commit();*/
            getSupportFragmentManager().popBackStack();
            return true;
        }
        return false;
    }


    /**
     * Select item from drawer list
     *
     * @param position index of drawer item
     */
    public void selectItem(int position) {
        switch (position) {
            case NavigationItemsAdapter.NAV_POSITION_WEL_COME:
                addFragmentToBackStack(new WelcomeEppmeFragment(), getString(R.string.welcome_to_eppme));
                ivHeaderLogo.setVisibility(View.GONE);
                llExtraMargin.setVisibility(View.GONE);
                layProfile.setVisibility(View.GONE);
                imgMessages.setVisibility(View.GONE);
                //   getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.welcome_to_eppme));

                break;
            case NavigationItemsAdapter.NAV_POSITION_HOME:
//                addFragmentToBackStack(new DashboardFragment(), getString(R.string.home));
                ivHeaderLogo.setVisibility(View.VISIBLE);
                llExtraMargin.setVisibility(View.VISIBLE);
                layProfile.setVisibility(View.VISIBLE);
                imgMessages.setVisibility(View.VISIBLE);
                //    getToolbar().setBackgroundColor(getResources().getColor(R.color.orange));
                updateToolbar("Hello " + Helper.getStringValue(KEY_USER_NAME) + ",");
                break;

            case NavigationItemsAdapter.NAV_POSITION_PROFILE:
                addFragmentToBackStack(new ProfileFragment(), getString(R.string.profile));
                ivHeaderLogo.setVisibility(View.GONE);
                llExtraMargin.setVisibility(View.GONE);
                layProfile.setVisibility(View.GONE);
                imgMessages.setVisibility(View.GONE);
                //    getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.profile));
                break;

            case NavigationItemsAdapter.NAV_POSITION_HISTORY:
                addFragmentToBackStack(new HistoryFragment(), getString(R.string.history));
                ivHeaderLogo.setVisibility(View.GONE);
                llExtraMargin.setVisibility(View.GONE);
                layProfile.setVisibility(View.GONE);
                imgMessages.setVisibility(View.GONE);
                //   getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.history));
                break;
         /*   case NavigationItemsAdapter.NAV_POSITION_SHOPPING:
                addFragmentToBackStack(new ShoppingFragment(), getString(R.string.shopping));
                ivHeaderLogo.setVisibility(View.GONE);
                llExtraMargin.setVisibility(View.GONE);
                layProfile.setVisibility(View.GONE);
                //   getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar("");
                break;*/
            case NavigationItemsAdapter.NAV_POSITION_NOTIFICATIONS:
                addFragmentToBackStack(new StatusReportFragment(), getString(R.string.status_reporter));
                ivHeaderLogo.setVisibility(View.GONE);
                llExtraMargin.setVisibility(View.GONE);
                layProfile.setVisibility(View.GONE);
                imgMessages.setVisibility(View.GONE);
                //     getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.status_reporter));
                break;

            case NavigationItemsAdapter.NAV_POSITION_ABOUT:
                addFragmentToBackStack(new AboutFragment(), getString(R.string.about));
                ivHeaderLogo.setVisibility(View.GONE);
                llExtraMargin.setVisibility(View.VISIBLE);
                layProfile.setVisibility(View.GONE);
                imgMessages.setVisibility(View.GONE);
                //   getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.about));
                break;

           /* case NavigationItemsAdapter.NAV_POSITION_WEDDING_INFO:
                addFragmentToBackStack(new WeddingInfoFragment(), getString(R.string.wedding_info));
                ivHeaderLogo.setVisibility(View.GONE);
                llExtraMargin.setVisibility(View.GONE);
                layProfile.setVisibility(View.GONE);
                imgMessages.setVisibility(View.GONE);
                //     getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.wedding_info));
                break;*/

        /*    case NavigationItemsAdapter.NAV_POSITION_FAQS:

                addFragmentToBackStack(new FAQsFragment(), getString(R.string.faqs));
                ivHeaderLogo.setVisibility(View.VISIBLE);
                llExtraMargin.setVisibility(View.VISIBLE);
                layProfile.setVisibility(View.GONE);
                getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.faqs));
                break;*/
         /*   case NavigationItemsAdapter.NAV_POSITION_TIPS:

                addFragmentToBackStack(new TipsFragment(), getString(R.string.tips));
                ivHeaderLogo.setVisibility(View.VISIBLE);
                llExtraMargin.setVisibility(View.VISIBLE);
                layProfile.setVisibility(View.GONE);
                getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.tips));
                break;*/

            case NavigationItemsAdapter.NAV_POSITION_SETTINGS:
                addFragmentToBackStack(new SettingFragment(), getString(R.string.settings));
                ivHeaderLogo.setVisibility(View.GONE);
                llExtraMargin.setVisibility(View.GONE);
                layProfile.setVisibility(View.GONE);
                imgMessages.setVisibility(View.GONE);
                //    getToolbar().setBackgroundColor(getResources().getColor(R.color.header_color_home));
                updateToolbar(getString(R.string.settings));
                break;


            default:
                break;
        }

        lytDrawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onBackPressed() {
        if (lytDrawer.isDrawerOpen(GravityCompat.START)) {
            lytDrawer.closeDrawer(GravityCompat.START);
        } else {
            if (clearBackStack()) {
                ivHeaderLogo.setVisibility(View.VISIBLE);
                llExtraMargin.setVisibility(View.VISIBLE);
                layProfile.setVisibility(View.VISIBLE);
                imgMessages.setVisibility(View.VISIBLE);
                //   getToolbar().setBackgroundColor(getResources().getColor(R.color.orange));
                updateToolbar("Hello " + Helper.getStringValue(KEY_USER_NAME) + ",");
            } else {
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }

                this.doubleBackToExitPressedOnce = true;
                UiHelper.toastinfo(getString(R.string.back_to_exit_app));

                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            }
        }
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        setUpToolbar();
        EventBus.getDefault().register(this);
        // getToolbar().setBackgroundColor(getResources().getColor(R.color.orange));
        Helper.getAuthKey();


        PermissionsManager.getInstance().requestAllManifestPermissionsIfNecessary(this,
                new PermissionsResultAction() {
                    @Override
                    public void onGranted() {

                    }

                    @Override
                    public void onDenied(String permission) {

                    }
                });

        lvDrawer.setOnItemClickListener(new DrawerItemClickListener());

        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, lytDrawer, getToolbar(), 0, 0);
        actionBarDrawerToggle.syncState();

        lytDrawer.addDrawerListener(actionBarDrawerToggle);

        NavigationItemsAdapter navigationItemsAdapter = new NavigationItemsAdapter(this);
        lvDrawer.setAdapter(navigationItemsAdapter);


        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.setCustomAnimations(R.anim.fade_in, R.anim.fade_out, R.anim.fade_in, R.anim.fade_out);
        transaction.replace(R.id.content_frame, new NewHomeFragment())
                .commit();

        updateToolbar("Hello " + Helper.getStringValue(KEY_USER_NAME) + ",");


        getMyProfile();

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            scheduleJob();
        }


        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                handleNotificationIntent(getIntent());
            }
        }, 1000);
    }

    private void getMyProfile() {
        if (!is_Network_Available()) {
            return;
        }
        BaseCallback<UserResponse> callback = new BaseCallback<UserResponse>() {
            @Override
            public void Success(Response<UserResponse> response) {
                stopProgress();
                App.currentUser = response.body().getUser();
                Helper.setUserValues(response.body().getUser());
                setUpUiData();

            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.getMyProfile().enqueue(callback);
    }

    private void setUpUiData() {
        if (null != App.getCurrentUser()) {
            tvName.setText(App.getCurrentUser().getName());
            if (StringUtils.isValid(App.getCurrentUser().getProfileUrl())) {
                Picasso.with(mActivity).load(App.getCurrentUser().getProfileUrl()).into(ivImage);
                Picasso.with(mActivity).load(App.getCurrentUser().getProfileUrl()).into(ivProfilePic);
            }
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    private void scheduleJob() {
        JobInfo myJob = new JobInfo.Builder(0, new ComponentName(this, NetworkSchedulerService.class))
                .setRequiresCharging(true)
                .setMinimumLatency(1000)
                .setOverrideDeadline(2000)
                .setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED)
                .setPersisted(true)
                .build();

        JobScheduler jobScheduler = (JobScheduler) getSystemService(Context.JOB_SCHEDULER_SERVICE);
        jobScheduler.schedule(myJob);
    }

    @Override
    protected void onStop() {
        // A service can be "started" and/or "bound". In this case, it's "started" by this Activity
        // and "bound" to the JobScheduler (also called "Scheduled" by the JobScheduler). This call
        // to stopService() won't prevent scheduled jobs to be processed. However, failing
        // to call stopService() would keep it alive indefinitely.
//        stopService(new Intent(this, NetworkSchedulerService.class));
        super.onStop();
    }

    @Override
    protected void onStart() {
        super.onStart();
        // Start service and provide it a way to communicate with this class.
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // only for gingerbread and newer versions
            Intent startServiceIntent = new Intent(this, NetworkSchedulerService.class);
            startService(startServiceIntent);
        }
    }

    public boolean is_Network_Available() {
        boolean toReturn;
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        toReturn = activeNetworkInfo != null && activeNetworkInfo.isConnected();


        return toReturn;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void refreshApi(EventProfileApiRefresh refresh) {
        getMyProfile();
    }


    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.e(TAG, "==new intent call==");
        handleNotificationIntent(intent);

    }

    private void handleNotificationIntent(final Intent intent) {

        if (null != intent) {
            if (intent.hasExtra(FCM_KEY_TYPE)) {
                String type = intent.getStringExtra(FCM_KEY_TYPE);
                switch (type) {
                    case NOTIFICATION_TYPE_HISTRY_STATUS:
                        Log.e(TAG, "==Status call==");
                        selectItem(6);
                        break;

                    case NOTIFICATION_TYPE_STUDENT_REPORT_HISTORY:
                        Log.e(TAG, "==student call==");
                        selectItem(5);
                        break;

                    case NOTIFICATION_TYPE_CUSTOM_ADMIN:
                        Log.e(TAG, "==Status call==");
//                        selectItem(4);
                        UiHelper.startMessageCenterActivity(mActivity);
                        break;
                }
            }
        }
    }

    public static void callDialog(Context context) {
        KidnapDialog kidnapDialog;
        kidnapDialog = new KidnapDialog(context);
        kidnapDialog.show();
    }
}
