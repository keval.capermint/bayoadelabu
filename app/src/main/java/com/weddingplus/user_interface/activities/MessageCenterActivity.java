package com.weddingplus.user_interface.activities;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.cloud_messaging.FirebaseMessaging;
import com.weddingplus.models.Notification;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.NotificationsAdapter;
import com.weddingplus.user_interface.interfaces.NotificationListener;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.NotificationsResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by keval on 22/2/18.
 */

public class MessageCenterActivity extends BaseAppCompatActivity implements NotificationListener {


    @BindView(R.id.rcv_notifications)
    RecyclerView rcvNotifications;
    @BindView(R.id.swiperefreshlayout)
    SwipeRefreshLayout swiperefreshlayout;
    @BindView(R.id.txt_no_data)
    XTextView txtNoData;

    NotificationsAdapter notificationsAdapter;
    NotificationManager notificationManager;
    @BindView(R.id.txt_dashboard_header)
    XTextView txtDashboardHeader;
    @BindView(R.id.txt_header)
    XTextView txtHeader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_delete)
    XButton btnDelete;
    @BindView(R.id.btn_cancel)
    XButton btnCancel;
    @BindView(R.id.ll_bottom)
    LinearLayout llBottom;
    private boolean isnotificationfound = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_center);
        ButterKnife.bind(this);
        setUpToolbar();
        updateToolbar(getString(R.string.message_center));

        txtNoData.setText(R.string.no_notifications);

        notificationManager = (NotificationManager) mActivity.getSystemService(Context.NOTIFICATION_SERVICE);

        FirebaseMessaging.setNotificationListener(this);

        notificationsAdapter = new NotificationsAdapter(mActivity) {
            @Override
            protected void onDoubleClickItem(Notification activities, int position, boolean isdoubletap) {
                super.onDoubleClickItem(activities, position, isdoubletap);
                if (isdoubletap) {
                    llBottom.setVisibility(View.VISIBLE);
                } else {
                    llBottom.setVisibility(View.GONE);
                    notificationsAdapter.isdoubletap=false;
                }
            }
        };
        rcvNotifications.setAdapter(notificationsAdapter);


        getNotifications();

        swiperefreshlayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getNotifications();
            }
        });

        manageVisibility();
    }

    private void getNotifications() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<NotificationsResponse> callback = new BaseCallback<NotificationsResponse>() {
            @Override
            public void Success(Response<NotificationsResponse> response) {
                if (null != swiperefreshlayout) {

                    swiperefreshlayout.setRefreshing(false);
                }
                notificationsAdapter.clearData();
                notificationsAdapter.addAll(response.body().getData());
                manageVisibility();
                if (null != txtNoData) {
                    txtNoData.setVisibility(View.GONE);
                }

                if (response.body().getData().size() > 0) {
                    if (null != txtNoData) {

                        txtNoData.setVisibility(View.GONE);
                    }
                } else {
                    if (null != txtNoData) {

                        txtNoData.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void Failure(String message) {
                Log.e("Message,", "" + message);
                if (null != swiperefreshlayout) {

                    swiperefreshlayout.setRefreshing(false);
                }
//                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        if (null != swiperefreshlayout) {
            swiperefreshlayout.setRefreshing(true);
        }
        WebServiceHelper.getNotifications().enqueue(callback);
    }

    private void manageVisibility() {
        int itemCount = notificationsAdapter.getItemCount();
        if (itemCount > 0) {
            isnotificationfound = true;
        } else {
            isnotificationfound = false;
        }
        if (null != mActivity) {
            invalidateOptionsMenu();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(isnotificationfound ? R.menu.clear_all : R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_clear_all) {
            UiHelper.showConfirmationDialog(mActivity, "Are you sure you want to delete all Notifications?", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (isNetworkAvailable()) {
                        clearNotifications();
                    }
                }
            });
        }
        return super.onOptionsItemSelected(item);

    }

    private void clearNotifications() {

        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                notificationsAdapter.clearAll();
                txtNoData.setVisibility(notificationsAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
                manageVisibility();
            }

            @Override
            public void Failure(String message) {
                Log.e("Message,", "" + message);
                stopProgress();
                txtNoData.setVisibility(notificationsAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
//                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                stopProgress();
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(true);
        WebServiceHelper.clearNotifications().enqueue(callback);


    }


    @Override
    public void NotificationListener(String type) {
    }

    @Override
    public void onNotificationClick(String type, String message) {
        SetOnUiCall();
    }

    private void SetOnUiCall() {
        if (mActivity != null)
            mActivity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    getNotifications();
                }
            });
    }


    @Override
    public void onStop() {
        super.onStop();
        FirebaseMessaging.removeNotificationListener();
    }

    @OnClick({R.id.btn_delete, R.id.btn_cancel})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_delete:
                if (notificationsAdapter.getSelectedNotificationId().size() == 0) {
                    toast(getString(R.string.validation_select_at_lease_one_notification));
                    return;
                }

                UiHelper.showConfirmationDialog(mActivity, "Are you sure you want to delete Notifications?", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (isNetworkAvailable()) {
                            String notificationIds = TextUtils.join(",", notificationsAdapter.getSelectedNotificationId());

                            ArrayList<String> strings = new ArrayList<>();
                            strings.addAll(notificationsAdapter.getSelectedNotificationId());
                            Log.e("notifid:",notificationIds);
                           // notificationsAdapter.removeItem(strings);
                           // notificationsAdapter.reArrangeItem();

                            deletePerticulerNotifications(notificationIds, -1);
                        }
                    }
                });


                llBottom.setVisibility(View.GONE);
                break;
            case R.id.btn_cancel:
                notificationsAdapter.onCancelButtonClicked();
                llBottom.setVisibility(View.GONE);
                break;
        }
    }

    private void deletePerticulerNotifications(String notifid, final int position) {

        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                toast(response.body().getMessage());
                getNotifications();
           /*     notificationsAdapter.reArrangeItem();
                notificationsAdapter.clearAll();
                txtNoData.setVisibility(notificationsAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
                manageVisibility();*/
            }

            @Override
            public void Failure(String message) {
                Log.e("Message,", "" + message);
                stopProgress();
                txtNoData.setVisibility(notificationsAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
//                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                stopProgress();
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(true);
        WebServiceHelper.deleteNotifications(notifid).enqueue(callback);


    }

}
