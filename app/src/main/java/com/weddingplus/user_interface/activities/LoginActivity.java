package com.weddingplus.user_interface.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.View;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.UserResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Manndeep Vachhani on 8/5/17.
 */

public class LoginActivity extends BaseAppCompatActivity {

    @BindView(R.id.tv_toolbar_title)
    XTextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.btn_sign_in)
    XTextView btnSignIn;
    @BindView(R.id.txt_sign_up)
    XTextView txtSignUp;
    @BindView(R.id.txt_forgot_pass)
    XTextView txtForgotPass;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.edt_password)
    XEditText edtPassword;
    @BindView(R.id.edt_email)
    XEditText edtEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        setUpToolbar();
        tvToolbarTitle.setVisibility(View.GONE);
        edtPassword.setTypeface(Typeface.DEFAULT);
        edtPassword.setTransformationMethod(new PasswordTransformationMethod());

    }

    @OnClick({R.id.btn_sign_in, R.id.txt_sign_up, R.id.txt_forgot_pass})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_sign_in:
                if (validateData()) {
                    LooginApi();
                }
                break;
            case R.id.txt_sign_up:
                UiHelper.startSignUpActivity(LoginActivity.this);
                break;
            case R.id.txt_forgot_pass:
                UiHelper.startForgotPasswordActivity(LoginActivity.this);
                break;
        }
    }

    private void LooginApi() {
        if (Helper.isNetworkAvailable(this)) {
            startProgress(false);
            WebServiceHelper.getLoginCall(edtEmail.getText().toString(), edtPassword.getText().toString())
                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            stopProgress();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    Helper.setUserValues(response.body().getUser());
                                    UiHelper.startMainActivity(LoginActivity.this,true);
                                    UiHelper.toasterror(response.body().getMessage());
                                } else {
                                    UiHelper.toasterror(response.body().getMessage());
                                }
                            } else {
                                UiHelper.toasterror(getString(R.string.login_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            UiHelper.toasterror(getString(R.string.login_error));
                            stopProgress();

                        }
                    });
        }

    }

    private boolean validateData() {
        if (!StringUtils.isValidEmail(edtEmail.getText().toString())) {
            UiHelper.toasterror(getString(R.string.email_validation_msg));
            return false;
        } else if (!StringUtils.isValid(edtPassword.getText().toString())) {
            UiHelper.toasterror(getString(R.string.password_validation_msg));
            return false;
        }
        return true;
    }

}
