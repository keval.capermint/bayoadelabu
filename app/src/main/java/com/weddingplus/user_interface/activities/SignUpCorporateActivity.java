package com.weddingplus.user_interface.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.UserResponse;
import com.rilixtech.CountryCodePicker;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by keval on 28/6/17.
 */

public class SignUpCorporateActivity extends BaseAppCompatActivity {

    @BindView(R.id.tv_toolbar_title)
    XTextView tvToolbarTitle;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.txt_term_and_cond)
    XTextView txtTermAndCond;
    @BindView(R.id.btn_sign_up)
    XButton btnSignUp;
    @BindView(R.id.edt_password)
    XEditText edtPassword;
    @BindView(R.id.edt_confirm_password)
    XEditText edtConfirmPassword;
    @BindView(R.id.txt_birthdate)
    XTextView txtBirthdate;
    @BindView(R.id.ll_main)
    LinearLayout llMain;
    @BindView(R.id.iv_user_imag)
    ImageView ivUserImag;


    @BindView(R.id.edt_first_name)
    XEditText edtFirstName;
    @BindView(R.id.edt_last_name)
    XEditText edtLastName;

    /*    @BindView(R.id.edt_company_code)
        XEditText edtCompanyCode;*/

    @BindView(R.id.edt_email)
    XEditText edtEmail;
    @BindView(R.id.edt_phone_no)
    XEditText edtPhoneNo;
    @BindView(R.id.edt_emergency_contact)

    XEditText edtEmergencyContact;
    @BindView(R.id.edt_nature_of_business)
    XEditText edtNatureOfBusiness;

    @BindView(R.id.edt_contact_person_one_name)
    XEditText edtContactPersonOneName;
    @BindView(R.id.edt_contact_person_one_number)
    XEditText edtContactPersonOneNumber;
    @BindView(R.id.edt_contact_person_two_name)
    XEditText edtContactPersonTwoName;
    @BindView(R.id.edt_contact_person_two_number)
    XEditText edtContactPersonTwoNumber;
    @BindView(R.id.tv_term_condition)
    TextView tvTermCondition;
    @BindView(R.id.tv_privacypolicy)
    TextView tvPrivacypolicy;

    @BindView(R.id.ccp1)
    CountryCodePicker ccp1;
    @BindView(R.id.ccp2)
    CountryCodePicker ccp2;
    @BindView(R.id.ccp3)
    CountryCodePicker ccp3;
    @BindView(R.id.ccp4)
    CountryCodePicker ccp4;
    @BindView(R.id.edt_address)
    XEditText edtAddress;
    @BindView(R.id.edt_faculty)
    XEditText edtFaculty;

    private Calendar startTimecalender;
    private int mYear, mMonth, mDay, mHour, mMinute;
    MultipartBody.Part part;

    private boolean isInEditMode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_corporate);
        ButterKnife.bind(this);
        setUpToolbar();
        isInEditMode = getIntent().getBooleanExtra(EXTRA_KEY_IS_IN_EDIT_MODE, false);
        if (isInEditMode) {
            tvToolbarTitle.setText(getResources().getString(R.string.edit_profile));
            btnSignUp.setText(getString(R.string.edit_profile));
            setExistingData();
        } else {
            tvToolbarTitle.setText(getResources().getString(R.string.sign_up));
        }

        viewLine.setVisibility(View.GONE);
        startTimecalender = Calendar.getInstance();

        edtPassword.setTypeface(Typeface.DEFAULT);
        edtPassword.setTransformationMethod(new PasswordTransformationMethod());
        edtConfirmPassword.setTypeface(Typeface.DEFAULT);
        edtConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());

        ccp1.registerPhoneNumberTextView(edtPhoneNo);
        ccp2.registerPhoneNumberTextView(edtContactPersonOneNumber);
        ccp3.registerPhoneNumberTextView(edtContactPersonTwoNumber);
        ccp4.registerPhoneNumberTextView(edtEmergencyContact);

/*
        SpannableString wordtoSpan = new SpannableString(getString(R.string.by_clicking_signup_accept));

        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 48, 68, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new ForegroundColorSpan(Color.RED), 74, 88, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        wordtoSpan.setSpan(new UnderlineSpan(), 48, 68, 0);
        wordtoSpan.setSpan(new UnderlineSpan(), 74, 88, 0);

        ClickableSpan clickableSpan = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                UiHelper.toast("call");
                UiHelper.startTermCondtionActivity(mActivity);
            }

            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
            }
        };
        wordtoSpan.setSpan(clickableSpan, 48, 68, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        ClickableSpan clickableSpan1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
                UiHelper.toast("call11");
                UiHelper.startPrivacyPolicyActivity(mActivity);
            }
            @Override
            public void updateDrawState(TextPaint ds) {
                super.updateDrawState(ds);
            }
        };
        wordtoSpan.setSpan(clickableSpan1, 74, 88, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        txtTermAndCond.setClickable(true);

        txtTermAndCond.setText(wordtoSpan);
        txtTermAndCond.setMovementMethod(LinkMovementMethod.getInstance());*/
    }

    private void setExistingData() {
      /*  User user = App.getCurrentUser();
        edtEmail.setText(user.getEmail());
        edtCompanyName.setText(user.getComponyName());
        edtEmergencyContact.setText(user.getContactNumber());
        edtAddress.setText(user.getAddress());*/
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }

    private void DateDialog() {
        mYear = startTimecalender.get(Calendar.YEAR);
        mMonth = startTimecalender.get(Calendar.MONTH);
        mDay = startTimecalender.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(this, R.style.datepicker,
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {


//                        txtBirthdate.setText(year + "/" + String.format("%02d", monthOfYear + 1) + "/" + String.format("%02d", dayOfMonth));
//                        sdf = new SimpleDateFormat("yyyy/MM/dd");
                        txtBirthdate.setText(String.format("%02d", dayOfMonth) + "/" + String.format("%02d", monthOfYear + 1) + "/" + year);


                    }
                }, mYear, mMonth, mDay);
        datePickerDialog.show();

        datePickerDialog.getDatePicker().setMaxDate(startTimecalender.getTimeInMillis());


    }

    @OnClick({R.id.txt_birthdate, R.id.btn_sign_up, R.id.iv_user_imag, R.id.tv_term_condition, R.id.tv_privacypolicy})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_user_imag:
                selectImage();
                break;
            case R.id.txt_birthdate:
                DateDialog();
                break;
            case R.id.btn_sign_up:
                if (validateData()) {
                    SignUpApi();
                }

                break;
            case R.id.tv_term_condition:
                UiHelper.startTermCondtionActivity(mActivity);
                break;
            case R.id.tv_privacypolicy:
                UiHelper.startPrivacyPolicyActivity(mActivity);
                break;
        }

    }

    private void SignUpApi() {
        if (Helper.isNetworkAvailable(this)) {
            startProgress(false);
            WebServiceHelper.getSignUpCall(part,
                    edtFirstName.getText().toString().trim(),
                    edtLastName.getText().toString().trim(),
                    edtFaculty.getText().toString().trim(),
                    // edtCompanyCode.getText().toString(),
                    edtAddress.getText().toString(),
                    edtEmail.getText().toString(),
                    edtPassword.getText().toString(),
                    ccp1.getSelectedCountryCode() + StringUtils.SPACE_STRING + edtPhoneNo.getText().toString(),
                    txtBirthdate.getText().toString(),
                    edtContactPersonOneName.getText().toString(),
                    ccp2.getSelectedCountryCode() + StringUtils.SPACE_STRING + edtContactPersonOneNumber.getText().toString(),
                    edtContactPersonTwoName.getText().toString(),
                    ccp3.getSelectedCountryCode() + StringUtils.SPACE_STRING + edtContactPersonTwoNumber.getText().toString(),
                    ccp4.getSelectedCountryCode() + StringUtils.SPACE_STRING + edtEmergencyContact.getText().toString())

                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            stopProgress();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    Helper.setUserValues(response.body().getUser());
                                    UiHelper.startMainActivity(SignUpCorporateActivity.this, true);
                                    UiHelper.toasterror(response.body().getMessage());
                                } else {
                                    UiHelper.toasterror(response.body().getMessage());
                                }
                            } else {
                                UiHelper.toasterror(getString(R.string.login_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            UiHelper.toasterror(getString(R.string.login_error));
                            stopProgress();

                        }
                    });
        }

    }

    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    cameraIntent();
                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    galleryIntent();
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                if (null == bm) {
                    Toast.makeText(this, "Not Valid Image", Toast.LENGTH_SHORT);
                } else {
                    part = createPartImageFromBitmap(bm, true);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        part = createPartImageFromBitmap(thumbnail, true);
    }

    public MultipartBody.Part createPartImageFromBitmap(Bitmap bmp, boolean toResize) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), destination);


            Picasso.with(SignUpCorporateActivity.this).load(destination).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(ivUserImag);
//            selectedImagePath = destination.getAbsolutePath();
//            Log.e("path", "" + selectedImagePath);
            return MultipartBody.Part.createFormData("profile", destination.getName(), reqFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean validateData() {

        if (!StringUtils.isValid(edtFirstName.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_first_name));
            return false;
        } else if (!StringUtils.isValid(edtLastName.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_last_name));
            return false;
        }
        /*else if (!StringUtils.isValid(edtCompanyCode.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_company_code));
            return false;
        } */
        else if (!StringUtils.isValid(edtAddress.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_address));
            return false;
        } else if (!StringUtils.isValidEmail(edtEmail.getText().toString())) {
            UiHelper.toasterror(getString(R.string.email_validation_msg));
            return false;
        } else if (!StringUtils.isValid(edtPassword.getText().toString())) {
            UiHelper.toasterror(getString(R.string.password_validation_msg));
            return false;
        } else if (!StringUtils.isValid(edtConfirmPassword.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_confirm_pass));
            return false;
        } else if (!edtPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            UiHelper.toasterror(getString(R.string.password_confirm_validation));
            return false;
        } else if (!StringUtils.isValid(edtPhoneNo.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_valid_pno));
            return false;

        }
       /* else if (!StringUtils.isValid(txtBirthdate.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_birth_date));
            return false;
        } */

        else if (!StringUtils.isValid(edtContactPersonOneName.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_contact_person_name));
            return false;
        } else if (!StringUtils.isValid(edtContactPersonOneNumber.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_contact_person_ph_no));
            return false;
        } else if (!StringUtils.isValid(edtContactPersonTwoName.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_contact_person_name));
            return false;
        } else if (!StringUtils.isValid(edtContactPersonTwoNumber.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_contact_person_ph_no));
            return false;
        } else if (!StringUtils.isValid(edtEmergencyContact.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_emergency_contact));
            return false;
        }
        return true;
    }
}
