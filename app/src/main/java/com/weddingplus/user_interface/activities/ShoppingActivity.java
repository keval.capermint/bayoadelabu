package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.CartItemCountResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.weddingplus.utilities.PreferenceConstants.KEY_USER_NAME;

public class ShoppingActivity extends BaseAppCompatActivity {


    @BindView(R.id.iv_header_logo_whatsapp)
    ImageView ivHeaderLogoWhatsapp;
    @BindView(R.id.iv_header_logo_fb)
    ImageView ivHeaderLogoFb;
    @BindView(R.id.iv_header_logo_twiter)
    ImageView ivHeaderLogoTwiter;
    @BindView(R.id.iv_header_logo_insta)
    ImageView ivHeaderLogoInsta;
    @BindView(R.id.iv_header_logo)
    LinearLayout ivHeaderLogo;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_welcome_msg)
    XTextView txtWelcomeMsg;
    @BindView(R.id.ll_instruction)
    LinearLayout llInstruction;
    @BindView(R.id.img_user)
    ImageView imgUser;
    @BindView(R.id.txt_cart_count)
    XTextView txtCartCount;
    @BindView(R.id.img_cart)
    FrameLayout imgCart;
    @BindView(R.id.lay_shop_couple)
    LinearLayout layShopCouple;
    @BindView(R.id.lay_shop_general)
    LinearLayout layShopGeneral;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shopping);
        ButterKnife.bind(this);
        setUpToolbar();
        txtWelcomeMsg.setText("" + "Hello " + Helper.getStringValue(KEY_USER_NAME) + ", Enjoy your Shopping");
    }

/*
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);

    }*/

    @Override
    public void onResume() {
        super.onResume();
        getItemCount();
    }


    private void getItemCount() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<CartItemCountResponse> callback = new BaseCallback<CartItemCountResponse>() {
            @Override
            public void Success(Response<CartItemCountResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    txtCartCount.setVisibility(View.VISIBLE);
                    txtCartCount.setText(response.body().getData());
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);

        WebServiceHelper.getCartItemCount().enqueue(callback);
    }

    @OnClick({R.id.iv_header_logo_whatsapp, R.id.iv_header_logo_fb, R.id.iv_header_logo_twiter, R.id.iv_header_logo_insta, R.id.lay_shop_couple, R.id.lay_shop_general, R.id.img_cart, R.id.img_user})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header_logo_whatsapp:
                Helper.WhatsAppIntent(mActivity);
                break;
            case R.id.iv_header_logo_fb:
                String url = "https://www.facebook.com/EppmeApp-192065647999244/";
                Helper.AppRedirectViewIntent(mActivity, url);
                break;
            case R.id.iv_header_logo_twiter:
                String url1 = "https://twitter.com/EppmeApp";
                Helper.AppRedirectViewIntent(mActivity, url1);
                break;
            case R.id.iv_header_logo_insta:
                String url2 = "https://www.instagram.com/eppmeapp/";
                Helper.AppRedirectViewIntent(mActivity, url2);
                break;
            case R.id.lay_shop_couple:
                UiHelper.startProductListActivity(mActivity, "SC");
                break;
            case R.id.lay_shop_general:
                UiHelper.startProductListActivity(mActivity, "GS");
                break;
            case R.id.img_user:
                break;
            case R.id.img_cart:
                UiHelper.starCartActivity(mActivity);
                break;
        }
    }

}
