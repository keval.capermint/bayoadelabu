package com.weddingplus.user_interface.activities;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.exoplayer2.DefaultLoadControl;
import com.google.android.exoplayer2.DefaultRenderersFactory;
import com.google.android.exoplayer2.ExoPlaybackException;
import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.PlaybackParameters;
import com.google.android.exoplayer2.Player;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.Timeline;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.TrackGroupArray;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelectionArray;
import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory;
import com.weddingplus.R;
import com.weddingplus.models.VideoData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.AppConstants;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.views.XTextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class FullScreenVideoActivity extends BaseAppCompatActivity implements AppConstants {

    @BindView(R.id.superVideoView)
    SimpleExoPlayerView superVideoview;
    @BindView(R.id.iv_play)
    ImageView ivPlay;
    @BindView(R.id.ivVideoThumbnail)
    ImageView ivVideoThumbnail;
    @BindView(R.id.txt_dashboard_header)
    XTextView txtDashboardHeader;
    @BindView(R.id.txt_header)
    XTextView txtHeader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    private VideoData mVideoData;

    private SimpleExoPlayer mSuperPlayer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_full_screen_video);
        ButterKnife.bind(this);
        setUpToolbar();
        txtDashboardHeader.setText(R.string.video);
        mVideoData = getIntent().getParcelableExtra(AppConstants.EXTRA_KEY_DATA);


        mSuperPlayer = ExoPlayerFactory.newSimpleInstance(
                new DefaultRenderersFactory(this),
                new DefaultTrackSelector(), new DefaultLoadControl());

        superVideoview.setPlayer(mSuperPlayer);
        mSuperPlayer.setRepeatMode(Player.REPEAT_MODE_OFF);


        mSuperPlayer.addListener(new Player.EventListener() {
            @Override
            public void onTimelineChanged(Timeline timeline, Object o) {

            }

            @Override
            public void onTracksChanged(TrackGroupArray trackGroupArray, TrackSelectionArray trackSelectionArray) {

            }

            @Override
            public void onLoadingChanged(boolean b) {
/*
                if (b) {
                    pDialog.show();
                } else {
                    pDialog.dismiss();
                }
*/
            }

            @Override
            public void onPlayerStateChanged(boolean b, int i) {
                if (i == Player.STATE_ENDED) {
                    Log.e("Stop", "CALL");
                    ivPlay.setVisibility(View.VISIBLE);
                    ivVideoThumbnail.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onRepeatModeChanged(int i) {

            }

            @Override
            public void onPlayerError(ExoPlaybackException e) {

            }

            @Override
            public void onPositionDiscontinuity() {

            }

            @Override
            public void onPlaybackParametersChanged(PlaybackParameters playbackParameters) {

            }
        });


        // Picasso.with(mActivity).load(mVideoData.getVideo_thumb()).error(R.drawable.iv_user_placeholder).into(ivVideoThumbnail);

        if (StringUtils.isValid(mVideoData.getAttachment())) {
            superVideoview.requestFocus();
            Uri uri = Uri.parse(mVideoData.getAttachment());
            MediaSource mediaSource = buildMediaSource(uri);
            mSuperPlayer.prepare(mediaSource, true, false);

            mSuperPlayer.setPlayWhenReady(true);
        }

    }

    private MediaSource buildMediaSource(Uri uri) {
        return new ExtractorMediaSource(uri,
                new DefaultHttpDataSourceFactory("ua"),
                new DefaultExtractorsFactory(), null, null);
    }

    @OnClick({R.id.iv_play})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_play:
                ivVideoThumbnail.setVisibility(View.GONE);
                ivPlay.setVisibility(View.GONE);
                superVideoview.requestFocus();
                Uri uri = Uri.parse(mVideoData.getAttachment());
                MediaSource mediaSource = buildMediaSource(uri);
                mSuperPlayer.prepare(mediaSource, true, false);

                mSuperPlayer.setPlayWhenReady(true);
                /*
               *//* if (mediacontroller == null) {
                    checkViewNullOrNot();
                }*/

/*
                superVideoview.start();
*/

                break;

        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (null != mSuperPlayer) {
            mSuperPlayer.release();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSuperPlayer.release();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_share, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_share)
        {
            Helper.shareIntent(mActivity,""+mVideoData.getAttachment());
        }
        return super.onOptionsItemSelected(item);
    }
}
