package com.weddingplus.user_interface.activities;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.weddingplus.App;
import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.AppConstants;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.LocationProviderHelper;
import com.weddingplus.utilities.MapMarkerBounce;
import com.weddingplus.utilities.PreferenceConstants;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MultipartBody;
import retrofit2.Response;

/**
 * Created by keval on 29/6/17.
 */

public class SocialCheckInActivity extends BaseAppCompatActivity implements OnMapReadyCallback {

    @BindView(R.id.txt_dashboard_header)
    XTextView txtDashboardHeader;
    @BindView(R.id.txt_header)
    XTextView txtHeader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.iv_header_logo)
    ImageView ivHeaderLogo;
    @BindView(R.id.btn_check_in)
    XButton btnCheckIn;

    String location;
    String desc;

    private SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap;

    LocationProviderHelper providerHelper;

    AddDescriptionDialog descriptionDialog;

        Category category;

        String cat_id;
    LatLng mCenterLatlNg;

    MapMarkerBounce mapRipple;
    LatLng latLng;
    MultipartBody.Part part;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socail_check_in);
        ButterKnife.bind(this);
        setUpToolbar();
        toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        Intent intent = getIntent();
        Bundle b = intent.getExtras();
        if (b != null) {
            category = intent.getParcelableExtra(AppConstants.EXTRA_KEY_CATEGORY);
            cat_id = String.valueOf(category.getCategory_id());
        }

        descriptionDialog = new AddDescriptionDialog(mActivity);
        txtDashboardHeader.setVisibility(View.GONE);

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        providerHelper = new LocationProviderHelper(mActivity, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                            new LatLng(location.getLatitude(), location.getLongitude()), 15);
                    latLng = new LatLng(location.getLatitude(), location.getLongitude());
//                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
//                            new LatLng(14.195294, 121.155930), 15);

                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(cameraUpdate);
                        mGoogleMap.clear();
//                        mGoogleMap.addMarker(markerOptions);
                        providerHelper.stopLocationUpdates();
                    }


                /*    mapRadar = new MapRadar(mGoogleMap, latLng, mActivity);
                    mapRadar.withDistance(500);
                    mapRadar.withOuterCircleStrokeColor(Color.parseColor("#ffffff"));
                    mapRadar.withRadarColors(Color.parseColor("#e25a57"), Color.parseColor("#fda967"));
                    mapRadar.startRadarAnimation();*/
                }
            }
        });

    }

    @OnClick(R.id.btn_check_in)
    public void onViewClicked() {
        descriptionDialog.show();
    }

    boolean firsttime = true;

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mGoogleMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {
                mCenterLatlNg = cameraPosition.target;
                Log.e("lat===", +mCenterLatlNg.latitude + "==lon==" + mCenterLatlNg.longitude);
                Helper.setStringValue(PreferenceConstants.KEY_LATTITUDE, String.valueOf(mCenterLatlNg.latitude));
                Helper.setStringValue(PreferenceConstants.KEY_LONGITUDE, String.valueOf(mCenterLatlNg.longitude));

/*
                if(firsttime) {
                    mapRipple = new MapMarkerBounce(mGoogleMap, mCenterLatlNg, mActivity);
                    mapRipple.withNumberOfRipples(3);
                    mapRipple.withFillColor(ContextCompat.getColor(mActivity,R.color.orange_end));
                    mapRipple.withStrokeColor(Color.WHITE);
                    mapRipple.withStrokewidth(5);      // 10dp
                    mapRipple.withDistance(1000);      // 2000 metres radius
                    mapRipple.withRippleDuration(12000);    //12000ms
                    mapRipple.withTransparency(0.3f);
                    mapRipple.startRippleMapAnimation();
                    firsttime=false;
                }
                else {
                    if (mapRipple.isAnimationRunning()) {
                        mapRipple.stopRippleMapAnimation();
                    }
                    else {
                        mapRipple.startRippleMapAnimation();
                    }
                }

                mapRipple.withLatLng(mCenterLatlNg);

*/
            }
        });

//        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
//        mGoogleMap.setMyLocationEnabled(false);

    }

    private void addMaker(double lat, double longi, String title) {
        MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.iv_map));

        markerOptions.position(new LatLng(lat, longi));
        markerOptions.title(title);
        mGoogleMap.addMarker(markerOptions);
    }

    public class AddDescriptionDialog extends Dialog {
        Context context;
        BaseAppCompatActivity activity;
        @BindView(R.id.btn_send)
        XButton btnSend;
        @BindView(R.id.txt_dialog_title)
        XTextView txtDialogTitle;
        @BindView(R.id.edt_desc)
        XEditText edtDesc;
        @BindView(R.id.txt_desc)
        XTextView txtDesc;
        @BindView(R.id.iv_attachment)
        ImageView ivAttachment;
        @BindView(R.id.txt_attach_url)
        XTextView txtAttachUrl;
        @BindView(R.id.ll_attch)
        LinearLayout llAttch;

        public AddDescriptionDialog(Context context) {
            super(context);
            this.activity = (BaseAppCompatActivity) context;

        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_add_description);
            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            ButterKnife.bind(this);

            if (Helper.getUserType().equalsIgnoreCase(INDIVIDUAL)) {
                txtDesc.setText(getString(R.string.description));
                llAttch.setVisibility(View.GONE);
            } else {
                txtDesc.setText(getString(R.string.location_report));
                llAttch.setVisibility(View.GONE);
            }

        }

        @Override
        public void dismiss() {
            super.dismiss();
        }

        @OnClick(R.id.btn_send)
        public void onViewClicked() {
            if (part != null) {
                App.ISUPLOAD = "Y";
            } else {
                App.ISUPLOAD = "N";
            }

            if (isNetworkAvailable()) {
                if (StringUtils.isValid(cat_id)) {
                    CheckInApi(cat_id, edtDesc.getText().toString(), edtDesc);
                }
            }

            View view = activity.getCurrentFocus();
            if (view != null) {
                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
            dismiss();
        }
    }

    private void CheckInApi(String cat_id, String desc, final XEditText xEditText) {
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                toast(response.body().getMessage());
                if (null != xEditText) {
                    xEditText.setText("");
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.CheckIn("", part, cat_id, desc, App.ISUPLOAD).enqueue(callback);
    }

}
