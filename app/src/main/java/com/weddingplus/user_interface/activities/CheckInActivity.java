package com.weddingplus.user_interface.activities;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Location;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.weddingplus.App;
import com.weddingplus.BuildConfig;
import com.weddingplus.R;
import com.weddingplus.sync.CheckInModel;
import com.weddingplus.sync.PreferenceApp;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.interfaces.EvnetDialogRefresh;
import com.weddingplus.user_interface.interfaces.StopRecording;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.LocationProviderHelper;
import com.weddingplus.utilities.MapMarkerBounce;
import com.weddingplus.utilities.MapRadar;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.weddingplus.utilities.PreferenceConstants.KEY_LATTITUDE;
import static com.weddingplus.utilities.PreferenceConstants.KEY_LONGITUDE;

/**
 * Created by keval on 29/6/17.
 */

public class CheckInActivity extends BaseAppCompatActivity implements OnMapReadyCallback {
    public static final int RequestPermissionCode = 1;
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "eppMe";
    /**
     * Called when the activity is first created.
     */
    File photoFile;

    @BindView(R.id.txt_dashboard_header)
    XTextView txtDashboardHeader;
    @BindView(R.id.txt_header)
    XTextView txtHeader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.iv_header_logo)
    ImageView ivHeaderLogo;
    @BindView(R.id.btn_check_in)
    XButton btnCheckIn;
    String location;
    String desc;
    String currenaudiopath;
    LocationProviderHelper providerHelper;
    AddDescriptionDialog descriptionDialog;
    LatLng latLng;
    MultipartBody.Part part;
    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = {MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP};
    private String file_exts[] = {AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP};
    private MapMarkerBounce mapRipple;
    private MapRadar mapRadar;
    private SupportMapFragment mapFragment;
    private GoogleMap mGoogleMap;
    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Log.e("Error: ", +what + ", " + extra);
            currenaudiopath = "";
        }
    };
    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            Log.e("Warning: ", +what + ", " + extra);
            currenaudiopath = "";
        }
    };

    int attechtype;
    private String type="",path;
    public static MultipartBody.Part createPartFileFromPath(String filepath) {
        File destination = new File(filepath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("video/*"), destination);
        return MultipartBody.Part.createFormData("attachment", destination.getName(), requestFile);
    }

    public static String getVideoFilePath(Uri uri, Context appCompatActivity) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = appCompatActivity.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            Log.e("video path", cursor.getString(column_index) + "");
            return cursor.getString(column_index);
        } else {
            return null;
        }

    }

    Uri outputFileUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_in);
        ButterKnife.bind(this);
        setUpToolbar();
        EventBus.getDefault().register(this);
        txtDashboardHeader.setVisibility(View.GONE);
//        toolbar.setBackgroundColor(getResources().getColor(R.color.white));
        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        providerHelper = new LocationProviderHelper(mActivity, new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                if (location != null) {
                    MarkerOptions markerOptions = new MarkerOptions();
                    markerOptions.position(new LatLng(location.getLatitude(), location.getLongitude()));
                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                            new LatLng(location.getLatitude(), location.getLongitude()), 15);
                    latLng = new LatLng(location.getLatitude(), location.getLongitude());

//                    CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
//                            new LatLng(14.195294, 121.155930), 15);

                    if (mGoogleMap != null) {
                        mGoogleMap.animateCamera(cameraUpdate);
                        mGoogleMap.clear();
                        mGoogleMap.addMarker(markerOptions);
                        providerHelper.stopLocationUpdates();
                    }
                    mapRipple = new MapMarkerBounce(mGoogleMap, latLng, mActivity);
                    mapRipple.withNumberOfRipples(3);
                    mapRipple.withFillColor(ContextCompat.getColor(mActivity, R.color.orange));
                    mapRipple.withStrokeColor(Color.WHITE);
                    mapRipple.withStrokewidth(5);      // 10dp
                    mapRipple.withDistance(1000);      // 2000 metres radius
                    mapRipple.withRippleDuration(9500);    //12000ms
                    mapRipple.withTransparency(0.3f);
                    mapRipple.startRippleMapAnimation();
                }
            }
        });
    }

    @OnClick(R.id.btn_check_in)
    public void onViewClicked() {
        descriptionDialog = new AddDescriptionDialog(mActivity);
        descriptionDialog.show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }


        mGoogleMap.getUiSettings().setScrollGesturesEnabled(false);
        mGoogleMap.getUiSettings().setZoomControlsEnabled(false);
        mGoogleMap.getUiSettings().setZoomGesturesEnabled(false);
        mGoogleMap.getUiSettings().setRotateGesturesEnabled(false);


//        mGoogleMap.setMyLocationEnabled(true);

    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outputFileUri != null) {
            outState.putString("cameraImageUri", outputFileUri.toString());
        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("cameraImageUri")) {
            outputFileUri = Uri.parse(savedInstanceState.getString("cameraImageUri"));
        }
    }
    private void addMaker(double lat, double longi, String title) {
        MarkerOptions markerOptions = new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.iv_map));

        markerOptions.position(new LatLng(lat, longi));
        markerOptions.title(title);
        mGoogleMap.addMarker(markerOptions);
    }

    private void CheckInApi(String desc, final XEditText xEditText) {
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                toast(response.body().getMessage());
                part = null;
                if (null != xEditText) {
                    xEditText.setText("");

                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast("Please try again later.");
                //toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.CheckIn(""+attechtype,part, "0", desc, App.ISUPLOAD).enqueue(callback);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttachClick(EvnetDialogRefresh attach) {
        selectImage();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStopRecording(StopRecording send) {
        stopRecording();
        if (descriptionDialog != null) {
            descriptionDialog.updateBtntext("stop");
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void selectImage() {

        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.take_video), getString(R.string.voice),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    cameraIntent("photo");
                }
               /* else if (items[item].equals(getString(R.string.choose_photo_from_library))) {
                    galleryIntent("photo");
                } */
                else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
//                else if (items[item].equals(getString(R.string.choose_video_from_library))) {
//                    galleryIntent("video");
//                }
                else if (items[item].equals(getString(R.string.take_video))) {
                    cameraIntent("video");
                } else if (items[item].equals(getString(R.string.voice))) {
                    cameraIntent("voice");
                }
            }
        });
        builder.show();
    }

    private void cameraIntent(String flag) {
        type = flag;
        if (flag.equals("photo")) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (photoFile != null) {
                    outputFileUri = FileProvider.getUriForFile(mActivity,  BuildConfig.APPLICATION_ID+ ".com.weddingplus.provider", photoFile);
                    //outputFileUri = Uri.fromFile(photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            }
           /* Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);*/
        } else if (flag.equals("video")) {
            Intent captureVideoIntent = new Intent(android.provider.MediaStore.ACTION_VIDEO_CAPTURE);
            captureVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
            captureVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
            startActivityForResult(captureVideoIntent, REQUEST_VIDEO);
        } else if (flag.equals("voice")) {
            if (recorder == null) {
                if (checkPermission()) {

                    startRecording();
                } else {
                    requestPermission();
                }
            } else {
                stopRecording();
            }
        }
    }

    public static File createImageFile() throws IOException {
      /*  File myDir = new File(Environment.getExternalStorageDirectory() + "/" + "beLocum");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
*/
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        // File file = new File(myDir, "locum_" + System.currentTimeMillis() + ".jpg");
        return destination;
    }
    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(output_formats[currentFormat]);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        recorder.setOutputFile(getFilename());
        recorder.setOnErrorListener(errorListener);
        recorder.setOnInfoListener(infoListener);

        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        if (descriptionDialog != null) {
            descriptionDialog.updateBtntext("start");
        }
    }

    private void stopRecording() {
        if (null != recorder) {
            recorder.stop();
            recorder.reset();
            recorder.release();

            recorder = null;
            path = currenaudiopath;
            if (StringUtils.isValid(currenaudiopath)) {
                File destination = new File(currenaudiopath);

                RequestBody reqFile = RequestBody.create(MediaType.parse("audio/*"), destination);
                if (null != descriptionDialog) {

                    descriptionDialog.updateudiofinish();
                }

                part = MultipartBody.Part.createFormData("attachment", destination.getName(), reqFile);
                attechtype=3;
            }
        }
    }

    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

        currenaudiopath = file.getAbsolutePath() + "/file"  + file_exts[currentFormat];
        path = currenaudiopath;
        return (file.getAbsolutePath() + "/file"  + file_exts[currentFormat]);
       /* currenaudiopath = file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat];
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat]);
    */
    }

    private void galleryIntent(String flag) {
        if (flag.equals("photo")) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_FILE);
        } else {

            Intent i = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_VIDEO);

           /* Intent intent = new Intent();
            intent.setAction(Intent.ACTION_PICK);//
            intent.setType("video*//*");
            startActivityForResult(Intent.createChooser(intent, "Select Video"), SELECT_VIDEO);*/
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (requestCode == REQUEST_VIDEO)
                onSelectFromVideoResult(data);
            else if (requestCode == SELECT_VIDEO)
                onCaptureVideoResult(data);

        }
    }

    private void onCaptureVideoResult(Intent data) {
        int msec = MediaPlayer.create(mActivity, data.getData()).getDuration();
        Log.e("video length", "" + msec);                // minutes 911111
        if (msec > videolimit) {
            toast(getString(R.string.video_limit));
            part = null;
        } else {

            //  Log.d(TAG, "onCaptureVideoResult: " + data.getData().getPath());
            String selectedVideoPath = getVideoFilePath(data.getData(), mActivity);
            path = selectedVideoPath;
            part = createPartFileFromPath(selectedVideoPath);
            attechtype=2;
         /*   File file = new File(data.getData().getPath() + ".mp4");
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file.getPath());
            part = MultipartBody.Part.createFormData("attachment", file.getName(), requestFile);*/
            if (descriptionDialog != null) {
                descriptionDialog.update(selectedVideoPath);
            }
        }

    }

    private void onSelectFromVideoResult(Intent data) {
        int msec = MediaPlayer.create(mActivity, data.getData()).getDuration();
        Log.e("video length", "" + msec);                // minutes 911111
        if (msec > videolimit) {
            toast(getString(R.string.video_limit));
            part = null;
        } else {
            Log.d(TAG, "onSelectFromVideoResult: video come" + data.getData().getPath());
            String selectedVideoPath = getVideoFilePath(data.getData(), mActivity);
            path = selectedVideoPath;
            part = createPartFileFromPath(selectedVideoPath);
            attechtype = 2;
       /* File file = new File(data.getData().getPath() + ".mp4");
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file.getPath());
        part = MultipartBody.Part.createFormData("attachment", file.getName(), requestFile);*/
            if (descriptionDialog != null) {

                descriptionDialog.update(selectedVideoPath);
            }
        }

    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        Log.d(TAG, "onSelectFromGalleryResult: " + data.getType());
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                if (null == bm) {
                    Toast.makeText(this, "Not Valid Image", Toast.LENGTH_SHORT);
                } else {
                   // part = createPartImageFromBitmap(bm, true);
                   // attechtype=1;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private void onCaptureImageResult(Intent data) {
        if (outputFileUri != null) {

            try {
                String selectedImagePath = photoFile.getPath();
                path = selectedImagePath;
               // String selectedImagePath = outputFileUri.getPath();
                Log.e("selectedImagePath", "" + selectedImagePath);
                part = createPartImageFromBitmap(selectedImagePath);
                attechtype = 1;
            /*    Bitmap bitmap = BitmapLoader.downSampleBitmap(selectedImagePath, imgRightPrectice);
                int imageAngle = StringUtils.getImageAngle(selectedImagePath);
                Bitmap rotateBitMap = StringUtils.rotateImage(bitmap, imageAngle);
                imgRightPrectice.setImageBitmap(rotateBitMap);
                hashMapImg.put("img1",selectedImagePath);*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        //  Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        // ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        //  part = createPartImageFromBitmap(thumbnail, true);
        //  attechtype = 1;

    }
    public MultipartBody.Part createPartImageFromBitmap(String path) {

        if (null != descriptionDialog) {

            descriptionDialog.update(path);
        }

        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), new File(path));
        return MultipartBody.Part.createFormData("attachment", new File(path).getName(), reqFile1);

       /* ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            RequestBody reqFile = RequestBody.create(MediaType.parse("image*//*"), destination);
            if (null != studentManagerDialog) {

                studentManagerDialog.update(destination.toString());
            }
            if (null != studentManagerDialog) {

                studentManagerDialog.update(destination.toString());
            }

//            Picasso.with(this).load(destination).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(ivUserImag);
//            selectedImagePath = destination.getAbsolutePath();
//            Log.e("path", "" + selectedImagePath);
            return MultipartBody.Part.createFormData("attachment", destination.getName(), reqFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;*/
    }


  /*  private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        part = createPartImageFromBitmap(thumbnail, true);
        attechtype=1;

    }

    public MultipartBody.Part createPartImageFromBitmap(Bitmap bmp, boolean toResize) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            RequestBody reqFile = RequestBody.create(MediaType.parse("image*//*"), destination);
            if (null != descriptionDialog) {

                descriptionDialog.update(destination.toString());
            }

//            Picasso.with(this).load(destination).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(ivUserImag);
//            selectedImagePath = destination.getAbsolutePath();
//            Log.e("path", "" + selectedImagePath);
            return MultipartBody.Part.createFormData("attachment", destination.getName(), reqFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
*/
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_instruction, menu);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_innstruction) {
            UiHelper.startEmergencyInstructionActivity(mActivity);
        }
        return super.onOptionsItemSelected(item);
    }

    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        startRecording();
                     /*   Toast.makeText(mActivity, "Permission Granted",
                                Toast.LENGTH_LONG).show();*/
                    } else {
                        Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    public class AddDescriptionDialog extends Dialog {
        Context context;
        BaseAppCompatActivity activity;
        @BindView(R.id.btn_send)
        XButton btnSend;
        @BindView(R.id.txt_dialog_title)
        XTextView txtDialogTitle;
        @BindView(R.id.edt_desc)
        XEditText edtDesc;

        @BindView(R.id.txt_desc)
        XTextView txtDesc;
        @BindView(R.id.iv_attachment)
        ImageView ivAttachment;
        @BindView(R.id.txt_attach_url)
        XTextView txtAttachUrl;
        @BindView(R.id.ll_attch)
        LinearLayout llAttch;
        boolean isAttached = false;

        public AddDescriptionDialog(Context context) {
            super(context);
            this.activity = (BaseAppCompatActivity) context;


        }

        @Override
        public void onCreate(Bundle savedInstanceState) {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_add_description);


            getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            ButterKnife.bind(this);

            if (Helper.getUserType().equalsIgnoreCase(INDIVIDUAL)) {
                txtDesc.setText(getString(R.string.description));
                llAttch.setVisibility(View.VISIBLE);
            } else {
                txtDesc.setText(getString(R.string.location_report));
                llAttch.setVisibility(View.VISIBLE);
            }


        }

        @Override
        public void dismiss() {
            super.dismiss();
        }


        @OnClick({R.id.ll_attch, R.id.btn_send, R.id.iv_attachment})
        public void onViewClicked(View view) {
            switch (view.getId()) {
                case R.id.ll_attch:
                    EventBus.getDefault().post(new EvnetDialogRefresh(""));
                    break;
                case R.id.btn_send:
                    if (btnSend.getText().toString().equals("Send")) {
                        if (view != null) {
                            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                        }

                        if (part != null) {
                            App.ISUPLOAD = "Y";
                        } else {
                            App.ISUPLOAD = "N";
                        }
                        if (is_Network_Available(CheckInActivity.this)) {
                            CheckInApi(edtDesc.getText().toString(), edtDesc);
                        }
                        else {
                            PreferenceApp pref = new PreferenceApp(getApplicationContext());
                            List<CheckInModel> list = new ArrayList<>();
                            list  = pref.getCheckInRequestList();
                            if (list == null)
                                list = new ArrayList<>();

                            CheckInModel obj = new CheckInModel();
                            obj.setAttachment_type(""+attechtype);
                            obj.setAuthkey(App.getAuthKey());
                            obj.setSocial_category_id("0");
                            obj.setDescription(edtDesc.getText().toString());
                            obj.setLatitude(Helper.getStringValue(KEY_LATTITUDE));
                            obj.setLongitude(Helper.getStringValue(KEY_LONGITUDE));
                            obj.setIs_upload(App.ISUPLOAD);
                            obj.setPath(path);
                            obj.setType(type);
                            obj.setPart(part);
                            list.add(obj);
                            pref.setCheckInRequestList(list);
                        }

                        /*if (isNetworkAvailable()) {
                            CheckInApi(edtDesc.getText().toString(), edtDesc);
                        }*/
                        dismiss();
                    } else {
                        EventBus.getDefault().post(new StopRecording());
                    }
                    break;


                case R.id.iv_attachment: {
                    if (isAttached) {
                        removefile();
                    }
                    break;
                }
            }
        }
        public boolean is_Network_Available(BaseAppCompatActivity context) {
            boolean toReturn;
            ConnectivityManager connectivityManager
                    = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

            toReturn = activeNetworkInfo != null && activeNetworkInfo.isConnected();
            return toReturn;
        }

        public void removefile() {
            part = null;
            ivAttachment.setImageResource(R.drawable.iv_plus_icon);
//            EventBus.getDefault().post(new RemoveImage());
            txtAttachUrl.setText("Photo / Video / Voice");
            isAttached = false;
        }

        public void update(String file_url) {
            txtAttachUrl.setText(file_url);
            isAttached = true;
            ivAttachment.setImageResource(R.drawable.iv_cross);

        }

        public void updateudiofinish() {
            txtAttachUrl.setText("Audio recorded");
            isAttached = true;
            ivAttachment.setImageResource(R.drawable.iv_cross);
            btnSend.setText("Send");
            llAttch.setEnabled(true);

        }

        public void updateBtntext(String status) {
            if (status.equals("start")) {
                btnSend.setText("Stop voice record");
                llAttch.setEnabled(false);

            } else {
                txtAttachUrl.setText("Audio recorded");
                isAttached = true;
                ivAttachment.setImageResource(R.drawable.iv_cross);
                btnSend.setText("Send");
                llAttch.setEnabled(true);
            }
        }
    }

}
