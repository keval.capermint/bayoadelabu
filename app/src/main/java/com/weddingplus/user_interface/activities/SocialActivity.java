package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.SocialAdapter;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.CategoryResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Response;

/**
 * Created by keval on 5/5/17.
 */

public class SocialActivity extends BaseAppCompatActivity {


    @BindView(R.id.tv_toolbar_title)
    XTextView tvToolbarTitle;
    @BindView(R.id.ll_main)
    LinearLayout llMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.rcv_socail)
    RecyclerView rcvSocail;

    SocialAdapter socialAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_socail);
        ButterKnife.bind(this);
        setUpToolbar();

    /*    if (Helper.getUserType().equalsIgnoreCase(INDIVIDUAL)) {
            tvToolbarTitle.setText(R.string.user_hub);
        }else {
            tvToolbarTitle.setText(R.string.user_hub);
        }*/

        tvToolbarTitle.setText(R.string.clique_zone);

        socialAdapter = new SocialAdapter(mActivity) {
            @Override
            protected void onItemClick(int adapterPosition, Category category) {
                super.onItemClick(adapterPosition, category);
                UiHelper.startSocialCheckInActivity(mActivity,category);
            }
        };
        rcvSocail.setAdapter(socialAdapter);
        getEmergencySocialApi();
    }

    private void getEmergencySocialApi() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<CategoryResponse> callback = new BaseCallback<CategoryResponse>() {
            @Override
            public void Success(Response<CategoryResponse> response) {
                stopProgress();
                socialAdapter.addAll(response.body().getData());

            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.getSocialCategory().enqueue(callback);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
