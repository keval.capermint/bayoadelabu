package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.ImageView;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.PreferenceConstants;
import com.weddingplus.utilities.UiHelper;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by keval on 28/6/17.
 */

public class Welcome2Activity extends BaseAppCompatActivity {
    @BindView(R.id.img_somarii)
    ImageView imgSomarii;
    @BindView(R.id.img_bravo)
    ImageView imgBravo;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_2);
        ButterKnife.bind(this);
        PermissionsManager.getInstance().requestAllManifestPermissionsIfNecessary(this,
                new PermissionsResultAction() {
                    @Override
                    public void onGranted() {

                    }

                    @Override
                    public void onDenied(String permission) {

                    }
                });

    }

    @OnClick({R.id.img_somarii, R.id.img_bravo})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.img_somarii:
                UiHelper.startNewsActivity(Welcome2Activity.this);
                break;
            case R.id.img_bravo:
                Helper.setStringValue(PreferenceConstants.KEY_USER_TYPE, INDIVIDUAL);
                UiHelper.startMainActivity(Welcome2Activity.this,false);
                break;
        }
    }

/*    @OnClick({R.id.btn_individual, R.id.btn_corporate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_individual:
                Helper.setStringValue(PreferenceConstants.KEY_USER_TYPE, INDIVIDUAL);
                UiHelper.startLoginActivity(WelcomeActivity.this);
                break;
            case R.id.btn_corporate:
                Helper.setStringValue(PreferenceConstants.KEY_USER_TYPE, CORPORATE);
                UiHelper.startLoginCorporateActivity(WelcomeActivity.this);
                break;
        }
    }*/
}
