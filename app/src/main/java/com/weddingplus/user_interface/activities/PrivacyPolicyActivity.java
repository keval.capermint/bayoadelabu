package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.views.XTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 5/5/17.
 */

public class PrivacyPolicyActivity extends BaseAppCompatActivity {


    @BindView(R.id.tv_toolbar_title)
    XTextView tvToolbarTitle;
    @BindView(R.id.ll_main)
    LinearLayout llMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.view_line)
    View viewLine;
    @BindView(R.id.txt_privacy_policy)
    XTextView txtPrivacyPolicy;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_and_cond);
        ButterKnife.bind(this);
        setUpToolbar();
        tvToolbarTitle.setText(R.string.privacy_policy);

        txtPrivacyPolicy.setText(Html.fromHtml(privacy_policy));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }

}
