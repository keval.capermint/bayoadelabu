package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import com.anthonycr.grant.PermissionsManager;
import com.anthonycr.grant.PermissionsResultAction;
import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.PreferenceConstants;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by keval on 28/6/17.
 */

public class WelcomeActivity extends BaseAppCompatActivity {

    @BindView(R.id.btn_individual)
    XButton btnIndividual;
    @BindView(R.id.btn_corporate)
    XButton btnCorporate;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);
        PermissionsManager.getInstance().requestAllManifestPermissionsIfNecessary(this,
                new PermissionsResultAction() {
                    @Override
                    public void onGranted() {

                    }

                    @Override
                    public void onDenied(String permission) {

                    }
                });

    }

    @OnClick({R.id.btn_individual, R.id.btn_corporate})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_individual:
                Helper.setStringValue(PreferenceConstants.KEY_USER_TYPE, INDIVIDUAL);
                UiHelper.startLoginActivity(WelcomeActivity.this);
                break;
            case R.id.btn_corporate:
                Helper.setStringValue(PreferenceConstants.KEY_USER_TYPE, CORPORATE);
                UiHelper.startLoginCorporateActivity(WelcomeActivity.this);
                break;
        }
    }
}
