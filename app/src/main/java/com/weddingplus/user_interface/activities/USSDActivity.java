package com.weddingplus.user_interface.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.recycler_adapters.UssdDialogAdapter;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.ApplyUSSDCodeResponse;
import com.weddingplus.web_services.responses.CategoryResponse;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

/**
 * Created by keval on 30/6/17.
 */

public class USSDActivity extends BaseAppCompatActivity {

    @BindView(R.id.txt_dashboard_header)
    XTextView txtDashboardHeader;
    @BindView(R.id.txt_header)
    XTextView txtHeader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_code)
    XEditText edtCode;
    @BindView(R.id.btn_send)
    XButton btnSend;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ussd);
        ButterKnife.bind(this);
        setUpToolbar();
        txtDashboardHeader.setText(R.string.ussd);


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.btn_send)
    public void onViewClicked() {
        if(validateData()) {
            applyussdcode();
        }
    }

    private void applyussdcode() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<ApplyUSSDCodeResponse> callback = new BaseCallback<ApplyUSSDCodeResponse>() {
            @Override
            public void Success(Response<ApplyUSSDCodeResponse> response) {
                stopProgress();

                if(response.body().isSuccess()) {
                    if (response.body().getData().getType().equals("emergency")) {
                        getEmergencyCatResponse();
                    } else if (response.body().getData().getType().equals("sos")) {
                       // onSendMessagePost();
                    }
                }
                else
                {
                    toast(response.body().getMessage());
                }


            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.applyUSSDCode(edtCode.getText().toString()).enqueue(callback);
    }

    private void getEmergencyCatResponse() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<CategoryResponse> callback = new BaseCallback<CategoryResponse>() {
            @Override
            public void Success(Response<CategoryResponse> response) {
                stopProgress();
                if(response.body().isSuccess()) {
                    showListDialog(response.body().getData());
                }

            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.getEmergencyCategory().enqueue(callback);
    }

    public void showListDialog(final ArrayList<Category> data)
    {
        AlertDialog.Builder sayWindows = new AlertDialog.Builder(
                mActivity);
        sayWindows.setMessage("Select Option");
        sayWindows.setPositiveButton("Send", null);
        sayWindows.setNegativeButton("Cancel", null);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        sayWindows.setView(dialogView);
        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        RecyclerView recycle_list=(RecyclerView)dialogView.findViewById(R.id.recycle_list);

        UssdDialogAdapter adapter=new UssdDialogAdapter(mActivity);
        recycle_list.setAdapter(adapter);
        adapter.addAll(data);

        final AlertDialog mAlertDialog = sayWindows.create();
        mAlertDialog.setOnShowListener(new DialogInterface.OnShowListener() {

            @Override
            public void onShow(DialogInterface dialog) {
                mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
                Button b = mAlertDialog.getButton(AlertDialog.BUTTON_POSITIVE);
                b.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something
                        if(StringUtils.isValid(edt.getText().toString())) {
                            boolean isCodeMatch=false;
                            for (int i = 0; i < data.size(); i++) {

                                if (StringUtils.equals(String.valueOf(data.get(i).getCategory_id()),edt.getText().toString()))
                                {
                                    isCodeMatch=true;
                                    if(null!=mAlertDialog && mAlertDialog.isShowing()) {
                                        mAlertDialog.dismiss();
                                    }
                                   // toast("code match");
                                }
                                else
                                {
                                    isCodeMatch=false;

                                    // toast("code match");
                                }
                            }
                           /* if(isCodeMatch)
                            {
                               // toast("code match");
                                mAlertDialog.dismiss();
                            }
                            else
                            {
                                toast("code not match");
                            }*/
                        }
                    }
                });
                Button btncancel = mAlertDialog.getButton(AlertDialog.BUTTON_NEGATIVE);
                btncancel.setOnClickListener(new View.OnClickListener() {

                    @Override
                    public void onClick(View view) {
                        // TODO Do something

                    }
                });
            }
        });
        mAlertDialog.show();


       /* AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this,R.style.AlertDialogStyle);
        LayoutInflater inflater = this.getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText edt = (EditText) dialogView.findViewById(R.id.edit1);
        RecyclerView recycle_list=(RecyclerView)dialogView.findViewById(R.id.recycle_list);

        UssdDialogAdapter adapter=new UssdDialogAdapter(mActivity);
        recycle_list.setAdapter(adapter);
        adapter.addAll(data);
        dialogBuilder.setCancelable(false);
        dialogBuilder.setTitle(R.string.app_name);
       // dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton("Send", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                if(StringUtils.isValid(edt.getText().toString())) {
                    for (int i = 0; i < data.size(); i++) {

                        if (StringUtils.equals(String.valueOf(data.get(i).getCategory_id()),edt.getText().toString()))
                        {
                            toast("code match");
                        }
                        else
                        {

                           // toast("code match");
                        }
                    }
                }
            }
        });
        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });
        final AlertDialog b = dialogBuilder.create();
        b.show();
        b.setOnShowListener( new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface arg0) {
                b.getButton(AlertDialog.BUTTON_NEGATIVE).setTextColor(getResources().getColor(R.color.black));
                b.getButton(AlertDialog.BUTTON_POSITIVE).setTextColor(getResources().getColor(R.color.black));
            }
        });*/

    }


  /*  private void onSendMessagePost() {

        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                if (response.body().isSuccess()) {
                    toast(response.body().getMessage());

                } else {
//                    toast(response.body().getMessage());
                    toast("Invalid Number");
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast("Invalid Number");
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.SendMessage(part, attechtype, caption, message).enqueue(callback);

    }*/

    private boolean validateData() {
        if (!StringUtils.isValid(edtCode.getText().toString())) {
            UiHelper.toasterror(getString(R.string.ussd_validation_msg));
            return false;
        }
        return true;
    }
}
