package com.weddingplus.user_interface.activities;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.text.method.PasswordTransformationMethod;
import android.view.Menu;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by keval on 30/6/17.
 */

public class ChangePasswordActivity extends BaseAppCompatActivity {

    @BindView(R.id.txt_dashboard_header)
    XTextView txtDashboardHeader;
    @BindView(R.id.txt_header)
    XTextView txtHeader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.edt_old_password)
    XEditText edtOldPassword;
    @BindView(R.id.edt_new_password)
    XEditText edtNewPassword;
    @BindView(R.id.edt_confirm_password)
    XEditText edtConfirmPassword;
    @BindView(R.id.btn_sava)
    XButton btnSava;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        setUpToolbar();
        txtDashboardHeader.setText(R.string.change_passsword);
        edtOldPassword.setTypeface(Typeface.DEFAULT);
        edtOldPassword.setTransformationMethod(new PasswordTransformationMethod());
        edtNewPassword.setTypeface(Typeface.DEFAULT);
        edtNewPassword.setTransformationMethod(new PasswordTransformationMethod());
        edtConfirmPassword.setTypeface(Typeface.DEFAULT);
        edtConfirmPassword.setTransformationMethod(new PasswordTransformationMethod());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick(R.id.btn_sava)
    public void onViewClicked() {
        if (validateData()) {
            ChangePasswordApi();
        }
    }

    private void ChangePasswordApi() {
        if (Helper.isNetworkAvailable(this)) {
            startProgress(false);
            WebServiceHelper.getChangePasswordCall(edtOldPassword.getText().toString(), edtNewPassword.getText().toString(), edtConfirmPassword.getText().toString())
                    .enqueue(new Callback<BaseResponse>() {
                        @Override
                        public void onResponse(Call<BaseResponse> call, Response<BaseResponse> response) {
                            stopProgress();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    finish();
                                    UiHelper.toasterror(response.body().getMessage());
                                } else {
                                    UiHelper.toasterror(response.body().getMessage());
                                }
                            } else {
                                UiHelper.toasterror(getString(R.string.login_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<BaseResponse> call, Throwable t) {
                            UiHelper.toasterror(getString(R.string.login_error));
                            stopProgress();

                        }
                    });
        }

    }

    private boolean validateData() {
        if (!StringUtils.isValid(edtOldPassword.getText().toString())) {
            UiHelper.toasterror(getString(R.string.password_validation_msg));
            return false;
        } else if (!StringUtils.isValid(edtNewPassword.getText().toString())) {
            UiHelper.toasterror(getString(R.string.password_validation_msg));
            return false;
        } else if (!StringUtils.isValid(edtConfirmPassword.getText().toString())) {
            UiHelper.toasterror(getString(R.string.password_validation_msg));
            return false;
        } else if (!edtNewPassword.getText().toString().equals(edtConfirmPassword.getText().toString())) {
            UiHelper.toasterror(getString(R.string.new_password_confirm_validation));
            return false;
        }
        return true;
    }
}
