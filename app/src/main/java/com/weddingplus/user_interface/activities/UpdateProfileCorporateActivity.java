package com.weddingplus.user_interface.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.mikhaellopez.circularimageview.CircularImageView;
import com.weddingplus.App;
import com.weddingplus.R;
import com.weddingplus.models.User;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.interfaces.EventProfileApiRefresh;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XEditText;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.UserResponse;
import com.rilixtech.CountryCodePicker;
import com.squareup.picasso.Picasso;

import org.greenrobot.eventbus.EventBus;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by keval on 29/7/17.
 */

public class UpdateProfileCorporateActivity extends BaseAppCompatActivity {

    @BindView(R.id.tv_toolbar_title)
    XTextView tvToolbarTitle;
    @BindView(R.id.ll_main)
    LinearLayout llMain;
    @BindView(R.id.toolbar)
    Toolbar toolbar;


    Picasso picasso;
    @BindView(R.id.iv_user_imag)
    CircularImageView ivUserImag;

    @BindView(R.id.edt_first_name)
    XEditText edtFirstName;
    @BindView(R.id.edt_last_name)
    XEditText edtLastName;

    /*   @BindView(R.id.edt_company_code)
       XEditText edtCompanyCode;*/
    @BindView(R.id.edt_nature_of_business)
    XEditText edtNatureOfBusiness;
    @BindView(R.id.edt_address)
    XEditText edtAddress;
    @BindView(R.id.edt_email)
    XEditText edtEmail;
    @BindView(R.id.edt_phone_no)
    XEditText edtPhoneNo;
    @BindView(R.id.txt_birthdate)
    XTextView txtBirthdate;
    @BindView(R.id.edt_contact_person_one_name)
    XEditText edtContactPersonOneName;
    @BindView(R.id.edt_contact_person_one_number)
    XEditText edtContactPersonOneNumber;
    @BindView(R.id.edt_contact_person_two_name)
    XEditText edtContactPersonTwoName;
    @BindView(R.id.edt_contact_person_two_number)
    XEditText edtContactPersonTwoNumber;
    @BindView(R.id.edt_emergency_contact)
    XEditText edtEmergencyContact;
    @BindView(R.id.btn_sign_up)
    XButton btnSignUp;

    MultipartBody.Part part;
    @BindView(R.id.ccp1)
    CountryCodePicker ccp1;
    @BindView(R.id.ccp2)
    CountryCodePicker ccp2;
    @BindView(R.id.ccp3)
    CountryCodePicker ccp3;
    @BindView(R.id.ccp4)
    CountryCodePicker ccp4;
    @BindView(R.id.edt_faculty)
    XEditText edtFaculty;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile_corporate);
        ButterKnife.bind(this);
        setUpToolbar();

        picasso = Picasso.with(UpdateProfileCorporateActivity.this);

        tvToolbarTitle.setText(getResources().getString(R.string.edit_profile));
        User user = App.getCurrentUser();

        edtEmail.setText(user.getEmail());
        edtFirstName.setText(user.getFirstName());
        edtLastName.setText(user.getLastName());
        edtFaculty.setText(user.getFaculty());
        //  edtCompanyCode.setText(user.getComponyCode());
        edtAddress.setText(user.getAddress());
        edtEmail.setText(user.getEmail());
        edtPhoneNo.setText(user.getContactNumber());
        txtBirthdate.setText(user.getBirthDate());
        edtContactPersonOneName.setText(user.getContactPersonName1());
        edtContactPersonOneNumber.setText(user.getContactPersonNumber1());
        edtContactPersonTwoName.setText(user.getContactPersonName2());
        edtContactPersonTwoNumber.setText(user.getContactPersonNumber2());
        edtEmergencyContact.setText(user.getEmergencyContacts());

        ccp1.registerPhoneNumberTextView(edtPhoneNo);
        ccp2.registerPhoneNumberTextView(edtContactPersonOneNumber);
        ccp3.registerPhoneNumberTextView(edtContactPersonTwoNumber);
        ccp4.registerPhoneNumberTextView(edtEmergencyContact);


        if (StringUtils.isValid(user.getProfileUrl())) {
            picasso.load(user.getProfileUrl()).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(ivUserImag);
        }

        if (StringUtils.isValid(user.getContactNumber())) {
            if (user.getContactNumber().contains(" ")) {

                String[] separated = user.getContactNumber().split(" ");
                ccp1.setCountryForPhoneCode(Integer.parseInt(separated[0]));
                edtPhoneNo.setText(separated[1]);
            }

        }
        if (StringUtils.isValid(user.getContactPersonNumber1())) {
            if (user.getContactPersonNumber1().contains(" ")) {

                String[] separated = user.getContactPersonNumber1().split(" ");

                ccp2.setCountryForPhoneCode(Integer.parseInt(separated[0]));
                edtContactPersonOneNumber.setText(separated[1]);
            }

        }
        if (StringUtils.isValid(user.getContactPersonNumber2())) {
            if (user.getContactPersonNumber2().contains(" ")) {

                String[] separated = user.getContactPersonNumber2().split(" ");
                ccp3.setCountryForPhoneCode(Integer.parseInt(separated[0]));
                edtContactPersonTwoNumber.setText(separated[1]);
            }

        }
        if (StringUtils.isValid(user.getEmergencyContacts())) {
            if (user.getEmergencyContacts().contains(" ")) {

                String[] separated = user.getEmergencyContacts().split(" ");

                ccp4.setCountryForPhoneCode(Integer.parseInt(separated[0]));
                edtEmergencyContact.setText(separated[1]);
            }

        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @OnClick({R.id.iv_user_imag, R.id.btn_sign_up})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_user_imag:
                selectImage();
                break;
            case R.id.btn_sign_up:
                if (validateData()) {
                    EditProfileApi();
                }

                break;
        }
    }

    private void EditProfileApi() {
        if (Helper.isNetworkAvailable(this)) {
            startProgress(false);
            WebServiceHelper.EditProfile(part,
                    edtFirstName.getText().toString().trim(),
                    edtLastName.getText().toString().trim(),
                    edtFaculty.getText().toString().trim(),
                   /* edtCompanyCode.getText().toString(),*/
                    edtAddress.getText().toString(),
                    edtEmail.getText().toString(),
                    ccp1.getSelectedCountryCode() + StringUtils.SPACE_STRING + edtPhoneNo.getText().toString(),
                    txtBirthdate.getText().toString(),
                    edtContactPersonOneName.getText().toString(),
                    ccp2.getSelectedCountryCode() + StringUtils.SPACE_STRING + edtContactPersonOneNumber.getText().toString(),
                    edtContactPersonTwoName.getText().toString(),
                    ccp3.getSelectedCountryCode() + StringUtils.SPACE_STRING + edtContactPersonTwoNumber.getText().toString(),
                    ccp4.getSelectedCountryCode() + StringUtils.SPACE_STRING + edtEmergencyContact.getText().toString())

                    .enqueue(new Callback<UserResponse>() {
                        @Override
                        public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                            stopProgress();
                            if (response.isSuccessful()) {
                                if (response.body().isSuccess()) {
                                    Helper.setUserValues(response.body().getUser());
                                    EventBus.getDefault().post(new EventProfileApiRefresh(""));
                                    UiHelper.toasterror(response.body().getMessage());
                                    finish();
                                } else {
                                    UiHelper.toasterror(response.body().getMessage());
                                }
                            } else {
                                UiHelper.toasterror(getString(R.string.login_error));
                            }
                        }

                        @Override
                        public void onFailure(Call<UserResponse> call, Throwable t) {
                            UiHelper.toasterror(getString(R.string.login_error));
                            stopProgress();

                        }
                    });
        }

    }

    private boolean validateData() {
        if (!StringUtils.isValid(edtFirstName.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_first_name));
            return false;
        } else if (!StringUtils.isValid(edtLastName.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_last_name));
            return false;
        }
       /* else if (!StringUtils.isValid(edtCompanyCode.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_company_code));
            return false;
        }*/
        else if (!StringUtils.isValid(edtAddress.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_address));
            return false;
        } else if (!StringUtils.isValid(edtPhoneNo.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_valid_pno));
            return false;
        }
        /*else if (!StringUtils.isValid(txtBirthdate.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_birth_date));
            return false;
        }*/

        else if (!StringUtils.isValid(edtContactPersonOneName.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_contact_person_name));
            return false;
        } else if (!StringUtils.isValid(edtContactPersonOneNumber.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_contact_person_ph_no));
            return false;
        } else if (!StringUtils.isValid(edtContactPersonTwoName.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_contact_person_name));
            return false;
        } else if (!StringUtils.isValid(edtContactPersonTwoNumber.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_contact_person_ph_no));
            return false;
        } else if (!StringUtils.isValid(edtEmergencyContact.getText().toString())) {
            UiHelper.toasterror(getString(R.string.enter_emergency_contact));
            return false;
        }
        return true;
    }

    private void selectImage() {
        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.choose_from_library),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    cameraIntent();
                } else if (items[item].equals(getString(R.string.choose_from_library))) {
                    galleryIntent();
                } else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void cameraIntent() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, REQUEST_CAMERA);
    }

    private void galleryIntent() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);//
        startActivityForResult(Intent.createChooser(intent, "Select File"), SELECT_FILE);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
        }
    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                if (null == bm) {
                    Toast.makeText(this, "Not Valid Image", Toast.LENGTH_SHORT);
                } else {
                    part = createPartImageFromBitmap(bm, true);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        part = createPartImageFromBitmap(thumbnail, true);

    }

    public MultipartBody.Part createPartImageFromBitmap(Bitmap bmp, boolean toResize) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 90, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            RequestBody reqFile = RequestBody.create(MediaType.parse("image/*"), destination);


            Picasso.with(this).load(destination).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(ivUserImag);
//            selectedImagePath = destination.getAbsolutePath();
//            Log.e("path", "" + selectedImagePath);
            return MultipartBody.Part.createFormData("profile", destination.getName(), reqFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }
}
