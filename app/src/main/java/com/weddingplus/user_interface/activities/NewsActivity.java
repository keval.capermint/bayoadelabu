package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.format.DateFormat;
import android.view.View;


import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.views.XTextView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.kaelaela.verticalviewpager.VerticalViewPager;
import retrofit2.Response;

/**
 * Created by keval on 5/5/17.
 */

public class NewsActivity extends BaseAppCompatActivity {


    public static VerticalViewPager pager;
    @BindView(R.id.txt_time)
    XTextView txtTime;
    @BindView(R.id.txt_date)
    XTextView txtDate;
    private Handler mHandler;
    private Calendar mCalender;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        ButterKnife.bind(this);
        mCalender = Calendar.getInstance(Locale.getDefault());
        pager = (VerticalViewPager) findViewById(R.id.pager);
       // getNewsCall();

        mHandler = new Handler();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                mCalender.setTimeInMillis(System.currentTimeMillis());
                // Helper.log("log", "Calender Update :: " + mCalender.getTimeInMillis());
                String time=getReminingTime();
                String date=getReminingDate();
                txtTime.setText(time);
                txtDate.setText(date);

                mHandler.postDelayed(this, 1000);
            }
        }, 1000);

    }
    private String getReminingTime() {
        String delegate = "hh:mm aaa";
        return (String) DateFormat.format(delegate,Calendar.getInstance().getTime());
    }
    private String getReminingDate() {
        String delegate = "d MMM. yyyy";
        return (String) DateFormat.format(delegate,Calendar.getInstance().getTime());
    }
 /*   private void getNewsCall() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<ProductListResponse> callback = new BaseCallback<ProductListResponse>() {
            @Override
            public void Success(Response<ProductListResponse> response) {
                stopProgress();


                ArrayList<ProductData> arrayList = new ArrayList<>();
                arrayList.addAll(response.body().getProductData());

          *//*      int size = 1;
                List<ProductData> sublist=new ArrayList<>();
                ArrayList<ArrayList<ProductData>> mainlist=new ArrayList<>();

                for (int start = 0; start < arrayList.size(); start += size) {
                    int end = Math.min(start + size, arrayList.size());
                    sublist = arrayList.subList(start, end);
                    Log.e("sublist size:",""+sublist.size());
                    mainlist.add(new ArrayList<ProductData>(sublist));
                    // ContentFragment contentFragment=new ContentFragment();
                    // contentFragment.newInstance(mActivity,new ArrayList<Category>(sublist),start);
                    // listfraFragments.add(contentFragment);
                }
                Log.e("sublist size:",""+mainlist.size());*//*
       *//*         CustomPagerNewsAdapter customPagerAdapter=new CustomPagerNewsAdapter(mActivity){
                    @Override
                    protected void onClick(ProductData productData, int adapterPosition) {
                        super.onClick(productData, adapterPosition);

                    }


                };
*//*
                MyPagerAdapter myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
                pager.setAdapter(myPagerAdapter);
                myPagerAdapter.addAll(arrayList);
                pager.setOffscreenPageLimit(0);
                //If you setting other scroll mode, the scrolled fade is shown from either side of display.
                //   pager.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
                // contentFragmentAdapter.addAll(listfraFragments);


            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startLoginActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.getProductList("").enqueue(callback);
    }

*/
    @OnClick({R.id.iv_header_logo_fb, R.id.iv_header_logo_twiter, R.id.iv_header_logo_insta, R.id.iv_header_logo_whatsapp})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header_logo_fb:

                String url = "https://www.facebook.com/EppmeApp-192065647999244/";
                Helper.AppRedirectViewIntent(mActivity, url);

                break;
            case R.id.iv_header_logo_twiter:
                String url1 = "https://twitter.com/EppmeApp";
                Helper.AppRedirectViewIntent(mActivity, url1);
                break;
            case R.id.iv_header_logo_insta:
                String url2 = "https://www.instagram.com/eppmeapp/";
                Helper.AppRedirectViewIntent(mActivity, url2);
                break;
            case R.id.iv_header_logo_whatsapp:
                Helper.WhatsAppIntent(mActivity);
                break;

        }
    }

}
