package com.weddingplus.user_interface.activities;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.weddingplus.App;
import com.weddingplus.BuildConfig;
import com.weddingplus.R;
import com.weddingplus.models.Category;
import com.weddingplus.models.SubCategoryData;
import com.weddingplus.sync.AddStudentManagerModel;
import com.weddingplus.sync.PreferenceApp;
import com.weddingplus.sync.StudentCategoriesModel;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.user_interface.adapters.CustomPagerAdapter;
import com.weddingplus.user_interface.adapters.recycler_adapters.EmergencyResponseAdapter;
import com.weddingplus.user_interface.dialog.MedicalDialog;
import com.weddingplus.user_interface.dialog.StudentManagerDialog;
import com.weddingplus.user_interface.interfaces.EventSend;
import com.weddingplus.user_interface.interfaces.EvnetDialogRefresh;
import com.weddingplus.user_interface.interfaces.RemoveImage;
import com.weddingplus.user_interface.interfaces.StopRecording;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.CategoryResponse;
import com.weddingplus.web_services.responses.SubCatResposne;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.kaelaela.verticalviewpager.VerticalViewPager;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Response;

import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static com.weddingplus.utilities.PreferenceConstants.KEY_LATTITUDE;
import static com.weddingplus.utilities.PreferenceConstants.KEY_LONGITUDE;

/**
 * Created by keval on 30/6/17.
 */

public class StudentManagerActivity extends BaseAppCompatActivity {

    public static final int RequestPermissionCode = 1;
    private static final String AUDIO_RECORDER_FILE_EXT_3GP = ".3gp";
    private static final String AUDIO_RECORDER_FILE_EXT_MP4 = ".mp4";
    private static final String AUDIO_RECORDER_FOLDER = "eppMe";
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.card_view_robbery)
    CardView cardViewRobbery;
    @BindView(R.id.card_view_rape)
    CardView cardViewRape;
    @BindView(R.id.card_view_fire)
    CardView cardViewFire;
    @BindView(R.id.card_view_medical)
    CardView cardViewMedical;
    @BindView(R.id.card_view_other_emergency)
    CardView cardViewOtherEmergency;
    //    RobberyDialog robberyDialog;
    StudentManagerDialog studentManagerDialog;
    MedicalDialog medicalDialog;
   // RecyclerView mRecyclerView;


    //    OtherEmergencyDialog otherEmergencyDialog;
/*    @BindView(R.id.rcv_emergency_response)
    RecyclerView rcvEmergencyResponse;*/
    EmergencyResponseAdapter emergencyResponseAdapter;
    MultipartBody.Part part;
    String emergency_cat_id;
    String currenaudiopath;
    @BindView(R.id.tv_toolbar_title)
    XTextView tvToolbarTitle;
    @BindView(R.id.lay_swipe_up)
    LinearLayout laySwipeUp;
    @BindView(R.id.pager)
    VerticalViewPager pager;
    /*    @BindView(R.id.pager)
        ViewPager pager;*/
    private MediaRecorder recorder = null;
    private int currentFormat = 0;
    private int output_formats[] = {MediaRecorder.OutputFormat.MPEG_4, MediaRecorder.OutputFormat.THREE_GPP};
    private String file_exts[] = {AUDIO_RECORDER_FILE_EXT_MP4, AUDIO_RECORDER_FILE_EXT_3GP};
    private MediaRecorder.OnErrorListener errorListener = new MediaRecorder.OnErrorListener() {
        @Override
        public void onError(MediaRecorder mr, int what, int extra) {
            Log.e("Error: ", +what + ", " + extra);
            currenaudiopath = "";
        }
    };
    private MediaRecorder.OnInfoListener infoListener = new MediaRecorder.OnInfoListener() {
        @Override
        public void onInfo(MediaRecorder mr, int what, int extra) {
            Log.e("Warning: ", +what + ", " + extra);
            currenaudiopath = "";
        }
    };

    int attechtype;
    String path, type = "";
    ArrayList<SubCategoryData> arrayList_sub_cat;
    File photoFile;

    StudentCategoriesModel allCategories;
    PreferenceApp pref;
    /**
     * Called when the activity is first created.
     */
    Uri outputFileUri;
    int pos = 1;

    public static String getVideoFilePath(Uri uri, Context appCompatActivity) {
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = appCompatActivity.getContentResolver().query(uri, projection, null, null, null);
        if (cursor != null) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            cursor.moveToFirst();
            Log.e("video path", cursor.getString(column_index) + "");
            return cursor.getString(column_index);
        } else {
            return null;
        }

    }

    public static MultipartBody.Part createPartFileFromPath(String filepath) {
        File destination = new File(filepath);
        RequestBody requestFile = RequestBody.create(MediaType.parse("video/*"), destination);
        return MultipartBody.Part.createFormData("attachment", destination.getName(), requestFile);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_reponse);
        ButterKnife.bind(this);
        EventBus.getDefault().register(StudentManagerActivity.this);

        setUpToolbar();

        // getToolbar().setBackgroundColor(getResources().getColor(R.color.red));
        pref = new PreferenceApp(this);
        tvToolbarTitle.setText(R.string.electiond_nmanager);
    /*    if (Helper.getUserType().equalsIgnoreCase(INDIVIDUAL)) {
            tvToolbarTitle.setText(R.string.student_manager);
        }else {
            tvToolbarTitle.setText(R.string.staff_manager);
        }
*/



        allCategories = pref.getStudentCategories();
        if (allCategories == null)
            allCategories = new StudentCategoriesModel();

        emergencyResponseAdapter = new EmergencyResponseAdapter(mActivity) {
            @Override
            protected void onItemClick(int adapterPosition, Category category) {
                super.onItemClick(adapterPosition, category);
                if (null != category) {

                    // getStudentSubCatResponse(category);
                    emergency_cat_id = String.valueOf(category.getCategory_id());

                    if (is_Network_Available())
                        getStudentSubCatResponse(category);
                    else {
                        ArrayList<SubCategoryData> arrayList_subcategory = new ArrayList<>();
                        for (int i = 0; i < allCategories.getData().size(); i++) {
                            if (allCategories.getData().get(i).getCategoryId().equals(category.getCategory_id())) {
                                for (int j = 0; j < allCategories.getData().get(i).getStudentSubcategory().size(); j++) {
                                    SubCategoryData subCategoryData = new SubCategoryData();
                                    subCategoryData.setCategoryId(allCategories.getData().get(i).getStudentSubcategory().get(j).getId());
                                    subCategoryData.setCategoryName(allCategories.getData().get(i).getStudentSubcategory().get(j).getName());
                                    arrayList_subcategory.add(subCategoryData);
                                }
//                                arrayList_sub_cat.addAll(new ArrayList<SubCategoryData>(arrayList_subcategory));

                                studentManagerDialog = new StudentManagerDialog(StudentManagerActivity.this, category.getCategory_name(), arrayList_subcategory
                                );
                                if (!studentManagerDialog.isShowing()) {
                                    studentManagerDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                                }
                                studentManagerDialog.show();
                                break;
                            }
                        }
                    }

                   /* emergency_cat_id = String.valueOf(category.getCategory_id());
                    if (String.valueOf(category.getCategory_id()).equalsIgnoreCase("1")) {
                        robberyDialog = new RobberyDialog(StudentManagerActivity.this, category.getCategory_name());
                        if (!robberyDialog.isShowing()) {
                            robberyDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                        }
                        robberyDialog.show();

                    } else if (String.valueOf(category.getCategory_id()).equalsIgnoreCase("2")) {
                        robberyDialog = new RobberyDialog(StudentManagerActivity.this, category.getCategory_name());
                        if (!robberyDialog.isShowing()) {
                            robberyDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                        }
                        robberyDialog.show();
                    } else if (String.valueOf(category.getCategory_id()).equalsIgnoreCase("3")) {
                        robberyDialog = new RobberyDialog(StudentManagerActivity.this, category.getCategory_name());
                        if (!robberyDialog.isShowing()) {
                            robberyDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                        }
                        robberyDialog.show();
                    } else if (String.valueOf(category.getCategory_id()).equalsIgnoreCase("4")) {
                        medicalDialog = new MedicalDialog(StudentManagerActivity.this, category.getCategory_name());

                        medicalDialog.show();
                    } else if (String.valueOf(category.getCategory_id()).equalsIgnoreCase("5")) {
                        otherEmergencyDialog = new OtherEmergencyDialog(StudentManagerActivity.this, "");
                        if (!otherEmergencyDialog.isShowing()) {
                            otherEmergencyDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                        }
                        otherEmergencyDialog.show();
                    }

                    else
                        {

                        robberyDialog = new RobberyDialog(StudentManagerActivity.this, category.getCategory_name());
                        if (!robberyDialog.isShowing()) {
                            robberyDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                        }
                        robberyDialog.show();
                    }
*/
                  /*  studentManagerDialog = new StudentManagerDialog(StudentManagerActivity.this, category.getCategory_name());
                    if (!studentManagerDialog.isShowing()) {
                        studentManagerDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                    }
                    studentManagerDialog.show();*/
                }
            }
        };
      //  rcvEmergencyResponse.setAdapter(emergencyResponseAdapter);

        // getStudentCatResponse();
        if (is_Network_Available()) {
            getStudentCatResponse();
        } else {

            ArrayList<Category> arrayList = new ArrayList<>();
            if (allCategories.getData() != null)
                for (int i = 0; i < allCategories.getData().size(); i++) {
                    Category obj = new Category();
                    obj.setCategory_id(allCategories.getData().get(i).getCategoryId());
                    obj.setCategory_name(allCategories.getData().get(i).getCategoryName());
                    obj.setCategory_image(allCategories.getData().get(i).getCategoryImage());
                    arrayList.add(obj);
                }

            int size = 8;
            List<Category> sublist=new ArrayList<>();
            ArrayList<ArrayList<Category>> mainlist=new ArrayList<>();

            for (int start = 0; start < arrayList.size(); start += size) {
                int end = Math.min(start + size, arrayList.size());
                sublist = arrayList.subList(start, end);
                Log.e("sublist size:",""+sublist.size());
                mainlist.add(new ArrayList<Category>(sublist));
                // ContentFragment contentFragment=new ContentFragment();
                // contentFragment.newInstance(mActivity,new ArrayList<Category>(sublist),start);
                // listfraFragments.add(contentFragment);
            }
            CustomPagerAdapter customPagerAdapter=new CustomPagerAdapter(mActivity){
                @Override
                protected void onClick(Category category, int adapterPosition) {
                    super.onClick(category, adapterPosition);
                    emergency_cat_id = String.valueOf(category.getCategory_id());

                    if (is_Network_Available())
                        getStudentSubCatResponse(category);
                    else {
                        ArrayList<SubCategoryData> arrayList_subcategory = new ArrayList<>();
                        for (int i = 0; i < allCategories.getData().size(); i++) {
                            if (allCategories.getData().get(i).getCategoryId().equals(category.getCategory_id())) {
                                for (int j = 0; j < allCategories.getData().get(i).getStudentSubcategory().size(); j++) {
                                    SubCategoryData subCategoryData = new SubCategoryData();
                                    subCategoryData.setCategoryId(allCategories.getData().get(i).getStudentSubcategory().get(j).getId());
                                    subCategoryData.setCategoryName(allCategories.getData().get(i).getStudentSubcategory().get(j).getName());
                                    arrayList_subcategory.add(subCategoryData);
                                }
//                                arrayList_sub_cat.addAll(new ArrayList<SubCategoryData>(arrayList_subcategory));

                                studentManagerDialog = new StudentManagerDialog(StudentManagerActivity.this, category.getCategory_name(), arrayList_subcategory
                                );
                                if (!studentManagerDialog.isShowing()) {
                                    studentManagerDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                                }
                                studentManagerDialog.show();
                                break;
                            }
                        }
                    }
                }
            };
            pager.setAdapter(customPagerAdapter);
            customPagerAdapter.addAll(mainlist);
            //If you setting other scroll mode, the scrolled fade is shown from either side of display.
            // pager.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
            // contentFragmentAdapter.addAll(listfraFragments);


           /* emergencyResponseAdapter.addAll(response.body().getData());
            if (response.body().getData().size() > 0) {
                laySwipeUp.setVisibility(View.VISIBLE);
            }*/

           // emergencyResponseAdapter.addAll(arrayList);
        }


  /*      laySwipeUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                GridLayoutManager layoutManager = ((GridLayoutManager) rcvEmergencyResponse.getLayoutManager());
                int firstVisiblePosition = layoutManager.findFirstVisibleItemPosition();
                int lastVisiblePosition = layoutManager.findLastVisibleItemPosition();

                Log.e("first pos", "" + firstVisiblePosition);
                Log.e("last pos", "" + lastVisiblePosition);

                rcvEmergencyResponse.scrollToPosition(lastVisiblePosition + 1);


             int scpos=pos*8;
                if(scpos<=rcvEmergencyResponse.getAdapter().getItemCount()) {
                    rcvEmergencyResponse.scrollToPosition(scpos);
                    pos = pos + 1;
                }
            }
        });*/

        laySwipeUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    pager.setCurrentItem(pager.getCurrentItem() + 1);
                }
                catch (Exception e)
                {e.printStackTrace();}
            }
        });
    }

    private void getStudentCatResponse() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<CategoryResponse> callback = new BaseCallback<CategoryResponse>() {
            @Override
            public void Success(Response<CategoryResponse> response) {
                stopProgress();


                ArrayList<Category> arrayList = new ArrayList<>();
                if (allCategories.getData() != null)
                    for (int i = 0; i < allCategories.getData().size(); i++) {

                       // arrayList=new ArrayList<>();
                        Category obj = new Category();
                        obj.setCategory_id(allCategories.getData().get(i).getCategoryId());
                        obj.setCategory_name(allCategories.getData().get(i).getCategoryName());
                        obj.setCategory_image(allCategories.getData().get(i).getCategoryImage());
                        arrayList.add(obj);


                    }
                int size = 8;
                List<Category> sublist=new ArrayList<>();
                ArrayList<ArrayList<Category>> mainlist=new ArrayList<>();

                for (int start = 0; start < arrayList.size(); start += size) {
                    int end = Math.min(start + size, arrayList.size());
                    sublist = arrayList.subList(start, end);
                    Log.e("sublist size:",""+sublist.size());
                    mainlist.add(new ArrayList<Category>(sublist));
                   // ContentFragment contentFragment=new ContentFragment();
                   // contentFragment.newInstance(mActivity,new ArrayList<Category>(sublist),start);
                   // listfraFragments.add(contentFragment);
                }

                CustomPagerAdapter customPagerAdapter=new CustomPagerAdapter(mActivity){
                    @Override
                    protected void onClick(Category category, int adapterPosition) {
                        super.onClick(category, adapterPosition);
                        emergency_cat_id = String.valueOf(category.getCategory_id());

                        if (is_Network_Available())
                            getStudentSubCatResponse(category);
                        else {
                            ArrayList<SubCategoryData> arrayList_subcategory = new ArrayList<>();
                            for (int i = 0; i < allCategories.getData().size(); i++) {
                                if (allCategories.getData().get(i).getCategoryId().equals(category.getCategory_id())) {
                                    for (int j = 0; j < allCategories.getData().get(i).getStudentSubcategory().size(); j++) {
                                        SubCategoryData subCategoryData = new SubCategoryData();
                                        subCategoryData.setCategoryId(allCategories.getData().get(i).getStudentSubcategory().get(j).getId());
                                        subCategoryData.setCategoryName(allCategories.getData().get(i).getStudentSubcategory().get(j).getName());
                                        arrayList_subcategory.add(subCategoryData);
                                    }
//                                arrayList_sub_cat.addAll(new ArrayList<SubCategoryData>(arrayList_subcategory));

                                    studentManagerDialog = new StudentManagerDialog(StudentManagerActivity.this, category.getCategory_name(), arrayList_subcategory
                                    );
                                    if (!studentManagerDialog.isShowing()) {
                                        studentManagerDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                                    }
                                    studentManagerDialog.show();
                                    break;
                                }
                            }
                        }
                    }

                    @Override
                    protected void onTouchV(boolean isScrollup) {
                        super.onTouchV(isScrollup);
                     //   mRecyclerView=view;
                        if(isScrollup) {
                            try {
                                pager.setCurrentItem(pager.getCurrentItem() - 1);

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                        else
                        {
                            try {
                                pager.setCurrentItem(pager.getCurrentItem() + 1);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }

                    }

                };
                pager.setAdapter(customPagerAdapter);
                customPagerAdapter.addAll(mainlist);
                //If you setting other scroll mode, the scrolled fade is shown from either side of display.
                pager.setOverScrollMode(View.OVER_SCROLL_ALWAYS);
               // contentFragmentAdapter.addAll(listfraFragments);


                emergencyResponseAdapter.addAll(response.body().getData());
                if (response.body().getData().size() > 0) {
                    laySwipeUp.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.getStudentCategory().enqueue(callback);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onAttachClick(EvnetDialogRefresh attach) {
        selectImage();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onClearImageClick(RemoveImage removeimage) {
        part = null;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onSendClick(EventSend send) {
        //onSendClickDataPost(send.getMessage(), send.getEmergency_category_id());
        if (is_Network_Available()) {
            onSendClickDataPost(send.getMessage(), send.getEmergency_category_id());
        } else {
            PreferenceApp pref = new PreferenceApp(getApplicationContext());
            List<AddStudentManagerModel> list = new ArrayList<>();
            list = pref.getStudentManagerList();
            if (list == null)
                list = new ArrayList<>();

            AddStudentManagerModel obj = new AddStudentManagerModel();
            obj.setAttachment_type("" + attechtype);
            obj.setAuth_key(App.getAuthKey());
            obj.setDescription(send.getMessage());
            obj.setStudent_category_id(emergency_cat_id);
            obj.setStudent_subcategory_id(send.getEmergency_category_id());
            obj.setLatitude(Helper.getStringValue(KEY_LATTITUDE));
            obj.setLongitude(Helper.getStringValue(KEY_LONGITUDE));
            obj.setPath(path);
            obj.setType(type);
            list.add(obj);
            pref.setStudentManagerList(list);
        }

    }

    public boolean is_Network_Available() {
        boolean toReturn;
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();

        toReturn = activeNetworkInfo != null && activeNetworkInfo.isConnected();

        return toReturn;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onStopRecording(StopRecording send) {
        stopRecording();
        if (studentManagerDialog != null) {
            studentManagerDialog.updateBtntext("stop");
        }
        if (studentManagerDialog != null) {
            studentManagerDialog.updateBtntext("stop");
        }
    }

    private void onSendClickDataPost(String desc, String sub_cat_id) {


        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                if (response.body().isSuccess()) {
                    part = null;


                    toast(response.body().getMessage());

                } else {
                    toast(response.body().getMessage());
                }
//                robberyDialog.dismiss();
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast("Please try again later.");
//                robberyDialog.dismiss();
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);
        WebServiceHelper.AddStudentManagerLog("" + attechtype, part, desc, emergency_cat_id, sub_cat_id).enqueue(callback);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBus.getDefault().unregister(this);
    }

    private void selectImage() {
        if (studentManagerDialog != null) {
            if (studentManagerDialog.isShowing()) {
                studentManagerDialog.getWindow().setWindowAnimations(0);
            }
        }
        if (studentManagerDialog != null) {
            if (studentManagerDialog.isShowing()) {
                studentManagerDialog.getWindow().setWindowAnimations(0);
            }
        }

        final CharSequence[] items = {getString(R.string.take_photo), getString(R.string.take_video), getString(R.string.voice),
                getString(R.string.cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(getString(R.string.add_photo));
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals(getString(R.string.take_photo))) {
                    cameraIntent("photo");
                }
              /*  else if (items[item].equals(getString(R.string.choose_photo_from_library))) {
                    galleryIntent("photo");
                } */
                else if (items[item].equals(getString(R.string.cancel))) {
                    dialog.dismiss();
                }
//                else if (items[item].equals(getString(R.string.choose_video_from_library))) {
//                    galleryIntent("video");
//                }
                else if (items[item].equals(getString(R.string.take_video))) {
                    cameraIntent("video");
                } else if (items[item].equals(getString(R.string.voice))) {
                    cameraIntent("voice");
                }
            }
        });
        builder.show();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (outputFileUri != null) {
            outState.putString("cameraImageUri", outputFileUri.toString());
        }

    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState.containsKey("cameraImageUri")) {
            outputFileUri = Uri.parse(savedInstanceState.getString("cameraImageUri"));
        }
    }

    private void cameraIntent(String flag) {
        type = flag;
        if (flag.equals("photo")) {
            Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            if (takePictureIntent.resolveActivity(this.getPackageManager()) != null) {
                photoFile = null;
                try {
                    photoFile = createImageFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                if (photoFile != null) {
                    outputFileUri = FileProvider.getUriForFile(mActivity, BuildConfig.APPLICATION_ID + ".com.weddingplus.provider", photoFile);
                    // outputFileUri = Uri.fromFile(photoFile);
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
                    takePictureIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    startActivityForResult(takePictureIntent, REQUEST_CAMERA);
                }
            }
     /*       Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(intent, REQUEST_CAMERA);*/
        } else if (flag.equals("video")) {
            Intent captureVideoIntent = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
            captureVideoIntent.putExtra(MediaStore.EXTRA_DURATION_LIMIT, 120);
            captureVideoIntent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 0);
            startActivityForResult(captureVideoIntent, REQUEST_VIDEO);
        } else if (flag.equals("voice")) {
            if (recorder == null) {
                if (checkPermission()) {

                    startRecording();

                } else {

                    requestPermission();

                }
            } else {
                stopRecording();
            }
        }
    }

    private void startRecording() {
        recorder = new MediaRecorder();
        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(output_formats[currentFormat]);
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        recorder.setOutputFile(getFilename());
        //  recorder.setAudioEncodingBitRate(20);
        recorder.setOnErrorListener(errorListener);
        recorder.setOnInfoListener(infoListener);

        try {
            recorder.prepare();
            recorder.start();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (studentManagerDialog != null) {
            studentManagerDialog.updateBtntext("start");
        }
        if (studentManagerDialog != null) {
            studentManagerDialog.updateBtntext("start");
        }
    }

    private void stopRecording() {
        if (null != recorder) {
            recorder.stop();
            recorder.reset();
            recorder.release();

            recorder = null;
            path = currenaudiopath;

            if (StringUtils.isValid(currenaudiopath)) {
                File destination = new File(currenaudiopath);

                RequestBody reqFile = RequestBody.create(MediaType.parse("audio/*"), destination);
                if (null != studentManagerDialog) {

                    studentManagerDialog.updateudiofinish();
                }
                if (null != studentManagerDialog) {

                    studentManagerDialog.updateudiofinish();
                }
                Log.e("audio pth :", "" + destination.getPath());

                part = MultipartBody.Part.createFormData("attachment", destination.getName(), reqFile);
                attechtype = 3;
            }
        }
    }

    private String getFilename() {
        String filepath = Environment.getExternalStorageDirectory().getPath();
        File file = new File(filepath, AUDIO_RECORDER_FOLDER);

        if (!file.exists()) {
            file.mkdirs();
        }

/*        currenaudiopath = file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat];
        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + file_exts[currentFormat]);*/

        currenaudiopath = file.getAbsolutePath() + "/file" + file_exts[currentFormat];
        return (file.getAbsolutePath() + "/file" + file_exts[currentFormat]);
    }

    private void galleryIntent(String flag) {
        if (flag.equals("photo")) {
            Intent intent = new Intent();
            intent.setAction(Intent.ACTION_GET_CONTENT);//
            intent.setType("image/*");
            startActivityForResult(Intent.createChooser(intent, "Select Image"), SELECT_FILE);
        } else {

            Intent i = new Intent(Intent.ACTION_PICK,
                    MediaStore.Video.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(i, SELECT_VIDEO);

           /* Intent intent = new Intent();
            intent.setAction(Intent.ACTION_PICK);//
            intent.setType("video*//*");
            startActivityForResult(Intent.createChooser(intent, "Select Video"), SELECT_VIDEO);*/
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == SELECT_FILE)
                onSelectFromGalleryResult(data);
            else if (requestCode == REQUEST_CAMERA)
                onCaptureImageResult(data);
            else if (requestCode == REQUEST_VIDEO)
                onSelectFromVideoResult(data);
            else if (requestCode == SELECT_VIDEO)
                onCaptureVideoResult(data);

        }
    }

    private void onCaptureVideoResult(Intent data) {
        int msec = MediaPlayer.create(mActivity, data.getData()).getDuration();
        Log.e("video length", "" + msec);                // minutes 911111
        if (msec > videolimit) {
            toast(getString(R.string.video_limit));
            part = null;
        } else {

            //  Log.d(TAG, "onCaptureVideoResult: " + data.getData().getPath());
            String selectedVideoPath = getVideoFilePath(data.getData(), mActivity);
            path = selectedVideoPath;
            part = createPartFileFromPath(selectedVideoPath);
            attechtype = 2;

         /*   File file = new File(data.getData().getPath() + ".mp4");
            RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file.getPath());
            part = MultipartBody.Part.createFormData("attachment", file.getName(), requestFile);*/
            if (studentManagerDialog != null) {
                studentManagerDialog.update(selectedVideoPath);
            }
            if (studentManagerDialog != null) {
                studentManagerDialog.update(selectedVideoPath);
            }
        }

    }

    private void onSelectFromVideoResult(Intent data) {
        int msec = MediaPlayer.create(mActivity, data.getData()).getDuration();
        Log.e("video length", "" + msec);                // minutes 911111
        if (msec > videolimit) {
            toast(getString(R.string.video_limit));
            part = null;
        } else {
            Log.d(TAG, "onSelectFromVideoResult: video come" + data.getData().getPath());
            String selectedVideoPath = getVideoFilePath(data.getData(), mActivity);
            path = selectedVideoPath;
            part = createPartFileFromPath(selectedVideoPath);
            attechtype = 2;
       /* File file = new File(data.getData().getPath() + ".mp4");
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file.getPath());
        part = MultipartBody.Part.createFormData("attachment", file.getName(), requestFile);*/
            if (studentManagerDialog != null) {

                studentManagerDialog.update(selectedVideoPath);
            }
            if (studentManagerDialog != null) {
                studentManagerDialog.update(selectedVideoPath);
            }
        }

//        Uri selectedImageUri = data.getData();
//        String selectedPath = selectedImageUri.getPath();
//        System.out.println("SELECT_VIDEO Path : " + selectedPath);

    }

    private void onSelectFromGalleryResult(Intent data) {
        Bitmap bm = null;
        Log.d(TAG, "onSelectFromGalleryResult: " + data.getType());
        if (data != null) {
            try {
                bm = MediaStore.Images.Media.getBitmap(getContentResolver(), data.getData());
                if (null == bm) {
                    Toast.makeText(this, "Not Valid Image", Toast.LENGTH_SHORT);
                } else {
                    //   part = createPartImageFromBitmap(bm, true);
                    //  attechtype = 1;
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


    }

    private void onCaptureImageResult(Intent data) {
        if (outputFileUri != null) {

            try {
                String selectedImagePath = photoFile.getPath();
                path = selectedImagePath;
                // String selectedImagePath = outputFileUri.getPath();
                Log.e("selectedImagePath", "" + selectedImagePath);
                part = createPartImageFromBitmap(selectedImagePath);
                attechtype = 1;
            /*    Bitmap bitmap = BitmapLoader.downSampleBitmap(selectedImagePath, imgRightPrectice);
                int imageAngle = StringUtils.getImageAngle(selectedImagePath);
                Bitmap rotateBitMap = StringUtils.rotateImage(bitmap, imageAngle);
                imgRightPrectice.setImageBitmap(rotateBitMap);
                hashMapImg.put("img1",selectedImagePath);*/
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        //  Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        // ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        // thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        //  part = createPartImageFromBitmap(thumbnail, true);
        //  attechtype = 1;

    }

    public static File createImageFile() throws IOException {
      /*  File myDir = new File(Environment.getExternalStorageDirectory() + "/" + "beLocum");
        if (!myDir.exists()) {
            myDir.mkdirs();
        }
*/
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        // File file = new File(myDir, "locum_" + System.currentTimeMillis() + ".jpg");
        return destination;
    }

    public MultipartBody.Part createPartImageFromBitmap(String path) {

        if (null != studentManagerDialog) {

            studentManagerDialog.update(path);
        }
        if (null != studentManagerDialog) {

            studentManagerDialog.update(path);
        }

        RequestBody reqFile1 = RequestBody.create(MediaType.parse("image/*"), new File(path));
        return MultipartBody.Part.createFormData("attachment", new File(path).getName(), reqFile1);

       /* ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();
            RequestBody reqFile = RequestBody.create(MediaType.parse("image*//*"), destination);
            if (null != studentManagerDialog) {

                studentManagerDialog.update(destination.toString());
            }
            if (null != studentManagerDialog) {

                studentManagerDialog.update(destination.toString());
            }

//            Picasso.with(this).load(destination).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(ivUserImag);
//            selectedImagePath = destination.getAbsolutePath();
//            Log.e("path", "" + selectedImagePath);
            return MultipartBody.Part.createFormData("attachment", destination.getName(), reqFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;*/
    }


    private void requestPermission() {
        ActivityCompat.requestPermissions(mActivity, new
                String[]{WRITE_EXTERNAL_STORAGE, RECORD_AUDIO}, RequestPermissionCode);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case RequestPermissionCode:
                if (grantResults.length > 0) {
                    boolean StoragePermission = grantResults[0] ==
                            PackageManager.PERMISSION_GRANTED;
                    boolean RecordPermission = grantResults[1] ==
                            PackageManager.PERMISSION_GRANTED;

                    if (StoragePermission && RecordPermission) {
                        startRecording();
                     /*   Toast.makeText(mActivity, "Permission Granted",
                                Toast.LENGTH_LONG).show();*/
                    } else {
                        Toast.makeText(mActivity, "Permission Denied", Toast.LENGTH_LONG).show();
                    }
                }
                break;
        }
    }

    public boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(getApplicationContext(),
                WRITE_EXTERNAL_STORAGE);
        int result1 = ContextCompat.checkSelfPermission(getApplicationContext(),
                RECORD_AUDIO);
        return result == PackageManager.PERMISSION_GRANTED &&
                result1 == PackageManager.PERMISSION_GRANTED;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.invisible, menu);

        return super.onCreateOptionsMenu(menu);
    }

    /*@Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_innstruction) {
            UiHelper.startEmergencyInstructionActivity(mActivity);
        }
        return super.onOptionsItemSelected(item);
    }*/

    private void getStudentSubCatResponse(final Category category) {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<SubCatResposne> callback = new BaseCallback<SubCatResposne>() {
            @Override
            public void Success(Response<SubCatResposne> response) {
                stopProgress();
                arrayList_sub_cat = new ArrayList<>();
                arrayList_sub_cat.addAll(new ArrayList<SubCategoryData>(response.body().getData()));

                studentManagerDialog = new StudentManagerDialog(StudentManagerActivity.this, category.getCategory_name(), arrayList_sub_cat);
                if (!studentManagerDialog.isShowing()) {
                    studentManagerDialog.getWindow().setWindowAnimations(R.style.DialogTheme);
                }
                studentManagerDialog.show();
//                emergencyResponseAdapter.addAll(response.body().getData());

            }

            @Override
            public void Failure(String message) {
                stopProgress();
                toast(message);
            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.getStudentSubCategory(String.valueOf(category.getCategory_id())).enqueue(callback);
    }


}
