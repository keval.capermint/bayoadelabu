package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.weddingplus.R;
import com.weddingplus.models.ProductData;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.AppConstants;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.XButton;
import com.weddingplus.views.XTextView;
import com.weddingplus.web_services.BaseCallback;
import com.weddingplus.web_services.BaseResponse;
import com.weddingplus.web_services.WebServiceHelper;
import com.weddingplus.web_services.responses.CartItemCountResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Response;

import static com.weddingplus.utilities.PreferenceConstants.KEY_USER_NAME;

/**
 * Created by keval on 5/5/17.
 */

public class ProductDetailsActivity extends BaseAppCompatActivity {


    ProductData mProductData;
    @BindView(R.id.txt_product_name)
    XTextView txtProductName;
    @BindView(R.id.txt_product_price)
    XTextView txtProductPrice;
    @BindView(R.id.txt_product_discription)
    XTextView txtProductDiscription;
    @BindView(R.id.txt_free_shipping)
    XTextView txtFreeShipping;
    @BindView(R.id.btn_buy_now)
    XButton btnBuyNow;
    @BindView(R.id.img_product)
    ImageView imgProduct;
    @BindView(R.id.txt_cart_count)
    XTextView txtCartCount;
    @BindView(R.id.txt_welcome_msg)
    XTextView txtWelcomeMsg;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        ButterKnife.bind(this);
        setUpToolbar();

        mProductData = getIntent().getParcelableExtra(AppConstants.EXTRA_KEY_DATA);

        setupViews();


    }

    @Override
    protected void onResume() {
        super.onResume();
        getItemCount();
    }

    public void setupViews() {

        if (StringUtils.isValid(mProductData.getAttachment())) {
            Picasso.with(mActivity).load(mProductData.getAttachment()).placeholder(R.mipmap.ic_launcher).resize(UiHelper.getDeviceWidthInPercentage(30), 0).into(imgProduct);
        }
        txtProductName.setText(mProductData.getName());
        txtProductDiscription.setText(mProductData.getDescription());
        txtProductPrice.setText(getResources().getString(R.string.currency_symbol, mProductData.getPrice()));
        txtWelcomeMsg.setText(""+"Hello " + Helper.getStringValue(KEY_USER_NAME) + ", Enjoy your Shopping");
    }

    private void getItemCount() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<CartItemCountResponse> callback = new BaseCallback<CartItemCountResponse>() {
            @Override
            public void Success(Response<CartItemCountResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    txtCartCount.setVisibility(View.VISIBLE);
                    txtCartCount.setText(response.body().getData());
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };
        startProgress(false);

        WebServiceHelper.getCartItemCount().enqueue(callback);
    }

    @OnClick({R.id.iv_header_logo_whatsapp, R.id.iv_header_logo_fb, R.id.iv_header_logo_twiter, R.id.iv_header_logo_insta, R.id.ll_instruction, R.id.btn_buy_now, R.id.img_cart, R.id.img_user})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.iv_header_logo_whatsapp:
                Helper.WhatsAppIntent(mActivity);
                break;
            case R.id.iv_header_logo_fb:
                String url = "https://www.facebook.com/EppmeApp-192065647999244/";
                Helper.AppRedirectViewIntent(mActivity, url);
                break;
            case R.id.iv_header_logo_twiter:
                String url1 = "https://twitter.com/EppmeApp";
                Helper.AppRedirectViewIntent(mActivity, url1);
                break;
            case R.id.iv_header_logo_insta:
                String url2 = "https://www.instagram.com/eppmeapp/";
                Helper.AppRedirectViewIntent(mActivity, url2);
                break;
            case R.id.ll_instruction:

                break;
            case R.id.btn_buy_now:
                addToCartCall();
                break;
            case R.id.img_user:
                break;
            case R.id.img_cart:
                UiHelper.starCartActivity(mActivity);
                break;

        }
    }

    private void addToCartCall() {
        if (!Helper.isNetworkAvailable(mActivity)) {
            return;
        }
        BaseCallback<BaseResponse> callback = new BaseCallback<BaseResponse>() {
            @Override
            public void Success(Response<BaseResponse> response) {
                stopProgress();
                if (response.isSuccessful()) {
                    //  toast(response.message());
                    UiHelper.starCartActivity(mActivity);
                } else {
                    toast(response.message());
                }
            }

            @Override
            public void Failure(String message) {
                stopProgress();
                Log.e("Message,", "" + message);

            }

            @Override
            public void SessionExpired(String message) {
                toast(message);
                Helper.clearPreferences();
                UiHelper.startWelcomeActivity(mActivity);
            }
        };

        startProgress(false);
        WebServiceHelper.addToCart(mProductData.getId()).enqueue(callback);
    }
}
