package com.weddingplus.user_interface.activities;

import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;

import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.UiHelper;

/**
 * Created by Manndeep Vachhani on 8/2/17.
 */
public class SplashActivity extends BaseAppCompatActivity {

    private DialogInterface.OnClickListener positiveButtonListener;
    int SPLASH_TIMEOUT = 4000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);


        Handler handler = new Handler();

        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                UiHelper.startAfterSplash(SplashActivity.this);
            }
        },2500);


        positiveButtonListener = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                Helper.clearPreferences();
                UiHelper.startAfterSplash(SplashActivity.this);
            }
        };
    }


 /*   @Override
    protected void onResume() {
        super.onResume();
        if (Helper.isNetworkAvailable(this)) {
            if (Helper.isLoggedIn()) {
                startProgress(false);
                WebServiceHelper.getAutoLoginCall(AppConstants.APP_VERSION, AppConstants.DEVICE_TYPE)
                        .enqueue(new Callback<UserResponse>() {
                            @Override
                            public void onResponse(Call<UserResponse> call, Response<UserResponse> response) {
                                stopProgress();
                                if (response.isSuccessful()) {
                                    if (response.body().isSuccess()) {
                                        Helper.setUserValues(response.body().getUser());
                                        UiHelper.startAfterSplash(SplashActivity.this);
                                    }
                                    else {
                                        UiHelper.showLogoutDialog(mActivity, getString(R.string.login_error), positiveButtonListener);
                                    }
                                } else {
//                                    UiHelper.showLogoutDialog(mActivity, getString(R.string.login_error), positiveButtonListener);
                                }
                            }

                            @Override
                            public void onFailure(Call<UserResponse> call, Throwable t) {
                                stopProgress();
//                                UiHelper.showLogoutDialog(mActivity, getString(R.string.login_error), positiveButtonListener);
                            }
                        });
            } else {
                *//*
                  Handler to fire event on splash time out ending
                 *//*
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        UiHelper.startAfterSplash(SplashActivity.this);
                    }
                }, SPLASH_TIMEOUT);

            }
        }
    }*/
}

