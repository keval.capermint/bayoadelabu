package com.weddingplus.user_interface.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.weddingplus.R;
import com.weddingplus.user_interface.BaseAppCompatActivity;
import com.weddingplus.utilities.AppConstants;
import com.weddingplus.utilities.Helper;
import com.weddingplus.utilities.StringUtils;
import com.weddingplus.utilities.UiHelper;
import com.weddingplus.views.TouchImageView;
import com.weddingplus.views.XTextView;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by keval on 30/6/17.
 */

public class ImageFullViewActivity extends BaseAppCompatActivity {


    @BindView(R.id.txt_dashboard_header)
    XTextView txtDashboardHeader;
    @BindView(R.id.txt_header)
    XTextView txtHeader;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.imageview)
    ImageView imageview;

    String imageurl;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_full_view);
        ButterKnife.bind(this);
        setUpToolbar();
        txtDashboardHeader.setText(R.string.image);


        imageurl=getIntent().getStringExtra(AppConstants.EXTRA_KEY_URL);

        if(StringUtils.isValid(imageurl))
        {
            Picasso.with(mActivity).load(imageurl).resize(UiHelper.getDeviceWidthInPercentage(50),0).into(imageview);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_share, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==R.id.action_share)
        {
            Helper.shareIntent(mActivity,""+imageurl);
        }
        return super.onOptionsItemSelected(item);
    }
}
