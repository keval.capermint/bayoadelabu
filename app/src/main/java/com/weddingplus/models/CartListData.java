package com.weddingplus.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by keval on 24/7/17.
 */

public class CartListData {

    @SerializedName("sub_tot")
    @Expose
    private String sub_tot;
    @SerializedName("tax_percentage")
    @Expose
    private String tax_percentage;
    @SerializedName("tax_amount")
    @Expose
    private String tax_amount;
    @SerializedName("total_amount")
    @Expose
    private String total_amount;
    @SerializedName("cart_data")
    @Expose
    private ArrayList<CartData> data;

    public ArrayList<CartData> getCartData() {
        return data;
    }

    public String getSub_tot() {
        return sub_tot;
    }

    public String getTax_percentage() {
        return tax_percentage;
    }

    public String getTax_amount() {
        return tax_amount;
    }

    public String getTotal_amount() {
        return total_amount;
    }

}
