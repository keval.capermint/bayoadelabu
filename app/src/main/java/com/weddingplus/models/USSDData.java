package com.weddingplus.models;

/**
 * Created by cap-13-pv-16 on 10/4/18.
 */

public class USSDData {

    private int id;
    private String code;
    private String message;
    private String type;

    public int getId() {
        return id;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String getType() {
        return type;
    }
}
