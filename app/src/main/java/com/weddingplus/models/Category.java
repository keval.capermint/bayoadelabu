package com.weddingplus.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Jay Kikani on 20/7/17.
 */

public class Category implements Parcelable {
    public enum ItemType {
        ONE_ITEM, TWO_ITEM;
    }

    private int category_id;
    private String category_name;
    private String category_image;
    private int checkin_count;
    private ItemType type;

    public ItemType getType() {
        return type;
    }

    public void setType(ItemType type) {
        this.type = type;
    }

    public int getCheckin_count() {
        return checkin_count;
    }

    public void setCheckin_count(int checkin_count) {
        this.checkin_count = checkin_count;
    }

    public String getCategory_image() {
        return category_image.replace(" ","%20");
    }

    public void setCategory_image(String category_image) {
        this.category_image = category_image;
    }


    public int getCategory_id() {
        return category_id;
    }

    public void setCategory_id(int category_id) {
        this.category_id = category_id;
    }

    public String getCategory_name() {
        return category_name;
    }

    public void setCategory_name(String category_name) {
        this.category_name = category_name;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.category_id);
        dest.writeString(this.category_name);
        dest.writeString(this.category_image);
        dest.writeInt(this.checkin_count);
        dest.writeInt(this.type == null ? -1 : this.type.ordinal());
    }

    public Category() {
    }

    protected Category(Parcel in) {
        this.category_id = in.readInt();
        this.category_name = in.readString();
        this.category_image = in.readString();
        this.checkin_count = in.readInt();
        int tmpType = in.readInt();
        this.type = tmpType == -1 ? null : ItemType.values()[tmpType];
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel source) {
            return new Category(source);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };
}
