package com.weddingplus.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by keval on 24/7/17.
 */

public class ProductData implements Parcelable {

    private String id;
    private String type;
    private String name;
    private String attachment_type;
    private String price;
    private String description;
    private String created;
    private String attachment;

    public String getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getAttachment_type() {
        return attachment_type;
    }

    public String getPrice() {
        return price;
    }

    public String getDescription() {
        return description;
    }

    public String getCreated() {
        return created;
    }

    public String getAttachment() {
        return attachment;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.type);
        dest.writeString(this.name);
        dest.writeString(this.attachment_type);
        dest.writeString(this.price);
        dest.writeString(this.description);
        dest.writeString(this.created);
        dest.writeString(this.attachment);
    }

    public ProductData() {
    }

    protected ProductData(Parcel in) {
        this.id = in.readString();
        this.type = in.readString();
        this.name = in.readString();
        this.attachment_type = in.readString();
        this.price = in.readString();
        this.description = in.readString();
        this.created = in.readString();
        this.attachment = in.readString();
    }

    public static final Parcelable.Creator<ProductData> CREATOR = new Parcelable.Creator<ProductData>() {
        @Override
        public ProductData createFromParcel(Parcel source) {
            return new ProductData(source);
        }

        @Override
        public ProductData[] newArray(int size) {
            return new ProductData[size];
        }
    };
}
