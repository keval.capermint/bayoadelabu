package com.weddingplus.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.weddingplus.utilities.StringUtils;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User implements Parcelable {

    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("first_name")
    @Expose
    private String firstName;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("user_name")
    @Expose
    private String userName;
    @SerializedName("user_type")
    @Expose
    private Integer userType;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("compony_code")
    @Expose
    private String componyCode;
    @SerializedName("profile_url")
    @Expose
    private String profileUrl;
    @SerializedName("contact_number")
    @Expose
    private String contactNumber;
    @SerializedName("birth_date")
    @Expose
    private String birthDate;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("contact_person_name1")
    @Expose
    private String contactPersonName1;
    @SerializedName("contact_person_number1")
    @Expose
    private String contactPersonNumber1;
    @SerializedName("contact_person_name2")
    @Expose
    private String contactPersonName2;
    @SerializedName("contact_person_number2")
    @Expose
    private String contactPersonNumber2;
    @SerializedName("person_to_call_name")
    @Expose
    private String personToCallName;
    @SerializedName("person_to_call_number")
    @Expose
    private String personToCallNumber;
    @SerializedName("blood_group")
    @Expose
    private String bloodGroup;
    @SerializedName("emergency_contacts")
    @Expose
    private String emergencyContacts;
    @SerializedName("is_notify")
    @Expose
    private Integer isNotify;

    private String reg_no;
    private String faculty;
    private String program;
    private String level;


    public String getReg_no() {
        return reg_no;
    }

    public void setReg_no(String reg_no) {
        this.reg_no = reg_no;
    }

    public String getFaculty() {
        return faculty;
    }

    public void setFaculty(String faculty) {
        this.faculty = faculty;
    }

    public String getProgram() {
        return program;
    }

    public void setProgram(String program) {
        this.program = program;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }




    public String getAuthkey() {
        return Authkey;
    }

    public void setAuthkey(String authkey) {
        Authkey = authkey;
    }

    private String Authkey;

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Integer getUserType() {
        return userType;
    }

    public void setUserType(Integer userType) {
        this.userType = userType;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getComponyCode() {
        return componyCode;
    }

    public void setComponyCode(String componyCode) {
        this.componyCode = componyCode;
    }

    public String getProfileUrl() {
        return profileUrl;
    }

    public void setProfileUrl(String profileUrl) {
        this.profileUrl = profileUrl;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public String getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(String birthDate) {
        this.birthDate = birthDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContactPersonName1() {
        return contactPersonName1;
    }

    public void setContactPersonName1(String contactPersonName1) {
        this.contactPersonName1 = contactPersonName1;
    }

    public String getContactPersonNumber1() {
        return contactPersonNumber1;
    }

    public void setContactPersonNumber1(String contactPersonNumber1) {
        this.contactPersonNumber1 = contactPersonNumber1;
    }

    public String getContactPersonName2() {
        return contactPersonName2;
    }

    public void setContactPersonName2(String contactPersonName2) {
        this.contactPersonName2 = contactPersonName2;
    }

    public String getContactPersonNumber2() {
        return contactPersonNumber2;
    }

    public void setContactPersonNumber2(String contactPersonNumber2) {
        this.contactPersonNumber2 = contactPersonNumber2;
    }

    public String getPersonToCallName() {
        return personToCallName;
    }

    public void setPersonToCallName(String personToCallName) {
        this.personToCallName = personToCallName;
    }

    public String getPersonToCallNumber() {
        return personToCallNumber;
    }

    public void setPersonToCallNumber(String personToCallNumber) {
        this.personToCallNumber = personToCallNumber;
    }

    public String getBloodGroup() {
        return bloodGroup;
    }

    public void setBloodGroup(String bloodGroup) {
        this.bloodGroup = bloodGroup;
    }

    public String getEmergencyContacts() {
        return emergencyContacts;
    }

    public void setEmergencyContacts(String emergencyContacts) {
        this.emergencyContacts = emergencyContacts;
    }

    public Integer getIsNotify() {
        return isNotify;
    }

    public void setIsNotify(Integer isNotify) {
        this.isNotify = isNotify;
    }

    public String getName(){
        if (StringUtils.isValid(firstName) && StringUtils.isValid(lastName)){
            return firstName + " " + lastName;
        }else{
            return getUserName();
        }

    }

    public User() {
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.userId);
        dest.writeString(this.firstName);
        dest.writeString(this.lastName);
        dest.writeString(this.userName);
        dest.writeValue(this.userType);
        dest.writeString(this.email);
        dest.writeString(this.componyCode);
        dest.writeString(this.profileUrl);
        dest.writeString(this.contactNumber);
        dest.writeString(this.birthDate);
        dest.writeString(this.address);
        dest.writeString(this.contactPersonName1);
        dest.writeString(this.contactPersonNumber1);
        dest.writeString(this.contactPersonName2);
        dest.writeString(this.contactPersonNumber2);
        dest.writeString(this.personToCallName);
        dest.writeString(this.personToCallNumber);
        dest.writeString(this.bloodGroup);
        dest.writeString(this.emergencyContacts);
        dest.writeValue(this.isNotify);
        dest.writeString(this.Authkey);
    }

    protected User(Parcel in) {
        this.userId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.userName = in.readString();
        this.userType = (Integer) in.readValue(Integer.class.getClassLoader());
        this.email = in.readString();
        this.componyCode = in.readString();
        this.profileUrl = in.readString();
        this.contactNumber = in.readString();
        this.birthDate = in.readString();
        this.address = in.readString();
        this.contactPersonName1 = in.readString();
        this.contactPersonNumber1 = in.readString();
        this.contactPersonName2 = in.readString();
        this.contactPersonNumber2 = in.readString();
        this.personToCallName = in.readString();
        this.personToCallNumber = in.readString();
        this.bloodGroup = in.readString();
        this.emergencyContacts = in.readString();
        this.isNotify = (Integer) in.readValue(Integer.class.getClassLoader());
        this.Authkey = in.readString();
    }

    public static final Creator<User> CREATOR = new Creator<User>() {
        @Override
        public User createFromParcel(Parcel source) {
            return new User(source);
        }

        @Override
        public User[] newArray(int size) {
            return new User[size];
        }
    };
}