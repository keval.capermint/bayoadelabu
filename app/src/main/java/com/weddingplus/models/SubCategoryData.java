package com.weddingplus.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by keval on 23/2/18.
 */

public class SubCategoryData implements Parcelable {

    @SerializedName("category_id")
    @Expose
    private Integer categoryId;
    @SerializedName("category_name")
    @Expose
    private String categoryName;

    public Integer getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(Integer categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(this.categoryId);
        dest.writeString(this.categoryName);
    }

    public SubCategoryData() {
    }

    protected SubCategoryData(Parcel in) {
        this.categoryId = (Integer) in.readValue(Integer.class.getClassLoader());
        this.categoryName = in.readString();
    }

    public static final Parcelable.Creator<SubCategoryData> CREATOR = new Parcelable.Creator<SubCategoryData>() {
        @Override
        public SubCategoryData createFromParcel(Parcel source) {
            return new SubCategoryData(source);
        }

        @Override
        public SubCategoryData[] newArray(int size) {
            return new SubCategoryData[size];
        }
    };

    @Override
    public String toString() {
        return getCategoryName();
    }
}
