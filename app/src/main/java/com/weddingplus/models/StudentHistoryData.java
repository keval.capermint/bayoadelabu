package com.weddingplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by keval on 24/7/17.
 */

public class StudentHistoryData {
    @SerializedName("list")
    @Expose
    private ArrayList<StudentHistoryDataDetail> list = null;

    public ArrayList<StudentHistoryDataDetail> getList() {
        return list;
    }

    public void setList(ArrayList<StudentHistoryDataDetail> list) {
        this.list = list;
    }

    public String getTotalRecords() {
        return totalRecords;
    }

    public void setTotalRecords(String totalRecords) {
        this.totalRecords = totalRecords;
    }

    public String getTotalpages() {
        return totalpages;
    }

    public void setTotalpages(String totalpages) {
        this.totalpages = totalpages;
    }

    @SerializedName("totalRecords")
    @Expose

    private String totalRecords;
    @SerializedName("totalpages")
    @Expose
    private String totalpages;



}
