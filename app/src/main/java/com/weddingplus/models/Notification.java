package com.weddingplus.models;

/**
 * Created by Jay Kikani on 20/7/17.
 */

public class Notification {


    private String unotification_id;
    private String title;
    private String message;
    private String is_read;
    private String created_at;
    private boolean allClicked;
    private boolean isSelected = false;

    public String getUnotification_id() {
        return unotification_id;
    }

    public void setUnotification_id(String unotification_id) {
        this.unotification_id = unotification_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getIs_read() {
        return is_read;
    }

    public void setIs_read(String is_read) {
        this.is_read = is_read;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public boolean isAllClicked() {
        return allClicked;
    }

    public void setAllClicked(boolean allClicked) {
        this.allClicked = allClicked;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
