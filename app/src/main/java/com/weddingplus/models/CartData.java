package com.weddingplus.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by keval on 24/7/17.
 */

public class CartData implements Parcelable {

    private String id;
    private String product_id;
    private String user_id;
    private String no_of_item;
    private String created;
    private String type;
    private String name;
    private String description;
    private String price;
    private String attachment;

    public String getId() {
        return id;
    }

    public String getProduct_id() {
        return product_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getNo_of_item() {
        return no_of_item;
    }

    public String getCreated() {
        return created;
    }

    public String getType() {
        return type;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public String getAttachment() {
        return attachment;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.product_id);
        dest.writeString(this.user_id);
        dest.writeString(this.no_of_item);
        dest.writeString(this.created);
        dest.writeString(this.type);
        dest.writeString(this.name);
        dest.writeString(this.description);
        dest.writeString(this.price);
        dest.writeString(this.attachment);
    }

    public CartData() {
    }

    protected CartData(Parcel in) {
        this.id = in.readString();
        this.product_id = in.readString();
        this.user_id = in.readString();
        this.no_of_item = in.readString();
        this.created = in.readString();
        this.type = in.readString();
        this.name = in.readString();
        this.description = in.readString();
        this.price = in.readString();
        this.attachment = in.readString();
    }

    public static final Creator<CartData> CREATOR = new Creator<CartData>() {
        @Override
        public CartData createFromParcel(Parcel source) {
            return new CartData(source);
        }

        @Override
        public CartData[] newArray(int size) {
            return new CartData[size];
        }
    };
}
