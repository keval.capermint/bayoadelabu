package com.weddingplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by keval on 24/7/17.
 */

public class GalleryData {

    @SerializedName("result_image")
    @Expose
    private ArrayList<ImageData> resultImage;

    @SerializedName("result_video")
    @Expose
    private ArrayList<VideoData> resultVideo;

    public ArrayList<ImageData> getResultImage() {
        return resultImage;
    }

    public ArrayList<VideoData> getResultVideo() {
        return resultVideo;
    }
}
