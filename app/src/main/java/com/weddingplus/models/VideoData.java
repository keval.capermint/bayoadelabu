package com.weddingplus.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by cap-13-pv-16 on 17/9/18.
 */

public class VideoData implements Parcelable{

    private String id;
    private String user_id;
    private String attachment;
    private String attachment_type;
    private String caption;
    private String description;

    public String getId() {
        return id;
    }

    public String getUser_id() {
        return user_id;
    }

    public String getAttachment() {
        return attachment;
    }

    public String getAttachment_type() {
        return attachment_type;
    }

    public String getCaption() {
        return caption;
    }

    public String getDescription() {
        return description;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.user_id);
        dest.writeString(this.attachment);
        dest.writeString(this.attachment_type);
        dest.writeString(this.caption);
        dest.writeString(this.description);
    }

    public VideoData() {
    }

    protected VideoData(Parcel in) {
        this.id = in.readString();
        this.user_id = in.readString();
        this.attachment = in.readString();
        this.attachment_type = in.readString();
        this.caption = in.readString();
        this.description = in.readString();
    }

    public static final Creator<VideoData> CREATOR = new Creator<VideoData>() {
        @Override
        public VideoData createFromParcel(Parcel source) {
            return new VideoData(source);
        }

        @Override
        public VideoData[] newArray(int size) {
            return new VideoData[size];
        }
    };
}
