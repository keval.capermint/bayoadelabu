package com.weddingplus.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by keval on 24/7/17.
 */

public class StudentHistoryDataDetail {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("student_category_id")
    @Expose
    private String studentCategoryId;
    @SerializedName("student_category_name")
    @Expose
    private String studentCategoryName;
    @SerializedName("student_category_image")
    @Expose
    private String studentCategoryImage;
    @SerializedName("student_subcategory_id")
    @Expose
    private String studentSubcategoryId;
    @SerializedName("student_subcategory_name")
    @Expose
    private String studentSubcategoryName;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("reported_date")
    @Expose
    private String reportedDate;
    @SerializedName("address")
    @Expose
    private String address;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStudentCategoryId() {
        return studentCategoryId;
    }

    public void setStudentCategoryId(String studentCategoryId) {
        this.studentCategoryId = studentCategoryId;
    }

    public String getStudentCategoryName() {
        return studentCategoryName;
    }

    public void setStudentCategoryName(String studentCategoryName) {
        this.studentCategoryName = studentCategoryName;
    }

    public String getStudentCategoryImage() {
        return studentCategoryImage.replace(" ","%20");
    }

    public void setStudentCategoryImage(String studentCategoryImage) {
        this.studentCategoryImage = studentCategoryImage;
    }

    public String getStudentSubcategoryId() {
        return studentSubcategoryId;
    }

    public void setStudentSubcategoryId(String studentSubcategoryId) {
        this.studentSubcategoryId = studentSubcategoryId;
    }

    public String getStudentSubcategoryName() {
        return studentSubcategoryName;
    }

    public void setStudentSubcategoryName(String studentSubcategoryName) {
        this.studentSubcategoryName = studentSubcategoryName;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReportedDate() {
        return reportedDate;
    }

    public void setReportedDate(String reportedDate) {
        this.reportedDate = reportedDate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
